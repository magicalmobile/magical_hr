package pt.magical.magicalcareers.views.fragments.recruiter;
/**
 * Created by Sachin on 16 Aug 2017.
 */

import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.interfaces.create_jd.CreateJdPresenterImpl;
import pt.magical.magicalcareers.interfaces.create_jd.ICreateJdPresenter;
import pt.magical.magicalcareers.interfaces.create_jd.ICreateJdView;
import pt.magical.magicalcareers.models.create_jd.CreateJdModel;
import pt.magical.magicalcareers.utils.Utils;
import pt.magical.magicalcareers.views.activities.recruiter.RecruiterDashboardActivity;

public class PositionInvolveFragment extends Fragment implements ICreateJdView {


    public PositionInvolveFragment() {
        // Required empty public constructor
    }

    //Create JD API Presenter
    private ICreateJdPresenter mICreateJdPresenter;
    //Fragment Manager
    private FragmentManager mFragmentManager;

    //Context Variable
    private Context mContext;
    //Parent Activity
    private RecruiterDashboardActivity activity;
    private static final int PICK_FILE_REQUEST = 1;

    private static String uploadDocString64;
    private boolean isPermissionGranted = false;
    private String selectedFilePath;


    @Override
    public void onAttach(Context context) {
        this.mContext = context;
        super.onAttach(context);
    }

    @BindView(R.id.frag_position_involve_upload_doc_name_tv)
    public TextView mUploadedDocNameTextView;

    @BindView(R.id.frag_position_involve_upload_doc_progressvalue_tv)
    public ProgressBar mUploadedDocProgressValueProgressBar;

    @BindView(R.id.frag_position_involve_upload_doc_ll)
    public LinearLayout mUploadedDocLL;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_position_involve, container, false);
        ButterKnife.bind(this, view);
        activity = (RecruiterDashboardActivity) getActivity();
        mFragmentManager = activity.getSupportFragmentManager();
        mICreateJdPresenter = new CreateJdPresenterImpl(this);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    /**
     * Setting Click Listeners to all the views of PositionInvolveFragment.
     */
    @OnClick({R.id.position_involve_frag_frag_close_iv, R.id.position_involve_frag_next_button, R.id.frag_position_involve_upload_doc_tv,
            R.id.frag_position_involve_remove_attached_doc_iv})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.position_involve_frag_frag_close_iv:
                Utils.closingThisFrag(mFragmentManager);
                break;
            case R.id.position_involve_frag_next_button:
                goToScreeningsQusFrag();
                break;

            case R.id.frag_position_involve_upload_doc_tv:
                try {
                    if (isPermissionGranted) {
                        showFileChooser();
                    } else {
                        isPermissionGranted = Utils.checkDocPermission(mContext);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.frag_position_involve_remove_attached_doc_iv:
                mUploadedDocLL.setVisibility(View.INVISIBLE);
                break;

        }
    }

    private void goToScreeningsQusFrag() {

        /*mFragmentManager.beginTransaction().setCustomAnimations(R.anim.right_to_left_enter, R.anim.right_to_left_exit, R.anim.left_to_right_enter, R.anim.left_to_right_exit).
                replace(R.id.recruiter_dashboard_home_act_frame_layout, new ScreeningQuestionsFragment())
                .addToBackStack(null).commit();*/

        Map<String, String> images = new HashMap<String, String>();
        Pattern p = Pattern.compile("src=\"(.*?)\"");
        images.put("job_description_file", selectedFilePath);


        mICreateJdPresenter.createJdApi("master_designation", "experience_from", "experience_to", "location", "latitude",
                "longitude", "ctc_from", "ctc_to"/*, String job_description_file*/, "benefits",
                "institute_type", "highest_education_level", "notice_period", "job_type",
                "job_description", images);
    }


    private void showFileChooser() {
        Intent intent = new Intent();
        //sets the select file to all types of files
        intent.setType("application/*");
        //allows to select data and return it
        intent.setAction(Intent.ACTION_GET_CONTENT);

        //starts new activity to select file and return data
        startActivityForResult(Intent.createChooser(intent, "Choose File to Upload.."), PICK_FILE_REQUEST);
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            if (getView() != null) {
                getView().setFocusableInTouchMode(true);
                getView().requestFocus();
                getView().setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_BACK) {
                            mFragmentManager.popBackStack();
                        }
                        return true;
                    }
                });
            }
        } catch (Exception e) {
            Log.e("error", "" + e);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == PICK_FILE_REQUEST) {
                if (data == null) {
                    //no data present
                    return;
                } else {
                    Uri selectedFileUri = data.getData();
                    File selectedFile = new File(getPath(mContext, selectedFileUri));
                    try {
                        convertFileToByteArray(selectedFile);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    selectedFilePath = getPath(mContext, selectedFileUri);
                    Log.i("Selected File Path:", "Data" + selectedFilePath);
                    if (selectedFilePath != null && !selectedFilePath.equals("")) {
                        Log.i("Selected File Path:", "Data" + selectedFilePath);
                        String finalname = selectedFilePath.substring(selectedFilePath.lastIndexOf("/") + 1);
                        mUploadedDocLL.setVisibility(View.VISIBLE);
                        mUploadedDocNameTextView.setText(finalname);
                        mUploadedDocProgressValueProgressBar.setProgress(100);
                    } else {
                        Toast.makeText(activity, "Cannot upload file to server", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
    }

    public static String convertFileToByteArray(File f) {
        byte[] byteArray = null;
        try {
            InputStream inputStream = new FileInputStream(f);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            byte[] b = new byte[1024 * 11];
            int bytesRead = 0;

            while ((bytesRead = inputStream.read(b)) != -1) {
                bos.write(b, 0, bytesRead);
            }

            byteArray = bos.toByteArray();

            Log.e("Byte array", ">" + byteArray);

        } catch (IOException e) {
            e.printStackTrace();
        }

        uploadDocString64 = Base64.encodeToString(byteArray, Base64.NO_WRAP);
        Log.e("upload doc ", uploadDocString64);
        return Base64.encodeToString(byteArray, Base64.NO_WRAP);
    }


    public String getPath(Context context, Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= 19;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(activity, contentUri, null, null);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(activity, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }


    @Override
    public void onCreateJdApiSuccess(int pid, CreateJdModel createJdModel) {

        Utils.stopProgress(mContext);
        Log.d("Datais", "this--" + new Gson().toJson(createJdModel));
    }

    @Override
    public void onCreateJdApiError(int pid, String errorData) {
        Utils.stopProgress(mContext);
        Utils.showToast(mContext, errorData);
    }
}
