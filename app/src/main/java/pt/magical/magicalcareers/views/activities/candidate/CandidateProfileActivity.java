package pt.magical.magicalcareers.views.activities.candidate;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.interfaces.BubbleLayoutInterface;
import pt.magical.magicalcareers.utils.Utils;
import pt.magical.magicalcareers.views.activities.common.SettingsActivity;

public class CandidateProfileActivity extends AppCompatActivity implements BubbleLayoutInterface{

    @BindView(R.id.candidate_profile_learning_progress_rl)
    public RelativeLayout mLearningProgressRl;

    @BindView(R.id.candidate_profile_anonymous_switch)
    public Switch mAnonymousSwitch;
    @BindView(R.id.candidate_profile_skill_rv)
    public RecyclerView skillRecyclerView;


    private ProgressDialog progressDialog;
    private ImageView fullTimeIv, partTimeIv, consultingIv, contractIv, internshipIv;
    private TextView fullTimeTv, partTimeTv, consultingTv, contractTv, internshipTv;

    private String[] skillName=new String[]{"Photoshop","Indesign","Illustrator","Coreldraw","After Effects"};
    private String[] expList=new String[]{"3","2","4","5","3"};
    private ArrayList<String> skillList;
    private ArrayList<String> skillExp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_candidate_profile);

        ButterKnife.bind(this);

        initializeView();

    }

    private void initializeView() {

        mAnonymousSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (mAnonymousSwitch.isChecked()) {
                    Toast.makeText(CandidateProfileActivity.this, "welcome", Toast.LENGTH_SHORT).show();
                    showCandidateAnontmousDialog();
                } else {

                }
                   /* statusSwitch1 = simpleSwitch1.getTextOff().toString();*/
            }
        });

        skillList=new ArrayList<String>();
        skillExp=new ArrayList<String>();
        for (int i=0;i<skillName.length;i++){
            skillList.add(skillName[i]);
            skillExp.add(expList[i]);
        }

        Utils.setBubbleLayout(this,this,skillRecyclerView,skillList,skillExp,true);


    }


    private void showCandidateAnontmousDialog() {

        try {
            if (null != progressDialog && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            progressDialog = ProgressDialog.show(this, "", "", true);
            progressDialog.setCanceledOnTouchOutside(true);
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progressDialog.setContentView(R.layout.dialog_act_candidate_anonymous_type);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick({R.id.candidate_profile_learning_progress_rl, R.id.candidate_profile_edit_iv, R.id.candidate_profile_act_contact_edit_iv,
            R.id.candidate_profile_act_professional_edit_iv, R.id.candidate_profile_act_skill_edit_iv, R.id.candidate_profile_act_education_edit_iv,
            R.id.candidate_profile_setting_iv, R.id.candidate_profile_act_employment_type_rl})

    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.candidate_profile_learning_progress_rl:
                openCandidateProgressFun();
                break;

            case R.id.candidate_profile_edit_iv:
                openEditProfileAct();
                break;

            case R.id.candidate_profile_act_contact_edit_iv:
                openEditContactAct();
                break;
            case R.id.candidate_profile_act_professional_edit_iv:
                openEditProfessionalAct();
                break;
            case R.id.candidate_profile_act_skill_edit_iv:
                openEditSkillAct();
                break;
            case R.id.candidate_profile_act_education_edit_iv:
                openEditEducationAct();
                break;
            case R.id.candidate_profile_setting_iv:
                openCandidateSettingAct();
                break;

            case R.id.candidate_profile_act_employment_type_rl:
                showCandidateEmploymentTypeDialog();
                break;
        }
    }

    private void showCandidateEmploymentTypeDialog() {

        try {
            if (null != progressDialog && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            progressDialog = ProgressDialog.show(this, "", "", true);
            progressDialog.setCanceledOnTouchOutside(true);
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progressDialog.setContentView(R.layout.dialog_act_candidate_employment_type);
            fullTimeTv = (TextView) progressDialog.findViewById(R.id.full_time_tv);
            partTimeTv = (TextView) progressDialog.findViewById(R.id.part_time_tv);
            consultingTv = (TextView) progressDialog.findViewById(R.id.consulting_tv);
            contractTv = (TextView) progressDialog.findViewById(R.id.contract_tv);
            internshipTv = (TextView) progressDialog.findViewById(R.id.internship_tv);


            fullTimeIv = (ImageView) progressDialog.findViewById(R.id.full_time_check_iv);
            partTimeIv = (ImageView) progressDialog.findViewById(R.id.part_time_iv);
            consultingIv = (ImageView) progressDialog.findViewById(R.id.consulting_iv);
            contractIv = (ImageView) progressDialog.findViewById(R.id.contract_iv);
            internshipIv = (ImageView) progressDialog.findViewById(R.id.internship_iv);


            fullTimeTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (fullTimeIv.getVisibility() == View.VISIBLE) {
                        fullTimeIv.setVisibility(View.INVISIBLE);

                    } else {
                        fullTimeIv.setVisibility(View.VISIBLE);
                    }
                }
            });
            partTimeTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (partTimeIv.getVisibility() == View.VISIBLE) {
                        partTimeIv.setVisibility(View.INVISIBLE);

                    } else {
                        partTimeIv.setVisibility(View.VISIBLE);
                    }
                }
            });
            consultingTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (consultingIv.getVisibility() == View.VISIBLE) {
                        consultingIv.setVisibility(View.INVISIBLE);

                    } else {
                        consultingIv.setVisibility(View.VISIBLE);
                    }
                }
            });
            contractTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (contractIv.getVisibility() == View.VISIBLE) {
                        contractIv.setVisibility(View.INVISIBLE);

                    } else {
                        contractIv.setVisibility(View.VISIBLE);
                    }
                }
            });
            internshipTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (internshipIv.getVisibility() == View.VISIBLE) {
                        internshipIv.setVisibility(View.INVISIBLE);

                    } else {
                        internshipIv.setVisibility(View.VISIBLE);
                    }
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void openCandidateSettingAct() {

        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

    private void openEditEducationAct() {
        Intent intent = new Intent(this, CandidateEditEducationActivity.class);
        startActivity(intent);

    }

    private void openEditSkillAct() {
    }

    private void openEditProfessionalAct() {
        Intent intent = new Intent(this, CandidateEditProfessionalActivity.class);
        startActivity(intent);

    }

    private void openEditContactAct() {

        Intent intent = new Intent(this, CandidateEditContactActivity.class);
        startActivity(intent);
    }

    private void openEditProfileAct() {
        Intent intent = new Intent(this, CandidateEditProfileActivity.class);
        startActivity(intent);
    }

    private void openCandidateProgressFun() {

        Intent intent = new Intent(CandidateProfileActivity.this, CandidateProgressActivity.class);
        startActivity(intent);
    }


    @Override
    public void onBubbleItemClick(int itemPosition, String itemTitle) {
        Log.d("itemPos",""+itemPosition);
        skillList.remove(itemPosition);
        skillExp.remove(itemPosition);
        Utils.setBubbleLayout(this,this,skillRecyclerView,skillList,skillExp,true);
    }
}
