package pt.magical.magicalcareers.views.activities.recruiter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.yarolegovich.discretescrollview.DiscreteScrollView;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import lecho.lib.hellocharts.gesture.ZoomType;
import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.AxisValue;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.model.Viewport;
import lecho.lib.hellocharts.util.ChartUtils;
import lecho.lib.hellocharts.view.LineChartView;
import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.adapters.recruiter.JdCandidatesListRecyclerAdapter;
import pt.magical.magicalcareers.adapters.recruiter.JdListDiscreteAdapter;
import pt.magical.magicalcareers.interfaces.recruiter_master_data.IRecruiterMasterDataPresenter;
import pt.magical.magicalcareers.interfaces.recruiter_master_data.IRecruiterMasterDataView;
import pt.magical.magicalcareers.interfaces.recruiter_master_data.RecruiterMasterDataPresenterImpl;
import pt.magical.magicalcareers.models.recruiter_master_data.BenfitsList;
import pt.magical.magicalcareers.models.recruiter_master_data.Data;
import pt.magical.magicalcareers.models.recruiter_master_data.DegreeList;
import pt.magical.magicalcareers.models.recruiter_master_data.DesignationList;
import pt.magical.magicalcareers.models.recruiter_master_data.EmployeeType;
import pt.magical.magicalcareers.models.recruiter_master_data.IndustryList;
import pt.magical.magicalcareers.models.recruiter_master_data.InstituteList;
import pt.magical.magicalcareers.models.recruiter_master_data.MasterKeyskillsList;
import pt.magical.magicalcareers.models.recruiter_master_data.NotificationStau;
import pt.magical.magicalcareers.models.recruiter_master_data.RecruiterMasterDataModel;
import pt.magical.magicalcareers.utils.Sharedpreferences;
import pt.magical.magicalcareers.utils.Utils;
import pt.magical.magicalcareers.utils.graph.PieGraph;
import pt.magical.magicalcareers.utils.graph.PieSlice;
import pt.magical.magicalcareers.views.fragments.recruiter.CreateQuestionFragment;
import pt.magical.magicalcareers.views.fragments.recruiter.TrendingQuestionsFragment;
import pt.magical.magicalcareers.views.fragments.recruiter.ChattingViewFragment;
import pt.magical.magicalcareers.views.fragments.recruiter.CreateJdParentFragment;
import pt.magical.magicalcareers.views.fragments.recruiter.ViewJdParentFragment;

public class RecruiterDashboardActivity extends AppCompatActivity implements DiscreteScrollView.OnItemChangedListener<RecyclerView.ViewHolder>,
        DiscreteScrollView.ScrollStateChangeListener<RecyclerView.ViewHolder>, JdListDiscreteAdapter.JdClickListener, IRecruiterMasterDataView {


    // Recruiter Master Data Presenter
    private IRecruiterMasterDataPresenter mIRecruiterMasterDataPresenter;
    private FragmentManager mFragmentManager;
    private String TAG = "RecruiterDashboardActivity";
    private Context mContext;
    private Sharedpreferences mPrefs;
    /**
     * All View Refernces of (activity_recruiter_dashboard.xml)
     **/

    @BindView(R.id.recruiter_dashboard_home_act_frame_layout)
    public FrameLayout mRecruiterHomeFrameLayout;
    @BindView(R.id.recruter_dash_home_add_iv)
    public ImageView mAddJdIV;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recruter_dash_home_line_graph)
    GraphView lineGraph;
    @BindView(R.id.recruter_dash_home_pie_graph)
    PieGraph pieGraph;
    @BindView(R.id.recruter_dash_home_main_rl)
    RelativeLayout mRecruterDashHomeMainRl;

    @BindView(R.id.recruter_dash_home_jd_ll)
    LinearLayout mRecruterDashHomeJdLl;
    @BindView(R.id.recruter_dash_home_jd_dsv)
    DiscreteScrollView mRecruterDashHomeJdDSV;
    @BindView(R.id.recruter_dash_home_jdcandidates_rv)
    RecyclerView mRecruterDashHomeJdcandidatesRv;
    LineChartView chartTop;
    private JdCandidatesListRecyclerAdapter mJdCandidateRecyclerAdapter;
    LineChartData lineData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recruiter_dashboard);
        ButterKnife.bind(this);
        initializeViews();
        drawLinegraph();
        drawPieGraph();
    }

    private void initializeViews() {
        mContext = RecruiterDashboardActivity.this;
        setSupportActionBar(toolbar);
        mFragmentManager = getSupportFragmentManager();
        mPrefs = Sharedpreferences.getUserDataObj(this);
        mPrefs.setAuthToken("Token e53f74ee6230dc019ab0ec74567a70a54818d883");

        mIRecruiterMasterDataPresenter = new RecruiterMasterDataPresenterImpl(this);

       // mIRecruiterMasterDataPresenter.getRecruiterMasterdata();

        setDiscreateAdapter(mContext);
        setCandidateRecyclerAdapter(mContext);
    }


    private void setDiscreateAdapter(Context mContext) {
        mRecruterDashHomeJdDSV.setAdapter(new JdListDiscreteAdapter(this));
        mRecruterDashHomeJdDSV.addOnItemChangedListener(this);
        mRecruterDashHomeJdDSV.addScrollStateChangeListener(this);
        mRecruterDashHomeJdDSV.scrollToPosition(1);
        mRecruterDashHomeJdDSV.setItemTransformer(new ScaleTransformer.Builder().setMinScale(0.8f).build());
    }

    private void setCandidateRecyclerAdapter(Context mContext) {
        mRecruterDashHomeJdcandidatesRv.setHasFixedSize(true);
        mJdCandidateRecyclerAdapter = new JdCandidatesListRecyclerAdapter(this);
        mRecruterDashHomeJdcandidatesRv.setAdapter(mJdCandidateRecyclerAdapter);
        mRecruterDashHomeJdcandidatesRv.setLayoutManager(new LinearLayoutManager(this));
        mJdCandidateRecyclerAdapter.notifyDataSetChanged();
    }


    /**
     * Setting Click Listeners to all the views of RecruiterHomeAct.
     */
    @OnClick({R.id.recruter_dash_home_chatting_view, R.id.recruter_dash_home_add_iv, R.id.recruter_dash_home_jdlist_ll, R.id.recruter_dash_home_profile_iv, R.id.recruter_dash_home_view_jd_bt})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.recruter_dash_home_chatting_view:
                recruiterChattingView();
                break;
            case R.id.recruter_dash_home_add_iv:
                recruiterCreateJD();
                break;
            case R.id.recruter_dash_home_view_jd_bt:
                recruiterViewJD();
                break;
            case R.id.recruter_dash_home_jdlist_ll:
                mRecruterDashHomeMainRl.setVisibility(View.GONE);
                mRecruterDashHomeJdLl.setVisibility(View.VISIBLE);
                break;
            case R.id.recruter_dash_home_profile_iv:
                Intent intent = new Intent(RecruiterDashboardActivity.this, RecruiterProfileParentActivity.class);
                overridePendingTransition(R.anim.right_to_left_enter, R.anim.right_to_left_exit);
                startActivity(intent);
                break;
        }
    }

    public void onJdClick(int position) {
        mRecruterDashHomeMainRl.setVisibility(View.VISIBLE);
        mRecruterDashHomeJdLl.setVisibility(View.GONE);
    }

    /**
     * Setting Create JD Process Fragment to recruiter_home_view
     */
    private void recruiterCreateJD() {
        mFragmentManager.beginTransaction().
                setCustomAnimations(R.anim.bottom_to_top_enter, R.anim.bottom_to_top_exit, R.anim.top_to_bottom_enter, R.anim.top_to_bottom_exit).
                replace(R.id.recruiter_dashboard_home_act_frame_layout, new CreateJdParentFragment())
                .addToBackStack(null).commit();
    }

    /**
     * Setting View JD Process Fragment to recruiter_home_view
     */
    private void recruiterViewJD() {
        mFragmentManager.beginTransaction().setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_up)
                .replace(R.id.recruiter_dashboard_home_act_frame_layout, new ViewJdParentFragment())
                .addToBackStack(null).commit();
    }


    /**
     * Setting Create JD Process Fragment to recruiter_home_view
     */
    private void recruiterChattingView() {
        //.setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_up)
        mFragmentManager.beginTransaction()
                .replace(R.id.recruiter_dashboard_home_act_frame_layout, new TrendingQuestionsFragment())
                .addToBackStack(null).commit();
    }

    private void drawPieGraph() {
        float density = getResources().getDisplayMetrics().density;
        float thickness = 15.0f * density;
        pieGraph.setThickness((int) thickness);

        PieSlice slice = new PieSlice();
        slice.setColor(getResources().getColor(R.color.colorGraphOrange));
        slice.setValue(3);
        // slice.setIcon(BitmapFactory.decodeResource(getResources(), R.drawable.icon_triangle));
        pieGraph.addSlice(slice);
        slice = new PieSlice();
        slice.setColor(getResources().getColor(R.color.colorGraphRed));
        slice.setValue(2);
        //   slice.setIcon(BitmapFactory.decodeResource(getResources(), R.drawable.icon_square));
        pieGraph.addSlice(slice);
        slice = new PieSlice();
        slice.setColor(getResources().getColor(R.color.colorGraphGreen));
        slice.setValue(5);
        //   slice.setIcon(BitmapFactory.decodeResource(getResources(), R.drawable.icon_star));
        pieGraph.addSlice(slice);

        pieGraph.setOnSliceClickedListener(new PieGraph.OnSliceClickedListener() {

            @Override
            public void onClick(int index) {

            }
        });
    }

    private void drawLinegraph() {
        LineGraphSeries<DataPoint> series = new LineGraphSeries<DataPoint>(new DataPoint[]{
                new DataPoint(21, 0),
                new DataPoint(22, 300),
                new DataPoint(23, 100),
                new DataPoint(26, 200),
                new DataPoint(25, 300),
                new DataPoint(24, 100)
        });
        lineGraph.addSeries(series);

        //
        chartTop = (LineChartView) findViewById(R.id.chart);
        generateInitialLineData();

    }

    private void generateInitialLineData() {
        int numValues = 7;

        String[] days = new String[]{"21 Jan", "22 Jan", "23 Jan", "24 Jan", "25 Jan", "26 Jan", "27 Jan",};

        List<AxisValue> axisValues = new ArrayList<AxisValue>();
        List<PointValue> values = new ArrayList<PointValue>();
        for (int i = 0; i < numValues; ++i) {
            values.add(new PointValue(i, 0));
            axisValues.add(new AxisValue(i).setLabel(days[i]));
        }

        Line line = new Line(values);
        line.setColor(ChartUtils.COLOR_GREEN).setCubic(true);

        List<Line> lines = new ArrayList<Line>();
        lines.add(line);

        lineData = new LineChartData(lines);
        lineData.setAxisXBottom(new Axis(axisValues).setHasLines(true));
        lineData.setAxisYLeft(new Axis().setHasLines(true).setMaxLabelChars(3));

        chartTop.setLineChartData(lineData);

        // For build-up animation you have to disable viewport recalculation.
        chartTop.setViewportCalculationEnabled(false);

        // And set initial max viewport and current viewport- remember to set viewports after data.
        Viewport v = new Viewport(0, 400, 6, 0);
        chartTop.setMaximumViewport(v);
        chartTop.setCurrentViewport(v);
        chartTop.setZoomType(ZoomType.HORIZONTAL);

        generateLineData(ChartUtils.COLOR_BLUE, 400);
    }

    private void generateLineData(int color, float range) {
        // Cancel last animation if not finished.
        chartTop.cancelDataAnimation();
        // Modify data targets
        Line line = lineData.getLines().get(0);// For this example there is always only one line.
        line.setStrokeWidth(1);
        line.setPointRadius(2);
        line.setColor(color);
        for (PointValue value : line.getValues()) {
            // Change target only for Y value.
            value.setTarget(value.getX(), (float) Math.random() * range);
        }
        // Start new data animation with 300ms duration;
        chartTop.startDataAnimation(300);
    }

    @Override
    public void onCurrentItemChanged(@Nullable RecyclerView.ViewHolder viewHolder, int adapterPosition) {

    }

    @Override
    public void onScrollStart(@NonNull RecyclerView.ViewHolder currentItemHolder, int adapterPosition) {

    }

    @Override
    public void onScrollEnd(@NonNull RecyclerView.ViewHolder currentItemHolder, int adapterPosition) {

    }

    @Override
    public void onScroll(float scrollPosition, @NonNull RecyclerView.ViewHolder currentHolder, @NonNull RecyclerView.ViewHolder newCurrent) {

    }

    public void DialogCreateJDCompletion() {
        // custom dialog
        final Dialog dialog = new Dialog(mContext);
        dialog.setContentView(R.layout.custom_dialog_jd_completion);

        TextView txtNotNow = (TextView) dialog.findViewById(R.id.text_not_now);
        txtNotNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    /**
     * Supporting callback onActivityResult in this activity's childrens fragments
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            super.onActivityResult(requestCode, resultCode, data);
            for (Fragment fragment : getSupportFragmentManager().getFragments()) {
                fragment.onActivityResult(requestCode, resultCode, data);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRecruiterMasterdataSuccess(int pid, RecruiterMasterDataModel recruiterMasterDataModel) {
        try {
            Utils.stopProgress(this);
            Utils.showToast(this, recruiterMasterDataModel.getMeta().getMessage().toString());

            Log.d("onRecruiter", "data is from server side" + new Gson().toJson(recruiterMasterDataModel));

            Data.deleteAll(Data.class);
            Data recruiterMasterData = new Data();
//        recruiterMasterData.setDesignationList(recruiterMasterData.getDesignationList());
//        recruiterMasterData.setEmployeeTypes(recruiterMasterData.getEmployeeTypes());
//        recruiterMasterData.setNotificationStaus(recruiterMasterData.getNotificationStaus());
//        recruiterMasterData.setDegreeList(recruiterMasterData.getDegreeList());
//        recruiterMasterData.setInstituteList(recruiterMasterData.getInstituteList());
//        recruiterMasterData.setIndustryList(recruiterMasterData.getIndustryList());
//        recruiterMasterData.setBenfitsList(recruiterMasterData.getBenfitsList());
            recruiterMasterData.setCtcUnit(recruiterMasterData.getCtcUnit());
            recruiterMasterData.setCtcRangeStart(recruiterMasterData.getCtcRangeStart());
            recruiterMasterData.setCtcRangeEnd(recruiterMasterData.getCtcRangeEnd());
            recruiterMasterData.save();

            DesignationList.deleteAll(DesignationList.class);
            for (DesignationList designationList : recruiterMasterDataModel.getData().getDesignationList()) {
                designationList.save();
            }

            DegreeList.deleteAll(DegreeList.class);
            for (DegreeList degreeList : recruiterMasterDataModel.getData().getDegreeList()) {
                degreeList.save();
            }

            EmployeeType.deleteAll(EmployeeType.class);
            for (EmployeeType employeeType : recruiterMasterDataModel.getData().getEmployeeTypes()) {
                employeeType.save();
            }

            InstituteList.deleteAll(InstituteList.class);
            for (InstituteList instituteList : recruiterMasterDataModel.getData().getInstituteList()) {
                instituteList.save();
            }

            NotificationStau.deleteAll(NotificationStau.class);
            for (NotificationStau notificationStau : recruiterMasterDataModel.getData().getNotificationStaus()) {
                notificationStau.save();
            }

            BenfitsList.deleteAll(BenfitsList.class);
            for (BenfitsList benfitsList : recruiterMasterDataModel.getData().getBenfitsList()) {
                benfitsList.save();
            }


            IndustryList.deleteAll(IndustryList.class);
            MasterKeyskillsList.deleteAll(MasterKeyskillsList.class);
            for (IndustryList industryList : recruiterMasterDataModel.getData().getIndustryList()) {

                for (MasterKeyskillsList masterKeyskillsList : industryList.getMasterKeyskillsList()) {
                    masterKeyskillsList.setMasterIndustryId(industryList.getMasterIndustryId());
                    masterKeyskillsList.save();
                }
                industryList.save();
            }

            Log.d("onRecruiter", "DesignationList" + new Gson().toJson(DesignationList.listAll(DesignationList.class)));
            Log.d("onRecruiter", "DegreeList" + new Gson().toJson(DegreeList.listAll(DegreeList.class)));
            Log.d("onRecruiter", "EmployeeType" + new Gson().toJson(EmployeeType.listAll(EmployeeType.class)));
            Log.d("onRecruiter", "InstituteList" + new Gson().toJson(InstituteList.listAll(InstituteList.class)));
            Log.d("onRecruiter", "NotificationStau" + new Gson().toJson(NotificationStau.listAll(NotificationStau.class)));
            Log.d("onRecruiter", "BenfitsList" + new Gson().toJson(BenfitsList.listAll(BenfitsList.class)));
            Log.d("onRecruiter", "IndustryList" + new Gson().toJson(IndustryList.listAll(IndustryList.class)));
            Log.d("onRecruiter", "MasterData" + new Gson().toJson(Data.listAll(Data.class)));
            Log.d("onRecruiter", "MasterKeyskillsList" + new Gson().toJson(MasterKeyskillsList.listAll(MasterKeyskillsList.class)));
        } catch (Exception e) {
            e.printStackTrace();
        }


      /*  for(int i=0;i<recruiterMasterData.getDesignationList().size();i++){

        }*/
    }

    @Override
    public void onRecruiterMasterdataError(int pid, String errorData) {
        Utils.stopProgress(this);
        Utils.showToast(this, errorData);

    }
}
