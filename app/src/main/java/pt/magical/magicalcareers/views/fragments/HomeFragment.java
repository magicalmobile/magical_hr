package pt.magical.magicalcareers.views.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.interfaces.pass_selected_data.IPassSelectedData;
import pt.magical.magicalcareers.utils.Utils;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements IPassSelectedData {


    public HomeFragment() {
        // Required empty public constructor
    }

    private Context mContext;
    private String selectedData;

    @Override
    public void onAttach(Context context) {
        this.mContext = context;
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                selectedData = bundle.getString("dataSelected");
            }
            if (selectedData != null)
                Toast.makeText(mContext, "" + selectedData, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @BindView(R.id.home_act_go_to_get_data_button)
    public Button mGoToGetDataButton;

    private FragmentManager mFragmentManager;

    private String pageTitle, dataTitle;
    private ArrayList<String> dataList = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);
        mFragmentManager = getActivity().getSupportFragmentManager();
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        pageTitle = "Add Skill";
        dataTitle = "Keyskill";

        dataList.clear();
        for (int i = 0; i < 10; i++) {
            dataList.add("Test Skill" + " " + i);
        }

        mGoToGetDataButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.callingAutoSearchPage(mContext, pageTitle, dataTitle, dataList);

            }
        });
    }


    @Override
    public void passDataToFragment(String selectedData) {
        Log.d("Event", "came data is" + selectedData);
    }
}
