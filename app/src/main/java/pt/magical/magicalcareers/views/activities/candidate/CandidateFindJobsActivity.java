package pt.magical.magicalcareers.views.activities.candidate;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pt.magical.magicalcareers.R;

/**
 * Created by danish on 05-08-2017.
 */

public class CandidateFindJobsActivity extends AppCompatActivity {



    @BindView(R.id.act_job_skill_ll)
    public LinearLayout mSkillLl;

    @BindView(R.id.act_job_location_ll)
    public LinearLayout mLocationLl;

    @BindView(R.id.act_job_profession_ll)
    public LinearLayout mProfessionLL;

    @BindView(R.id.act_job_skill_edit_tv)
    public TextView mSkillEditTV;
    @BindView(R.id.act_job_profession_edit_tv)
    public TextView mProfEditTV;
    @BindView(R.id.act_job_profession_proceed_btn)
    public Button mProfButton;
    @BindView(R.id.act_job_skill_proceed_btn)
    public Button mSkillButton;

    @BindView(R.id.act_job_profession_title_tv)
    public TextView profTitleTV;
    @BindView(R.id.act_job_profession_subtitle_tv)
    public TextView profSubTitleTV;

    public static String mIndustry="";
    public static String mExp="";
    public static String mSalary="";
    public static int mIndustryPos=0;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.candidate_find_job_activity);
        ButterKnife.bind(this);

        initializeViews();
    }

    private void initializeViews() {
        mProfessionLL.setAlpha((float) 1.0);
        mSkillLl.setAlpha((float) 0.3);
        mLocationLl.setAlpha((float) 0.3);
        mSkillButton.setVisibility(View.GONE);
    }


    @OnClick({R.id.act_job_profession_proceed_btn, R.id.act_job_skill_proceed_btn, R.id.cand_find_job_act_browse_button,R.id.act_job_profession_edit_tv})
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.act_job_profession_proceed_btn:
               callProfessionActivityFunc();
                break;
            case R.id.act_job_skill_proceed_btn:
                Intent intent1 = new Intent(this, CandidateFindJobDetailActivity.class);
                intent1.putExtra("title", "skill");
                startActivity(intent1);
                break;

            case R.id.cand_find_job_act_browse_button:
                Intent intent2 = new Intent(this, CandidateHomeActivity.class);
                startActivity(intent2);
                break;
            case R.id.act_job_profession_edit_tv:
                callProfessionActivityFunc();
                break;


        }
    }

    private void callProfessionActivityFunc() {
        Intent intent = new Intent(this, CandidateFindJobDetailActivity.class);
        intent.putExtra("title", "profession");
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mSalary!=null && !mSalary.equalsIgnoreCase("")){
            mProfessionLL.setAlpha((float) 1.0);
            mSkillLl.setAlpha((float) 1.0);
            mLocationLl.setAlpha((float) 0.3);
            mProfEditTV.setVisibility(View.VISIBLE);
            mProfButton.setVisibility(View.GONE);
            profTitleTV.setAlpha((float) 0.3);
            profSubTitleTV.setAlpha((float) 0.3);
            mSkillButton.setVisibility(View.VISIBLE);
        }
    }
}

