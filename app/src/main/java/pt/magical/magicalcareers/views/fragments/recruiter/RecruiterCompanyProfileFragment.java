package pt.magical.magicalcareers.views.fragments.recruiter;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.adapters.recruiter.RecruiterCompanyImagesViewAdapter;
import pt.magical.magicalcareers.adapters.recruiter.RecruiterCompanyVideosViewAdapter;
import pt.magical.magicalcareers.views.activities.recruiter.RecruiterProfileParentActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class RecruiterCompanyProfileFragment extends Fragment {


    public RecruiterCompanyProfileFragment() {
        // Required empty public constructor
    }

    //Fragment Manager
    private FragmentManager mFragmentManager;
    //Context Variable
    private Context mContext;
    //Parent Activity
    private RecruiterProfileParentActivity activity;

    @Override
    public void onAttach(Context context) {
        this.mContext = context;
        super.onAttach(context);
    }

    @BindView(R.id.recruiter_company_profile_videos_recycler_view)
    public RecyclerView mVideosRecyclerView;

    private RecruiterCompanyVideosViewAdapter mRecruiterCompanyVideosViewAdapter;

    @BindView(R.id.recruiter_company_profile_images_recycler_view)
    public RecyclerView mImagesRecyclerView;

    private RecruiterCompanyImagesViewAdapter mRecruiterCompanyImagesViewAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_recruiter_company_profile, container, false);
        ButterKnife.bind(this, view);
        activity = (RecruiterProfileParentActivity) getActivity();
        mFragmentManager = activity.getSupportFragmentManager();
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        settingUpVideosLayout();
        settingUpImagesLayout();
    }

    private void settingUpVideosLayout() {
        mVideosRecyclerView.setHasFixedSize(true);
        mVideosRecyclerView.setLayoutManager(new StaggeredGridLayoutManager(3, 1));
        mRecruiterCompanyVideosViewAdapter = new RecruiterCompanyVideosViewAdapter(mContext);
        mVideosRecyclerView.setAdapter(mRecruiterCompanyVideosViewAdapter);
        mVideosRecyclerView.setNestedScrollingEnabled(false);
    }

    private void settingUpImagesLayout() {
        mImagesRecyclerView.setHasFixedSize(true);
        mImagesRecyclerView.setLayoutManager(new StaggeredGridLayoutManager(3, 1));
        mRecruiterCompanyImagesViewAdapter = new RecruiterCompanyImagesViewAdapter(mContext);
        mImagesRecyclerView.setAdapter(mRecruiterCompanyImagesViewAdapter);
        mImagesRecyclerView.setNestedScrollingEnabled(false);
    }


    /**
     * Setting Click Listeners to all the views of RecruiterCompanyProfileFragment.
     */
    @OnClick({R.id.recruiter_company_profile_general_details_edit_iv, R.id.recruiter_company_profile_senior_person_edit_iv,
            R.id.recruiter_company_profile_about_info_edit_iv, R.id.recruiter_company_profile_videos_edit_iv,
            R.id.recruiter_company_profile_images_edit_iv})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.recruiter_company_profile_general_details_edit_iv:
                goToEditCompanyProfileInfoDetails();
                break;
            case R.id.recruiter_company_profile_senior_person_edit_iv:
                goToEditCompanySeniorPersonDetails();
                break;
            case R.id.recruiter_company_profile_about_info_edit_iv:
                goToEditCompanyAboutInfoDetails();
                break;
            case R.id.recruiter_company_profile_videos_edit_iv:
                goToEditCompanyVideoDetails();
                break;
            case R.id.recruiter_company_profile_images_edit_iv:
                goToEditCompanyImagesDetails();
                break;
        }
    }

    private void goToEditCompanyVideoDetails() {
        mFragmentManager.beginTransaction().
                setCustomAnimations(R.anim.bottom_to_top_enter, R.anim.bottom_to_top_exit, R.anim.top_to_bottom_enter, R.anim.top_to_bottom_exit).
                replace(R.id.recruiter_own_profile_frame_layout, new RecruiterCompanyVideoCollectionFragment())
                .addToBackStack(null).commit();
    }

    private void goToEditCompanyAboutInfoDetails() {
        mFragmentManager.beginTransaction().
                setCustomAnimations(R.anim.bottom_to_top_enter, R.anim.bottom_to_top_exit, R.anim.top_to_bottom_enter, R.anim.top_to_bottom_exit).
                replace(R.id.recruiter_own_profile_frame_layout, new RecruiterOwnProfileAboutInfoFragment())
                .addToBackStack(null).commit();
    }

    private void goToEditCompanySeniorPersonDetails() {
        mFragmentManager.beginTransaction().
                setCustomAnimations(R.anim.bottom_to_top_enter, R.anim.bottom_to_top_exit, R.anim.top_to_bottom_enter, R.anim.top_to_bottom_exit).
                replace(R.id.recruiter_own_profile_frame_layout, new RecruiterCompanySeniorPersonInfoFragment())
                .addToBackStack(null).commit();

    }

    private void goToEditCompanyProfileInfoDetails() {
        mFragmentManager.beginTransaction().
                setCustomAnimations(R.anim.bottom_to_top_enter, R.anim.bottom_to_top_exit, R.anim.top_to_bottom_enter, R.anim.top_to_bottom_exit).
                replace(R.id.recruiter_own_profile_frame_layout, new RecruiterCompanyProfileBasicInfoFragment())
                .addToBackStack(null).commit();
    }

    private void goToEditCompanyImagesDetails() {
        mFragmentManager.beginTransaction().
                setCustomAnimations(R.anim.bottom_to_top_enter, R.anim.bottom_to_top_exit, R.anim.top_to_bottom_enter, R.anim.top_to_bottom_exit).
                replace(R.id.recruiter_own_profile_frame_layout, new RecruiterCompanyImagesCollectionFragment())
                .addToBackStack(null).commit();
    }
}
