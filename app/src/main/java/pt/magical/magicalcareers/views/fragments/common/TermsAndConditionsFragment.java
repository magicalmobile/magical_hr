package pt.magical.magicalcareers.views.fragments.common;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.interfaces.add_company.AddCompanyPresenterImpl;
import pt.magical.magicalcareers.interfaces.t_and_c.ITermsAndConditionsPresenter;
import pt.magical.magicalcareers.interfaces.t_and_c.ITermsAndConditionsView;
import pt.magical.magicalcareers.interfaces.t_and_c.TermsAndConditionsPresenterImpl;
import pt.magical.magicalcareers.models.t_and_c.TermsAndConditionsModel;
import pt.magical.magicalcareers.utils.Utils;
import pt.magical.magicalcareers.views.activities.common.SettingsActivity;
import pt.magical.magicalcareers.views.activities.recruiter.RecruiterDashboardActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class TermsAndConditionsFragment extends Fragment implements ITermsAndConditionsView {


    public TermsAndConditionsFragment() {
        // Required empty public constructor
    }

    private ITermsAndConditionsPresenter mITermsAndConditionsPresenter;
    //Fragment Manager
    private FragmentManager mFragmentManager;
    //Context Variable
    private Context mContext;
    //Parent Activity
    private SettingsActivity activity;

    @BindView(R.id.frag_terms_and_cond_back_iv)
    public ImageView mBackIV;

    @BindView(R.id.frag_terms_and_cond_title_tv)
    public TextView mTermsAndConditionTitleTV;
    @BindView(R.id.frag_terms_and_cond_content_tv)
    public TextView mTermsAndConditionContentTV;

    @Override
    public void onAttach(Context context) {
        this.mContext = context;
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_terms_and_conditions, container, false);
        ButterKnife.bind(this, view);
        activity = (SettingsActivity) getActivity();
        mFragmentManager = activity.getSupportFragmentManager();
        mITermsAndConditionsPresenter = new TermsAndConditionsPresenterImpl(this);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mITermsAndConditionsPresenter.getTermsAndconditions();
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            if (getView() != null) {
                getView().setFocusableInTouchMode(true);
                getView().requestFocus();
                getView().setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_BACK) {
                            mFragmentManager.popBackStack();
                        }
                        return true;
                    }
                });
            }
        } catch (Exception e) {
            Log.e("error", "" + e);
        }
    }

    @Override
    public void getTermsAndconditionsSuccess(int pid, TermsAndConditionsModel termsAndConditionsModel) {
        Utils.stopProgress(activity);
        mTermsAndConditionTitleTV.setText(termsAndConditionsModel.getData().getTitle().toString());
        mTermsAndConditionContentTV.setText(termsAndConditionsModel.getData().getContent().toString());


    }

    @Override
    public void getTermsAndconditionsError(int pid, String errorData) {
        Utils.stopProgress(activity);
        Utils.showToast(activity, errorData);

    }
}
