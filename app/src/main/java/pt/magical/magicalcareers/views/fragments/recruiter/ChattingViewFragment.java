package pt.magical.magicalcareers.views.fragments.recruiter;
/**
 * Created by RAdhu on 19 Aug 2017.
 */

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.view.menu.MenuPopupHelper;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.flexbox.FlexboxLayout;
import com.yarolegovich.discretescrollview.DiscreteScrollView;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;

import java.lang.reflect.Field;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fisk.chipcloud.ChipCloud;
import fisk.chipcloud.ChipCloudConfig;
import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.adapters.candidate.CandidateLearnDetailsDiscreteScrollAdapter;
import pt.magical.magicalcareers.adapters.recruiter.ChattingViewTabAdapter;
import pt.magical.magicalcareers.adapters.recruiter.JdListChattngDiscreteAdapter;
import pt.magical.magicalcareers.adapters.recruiter.JdListDiscreteAdapter;
import pt.magical.magicalcareers.adapters.recruiter.JdScreeningQusListRecyclerAdapter;
import pt.magical.magicalcareers.adapters.recruiter.RecruiterParentProfileAdapter;
import pt.magical.magicalcareers.utils.CustomViewPager;
import pt.magical.magicalcareers.utils.LogToastUtility;
import pt.magical.magicalcareers.views.activities.recruiter.RecruiterDashboardActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChattingViewFragment extends Fragment implements JdListDiscreteAdapter.JdClickListener, DiscreteScrollView.OnItemChangedListener<RecyclerView.ViewHolder>,JdListChattngDiscreteAdapter.JdClickListener {

    private ChipCloudConfig mChipCloudConfig;
    private JdScreeningQusListRecyclerAdapter mScreeningQusRecyclerAdapter;
    private ChattingViewTabAdapter mChattingViewTabAdapter;
    private String TAG="ChattingViewFragment";

    // Required empty public constructor
    public ChattingViewFragment() {
    }


    //Fragment Manager
    private FragmentManager mFragmentManager;
    //Context Variable
    private Context mContext;
    //Parent Activity
    private RecruiterDashboardActivity activity;


    @BindView(R.id.fragment_chatting_view_viewpager)
    public CustomViewPager mViewPager;

    @BindView(R.id.fragment_chatting_view_tablayout)
    public TabLayout mTabLayout;
    @BindView(R.id.fragment_chatting_view_jd_selection_dv)
    public DiscreteScrollView mJdSelectionDV;

    @BindView(R.id.fragment_chatting_view_ll)
    public LinearLayout mJdSelectionView;


    /**
     * Variable Instances and Views OnBind (all fragment_create_jd_parent.xml views)
     */

    @Override
    public void onAttach(Context context) {
        this.mContext = context;
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chatting_view, container, false);
        ButterKnife.bind(this, view);
        activity = (RecruiterDashboardActivity) getActivity();
        mFragmentManager = activity.getSupportFragmentManager();
        initializeViews();
        return view;
    }

    private void initializeViews() {
        settingUpTabLayout();
        settingUpViewPager();
        setDiscreateAdapter();
    }

    private void setDiscreateAdapter() {
        mJdSelectionDV.setAdapter(new JdListChattngDiscreteAdapter(mContext,this));
        mJdSelectionDV.addOnItemChangedListener(this);
        mJdSelectionDV.scrollToPosition(1);
        mJdSelectionDV.setItemTransformer(new ScaleTransformer.Builder().setMinScale(0.8f).build());
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        try {

            if (getView() != null) {
                getView().setFocusableInTouchMode(true);
                getView().requestFocus();
                getView().setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_BACK) {
                            Intent intent = new Intent(mContext, RecruiterDashboardActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                        return true;
                    }
                });
            }
        } catch (Exception e) {
            Log.e("error", "" + e);
        }
    }

    /**
     * Setting Click Listeners to all the views of CreateJdParentFragment.
     */
    @OnClick({R.id.fragment_chatting_view_close_iv, R.id.fragment_chatting_view_more_iv,R.id.fragment_chatting_view_ll})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_chatting_view_close_iv:
                Intent intent = new Intent(mContext, RecruiterDashboardActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                break;
            case R.id.fragment_chatting_view_more_iv:
                makePopupMenu(v);
                break;
            case R.id.fragment_chatting_view_ll:
                mJdSelectionDV.setVisibility(View.VISIBLE);
                break;
        }
    }


    private void makePopupMenu(View v) {
        PopupMenu popup = new PopupMenu(this.getContext(), v);
        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.menu.menu_chatting_view, popup.getMenu());
        Field field = null;
        try {
            field = popup.getClass().getDeclaredField("mPopup");
            field.setAccessible(true);
            MenuPopupHelper menuPopupHelper = (MenuPopupHelper) field.get(popup);
            menuPopupHelper.setForceShowIcon(true);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_mute:
                        LogToastUtility.TOAST_L(mContext, "mute...");
                        break;
                }

                return true;
            }
        });
        popup.show();

    }

    private void settingUpViewPager() {
        //  mViewPager.removeAllViews();
        mChattingViewTabAdapter = new ChattingViewTabAdapter(mFragmentManager, mTabLayout.getTabCount());
        mViewPager.setAdapter(mChattingViewTabAdapter);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));
        mViewPager.disableScroll(true);
        mViewPager.setPagingEnabled(false);
        mViewPager.beginFakeDrag();
        mViewPager.setOffscreenPageLimit(0);
        mChattingViewTabAdapter.notifyDataSetChanged();
       /* mViewPager.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                mViewPager.requestDisallowInterceptTouchEvent(true);
                return true;
            }
        });
*/

    }

    private void settingUpTabLayout() {
        mTabLayout.removeAllTabs();
        mTabLayout.addTab(mTabLayout.newTab().setText("Matched"));
        mTabLayout.addTab(mTabLayout.newTab().setText("Shortlisted"));
        mTabLayout.addTab(mTabLayout.newTab().setText("Archived"));
        mTabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        mTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
                //  tab.getIcon().setColorFilter(Color.parseColor("#e15822"), PorterDuff.Mode.SRC_IN);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
                //    tab.getIcon().setColorFilter(Color.parseColor("#000000"), PorterDuff.Mode.SRC_IN);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public void onJdClick(int position) {
        LogToastUtility.LI(TAG,"jd click");
        mJdSelectionDV.setVisibility(View.GONE);
    }

    @Override
    public void onCurrentItemChanged(@Nullable RecyclerView.ViewHolder viewHolder, int adapterPosition) {

    }
}
