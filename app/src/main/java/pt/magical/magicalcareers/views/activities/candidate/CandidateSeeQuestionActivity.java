package pt.magical.magicalcareers.views.activities.candidate;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import butterknife.BindView;
import butterknife.ButterKnife;
import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.adapters.candidate.CandidateSeeQuestionRecyclerAdapter;

public class CandidateSeeQuestionActivity extends AppCompatActivity {


    @BindView(R.id.see_question_recyclerview)
    public RecyclerView mRecyclerview;



    private CandidateSeeQuestionRecyclerAdapter mCandidateSeeQuestionRecyclerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_candidate_see_question);

        ButterKnife.bind(this);

        initializeView();

    }

    private void initializeView() {

        mRecyclerview.setVisibility(View.VISIBLE);
        mRecyclerview.setHasFixedSize(true);
        mCandidateSeeQuestionRecyclerAdapter = new CandidateSeeQuestionRecyclerAdapter(this);
        mRecyclerview.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mRecyclerview.setAdapter(mCandidateSeeQuestionRecyclerAdapter);
    }

}
