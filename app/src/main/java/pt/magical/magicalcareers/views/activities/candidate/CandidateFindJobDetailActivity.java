package pt.magical.magicalcareers.views.activities.candidate;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.views.fragments.candidate.CandidateFindJobDetailFragment;

public class CandidateFindJobDetailActivity extends AppCompatActivity {


    @BindView(R.id.candidate_job_details_frame_layout)
    public FrameLayout mHomeFrameLayout;
    private FragmentManager mFragmentManager;
    private String title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_candidate_find_job_detail);

        ButterKnife.bind(this);

        initializeViews();
    }

    private void initializeViews() {


        try {
            title = getIntent().getStringExtra("title");
            Log.d("titleActivity", title);
            mFragmentManager = getSupportFragmentManager();
            mFragmentManager.beginTransaction().add(R.id.candidate_job_details_frame_layout, CandidateFindJobDetailFragment.newInstance(title)).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

}
