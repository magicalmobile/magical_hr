package pt.magical.magicalcareers.views.activities.common;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.interfaces.forgot_password.ForgotPasswordPresenterImpl;
import pt.magical.magicalcareers.interfaces.forgot_password.IForgotPasswordPresenter;
import pt.magical.magicalcareers.interfaces.forgot_password.IForgotPasswordView;
import pt.magical.magicalcareers.models.candidate_profile.CandidateProfileModel;
import pt.magical.magicalcareers.models.forgot_password.ForgotPasswordModel;
import pt.magical.magicalcareers.utils.Utils;

public class ForgotPasswordActivity extends AppCompatActivity implements IForgotPasswordView {

    private IForgotPasswordPresenter mIForgotPasswordPresenter;

    @BindView(R.id.act_forgot_pass_email_et)
    public EditText mEmailEt;


    private String usernameValue;
    private String otpType="2";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        ButterKnife.bind(this);


        initializeViews();
    }

    private void initializeViews() {

        mIForgotPasswordPresenter = new ForgotPasswordPresenterImpl(this);

    }

    @OnClick({R.id.act_forgot_pass_submit_button})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.act_forgot_pass_submit_button:
                forgotPassFun();
                break;

        }
    }

    private void forgotPassFun() {

        usernameValue = mEmailEt.getText().toString();
        if (TextUtils.isEmpty(usernameValue)) {
            Utils.showToast(this, "Please enter your registered mobile number or email");
        } else {
            mIForgotPasswordPresenter.forgotPasswordApi(usernameValue);
        }
    }

    @Override
    public void forgotPasswordApiSuccess(int pid, ForgotPasswordModel forgotPasswordModel) {

        Utils.stopProgress(ForgotPasswordActivity.this);
        if (forgotPasswordModel.getMeta().getCode() == 200) {
            Intent intent=new Intent(this,OtpActivity.class);
            intent.putExtra("username", usernameValue);
           intent.putExtra("otp_type",otpType);
            startActivity(intent);
        }

    }

    @Override
    public void forgotPasswordApiError(int pid, String errorData) {

        Utils.stopProgress(ForgotPasswordActivity.this);
        Utils.showToast(ForgotPasswordActivity.this, errorData);

    }

}
