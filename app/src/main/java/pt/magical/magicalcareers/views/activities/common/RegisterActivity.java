package pt.magical.magicalcareers.views.activities.common;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;
import com.scottyab.showhidepasswordedittext.ShowHidePasswordEditText;

import org.apache.commons.codec.binary.Base64;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.interfaces.custom_register.CustomRegisterPresenterImpl;
import pt.magical.magicalcareers.interfaces.custom_register.ICustomRegisterView;
import pt.magical.magicalcareers.models.common.custom_register.CustomRegisterModel;
import pt.magical.magicalcareers.models.common.custom_register_error.CustomRegisterErrorModel;
import pt.magical.magicalcareers.utils.AppConstants;
import pt.magical.magicalcareers.utils.Sharedpreferences;
import pt.magical.magicalcareers.utils.Utils;
import pt.magical.magicalcareers.views.activities.HomeActivity;

public class RegisterActivity extends AppCompatActivity implements ICustomRegisterView,Validator.ValidationListener{

    @BindView(R.id.act_reg_email_et)
    public EditText mEmailEt;

    @Length(min = 8, max = 15, message = "password should be 8-15 digit long")
    @Password(message = "password should be 8-15 digit long")
    @BindView(R.id.act_reg_pass_et)
    public ShowHidePasswordEditText mPasswordEt;

    @NotEmpty(message = "Please enter name")
    @BindView(R.id.act_reg_name_et)
    public EditText mNameEt;

    private String username;
    private CustomRegisterPresenterImpl mCustomRegister;
    private String imeiNo;
    private TelephonyManager telephonyManager;
    private int androidVersion;
    private String mUsername;
    private String mPassword;
    private String mName;
    private Validator validator;
    private String validUser=null;
    private String role;
    private String latitude;
    private String longitude;
    private String location;
    private Sharedpreferences mPrefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        ButterKnife.bind(this);

        initializeView();
    }

    private void initializeView() {
        try {
            validator = new Validator(this);
            validator.setValidationListener(this);
            username=getIntent().getStringExtra("username");
            mEmailEt.setText(username);
            getIMEIno();
            getAndroidVersion();
            mPrefs = Sharedpreferences.getUserDataObj(this);
            role= String.valueOf(mPrefs.getRoleType());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getAndroidVersion() {
        androidVersion=android.os.Build.VERSION.SDK_INT;
    }

    @OnClick({R.id.act_reg_submit_button})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.act_reg_submit_button:
                usernameValidation();
                break;

        }
    }

    private void usernameValidation() {
        mUsername=mEmailEt.getText().toString().trim();
        if (mUsername!=null && !mUsername.equalsIgnoreCase("")){
            if (android.util.Patterns.EMAIL_ADDRESS.matcher(mUsername).matches()) {
                validUser = mUsername;
                validator.validate();
            }else if (android.util.Patterns.PHONE.matcher(mUsername).matches() && mUsername.length()==10) {
                validUser="91"+mUsername;
                validator.validate();
            }else{
                mEmailEt.setError("Please enter valid username");
            }
        }else{
            mEmailEt.setError("Please enter username");
        }
    }

    private void callRegisterApi(String username,String password,String name,String role,String device_id,String device_type,String device_version,String longitude,String latitude,String location,boolean show){
        mCustomRegister=new CustomRegisterPresenterImpl(this);
        mCustomRegister.customRegisterApi(username,password,name,role,device_id,device_type,device_version,longitude,latitude,location,show);
    }

    private void getIMEIno() {
        try {

            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
                if (ContextCompat.checkSelfPermission(RegisterActivity.this, Manifest.permission.READ_PHONE_STATE)
                        == PackageManager.PERMISSION_GRANTED) {
                    telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                    imeiNo=telephonyManager.getDeviceId();
                } else {
                    requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE}, 1);
                }
            }else{
                telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                imeiNo=telephonyManager.getDeviceId();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        Log.e("permission", "onPermissionResult" + grantResults[0] + "requestCode" + requestCode);
        if (grantResults[0] == 0) {
            telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            imeiNo=telephonyManager.getDeviceId();
        }
    }



    @Override
    public void customRegisterApiSuccess(int pid, CustomRegisterModel customRegisterModel) {
        try {
            if (customRegisterModel.getMeta().getCode()== AppConstants.SUCCESS_CODE){
                Utils.stopProgress(RegisterActivity.this);
                mPrefs.setAuthToken("Token " + customRegisterModel.getData().getToken().toString());
                mPrefs.setRoleType(Integer.parseInt(customRegisterModel.getData().getRole()));
                Intent i=new Intent(RegisterActivity.this,OtpActivity.class);
                i.putExtra("username", username);
                i.putExtra("otp_type","1");
                startActivity(i);
                finish();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void customRegisterApiError(int pid, CustomRegisterErrorModel customRegisterErrorModel) {
        try {
            Utils.stopProgress(RegisterActivity.this);
            String msg = customRegisterErrorModel.getData().getUsername().get(0).toString();
                if (msg.equalsIgnoreCase("Already this username is exist!")){
                    Utils.showToast(this,"Already username register");
                    Intent i=new Intent(RegisterActivity.this,LoginActivity.class);
                    i.putExtra("username",mUsername);
                    startActivity(i);
                    finish();
                }else{
                    Utils.showToast(this,"Oops something went wrong! please try again");
                }

        } catch (Exception e) {
            e.printStackTrace();
            Utils.showToast(this,"Oops something went wrong! please try again");
        }
    }

    @Override
    public void onValidationSucceeded() {
        try {
            mPassword=mPasswordEt.getText().toString().trim();
            byte[] encodedBytes = Base64.encodeBase64(mPassword.getBytes());
            mName=mNameEt.getText().toString().trim();
            latitude="17.986932";
            longitude="75.343876";
            location="Bangalore";
            if (validUser!=null && !validUser.equalsIgnoreCase("") && imeiNo!=null && !imeiNo.equalsIgnoreCase("")){
                callRegisterApi(validUser,new String(encodedBytes),mName,role,imeiNo,"1",String.valueOf(androidVersion),latitude,longitude,location,true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        try {
            for (ValidationError error : errors) {
                View view = error.getView();
                String message = error.getCollatedErrorMessage(this);
                if (view instanceof EditText) {
                    view.requestFocus();
                    if (view.getId() == R.id.act_reg_pass_et) {
                        ((EditText) view).setError("password should be 8-15 digit long");
                    } else {
                        ((EditText) view).setError(message);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
