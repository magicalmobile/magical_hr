package pt.magical.magicalcareers.views.fragments.recruiter;

/**
 * Created by Sachin on 18 Aug 2017.
 */

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import org.apache.commons.codec.binary.Base64;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.interfaces.change_password.ChangePasswordPresenterImpl;
import pt.magical.magicalcareers.interfaces.change_password.IChangePasswordPresenter;
import pt.magical.magicalcareers.interfaces.change_password.IChangePasswordView;
import pt.magical.magicalcareers.models.change_password.ChangePasswordModel;
import pt.magical.magicalcareers.models.change_password.error.ChangePasswordErrorModel;
import pt.magical.magicalcareers.utils.Utils;
import pt.magical.magicalcareers.views.activities.common.SettingsActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChangePasswordFragment extends Fragment implements IChangePasswordView {


    public ChangePasswordFragment() {
        // Required empty public constructor
    }

    private IChangePasswordPresenter mIChangePasswordPresenter;

    //Fragment Manager
    private FragmentManager mFragmentManager;
    //Context Variable
    private Context mContext;
    //Parent Activity
    private SettingsActivity activity;


    @BindView(R.id.frag_change_password_old_pass_value_et)
    public EditText mOldPasswordET;
    @BindView(R.id.frag_change_password_new_pass_value_et)
    public EditText mNewPasswordET;

    String oldPasswordValue, newPasswordValue;

    @Override
    public void onAttach(Context context) {
        this.mContext = context;
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_change_password, container, false);
        ButterKnife.bind(this, view);
        activity = (SettingsActivity) getActivity();
        mFragmentManager = activity.getSupportFragmentManager();
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mIChangePasswordPresenter = new ChangePasswordPresenterImpl(this);

        mNewPasswordET.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    changePasswordApiFunc();
                }
                return false;
            }
        });
    }


    /**
     * Setting Click Listeners to all the views of WhatPositionFragment.
     */
    @OnClick({R.id.frag_change_password_back_iv, R.id.frag_change_password_submit_button})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.frag_change_password_back_iv:
                mFragmentManager.popBackStack();
                break;

            case R.id.frag_change_password_submit_button:
                changePasswordApiFunc();
                break;
        }
    }

    private void changePasswordApiFunc() {

        oldPasswordValue = mOldPasswordET.getText().toString();
        byte[] encodedBytesOld = Base64.encodeBase64(oldPasswordValue.getBytes());

        newPasswordValue = mNewPasswordET.getText().toString();
        byte[] encodedBytesNew = Base64.encodeBase64(newPasswordValue.getBytes());

        mIChangePasswordPresenter.changePassword(new String(encodedBytesOld), new String(encodedBytesNew));


    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            if (getView() != null) {
                getView().setFocusableInTouchMode(true);
                getView().requestFocus();
                getView().setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_BACK) {
                            mFragmentManager.popBackStack();
                        }
                        return true;
                    }
                });
            }
        } catch (Exception e) {
            Log.e("error", "" + e);
        }
    }

    @Override
    public void changePasswordSuccess(int pid, ChangePasswordModel changePasswordModel) {
        Utils.stopProgress(mContext);

        Utils.showToast(mContext, changePasswordModel.getMeta().getMessage().toString());
        mFragmentManager.popBackStack();
    }

    @Override
    public void changePasswordError(int pid, ChangePasswordErrorModel changePasswordErrorModel) {
        Utils.stopProgress(mContext);
        if (changePasswordErrorModel.getData().getCurrentPassword() != null) {
            Utils.showToast(mContext, changePasswordErrorModel.getData().getCurrentPassword().get(0).toString());
        }
        if (changePasswordErrorModel.getData().getNewPassword() != null) {
            Utils.showToast(mContext, changePasswordErrorModel.getData().getNewPassword().get(0).toString());
        }

    }
}
