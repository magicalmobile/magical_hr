package pt.magical.magicalcareers.views.fragments.recruiter;
/**
 * Created by Sachin on 14 Aug 2017.
 */

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import com.jaygoo.widget.RangeSeekBar;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.utils.Utils;
import pt.magical.magicalcareers.views.activities.common.AutoSearchActivity;
import pt.magical.magicalcareers.views.activities.recruiter.RecruiterDashboardActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class WhatPositionFragment extends Fragment {


    public WhatPositionFragment() {
        // Required empty public constructor
    }

    //Fragment Manager
    private FragmentManager mFragmentManager;

    //Context Variable
    private Context mContext;
    //Parent Activity
    private RecruiterDashboardActivity activity;

    @Override
    public void onAttach(Context context) {
        this.mContext = context;
        super.onAttach(context);
    }


    @BindView(R.id.what_pos_location_tv)
    public TextView mLocationTV;

    @BindView(R.id.what_pos_designation_tv)
    public TextView mDesignationTV;

    @BindView(R.id.what_pos_location_et)
    public EditText mLocationET;

    @BindView(R.id.what_pos_designation_et)
    public EditText mDesignationET;

    @BindView(R.id.experience_seekbar)
    public RangeSeekBar mExperienceSeekBar;

    @BindView(R.id.selected_experience_value_tv)
    public TextView mSelectedExperienceTV;

    @BindView(R.id.what_pos_exp_tv)
    public TextView mExperienceTV;

    private String dataChoosed, dataType, locationValue, designationValue;

    private float minValue, maxValue;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_what_position, container, false);
        ButterKnife.bind(this, view);
        activity = (RecruiterDashboardActivity) getActivity();
        mFragmentManager = activity.getSupportFragmentManager();
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try {
            mExperienceSeekBar.setOnRangeChangedListener(new RangeSeekBar.OnRangeChangedListener() {
                @Override
                public void onRangeChanged(RangeSeekBar view, float min, float max, boolean isFromUser) {

                    minValue = Utils.round(min, 1);
                    maxValue = Utils.round(max, 1);
                    Log.d("DataValues", "are" + minValue + maxValue);
                    mExperienceSeekBar.setProgressDescription((int) min + "%");
                    mSelectedExperienceTV.setText("(" + minValue + " yr" + "  to  " + maxValue + " yr" + ")");
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Setting Click Listeners to all the views of WhatPositionFragment.
     */
    @OnClick({R.id.what_pos_close_iv, R.id.what_position_next_button, R.id.what_pos_location_et, R.id.what_pos_designation_et})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.what_pos_close_iv:
                Utils.closingThisFrag(mFragmentManager);
                break;
            case R.id.what_position_next_button:
                goToCompanyBenifitsFrag();
                break;
            case R.id.what_pos_location_et:
                goToChooseLocationFromList();
                break;
            case R.id.what_pos_designation_et:
                goToChooseDesignationFromList();
                break;
        }
    }


    private void goToChooseDesignationFromList() {

        ArrayList<String> designationList = new ArrayList<>();
        designationList.clear();
        for (int i = 0; i < 10; i++) {
            designationList.add("Designation " + (i + 1));
        }
        Intent intent = new Intent(mContext, AutoSearchActivity.class);
        intent.putExtra("dataType", Utils.dataDesignation);
        intent.putExtra("pageTitle", "Add Designation");
        intent.putExtra("dataTitle", "Designation Name");
        intent.putStringArrayListExtra("dataList", designationList);
        startActivityForResult(intent, Utils.CHOOSE_DATA_FROM_GIVEN_LIST);// Activity is started with requestCode 2


    }


    private void goToCompanyBenifitsFrag() {
        if (TextUtils.isEmpty(locationValue) && TextUtils.isEmpty(designationValue)) {
            Utils.showToast(mContext, "Please enter these values");
            mLocationTV.setTextColor(Color.RED);
            mDesignationTV.setTextColor(Color.RED);
            mExperienceTV.setTextColor(getResources().getColor(R.color.colorRed));
            mLocationET.setBackground(getResources().getDrawable(R.drawable.error_transparent_backcground));
            mDesignationET.setBackground(getResources().getDrawable(R.drawable.error_transparent_backcground));
        } else if (TextUtils.isEmpty(locationValue)) {
            mLocationTV.setTextColor(Color.RED);
            mLocationET.setBackground(getResources().getDrawable(R.drawable.error_transparent_backcground));
            mDesignationTV.setTextColor(getResources().getColor(R.color.frag_value_head_color));
            mDesignationET.setBackground(getResources().getDrawable(R.drawable.oval_button_transparent_backcground));
        } else if (TextUtils.isEmpty(designationValue)) {
            mDesignationTV.setTextColor(Color.RED);
            mDesignationET.setBackground(getResources().getDrawable(R.drawable.error_transparent_backcground));
            mLocationTV.setTextColor(getResources().getColor(R.color.frag_value_head_color));
            mLocationET.setBackground(getResources().getDrawable(R.drawable.oval_button_transparent_backcground));
        } else {
            mLocationTV.setTextColor(getResources().getColor(R.color.frag_value_head_color));
            mLocationET.setBackground(getResources().getDrawable(R.drawable.oval_button_transparent_backcground));
            mDesignationTV.setTextColor(getResources().getColor(R.color.frag_value_head_color));
            mDesignationET.setBackground(getResources().getDrawable(R.drawable.oval_button_transparent_backcground));


            if (minValue == maxValue || maxValue == 0.0) {
                mExperienceTV.setTextColor(getResources().getColor(R.color.colorRed));
                Utils.showToast(mContext, "Please choose your experience range");
            } else {
                mExperienceTV.setTextColor(getResources().getColor(R.color.colorBlack));

                mFragmentManager.beginTransaction().setCustomAnimations(R.anim.right_to_left_enter, R.anim.right_to_left_exit, R.anim.left_to_right_enter, R.anim.left_to_right_exit).
                        replace(R.id.recruiter_dashboard_home_act_frame_layout, new CompanyBenefitsFragment())
                        .addToBackStack(null).commit();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        try {

            locationValue = mLocationET.getText().toString();
            designationValue = mDesignationET.getText().toString();

            mLocationET.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (s.length() > 0) {
                        mLocationTV.setTextColor(getResources().getColor(R.color.colorBlack));
                        mLocationET.setBackground(getResources().getDrawable(R.drawable.oval_button_transparent_backcground));
                    }
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (s.length() > 0) {
                        mLocationTV.setTextColor(getResources().getColor(R.color.colorBlack));
                        mLocationET.setBackground(getResources().getDrawable(R.drawable.oval_button_transparent_backcground));
                    }
                }
            });

            mDesignationET.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (s.length() > 0) {
                        mDesignationTV.setTextColor(getResources().getColor(R.color.colorBlack));
                        mDesignationET.setBackground(getResources().getDrawable(R.drawable.oval_button_transparent_backcground));
                    }
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (s.length() > 0) {
                        mDesignationTV.setTextColor(getResources().getColor(R.color.colorBlack));
                        mDesignationET.setBackground(getResources().getDrawable(R.drawable.oval_button_transparent_backcground));
                    }
                }
            });


            if (getView() != null) {
                getView().setFocusableInTouchMode(true);
                getView().requestFocus();
                getView().setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_BACK) {
                            mFragmentManager.popBackStack();
                        }
                        return true;
                    }
                });
            }
        } catch (Exception e) {
            Log.e("error", "" + e);
        }
    }

    private void goToChooseLocationFromList() {

        ArrayList<String> locationList = new ArrayList<>();
        locationList.clear();
        for (int i = 0; i < 10; i++) {
            locationList.add("Location " + (i + 1));
        }

        Intent intent = new Intent(mContext, AutoSearchActivity.class);
        intent.putExtra("dataType", Utils.dataLocation);
        intent.putExtra("pageTitle", "Add Location");
        intent.putExtra("dataTitle", "Location Name");
        intent.putStringArrayListExtra("dataList", locationList);
        startActivityForResult(intent, Utils.CHOOSE_DATA_FROM_GIVEN_LIST);// Activity is started with requestCode 2

    }

    // Call Back method  to get the Message form other Activity

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            // check if the request code is same as what is passed  here it is Utils.CHOOSE_DATA_FROM_GIVEN_LIST
            if (requestCode == Utils.CHOOSE_DATA_FROM_GIVEN_LIST) {
                dataChoosed = data.getStringExtra(Utils.dataChoosed);
                dataType = data.getStringExtra(Utils.dataType);

                Log.d("DataChoosed", "in WhatPositionFragment" + dataChoosed + " of  " + dataType);

                /**
                 * Checking the choosed data type and setting at respective place
                 */
                if (dataType.equals(Utils.dataLocation)) {
                    mLocationET.setText(dataChoosed);
                } else if (dataType.equals(Utils.dataDesignation)) {
                    mDesignationET.setText(dataChoosed);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
