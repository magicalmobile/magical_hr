package pt.magical.magicalcareers.views.activities.common;

/**
 * Created By danish 26-08-2017
 */

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;


import com.google.gson.Gson;
import com.scottyab.showhidepasswordedittext.ShowHidePasswordEditText;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.interfaces.custom_login.CustomLoginPresenterImpl;
import pt.magical.magicalcareers.interfaces.custom_login.ICustomLoginView;
import pt.magical.magicalcareers.interfaces.custom_proceed.CustomProceedPresenterImpl;
import pt.magical.magicalcareers.models.candidate_profile.CandidateProfileModel;
import pt.magical.magicalcareers.models.candidate_profile.Data;
import pt.magical.magicalcareers.models.candidate_profile.EducationList;
import pt.magical.magicalcareers.models.candidate_profile.EmployeeType;
import pt.magical.magicalcareers.models.candidate_profile.KeySkill;
import pt.magical.magicalcareers.utils.AppConstants;
import pt.magical.magicalcareers.utils.Sharedpreferences;
import pt.magical.magicalcareers.utils.Utils;
import pt.magical.magicalcareers.views.activities.candidate.CandidateHomeActivity;
import pt.magical.magicalcareers.views.activities.recruiter.RecruiterDashboardActivity;

public class LoginActivity extends AppCompatActivity implements ICustomLoginView {

    @BindView(R.id.act_login_email_et)
    public EditText mEmail;

    @BindView(R.id.act_login_pass_et)
    public ShowHidePasswordEditText mPass;
    private String username;
    private String mPassword;
    private CustomLoginPresenterImpl mCustomLoginPresenter;
    private CandidateProfileModel candidateProfileModel;
    private Sharedpreferences mPref;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);


        initializeViews();
    }


    /**
     * Initialization of all Instances created up , Filter Setting
     */
    private void initializeViews() {
        try {
            username=getIntent().getStringExtra("username");
            mEmail.setText(username);
            mEmail.setClickable(false);
            mPref=new Sharedpreferences(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick({R.id.act_login_submit_button, R.id.act_login_forgot_pass_tv})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.act_login_submit_button:
                usernameValidation();
                break;

            case R.id.act_login_forgot_pass_tv:
                openForgotPassFun();
                break;

        }
    }

    private void usernameValidation() {
        mPassword=mPass.getText().toString().trim();
        if (username!=null && !username.equalsIgnoreCase("")){
            if (android.util.Patterns.EMAIL_ADDRESS.matcher(username).matches()) {
                if (mPassword!=null && !mPassword.equalsIgnoreCase("") && mPassword.length()>7)
                checkUserExistingFunc(username,mPassword);
                else
                    mPass.setError("Password should be 8-15 character");
            }else if (android.util.Patterns.PHONE.matcher(username).matches() && username.length()==10) {
                if (mPassword!=null && !mPassword.equalsIgnoreCase("") && mPassword.length()>7)
                    checkUserExistingFunc("91"+username,mPassword);
                else
                    mPass.setError("Password should be 8-15 character");
            }else{
                mEmail.setError("Please enter valid username");
            }
        }else{
            mEmail.setError("Please enter username");
        }
    }

    private void checkUserExistingFunc(String username, String mPassword) {
        byte[] encodedBytes = Base64.encodeBase64(mPassword.getBytes());
        mCustomLoginPresenter=new CustomLoginPresenterImpl(this);
        mCustomLoginPresenter.customLoginApiCall(username,new String(encodedBytes),true);
    }


    private void openForgotPassFun() {
        Intent intent=new Intent(this,ForgotPasswordActivity.class);
        startActivity(intent);
    }


    @Override
    public void customLoginApiCallSuccess(int pid, String successResponse) {
        try {
            Utils.stopProgress(LoginActivity.this);
            Gson gson = new Gson();
            JSONObject jsonObject = null;
            jsonObject = new JSONObject(successResponse);
            JSONObject jsonObjectMeta = jsonObject.getJSONObject("meta");
            int successCode=jsonObjectMeta.getInt("code");
            if (successCode==AppConstants.SUCCESS_CODE) {
                JSONObject jsonObjectData = jsonObject.getJSONObject("data");
                int roleType = jsonObjectData.getInt("role");
                Log.d("role type", "" + roleType);
                if (roleType == 1) {
                    candidateProfileModel = gson.fromJson(successResponse, CandidateProfileModel.class);

                    if (candidateProfileModel.getMeta().getCode() == AppConstants.SUCCESS_CODE) {

                        Data.deleteAll(Data.class);

                        Data data = new Data();
                        data = candidateProfileModel.getData();
                        for (EducationList educationList : candidateProfileModel.getData().getEducationList()) {
                            educationList.save();
                        }
                        for (EmployeeType employeeType : candidateProfileModel.getData().getEmployeeType()) {
                            employeeType.save();
                        }
                        for (KeySkill keySkill : candidateProfileModel.getData().getKeySkills()) {
                            keySkill.save();
                        }
                        data.save();

                        mPref.setAuthToken("Token " + candidateProfileModel.getData().getToken().toString());
                        mPref.setRoleType(candidateProfileModel.getData().getRole());
                        Log.d("candidate data", "" + gson.toJson(Data.listAll(Data.class)));

                        Intent i = new Intent(LoginActivity.this, CandidateHomeActivity.class);
                        startActivity(i);
                        finish();

                    }
                } else if (roleType == 2) {
                    Intent i = new Intent(LoginActivity.this, RecruiterDashboardActivity.class);
                    startActivity(i);
                    finish();
                }
            }else{
                Utils.showToast(LoginActivity.this, ""+jsonObjectMeta.getString("message"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void customLoginApiError(int pid, String error) {
        try {
            Utils.stopProgress(LoginActivity.this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
