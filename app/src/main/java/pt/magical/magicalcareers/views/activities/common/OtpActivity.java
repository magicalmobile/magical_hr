package pt.magical.magicalcareers.views.activities.common;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.interfaces.otp_varification.IOtpVarificationPresenter;
import pt.magical.magicalcareers.interfaces.otp_varification.IOtpVarificationView;
import pt.magical.magicalcareers.interfaces.otp_varification.OtpVarificationModelImpl;
import pt.magical.magicalcareers.models.candidate_profile.CandidateProfileModel;
import pt.magical.magicalcareers.models.candidate_profile.Data;
import pt.magical.magicalcareers.models.candidate_profile.EducationList;
import pt.magical.magicalcareers.models.candidate_profile.EmployeeType;
import pt.magical.magicalcareers.models.candidate_profile.KeySkill;
import pt.magical.magicalcareers.models.otp_error.ForgotPasswordConfirmationErrorModel;
import pt.magical.magicalcareers.models.resend_otp.ResendOtpModel;
import pt.magical.magicalcareers.utils.AppConstants;
import pt.magical.magicalcareers.utils.Sharedpreferences;
import pt.magical.magicalcareers.utils.Utils;
import pt.magical.magicalcareers.views.activities.candidate.CandidateHomeActivity;

public class OtpActivity extends AppCompatActivity implements IOtpVarificationView {

    @BindView(R.id.act_otp_mobile_no_tv)
    public TextView mMobileEmailTv;

    @BindView(R.id.act_opt_et)
    public EditText mOtpEt;

    @BindView(R.id.act_otp_resent_tv)
    public TextView mResendTv;

    @BindView(R.id.act_otp_done_tv)
    public TextView mSendTv;

    private IOtpVarificationPresenter mIOtpVarificationPresenter;
    private String otpValue;

    private String emailValue;



    private String username;
    private String otpType;
    private String validUsername;
    private Sharedpreferences mPref;
    private CandidateProfileModel candidateProfileModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);

        ButterKnife.bind(this);

        initializeViews();

    }

    private void initializeViews() {
        try {
            username = getIntent().getStringExtra("username");
            otpType = getIntent().getStringExtra("otp_type");
            usernameValidation(username);
            mIOtpVarificationPresenter = new OtpVarificationModelImpl(this);
            mMobileEmailTv.setText(username);
            mOtpEt.addTextChangedListener(textWatcher);
            mPref = new Sharedpreferences(OtpActivity.this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void usernameValidation(String username) {
        if (username != null && !username.equalsIgnoreCase("")) {
            if (android.util.Patterns.EMAIL_ADDRESS.matcher(username).matches())
                validUsername = username;
            else if (android.util.Patterns.PHONE.matcher(username).matches() && username.length() == 10) {
                validUsername = "91" + username;
            }
        }

    }

    private final TextWatcher textWatcher = new TextWatcher() {
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            mResendTv.setVisibility(View.VISIBLE);

        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (s.length() == 6) {
                mSendTv.setVisibility(View.VISIBLE);
                mResendTv.setVisibility(View.GONE);
            }

        }

        public void afterTextChanged(Editable s) {
            mResendTv.setVisibility(View.VISIBLE);
            /*else{
                textView.setText("You have entered : " + passwordEditText.getText());
            }*/
        }
    };

    @OnClick({R.id.act_otp_resent_tv, R.id.act_otp_done_tv})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.act_otp_done_tv:
                otpDoneFun();
                break;

            case R.id.act_otp_resent_tv:
                reSendOtpFun();
                break;

        }
    }

    private void reSendOtpFun() {

        mIOtpVarificationPresenter.resendOtp("2",emailValue);
        Log.d("emailvalue",""+emailValue);
    }

    private void otpDoneFun() {
        otpValue = mOtpEt.getText().toString();
        if (TextUtils.isEmpty(otpValue)) {
            Utils.showToast(this, "Please enter your registered mobile number or email");
        } else {
            if (otpType.equalsIgnoreCase("2") && validUsername != null && !validUsername.equalsIgnoreCase("")) {
                mIOtpVarificationPresenter.otpVerificationApiCall(otpType, otpValue, validUsername);
            } else if (otpType.equalsIgnoreCase("1")) {
                mIOtpVarificationPresenter.otpVerificationApiCall(otpType, otpValue, "");
            }
        }
    }


    @Override
    public void otpVerificationApiCallSuccess(int pid, String successResponse) {
        try {
        Utils.stopProgress(OtpActivity.this);

        Gson gson = new Gson();

        JSONObject jsonObject = null;
            jsonObject = new JSONObject(successResponse);
            JSONObject jsonObjectMeta = jsonObject.getJSONObject("meta");
            int successCode=jsonObjectMeta.getInt("code");
            if (successCode==AppConstants.SUCCESS_CODE) {
                JSONObject jsonObjectData = jsonObject.getJSONObject("data");
                int roleType = jsonObjectData.getInt("role");
                System.out.println(roleType);
                if (roleType == 1) {
                    candidateProfileModel = gson.fromJson(successResponse, CandidateProfileModel.class);

                    if (candidateProfileModel.getMeta().getCode() == AppConstants.SUCCESS_CODE) {
                        if (candidateProfileModel.getData().getRole() == 1 && otpType.equalsIgnoreCase("1")) {
                            Data.deleteAll(Data.class);

                            Data data = new Data();
                            data = candidateProfileModel.getData();
                            for (EducationList educationList : candidateProfileModel.getData().getEducationList()) {
                                educationList.save();
                            }
                            for (EmployeeType employeeType : candidateProfileModel.getData().getEmployeeType()) {
                                employeeType.save();
                            }
                            for (KeySkill keySkill : candidateProfileModel.getData().getKeySkills()) {
                                keySkill.save();
                            }
                            data.save();

                            mPref.setAuthToken("Token " + candidateProfileModel.getData().getToken().toString());
                            mPref.setRoleType(candidateProfileModel.getData().getRole());
                            Log.d("candidate data", "" + gson.toJson(Data.listAll(Data.class)));

                            Intent i = new Intent(OtpActivity.this, CandidateHomeActivity.class);
                            startActivity(i);
                            finish();
                        } else if (candidateProfileModel.getData().getRole() == 1 && otpType.equalsIgnoreCase("2")) {
                            mPref.setAuthToken("Token " + candidateProfileModel.getData().getToken().toString());
                            mPref.setRoleType(candidateProfileModel.getData().getRole());

                            Intent intent=new Intent(this,SetPasswordActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    }
                } else if (roleType == 2) {

                }
            }

        } catch (Exception e) {
            e.printStackTrace();

        }

    }

    @Override
    public void otpVerificationApiCallError(int pid, ForgotPasswordConfirmationErrorModel errorModel) {
        try {
            Utils.stopProgress(OtpActivity.this);
            Utils.showToast(OtpActivity.this, errorModel.getData().getOtp().get(0));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void resendOtpSuccess(int pid, ResendOtpModel resendOtpModel) {

        try {
            Log.d("resend","success000");
            Utils.stopProgress(OtpActivity.this);
            if (resendOtpModel.getMeta().getCode() == 200) {
               Intent intent=new Intent(this,SettingsActivity.class);
                startActivity(intent);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public void resendOtpError(int pid, String errorData) {
        try {
            Utils.stopProgress(OtpActivity.this);
            Utils.showToast(OtpActivity.this, errorData);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
