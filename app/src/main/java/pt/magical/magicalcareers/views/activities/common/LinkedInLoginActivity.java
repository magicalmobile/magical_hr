package pt.magical.magicalcareers.views.activities.common;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.linkedin.platform.APIHelper;
import com.linkedin.platform.LISession;
import com.linkedin.platform.LISessionManager;
import com.linkedin.platform.errors.LIApiError;
import com.linkedin.platform.errors.LIAuthError;
import com.linkedin.platform.listeners.ApiListener;
import com.linkedin.platform.listeners.ApiResponse;
import com.linkedin.platform.listeners.AuthListener;
import com.linkedin.platform.utils.Scope;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.interfaces.linkedin_login.ILinkedInView;
import pt.magical.magicalcareers.interfaces.linkedin_login.LinkedInPresenterImpl;
import pt.magical.magicalcareers.interfaces.networkstate.OnDialogButtonClickListener;
import pt.magical.magicalcareers.models.add_company.AddCompanyModel;
import pt.magical.magicalcareers.models.candidate_profile.CandidateProfileModel;
import pt.magical.magicalcareers.models.candidate_profile.Data;
import pt.magical.magicalcareers.models.candidate_profile.EducationList;
import pt.magical.magicalcareers.models.candidate_profile.EmployeeType;
import pt.magical.magicalcareers.models.candidate_profile.KeySkill;
import pt.magical.magicalcareers.utils.AppConstants;
import pt.magical.magicalcareers.utils.LogToastUtility;
import pt.magical.magicalcareers.utils.Sharedpreferences;
import pt.magical.magicalcareers.utils.Utils;
import pt.magical.magicalcareers.views.activities.candidate.CandidateFindJobsActivity;
import pt.magical.magicalcareers.views.activities.candidate.CandidateHomeActivity;
import pt.magical.magicalcareers.views.activities.recruiter.RecruiterDashboardActivity;
import pt.magical.magicalcareers.views.custom_component.CustomProgressBar;

import static pt.magical.magicalcareers.utils.AppConstants.LINKEDIN_REQUEST_CODE;

public class LinkedInLoginActivity extends AppCompatActivity implements ILinkedInView {

    CustomProgressBar progress;
    private String TAG = "LinkedInLoginActivity";
    private Context mContext;

    public ImageView mCloseBT;
    @BindView(R.id.fragment_login_dashboard_linkedin_bt)
    public Button mLinkedInBT;
    @BindView(R.id.fragment_login_dashboard_email_phone_bt)
    public Button mEmailPhoneBT;
    @BindView(R.id.linkeedin_login_act_frame_layout)
    public FrameLayout mlinkedinLoginFrameLayout;
    private FragmentManager mFragmentManager;
    private Sharedpreferences mPref;
    private LinkedInPresenterImpl mLinkedInView;
    private String linkedInId;
    String designation = "", organisation = "", profile_photo = "", department = "", email = "", fname = "", lname = "";
    private String headline;
    private TelephonyManager telephonyManager;
    private String imeiNo;
    private int androidVersion;
    private String roleType;
    private CandidateProfileModel candidateProfileModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_linkedin_login);
        ButterKnife.bind(this);
        mContext = LinkedInLoginActivity.this;
        initializeViews();
    }

    @OnClick({R.id.fragment_login_dashboard_email_phone_bt, R.id.fragment_login_dashboard_linkedin_bt, R.id.fragment_login_dashboard_close_iv})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_login_dashboard_email_phone_bt:
                Intent i = new Intent(LinkedInLoginActivity.this, UserProceedActivity.class);
                startActivity(i);
                finish();
                break;
            case R.id.fragment_login_dashboard_linkedin_bt:
                LogToastUtility.TOAST_L(mContext, "In progress");
                try {
                    liUserAuth();
                }catch (Exception e)
                {e.printStackTrace();}

                /*Intent i1 = new Intent(LinkedInLoginActivity.this, RecruiterDashboardActivity.class);
                startActivity(i1);*/

                break;
            case R.id.fragment_login_dashboard_close_iv:
                Intent in = new Intent(this, RecruiterDashboardActivity.class);
                startActivity(in);
                overridePendingTransition(R.anim.slide_in_up, 0);
                finish();
                break;

        }
    }

    /**
     * Initialization of all Instances created up , Filter Setting
     */
    private void initializeViews() {
      mPref=new Sharedpreferences(this);
        roleType= String.valueOf(mPref.getRoleType());
        mLinkedInView= new LinkedInPresenterImpl(this);
        androidVersion=android.os.Build.VERSION.SDK_INT;
        getIMEIno();

    }

    private void getIMEIno() {
        try {

            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
                if (ContextCompat.checkSelfPermission(LinkedInLoginActivity.this, Manifest.permission.READ_PHONE_STATE)
                        == PackageManager.PERMISSION_GRANTED) {
                    telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                    imeiNo=telephonyManager.getDeviceId();
                } else {
                    requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE}, 1);
                }
            }else{
                telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                imeiNo=telephonyManager.getDeviceId();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        Log.e("permission", "onPermissionResult" + grantResults[0] + "requestCode" + requestCode);
        if (grantResults[0] == 0) {
            telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            imeiNo=telephonyManager.getDeviceId();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }



    //linkedin
    private void liUserAuth() {
        /*String liAccessToken = CACSharedPrefManager.getInstance().readString(CACSharedPrefManager.KEY_LINKED_IN_ACCESS_TOKEN_VALUE);
        if (liAccessToken != null) {
            //use existing access token
            LTU.LI(TAG, ">>use existing access token");
            LISessionManager.getInstance(getApplicationContext()).init(com.linkedin.platform.AccessToken.buildAccessToken(liAccessToken));

        } else {*/

        //generate a new access token
        LogToastUtility.LE(TAG, ">>generate new access token");
        LISessionManager.getInstance(this).init(this, buildScope(), new AuthListener() {
            @Override
            public void onAuthSuccess() {
                LogToastUtility.LE(TAG, "onAuthSuccess");
                setUpdateState();
                //Toast.makeText(getApplicationContext(), "success" + LISessionManager.getInstance(getApplicationContext()).getSession().getAccessToken().toString(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onAuthError(LIAuthError error) {
                LogToastUtility.LE(TAG, "onAuthError" + error.toString());
                setUpdateState();
                //Toast.makeText(getApplicationContext(), "failed " + error.toString(), Toast.LENGTH_LONG).show();
            }
        }, true);
        /*}*/
    }

    /**
     * A list of LinkedIn member permissions that your application requires (e.g. r_basicprofile).
     *
     * @return
     */


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        LogToastUtility.LE(TAG, "onActivityResult_req_res" + requestCode + "_" + resultCode);
        if (requestCode == LINKEDIN_REQUEST_CODE) {
            LISessionManager.getInstance(this).onActivityResult(this, requestCode, resultCode, data);
        }
    }

    private static Scope buildScope() {
        return Scope.build(Scope.R_BASICPROFILE, Scope.R_EMAILADDRESS, Scope.W_SHARE);
    }

    private void setUpdateState() {
        LISessionManager sessionManager = LISessionManager.getInstance(this);
        LISession session = sessionManager.getSession();
        boolean accessTokenValid = session.isValid();
        LogToastUtility.LE(TAG, "#1" + (accessTokenValid ? session.getAccessToken().toString() : "Sync with LinkedIn to enable these buttons"));
        try {
            JSONObject responseJsonObject = new JSONObject(session.getAccessToken().toString());
            String accessTokenValue = responseJsonObject.getString("accessTokenValue");
        } catch (Exception e) {
            e.printStackTrace();
        }
        liRetrieveBasicProfileInfo();
    }

    private void liRetrieveBasicProfileInfo() {
//        String url = "https://api.linkedin.com/v1/people/~:(id,first-name,last-name,email-address)";
        String url = "https://api.linkedin.com/v1/people/~:(id,first-name,last-name,email-address,date-of-birth,industry,associations,headline,picture-url,summary,specialties,positions:(id,title,summary,start-date,end-date,is-current,company:(id,name,type,size,industry,ticker)))";
        //   String url = "https://api.linkedin.com/v1/people/~:(id,first-name,last-name,email-address,date-of-birth,industry,associations,headline,picture-url,summary,specialties,positions:(id,title,summary,start-date,end-date,is-current,company:(id,name,type,size,industry,ticker)),educations:(id,school-name,field-of-study,start-date,end-date,degree,activities,notes),associations,interests,num-recommenders,date-of-birth,publications:(id,title,publisher:(name),authors:(id,name),date,url,summary),patents:(id,title,summary,number,status:(id,name),office:(name),inventors:(id,name),date,url),languages:(id,language:(name),proficiency:(level,name)),skills:(id,skill:(name)),certifications:(id,name,authority:(name),number,start-date,end-date),courses:(id,name,number),recommendations-received:(id,recommendation-type,recommendation-text,recommender),honors-awards,three-current-positions,three-past-positions,volunteer)";
        APIHelper apiHelper = APIHelper.getInstance(this);
        //Context mContext, String url, ApiListener apiListener
        APIHelper.getInstance(this).getRequest(this, url, new ApiListener() {
            @Override
            public void onApiSuccess(ApiResponse apiResponse) {
                try {
                    JSONObject respJsonObject = apiResponse.getResponseDataAsJson();
                    LogToastUtility.LI(TAG, ">>Data: " + respJsonObject.toString());

                    profile_photo = respJsonObject.getString("pictureUrl");
                    fname = respJsonObject.getString("firstName");
                    lname = respJsonObject.getString("lastName");
                    email = respJsonObject.getString("emailAddress");
                    department = respJsonObject.getString("industry");
                    linkedInId = respJsonObject.getString("id");
                    headline = respJsonObject.getString("headline");
                    Utils.showToast(mContext, "hello " + fname);
                    try {
                        if (respJsonObject.has("positions")) {
                            //  designation=respJsonObject.getJSONObject("positions").getJSONObject("title");
                            if (respJsonObject.getJSONObject("positions").getInt("_total") >= 1) {
                                JSONArray org = respJsonObject.getJSONObject("positions").getJSONArray("values");
                                if (org.length() > 0) {
                                    designation = org.getJSONObject(0).getString("title");
                                    if (org.getJSONObject(0).getJSONObject("company").has("name")) {
                                        organisation = org.getJSONObject(0).getJSONObject("company").getString("name");
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    LogToastUtility.LI(TAG, "department: " + department + ",organisation" + organisation + ",profile_photo" + profile_photo);
                    LogToastUtility.LI(TAG, "designation: " + designation + ",organisation" + organisation);
                    LogToastUtility.LI(TAG, "fname: " + fname + ",lname" + lname);

                    mLinkedInView.linkedInLoginApiCall(linkedInId,headline,fname,lname,imeiNo,"1", String.valueOf(androidVersion),roleType);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onApiError(LIApiError LIApiError) {
                LogToastUtility.LI(TAG, ">>onApiError");
            }
        });
    }

    @Override
    public void linkedInLoginApiCallSuccess(int pid, String responseString) {
        try {
            Utils.stopProgress(LinkedInLoginActivity.this);
            Gson gson = new Gson();

            JSONObject jsonObject = null;
            jsonObject = new JSONObject(responseString);
            JSONObject jsonObjectMeta = jsonObject.getJSONObject("meta");
            int successCode=jsonObjectMeta.getInt("code");
            if (successCode== AppConstants.SUCCESS_CODE) {
                JSONObject jsonObjectData = jsonObject.getJSONObject("data");
                int roleType = jsonObjectData.getInt("role");
                System.out.println(roleType);
                if (roleType == 1) {
                    candidateProfileModel = gson.fromJson(responseString, CandidateProfileModel.class);

                    if (candidateProfileModel.getMeta().getCode() == AppConstants.SUCCESS_CODE) {
                        if (candidateProfileModel.getData().getRole() == 1 ) {
                            Data.deleteAll(Data.class);

                            Data data = new Data();
                            data = candidateProfileModel.getData();
                            for (EducationList educationList : candidateProfileModel.getData().getEducationList()) {
                                educationList.save();
                            }
                            for (EmployeeType employeeType : candidateProfileModel.getData().getEmployeeType()) {
                                employeeType.save();
                            }
                            for (KeySkill keySkill : candidateProfileModel.getData().getKeySkills()) {
                                keySkill.save();
                            }
                            data.save();

                            mPref.setAuthToken("Token " + candidateProfileModel.getData().getToken().toString());
                            mPref.setRoleType(candidateProfileModel.getData().getRole());
                            Log.d("candidate data", "" + gson.toJson(Data.listAll(Data.class)));

                            Intent i = new Intent(LinkedInLoginActivity.this, CandidateHomeActivity.class);
                            startActivity(i);
                            finish();
                        }
                    }
                } else if (roleType == 2) {

                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void linkedInLoginApiCallError(int pid, String error) {
        try {
            Utils.stopProgress(LinkedInLoginActivity.this);
            Utils.showToast(LinkedInLoginActivity.this, "Please retry");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
