package pt.magical.magicalcareers.views.fragments.recruiter;
/**
 * Created by RAdhu on 16 Aug 2017.
 */

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.print.PageRange;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintDocumentInfo;
import android.print.PrintManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.view.menu.MenuPopupHelper;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.flexbox.FlexboxLayout;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fisk.chipcloud.ChipCloud;
import fisk.chipcloud.ChipCloudConfig;
import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.adapters.recruiter.JdCandidatesListRecyclerAdapter;
import pt.magical.magicalcareers.adapters.recruiter.JdScreeningQusListRecyclerAdapter;
import pt.magical.magicalcareers.interfaces.review_jd.IReviewJDPresenter;
import pt.magical.magicalcareers.interfaces.review_jd.IReviewJDView;
import pt.magical.magicalcareers.interfaces.review_jd.ReviewJDImpl;
import pt.magical.magicalcareers.models.recruiter.review_jd.Question;
import pt.magical.magicalcareers.models.recruiter.review_jd.ReviewJDModel;
import pt.magical.magicalcareers.utils.AppConstants;
import pt.magical.magicalcareers.utils.LogToastUtility;
import pt.magical.magicalcareers.utils.Utils;
import pt.magical.magicalcareers.views.activities.recruiter.RecruiterDashboardActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class ViewJdParentFragment extends Fragment implements IReviewJDView{

    private IReviewJDPresenter mIReviewJDPresenter;
    private ChipCloudConfig mChipCloudConfig;
    private JdScreeningQusListRecyclerAdapter mScreeningQusRecyclerAdapter;
    private ReviewJDModel mReviewJDModel;
    InputStream input = null;
    OutputStream output = null;

    // Required empty public constructor
    public ViewJdParentFragment() {
    }


    //Fragment Manager
    private FragmentManager mFragmentManager;
    //Context Variable
    private Context mContext;
    //Parent Activity
    private RecruiterDashboardActivity activity;

    /**
     * Variable Instances and Views OnBind (all fragment_create_jd_parent.xml views)
     */

    @BindView(R.id.fragment_view_jd_parent_skill_fl)
    public FlexboxLayout mSkillFlexboxLayout;
    @BindView(R.id.fragment_view_jd_parent_screening_qus_rv)
    public RecyclerView mScreeningQusRV;
    @BindView(R.id.fragment_view_jd_parent_designation_tv)
    public TextView mDesignationTV;
    @BindView(R.id.fragment_view_jd_parent_company_tv)
    public TextView mCompanyTV;
    @BindView(R.id.fragment_view_jd_parent_postedon_tv)
    public TextView mPostedOnTV;
    @BindView(R.id.fragment_view_jd_parent_logo_iv)
    public ImageView mLogoIV;
    @BindView(R.id.fragment_view_jd_parent_linkedin_iv)
    public ImageView mLinkedInIV;
    @BindView(R.id.fragment_view_jd_parent_web_iv)
    public ImageView mWebIV;
    @BindView(R.id.fragment_view_jd_parent_info_iv)
    public ImageView mInfoIV;
    @BindView(R.id.fragment_view_jd_parent_experience_tv)
    public TextView mExperienceTV;
    @BindView(R.id.fragment_view_jd_parent_notice_period_tv)
    public TextView mnoticePeriodTV;
    @BindView(R.id.fragment_view_jd_parent_position_tv)
    public TextView mPositionTV;
    @BindView(R.id.fragment_view_jd_parent_salary_tv)
    public TextView mSalaryTV;
 @BindView(R.id.fragment_view_jd_parent_benifits_tv)
    public TextView mBenifitsTV;
 @BindView(R.id.fragment_view_jd_parent_qualification_tv)
    public TextView mQualificationTV;
 @BindView(R.id.fragment_view_jd_parent_job_description_tv)
    public TextView mJobDescriptionTV;
 @BindView(R.id.fragment_view_jd_parent_responsibility_tv)
    public TextView mResponsibilityTV;
 @BindView(R.id.fragment_view_jd_parent_job_desc_file_tv)
    public TextView mJobDescFileTV;
 @BindView(R.id.fragment_view_jd_parent_screening_question_tv)
    public TextView mScreeningQuestionTV;




    @Override
    public void onAttach(Context context) {
        this.mContext = context;
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_view_jd_parent, container, false);
        ButterKnife.bind(this, view);
        activity = (RecruiterDashboardActivity) getActivity();
        mFragmentManager = activity.getSupportFragmentManager();
        initializeViews();
        return view;
    }

    private void initializeViews() {
        mIReviewJDPresenter = new ReviewJDImpl(this);
        int jd_id=23;
        mIReviewJDPresenter.reviewJdData(jd_id);
    }

    private void setCandidateRecyclerAdapter(List<Question> questions) {
        if(questions.size()>0) {
            mScreeningQusRV.setHasFixedSize(true);
            mScreeningQusRecyclerAdapter = new JdScreeningQusListRecyclerAdapter(this, questions);
            mScreeningQusRV.setAdapter(mScreeningQusRecyclerAdapter);
            mScreeningQusRV.setLayoutManager(new LinearLayoutManager(getActivity()));
            mScreeningQusRecyclerAdapter.notifyDataSetChanged();
        }else
        {

        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            if (getView() != null) {
                getView().setFocusableInTouchMode(true);
                getView().requestFocus();
                getView().setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_BACK) {
                            mFragmentManager.popBackStack();
                        }
                        return true;
                    }
                });
            }
        } catch (Exception e) {
            Log.e("error", "" + e);
        }
    }

    /**
     * Setting Click Listeners to all the views of CreateJdParentFragment.
     */
    @OnClick({R.id.fragment_view_jd_parent_back_iv,R.id.fragment_view_jd_parent_menu_iv,R.id.fragment_view_jd_parent_web_iv,R.id.fragment_view_jd_parent_linkedin_iv,R.id.fragment_view_jd_parent_info_iv,R.id.fragment_view_jd_parent_job_desc_file_tv})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_view_jd_parent_back_iv:
                mFragmentManager.popBackStack();
                break;
            case R.id.fragment_view_jd_parent_web_iv:
                try {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(mReviewJDModel.getData().getWebsiteUrl()));
                    startActivity(browserIntent);
                }catch (Exception e)
                {e.printStackTrace();}
                break;
            case R.id.fragment_view_jd_parent_linkedin_iv:
                try {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(mReviewJDModel.getData().getLinkedInUrl()));
                    startActivity(browserIntent);
                }catch (Exception e)
                {e.printStackTrace();}
                break;
            case R.id.fragment_view_jd_parent_job_desc_file_tv:
                LogToastUtility.LI("ViewJD","urlpdf:"+AppConstants.URL.BASE_URL.getBaseUrl()+mReviewJDModel.getData().getJdFile().toString());
                printPaper(AppConstants.URL.BASE_URL.getBaseUrl()+""+mReviewJDModel.getData().getJdFile().toString());
                break;
            case R.id.fragment_view_jd_parent_menu_iv:
                //LogToastUtility.TOAST_L(mContext,"More......");
                makePopupMenu(v);
                break;
        }
    }

    private void makePopupMenu(View v) {
        PopupMenu popup = new PopupMenu(this.getContext(), v);
        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.menu.menu_view_jd, popup.getMenu());
        Field field = null;
        try {
            field = popup.getClass().getDeclaredField("mPopup");
        field.setAccessible(true);
        MenuPopupHelper menuPopupHelper = (MenuPopupHelper) field.get(popup);
        menuPopupHelper.setForceShowIcon(true);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_edit:
                        LogToastUtility.TOAST_L(mContext,"edit...");
                        break;
                    case R.id.action_archive:
                        DialogCreateArchivedJD();
                        break;
                    case R.id.action_share:
                        LogToastUtility.TOAST_L(mContext,"share...");
                        break;
                    case R.id.action_copy:
                        LogToastUtility.TOAST_L(mContext,"copy...");
                        break;
                }

                return true;
            }
        });
        popup.show();

    }

    private void goToWhoAreYouHiringForPage() {
        mFragmentManager.beginTransaction().setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_up).
                replace(R.id.recruiter_dashboard_home_act_frame_layout, new WhoAreUHiringForFragment())
                .addToBackStack(null).commit();
    }


    private void settingUpSkillSelected(ReviewJDModel mReviewJDModel) {

         mChipCloudConfig = new ChipCloudConfig()
                .selectMode(ChipCloud.SelectMode.multi)
                .checkedChipColor(Color.parseColor("#e0e0e0"))
                .checkedTextColor(Color.parseColor("#000000"))
                .uncheckedChipColor(Color.parseColor("#e0e0e0"))
                .uncheckedTextColor(Color.parseColor("#000000"));

        ChipCloud deleteableCloud = new ChipCloud(getActivity(), mSkillFlexboxLayout, mChipCloudConfig);

        if(mReviewJDModel.getData().getSkills().size()>0) {
            for (int i = 0; i < mReviewJDModel.getData().getSkills().size(); i++) {
                deleteableCloud.addChip(mReviewJDModel.getData().getSkills().get(i).toString());
            }
        }
    }


    public void DialogCreateArchivedJD()
    {
        // custom dialog
        final Dialog dialog = new Dialog(mContext);
        dialog.setContentView(R.layout.custom_dialog_archive_jd);
        TextView txtCancel=(TextView) dialog.findViewById(R.id.custom_dialog_archive_jd_cancle_tv);
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void getReviewJDDataSuccess(int pId, ReviewJDModel mReviewJDModel) {
        this.mReviewJDModel=mReviewJDModel;
        Utils.stopProgress(mContext);
        settingUpSkillSelected(mReviewJDModel);
        setData(mReviewJDModel);
        setCandidateRecyclerAdapter(mReviewJDModel.getData().getQuestions());
    }

    private void setData(ReviewJDModel mReviewJDModel) {
        //mSkillFlexboxLayout;
        mDesignationTV.setText(mReviewJDModel.getData().getDesignation());
        mCompanyTV.setText(mReviewJDModel.getData().getCompanyName());
        mPostedOnTV.setText(mReviewJDModel.getData().getDate()+"");
        LogToastUtility.LI("ViewJD","url:"+AppConstants.URL.BASE_URL.getBaseUrl()+mReviewJDModel.getData().getCompanyLogo().toString());
        Glide.with(mContext).load(AppConstants.URL.BASE_URL.getBaseUrl()+mReviewJDModel.getData().getCompanyLogo().toString()).into(mLogoIV);
        mExperienceTV.setText(mReviewJDModel.getData().getExperienceFrom()+"-"+mReviewJDModel.getData().getExperienceTo()+" Years");
        mnoticePeriodTV.setText(""+mReviewJDModel.getData().getNoticePeriod()+" days");

        if(mReviewJDModel.getData().getJobType().size()>0) {
            for (int i = 0; i < mReviewJDModel.getData().getJobType().size(); i++) {
                mPositionTV.setText(mReviewJDModel.getData().getJobType().get(i).toString() + ",");
            }
        }else
            mPositionTV.setText("-");

        mSalaryTV.setText(mReviewJDModel.getData().getCtcFrom()+"-"+mReviewJDModel.getData().getCtcTo());
        if(mReviewJDModel.getData().getBenefits().size()>0) {
            String benifits="";
            for (int i = 0; i < mReviewJDModel.getData().getBenefits().size(); i++) {
                benifits=benifits+"\u25CF"+mReviewJDModel.getData().getBenefits().get(i).toString()+"";
            }
            mBenifitsTV.setText(benifits);
        }else
            mBenifitsTV.setText("-");

        mQualificationTV.setText(mReviewJDModel.getData().getHighestEducationLevel());
        mJobDescriptionTV.setText(mReviewJDModel.getData().getJobDescription());
        mResponsibilityTV.setText("--");
        mJobDescFileTV.setText(mReviewJDModel.getData().getCompanyName()+"("+mReviewJDModel.getData().getDesignation()+")");
        mScreeningQuestionTV.setText("Screening Question("+mReviewJDModel.getData().getQuestions().size()+")");

    }

    @Override
    public void getReviewJDDataError(int pId, String errorData) {
        try {
            LogToastUtility.TOAST_L(mContext,errorData);
            Utils.stopProgress(mContext);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    /**
     * Going to see this paper print (in {@link ViewJdParentFragment}.java)
     *
     * @param file_name
     */
    @SuppressLint("NewApi")
    public void printPaper(final String file_name) {
        try {
            //Your code goes here
            PrintManager printManager = (PrintManager) getActivity().getSystemService(Context.PRINT_SERVICE);
            String jobName = "HR All" + " Document";
            PrintDocumentAdapter pda = null;

            pda = new PrintDocumentAdapter() {

                @Override
                public void onLayout(PrintAttributes printAttributes, PrintAttributes printAttributes1, android.os.CancellationSignal cancellationSignal, LayoutResultCallback layoutResultCallback, Bundle bundle) {
                    {

                        if (cancellationSignal.isCanceled()) {
                            layoutResultCallback.onLayoutCancelled();
                            return;
                        }


                        PrintDocumentInfo pdi = new PrintDocumentInfo.Builder("Name of file").setContentType(PrintDocumentInfo.CONTENT_TYPE_DOCUMENT).build();

                        layoutResultCallback.onLayoutFinished(pdi, true);
                    }
                }

                @Override
                public void onWrite(PageRange[] pageRanges, final ParcelFileDescriptor parcelFileDescriptor, android.os.CancellationSignal cancellationSignal, final WriteResultCallback writeResultCallback) {
                    //for local
                    // input = new FileInputStream(file_name);

                    //for server url
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            //creating new thread to handle Http Operations
                            try {
                                URL url = null;
                                try {
                                    url = new URL(file_name);
                                } catch (MalformedURLException e) {
                                    e.printStackTrace();
                                }
                                URLConnection uconn = null;
                                try {
                                    uconn = url.openConnection();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    input = uconn.getInputStream();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                output = new FileOutputStream(parcelFileDescriptor.getFileDescriptor());
                                byte[] buf = new byte[1024];
                                int bytesRead;

                                while ((bytesRead = input.read(buf)) > 0) {
                                    output.write(buf, 0, bytesRead);
                                }
                                writeResultCallback.onWriteFinished(new PageRange[]{PageRange.ALL_PAGES});
                            } catch (FileNotFoundException ee) {
                                //Catch exception
                                ee.printStackTrace();
                            } catch (Exception e) {
                                //Catch exception
                                e.printStackTrace();
                            } finally {
                                try {
                                    input.close();
                                    output.close();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }).start();
                }
            };
            printManager.print(jobName, pda, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
