package pt.magical.magicalcareers.views.fragments.recruiter;
/**
 * Created by Sachin on 14 Aug 2017.
 */

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.views.activities.recruiter.RecruiterDashboardActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class CreateJdParentFragment extends Fragment {

    // Required empty public constructor
    public CreateJdParentFragment() {
    }

    //Fragment Manager
    private FragmentManager mFragmentManager;
    //Context Variable
    private Context mContext;
    //Parent Activity
    private RecruiterDashboardActivity activity;

    /**
     * Variable Instances and Views OnBind (all fragment_create_jd_parent.xml views)
     */

    @BindView(R.id.create_jd_parent_close_iv)
    public ImageView mCloseIV;


    @BindView(R.id.create_jd_parent_first_ll)
    public LinearLayout mFirstLL;
    @BindView(R.id.create_jd_parent_second_ll)
    public LinearLayout mSecondLL;
    @BindView(R.id.create_jd_parent_third_ll)
    public LinearLayout mThirdLL;
    @BindView(R.id.create_jd_parent_fourth_ll)
    public LinearLayout mFourthLL;
    @BindView(R.id.create_jd_parent_fifth_ll)
    public LinearLayout mFifthLL;
    @BindView(R.id.create_jd_parent_sixth_ll)
    public LinearLayout mSixthLL;


    @BindView(R.id.create_jd_parent_proceed1_button)
    public Button mProceed1Button;
    @BindView(R.id.create_jd_parent_proceed2_button)
    public Button mProceed2Button;
    @BindView(R.id.create_jd_parent_proceed3_button)
    public Button mProceed3Button;
    @BindView(R.id.create_jd_parent_proceed4_button)
    public Button mProceed4Button;
    @BindView(R.id.create_jd_parent_proceed5_button)
    public Button mProceed5Button;
    @BindView(R.id.create_jd_parent_proceed6_button)
    public Button mProceed6Button;

    @Override
    public void onAttach(Context context) {
        this.mContext = context;
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_jd_parent, container, false);
        ButterKnife.bind(this, view);
        activity = (RecruiterDashboardActivity) getActivity();
        mFragmentManager = activity.getSupportFragmentManager();
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mSecondLL.setAlpha((float) 0.3);
        mThirdLL.setAlpha((float) 0.3);
        mFourthLL.setAlpha((float) 0.3);
        mFifthLL.setAlpha((float) 0.3);
        mSixthLL.setAlpha((float) 0.3);
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            if (getView() != null) {
                getView().setFocusableInTouchMode(true);
                getView().requestFocus();
                getView().setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_BACK) {
                            mFragmentManager.popBackStack();
                        }
                        return true;
                    }
                });
            }
        } catch (Exception e) {
            Log.e("error", "" + e);
        }
    }

    /**
     * Setting Click Listeners to all the views of CreateJdParentFragment.
     */
    @OnClick({R.id.create_jd_parent_close_iv, R.id.create_jd_parent_proceed1_button})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.create_jd_parent_proceed1_button:
                goToWhoAreYouHiringForPage();
                break;
            case R.id.create_jd_parent_close_iv:
                closeThisFrag();
                break;

        }
    }

    private void closeThisFrag() {
        mFragmentManager.popBackStack();
    }

    private void goToWhoAreYouHiringForPage() {
        mFragmentManager.beginTransaction().
                setCustomAnimations(R.anim.bottom_to_top_enter, R.anim.bottom_to_top_exit, R.anim.top_to_bottom_enter, R.anim.top_to_bottom_exit).
                replace(R.id.recruiter_dashboard_home_act_frame_layout, new WhoAreUHiringForFragment())
                    .addToBackStack("CreateJdParentFragment").commit();
    }
}
