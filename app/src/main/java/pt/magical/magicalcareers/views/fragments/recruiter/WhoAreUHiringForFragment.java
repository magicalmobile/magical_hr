package pt.magical.magicalcareers.views.fragments.recruiter;
/**
 * Created by Sachin on 14 Aug 2017.
 */

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.engine.impl.GlideEngine;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.interfaces.add_company.AddCompanyPresenterImpl;
import pt.magical.magicalcareers.interfaces.add_company.IAddCompanyPresenter;
import pt.magical.magicalcareers.interfaces.add_company.IAddCompanyView;
import pt.magical.magicalcareers.models.add_company.AddCompanyModel;
import pt.magical.magicalcareers.models.add_company.error.AddCompanyErrorModel;
import pt.magical.magicalcareers.models.create_jd_flow_data_in_app.CreateJdFlowDataInAppModel;
import pt.magical.magicalcareers.utils.Utils;
import pt.magical.magicalcareers.views.activities.common.AutoSearchActivity;
import pt.magical.magicalcareers.views.activities.recruiter.RecruiterDashboardActivity;

import static android.R.attr.data;
import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class WhoAreUHiringForFragment extends Fragment implements IAddCompanyView {


    public WhoAreUHiringForFragment() {
        // Required empty public constructor
    }


    private IAddCompanyPresenter mIAddCompanyPresenter;

    private static final int REQUEST_CODE_CHOOSE = 23;
    private List<Uri> mSelected;

    //Fragment Manager
    private FragmentManager mFragmentManager;
    //Context Variable
    private Context mContext;
    //Parent Activity
    private RecruiterDashboardActivity activity;


    private String dataChoosed, dataType;


    @Override
    public void onAttach(Context context) {
        this.mContext = context;
        super.onAttach(context);
    }

    @BindView(R.id.reg_act_company_name_tv)
    public TextView mCompanyNameTV;

    @BindView(R.id.reg_act_industry_name_tv)
    public TextView mIndustryNameTV;

    @BindView(R.id.reg_act_company_name_et)
    public EditText mCompanyNameET;

    @BindView(R.id.reg_act_industry_name_et)
    public EditText mIndustryNameET;

    @BindView(R.id.reg_act_website_et)
    public EditText mWebsiteET;

    @BindView(R.id.reg_act_linkedin_et)
    public EditText mLinkedInET;

    @BindView(R.id.reg_act_compay_logo_iv)
    public ImageView mCompanyLogoIV;

    private String mCompanyNameVaue, mIndustryValue, mWebsiteValue, mLinkedInValue;

    private String selectedLogoPath;


    private Map<String, String> images;


    private List<CreateJdFlowDataInAppModel> mCreateJdFlowDataInAppDb;


    private CreateJdFlowDataInAppModel mCreateJdFlowDataInAppModel = new CreateJdFlowDataInAppModel();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_who_are_uhiring_for, container, false);
        ButterKnife.bind(this, view);
        activity = (RecruiterDashboardActivity) getActivity();
        mFragmentManager = activity.getSupportFragmentManager();
        mIAddCompanyPresenter = new AddCompanyPresenterImpl(this);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }


    /**
     * Setting Click Listeners to all the views of WhoAreUHiringForFragment.
     */
    @OnClick({R.id.who_are_you_hiring_for_close_iv, R.id.who_are_you_hiring_next_button,
            R.id.reg_act_company_name_et, R.id.reg_act_industry_name_et, R.id.reg_act_compay_logo_iv})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.who_are_you_hiring_for_close_iv:
                Utils.closingThisFrag(mFragmentManager);
                break;
            case R.id.who_are_you_hiring_next_button:
                goToWhatPositionFrag();
                break;
            case R.id.reg_act_company_name_et:
                goToChooseCompanyFromList();
                break;
            case R.id.reg_act_industry_name_et:
                goToChooseIndustryFromList();
                break;
            case R.id.reg_act_compay_logo_iv:
                goToChooseCompanyLogoPic();
                break;
        }
    }


    private void goToWhatPositionFrag() {
        try {
            if (TextUtils.isEmpty(mCompanyNameET.getText().toString()) && TextUtils.isEmpty(mIndustryNameET.getText().toString())) {
                Utils.showToast(mContext, "Please enter these values");
                mCompanyNameTV.setTextColor(Color.RED);
                mIndustryNameTV.setTextColor(Color.RED);
                mCompanyNameET.setBackground(getResources().getDrawable(R.drawable.error_transparent_backcground));
                mIndustryNameET.setBackground(getResources().getDrawable(R.drawable.error_transparent_backcground));
            } else if (TextUtils.isEmpty(mCompanyNameET.getText().toString())) {
                mCompanyNameTV.setTextColor(Color.RED);
                mCompanyNameET.setBackground(getResources().getDrawable(R.drawable.error_transparent_backcground));
                mIndustryNameTV.setTextColor(getResources().getColor(R.color.frag_value_head_color));
                mIndustryNameET.setBackground(getResources().getDrawable(R.drawable.oval_button_transparent_backcground));
            } else if (TextUtils.isEmpty(mIndustryNameET.getText().toString())) {
                mIndustryNameTV.setTextColor(Color.RED);
                mIndustryNameET.setBackground(getResources().getDrawable(R.drawable.error_transparent_backcground));
                mCompanyNameTV.setTextColor(getResources().getColor(R.color.frag_value_head_color));
                mCompanyNameET.setBackground(getResources().getDrawable(R.drawable.oval_button_transparent_backcground));

            } else {
                mIndustryNameTV.setTextColor(getResources().getColor(R.color.frag_value_head_color));
                mIndustryNameET.setBackground(getResources().getDrawable(R.drawable.oval_button_transparent_backcground));
                mCompanyNameTV.setTextColor(getResources().getColor(R.color.frag_value_head_color));
                mCompanyNameET.setBackground(getResources().getDrawable(R.drawable.oval_button_transparent_backcground));

                savingDataInAppDB();

               /* mFragmentManager.beginTransaction().
                        setCustomAnimations(R.anim.right_to_left_enter, R.anim.right_to_left_exit, R.anim.left_to_right_enter, R.anim.left_to_right_exit).
                        replace(R.id.recruiter_dashboard_home_act_frame_layout, new WhatPositionFragment())
                        .addToBackStack(null).commit();

*/
                //mIAddCompanyPresenter.addACompanyApi(mCompanyNameVaue, "1", mWebsiteValue, mLinkedInValue, images);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        try {

            mCompanyNameET.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (s.length() > 0) {
                        mCompanyNameTV.setTextColor(getResources().getColor(R.color.colorBlack));
                        mCompanyNameET.setBackground(getResources().getDrawable(R.drawable.oval_button_transparent_backcground));

                    }
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    mCompanyNameVaue = s.toString();

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (s.length() > 0) {
                        mCompanyNameTV.setTextColor(getResources().getColor(R.color.colorBlack));
                        mCompanyNameET.setBackground(getResources().getDrawable(R.drawable.oval_button_transparent_backcground));

                        mCompanyNameVaue = s.toString();
                        mCompanyNameET.setText(s.toString());


                    }
                }
            });

            mIndustryNameET.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (s.length() > 0) {
                        mIndustryNameTV.setTextColor(getResources().getColor(R.color.colorBlack));
                        mIndustryNameET.setBackground(getResources().getDrawable(R.drawable.oval_button_transparent_backcground));
                    }
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    mIndustryValue = s.toString();
                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (s.length() > 0) {
                        mIndustryNameTV.setTextColor(getResources().getColor(R.color.colorBlack));
                        mIndustryNameET.setBackground(getResources().getDrawable(R.drawable.oval_button_transparent_backcground));
                        mIndustryValue = s.toString();
                        mIndustryNameET.setText(s.toString());
                    }
                }
            });

            mWebsiteET.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    mWebsiteValue = s.toString();
                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (s.length() > 0) {
                        mWebsiteValue = s.toString();

                    }
                }
            });

            mLinkedInET.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    mLinkedInValue = s.toString();
                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (s.length() > 0) {
                        mLinkedInValue = s.toString();

                    }
                }
            });

            settingTheDataFromAppDB();

            if (getView() != null) {
                getView().setFocusableInTouchMode(true);
                getView().requestFocus();
                getView().setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_BACK) {
                            mFragmentManager.popBackStack();
                        }
                        return true;
                    }
                });
            }
        } catch (Exception e) {
            Log.e("error", "" + e);
        }
    }

    private void settingTheDataFromAppDB() {
        try {
            mCreateJdFlowDataInAppDb = CreateJdFlowDataInAppModel.listAll(CreateJdFlowDataInAppModel.class);

            if (mCreateJdFlowDataInAppDb.get(0).getCompanyName().length() > 0) {
                mCompanyNameET.setText(mCreateJdFlowDataInAppDb.get(0).getCompanyName().toString());
                mCompanyNameET.setSelection(mCreateJdFlowDataInAppDb.get(0).getCompanyName().length());
            }

            if (mCreateJdFlowDataInAppDb.get(0).getIndustryName().length() > 0) {
                mIndustryNameET.setText(mCreateJdFlowDataInAppDb.get(0).getIndustryName().toString());
                mIndustryNameET.setSelection(mCreateJdFlowDataInAppDb.get(0).getIndustryName().length());
            }

            if (mCreateJdFlowDataInAppDb.get(0).getWebsiteLink().length() > 0) {
                mWebsiteET.setText(mCreateJdFlowDataInAppDb.get(0).getWebsiteLink());
                mWebsiteET.setSelection(mCreateJdFlowDataInAppDb.get(0).getWebsiteLink().length());
            }

            if (mCreateJdFlowDataInAppDb.get(0).getLinkedinLink().length() > 0) {
                mLinkedInET.setText(mCreateJdFlowDataInAppDb.get(0).getLinkedinLink());
                mLinkedInET.setSelection(mCreateJdFlowDataInAppDb.get(0).getLinkedinLink().length());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void goToChooseCompanyFromList() {

        try {
            ArrayList<String> companyList = new ArrayList<>();
            companyList.clear();
            for (int i = 0; i < 10; i++) {
                companyList.add("Company " + (i + 1));
            }

            Intent intent = new Intent(mContext, AutoSearchActivity.class);
            intent.putExtra("dataType", Utils.dataCompany);
            intent.putExtra("pageTitle", "Add Company");
            intent.putExtra("dataTitle", "Company Name");
            intent.putStringArrayListExtra("dataList", companyList);
            startActivityForResult(intent, Utils.CHOOSE_DATA_FROM_GIVEN_LIST);// Activity is started with requestCode 2
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void goToChooseIndustryFromList() {
        try {
            ArrayList<String> industryList = new ArrayList<>();
            industryList.clear();
            for (int i = 0; i < 10; i++) {
                industryList.add("Industry " + (i + 1));
            }
            Intent intent = new Intent(mContext, AutoSearchActivity.class);
            intent.putExtra("dataType", Utils.dataIndustry);
            intent.putExtra("pageTitle", "Add Industry");
            intent.putExtra("dataTitle", "Industry Name");
            intent.putStringArrayListExtra("dataList", industryList);
            startActivityForResult(intent, Utils.CHOOSE_DATA_FROM_GIVEN_LIST);// Activity is started with requestCode 2
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //capture and select image from gallery
    private void selectAndCaptureImage() {

        final CharSequence[] items = {"Take Photo", "Choose from Gallery",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, 101);
                } else if (items[item].equals("Choose from Gallery")) {
                    chooseImagesFromDeviceMemory();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    /**
     * going to choose the company Logo from device memory
     */
    private void goToChooseCompanyLogoPic() {
        if (Utils.isStoragePermissionGranted(getActivity())) {
            selectAndCaptureImage();
        }
    }

    /**
     * Going to choose Images from Memory of Device
     */
    private void chooseImagesFromDeviceMemory() {
        Matisse.from(activity).
                choose(MimeType.allOf()).
                countable(true).
                maxSelectable(1).
                restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED).
                thumbnailScale(0.85f).
                imageEngine(new GlideEngine()).
                forResult(REQUEST_CODE_CHOOSE);
    }


    /**
     * Coming the result from another activity after choosing from a list given there or camera/gallery intent result
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            // check if the request code is same as what is passed  here it is Utils.CHOOSE_DATA_FROM_GIVEN_LIST
            if (requestCode == Utils.CHOOSE_DATA_FROM_GIVEN_LIST) {
                dataChoosed = data.getStringExtra(Utils.dataChoosed);
                dataType = data.getStringExtra(Utils.dataType);


                Log.d("DataChoosed", "in WhoAreUHiringForFrag" + dataChoosed + " of  " + dataType);

                /**
                 * Checking the choosed data type and setting at respective place
                 */
                if (dataType.equals(Utils.dataCompany)) {
                    mCompanyNameET.setText(dataChoosed);
                    if (mCreateJdFlowDataInAppDb.size() > 0) {
                        mCreateJdFlowDataInAppDb.get(0).setCompanyName(dataChoosed);
                    } else {
                        mCreateJdFlowDataInAppModel.setCompanyName(dataChoosed);
                    }
                } else if (dataType.equals(Utils.dataIndustry)) {
                    mIndustryNameET.setText(dataChoosed);
                    if (mCreateJdFlowDataInAppDb.size() > 0) {
                        mCreateJdFlowDataInAppDb.get(0).setIndustryName(dataChoosed);
                    } else {
                        mCreateJdFlowDataInAppModel.setIndustryName(dataChoosed);
                    }
                }
            } else if (requestCode == REQUEST_CODE_CHOOSE && resultCode == RESULT_OK) {
                mSelected = Matisse.obtainResult(data);
                Log.d("Matisse", "mSelected: " + mSelected);

                settingUpCompanyLogoImage(mSelected);

            } else if (requestCode == 101 && resultCode == RESULT_OK) {
                settingUpCompanyLogoCaptured(data);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Setting the company Logo captured by device
     */
    private void settingUpCompanyLogoCaptured(Intent data) {
        try {
            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            thumbnail.compress(Bitmap.CompressFormat.PNG, 90, bytes);
            File destination = new File(Environment.getExternalStorageDirectory(),
                    System.currentTimeMillis() + ".png");
            FileOutputStream fo;
            Log.e("capture", "capture" + destination.getAbsolutePath());

            selectedLogoPath = destination.getAbsolutePath();


            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
            mCompanyLogoIV.setImageBitmap(thumbnail);
            mCompanyLogoIV.setBackground(getResources().getDrawable(R.drawable.rectangle_background));
            Log.e("tag", "imagefile" + destination.getAbsolutePath());
            images = new HashMap<String, String>();
            images.put("company_logo", destination.getAbsolutePath());


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    /**
     * Setting the company Logo Picked from device memory
     */
    private void settingUpCompanyLogoImage(List<Uri> mSelected) {
        try {
            if (mSelected.size() > 0) {
                String path = mSelected.get(0).getPath().toString();
                images = new HashMap<String, String>();
                images.put("company_logo", path);

                Glide.with(mContext).load(mSelected.get(0)).asBitmap().centerCrop().into(mCompanyLogoIV);
                mCompanyLogoIV.setBackground(getResources().getDrawable(R.drawable.rectangle_background));
            }
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onaddACompanyApiSuccess(int pid, AddCompanyModel addCompanyModel) {
        try {
            Utils.stopProgress(mContext);
            Utils.showToast(mContext, addCompanyModel.getMeta().getMessage().toString());
            mFragmentManager.beginTransaction().
                    setCustomAnimations(R.anim.right_to_left_enter, R.anim.right_to_left_exit, R.anim.left_to_right_enter, R.anim.left_to_right_exit).
                    replace(R.id.recruiter_dashboard_home_act_frame_layout, new WhatPositionFragment())
                    .addToBackStack(null).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onaddACompanyApiError(int pid, AddCompanyErrorModel addCompanyErrorModel) {
        Utils.stopProgress(mContext);
        if (addCompanyErrorModel.getData().getLinkedInUrl() != null) {
            // Utils.showToast(mContext, addCompanyErrorModel.getData().getLinkedInUrl().get(0).toString());
            mLinkedInET.setError(addCompanyErrorModel.getData().getLinkedInUrl().get(0).toString());
        }
        if (addCompanyErrorModel.getData().getWebsiteUrl() != null) {
            //Utils.showToast(mContext, addCompanyErrorModel.getData().getWebsiteUrl().get(0).toString());
            mWebsiteET.setError(addCompanyErrorModel.getData().getWebsiteUrl().get(0).toString());
        }
    }

    private void savingDataInAppDB() {
        Log.d("Values", "are" + mCompanyNameVaue + mIndustryValue + mWebsiteValue + mLinkedInValue);
        if (mCreateJdFlowDataInAppDb.size() > 0) {
            mCreateJdFlowDataInAppModel = mCreateJdFlowDataInAppDb.get(0);
            mCreateJdFlowDataInAppModel.setCompanyName(mCompanyNameVaue);
            mCreateJdFlowDataInAppModel.setIndustryName(mIndustryValue);
            mCreateJdFlowDataInAppModel.setWebsiteLink(mWebsiteValue);
            mCreateJdFlowDataInAppModel.setLinkedinLink(mLinkedInValue);
            mCreateJdFlowDataInAppModel.save();
        } else {
            mCreateJdFlowDataInAppModel.setCompanyName(mCompanyNameVaue);
            mCreateJdFlowDataInAppModel.setIndustryName(mIndustryValue);
            mCreateJdFlowDataInAppModel.setWebsiteLink(mWebsiteValue);
            mCreateJdFlowDataInAppModel.setLinkedinLink(mLinkedInValue);
            mCreateJdFlowDataInAppModel.save();

        }
    }

}
