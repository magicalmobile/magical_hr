package pt.magical.magicalcareers.views.fragments.recruiter;
/**
 * Created by RAdhu on 17 Aug 2017.
 */

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.adapters.recruiter.TrendingQuestionViewPagerAdapter;
import pt.magical.magicalcareers.interfaces.add_trending_question.AddTrendingQuestionImpl;
import pt.magical.magicalcareers.interfaces.add_trending_question.IAddTrendingQuestionPresenter;
import pt.magical.magicalcareers.interfaces.add_trending_question.IAddTrendingQuestionView;
import pt.magical.magicalcareers.interfaces.review_jd.ReviewJDImpl;
import pt.magical.magicalcareers.interfaces.trending_question_list.ITrendingQuestionListPresenter;
import pt.magical.magicalcareers.interfaces.trending_question_list.ITrendingQuestionListView;
import pt.magical.magicalcareers.interfaces.trending_question_list.TrendingQuestionListImpl;
import pt.magical.magicalcareers.models.recruiter.add_trending_question.AddTrendingQuestionModel;
import pt.magical.magicalcareers.models.recruiter.review_jd.Question;
import pt.magical.magicalcareers.models.recruiter.trending_question_list.AnswerList;
import pt.magical.magicalcareers.models.recruiter.trending_question_list.Datum;
import pt.magical.magicalcareers.models.recruiter.trending_question_list.TrendingQuestionListModel;
import pt.magical.magicalcareers.models.recruiter_master_data.Data;
import pt.magical.magicalcareers.utils.LogToastUtility;
import pt.magical.magicalcareers.utils.Utils;
import pt.magical.magicalcareers.views.activities.recruiter.RecruiterDashboardActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class TrendingQuestionsFragment extends Fragment implements IAddTrendingQuestionView,ITrendingQuestionListView{


    private List<Datum> questionList;
    private TrendingQuestionViewPagerAdapter questionAdapter;

    public TrendingQuestionsFragment() {
        // Required empty public constructor
    }

    IAddTrendingQuestionPresenter iAddTrendingQuestionPresenter;
    ITrendingQuestionListPresenter iTrendingQuestionListPresenter;
    //Fragment Manager
    private FragmentManager mFragmentManager;
    //Context Variable
    private Context mContext;
    //Parent Activity
    private RecruiterDashboardActivity activity;

    @BindView(R.id.fragment_trending_questions_back_iv)
    public ImageView mBackIV;
    @BindView(R.id.fragment_trending_questions_add_iv)
    public ImageView mAddIV;

    @BindView(R.id.fragment_trending_questions_list_vp)
    public ViewPager mTrendingQuestionVP;
    @BindView(R.id.fragment_trending_questions_title_tv)
    public TextView mTrendingQuestionTitleTV;
    @BindView(R.id.fragment_trending_questions_search_iv)
    public ImageView mTrendingQuestionSearchIV;
    @BindView(R.id.fragment_trending_questions_search_close_iv)
    public ImageView mTrendingQuestionSearchCloseIV;
    @BindView(R.id.fragment_trending_questions_search_et)
    public EditText mTrendingQuestionSearchET;
    @BindView(R.id.fragment_trending_questions_search_rl)
    public RelativeLayout mTrendingQuestionSearchRL;
    @BindView(R.id.fragment_trending_questions_total_tv)
    public TextView mTrendingQuestionTotalTV;

    @Override
    public void onAttach(Context context) {
        this.mContext = context;
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_trending_questions, container, false);
        ButterKnife.bind(this, view);
        activity = (RecruiterDashboardActivity) getActivity();
        mFragmentManager = activity.getSupportFragmentManager();

        initializeView();
        return view;
    }

    private void initializeView() {
        iAddTrendingQuestionPresenter = new AddTrendingQuestionImpl(this);
        iTrendingQuestionListPresenter = new TrendingQuestionListImpl(this);
        int master_key_skill=1;
        iTrendingQuestionListPresenter.getTrendingQuestionList(master_key_skill);
    }


    /**
     * Setting Click Listeners to all the views of CreateJdParentFragment.
     */
    @OnClick({R.id.fragment_trending_questions_back_iv,R.id.fragment_trending_questions_add_iv,R.id.fragment_trending_questions_search_close_iv,R.id.fragment_trending_questions_filter_iv,R.id.fragment_trending_questions_search_iv})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_trending_questions_back_iv:
                mFragmentManager.popBackStack();
                break;
           case R.id.fragment_trending_questions_add_iv:
               // mFragmentManager.popBackStack();
                mFragmentManager.beginTransaction().setCustomAnimations(R.anim.right_to_left_enter, R.anim.right_to_left_exit,R.anim.left_to_right_enter,R.anim.left_to_right_exit).
                        replace(R.id.recruiter_dashboard_home_act_frame_layout, new ScreeningQuestionListFragment())
                        .addToBackStack(null).commit();
                break;
            case R.id.fragment_trending_questions_filter_iv:
                //mFragmentManager.popBackStack();
                mFragmentManager.beginTransaction().setCustomAnimations(R.anim.right_to_left_enter, R.anim.right_to_left_exit,R.anim.left_to_right_enter,R.anim.left_to_right_exit).
                        replace(R.id.recruiter_dashboard_home_act_frame_layout, new TrendingQuestionsFilterFragment())
                        .addToBackStack(null).commit();
                break;
            case R.id.fragment_trending_questions_search_iv:
                mTrendingQuestionSearchRL.setVisibility(View.VISIBLE);
                mTrendingQuestionTitleTV.setVisibility(View.GONE);
                mTrendingQuestionSearchIV.setVisibility(View.GONE);
                break;
            case R.id.fragment_trending_questions_search_close_iv:
                mTrendingQuestionSearchRL.setVisibility(View.GONE);
                mTrendingQuestionTitleTV.setVisibility(View.VISIBLE);
                mTrendingQuestionSearchIV.setVisibility(View.VISIBLE
                );
                break;
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    @Override
    public void getAddTrendingQuestionDataSuccess(int pId, AddTrendingQuestionModel mAddTrendingQuestionModel) {
        try {

            LogToastUtility.TOAST_L(mContext,mAddTrendingQuestionModel.getMeta().getMessage());
            Utils.stopProgress(mContext);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getAddTrendingQuestionDataError(int pId, String errorData) {
        try {
            LogToastUtility.TOAST_L(mContext,errorData);
            Utils.stopProgress(mContext);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*on add question click*/
    public void onAddClick(int position,int master_ques_id) {
        try {
            int jd_id=16;
            if(questionList.get(position).getIsAdded())
                LogToastUtility.TOAST_L(mContext,"Already aadded.");
            else {
                iAddTrendingQuestionPresenter.addTrendingQuestion(jd_id, master_ques_id);
                Datum question = questionList.get(position);
                question.setIsAdded(true);
                question.save();
                setAdapter(position);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getTrendingQuestionDataSuccess(int pId, TrendingQuestionListModel mTrendingQuestionListModel) {
            Utils.stopProgress(mContext);
        Datum.deleteAll(Datum.class);
        AnswerList.deleteAll(AnswerList.class);
        for (Datum questionList:mTrendingQuestionListModel.getData()){
            questionList.setIsAdded(false);
            questionList.save();
            LogToastUtility.LI("TAG","question:"+questionList.getAnswerList().size()+""+questionList.getQuestion());
            for (AnswerList answerList:questionList.getAnswerList()) {
                answerList.setQuestionId(questionList.getMasterScreeningQuestionId());
                answerList.save();
            }
        }

       setAdapter(0);
    }

    private void setAdapter(int current) {
        questionList = Datum.findWithQuery(Datum.class, "SELECT * FROM trending_question_list");
        if(questionList.size()>0) {
            questionAdapter=new TrendingQuestionViewPagerAdapter(mContext, this, questionList);
            mTrendingQuestionVP.setAdapter(questionAdapter);
            mTrendingQuestionVP.setCurrentItem(current);
            int added_count=0;
            for(int i=0;i<questionList.size();i++)
            {
                if(questionList.get(i).getIsAdded())
                    added_count++;
            }

            if(added_count>0)
            {
                mBackIV.setVisibility(View.GONE);
                mAddIV.setVisibility(View.VISIBLE);
            }else
            {
                mBackIV.setVisibility(View.VISIBLE);
                mAddIV.setVisibility(View.GONE);
            }

            mTrendingQuestionTotalTV.setText(added_count+"/"+questionList.size()+" Added");
        }
    }

    @Override
    public void getTrendingQuestionDataError(int pId, String errorData) {
        try {
            Utils.stopProgress(mContext);
            LogToastUtility.TOAST_L(mContext,errorData);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}

