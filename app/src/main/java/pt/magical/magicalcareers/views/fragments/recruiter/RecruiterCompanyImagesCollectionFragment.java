package pt.magical.magicalcareers.views.fragments.recruiter;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.engine.impl.GlideEngine;
import com.zhihu.matisse.filter.Filter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.adapters.recruiter.RecruiterCompanyImagesViewAdapter;
import pt.magical.magicalcareers.adapters.recruiter.RecruiterCompanyVideosViewAdapter;
import pt.magical.magicalcareers.utils.Utils;
import pt.magical.magicalcareers.views.activities.recruiter.RecruiterProfileParentActivity;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class RecruiterCompanyImagesCollectionFragment extends Fragment {

    private static final int REQUEST_CODE_CHOOSE = 23;
    private List<Uri> mSelected;

    public RecruiterCompanyImagesCollectionFragment() {
        // Required empty public constructor
    }

    private RecruiterProfileParentActivity activity;
    //Fragment Manager
    private FragmentManager mFragmentManager;
    //Context Variable
    private Context mContext;


    @Override
    public void onAttach(Context context) {
        this.mContext = context;
        super.onAttach(context);
    }

    @BindView(R.id.recruiter_company_images_collections_recycler_view)
    public RecyclerView mImagesCollectionsRecyclerView;
    @BindView(R.id.recruiter_company_images_add_from_gallery_tv)
    public TextView mAddFromDeviceMemoryTextView;

    private RecruiterCompanyImagesViewAdapter mRecruiterCompanyImagesViewAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_recruiter_company_images_collections, container, false);
        ButterKnife.bind(this, view);
        activity = (RecruiterProfileParentActivity) getActivity();
        mFragmentManager = activity.getSupportFragmentManager();
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void settingUpImagesLayout(List<Uri> mSelected) {
        mImagesCollectionsRecyclerView.setHasFixedSize(true);
        mImagesCollectionsRecyclerView.setLayoutManager(new StaggeredGridLayoutManager(3, 1));
        mRecruiterCompanyImagesViewAdapter = new RecruiterCompanyImagesViewAdapter(mContext,mSelected);
        mImagesCollectionsRecyclerView.setAdapter(mRecruiterCompanyImagesViewAdapter);
        mImagesCollectionsRecyclerView.setNestedScrollingEnabled(false);
    }

    /**
     * Setting Click Listeners to all the views of RecruiterCompanyVideoCollectionsFragment.
     */
    @OnClick({R.id.recruiter_company_images_collections_back_iv, R.id.recruiter_company_images_add_from_gallery_tv})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.recruiter_company_images_collections_back_iv:
                mFragmentManager.popBackStack();
                break;
            case R.id.recruiter_company_images_add_from_gallery_tv:
                if (Utils.isStoragePermissionGranted(getActivity())) {
                    selectAndCaptureImage();
                }
                break;
        }
    }

    //capture and select image from gallery
    private void selectAndCaptureImage() {

        final CharSequence[] items = {"Take Photo", "Choose from Gallery",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, 101);
                } else if (items[item].equals("Choose from Gallery")) {
                    chooseImagesFromDeviceMemory();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    /**
     * Going to choose Images from Memory of Device
     */
    private void chooseImagesFromDeviceMemory() {
        Matisse.from(activity).
                choose(MimeType.allOf()).
                countable(true).
                maxSelectable(Utils.MAX_IMAGES_SELECTABLE).
                restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED).
                thumbnailScale(0.85f).
                imageEngine(new GlideEngine()).
                forResult(REQUEST_CODE_CHOOSE);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_CHOOSE && resultCode == RESULT_OK) {
            mSelected = Matisse.obtainResult(data);
            Log.d("Matisse", "mSelected: " + mSelected);

            settingUpImagesLayout(mSelected);

        }
    }


}
