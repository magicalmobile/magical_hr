package pt.magical.magicalcareers.views.activities.common;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import org.apache.commons.codec.binary.Base64;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.interfaces.otp_varification.IOtpVarificationPresenter;
import pt.magical.magicalcareers.interfaces.otp_varification.OtpVarificationModelImpl;
import pt.magical.magicalcareers.interfaces.set_password.ISetPasswordPresenter;
import pt.magical.magicalcareers.interfaces.set_password.ISetPasswordView;
import pt.magical.magicalcareers.interfaces.set_password.SetPasswordModelImpl;
import pt.magical.magicalcareers.models.set_password.SetPasswordModel;
import pt.magical.magicalcareers.utils.Utils;

public class SetPasswordActivity extends AppCompatActivity implements ISetPasswordView {

    @BindView(R.id.act_set_pass_et)
    public EditText mPass;


    private ISetPasswordPresenter mISetPasswordPresenter;
    private String passwordValue;

    private Dialog verifyOtpDialog;
    private Button mLogin;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_password);

        ButterKnife.bind(this);


        initializeViews();

    }

    private void initializeViews() {

        mISetPasswordPresenter = new SetPasswordModelImpl(this);


    }


    @OnClick({R.id.act_set_pass_submit_button})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.act_set_pass_submit_button:

                setPasswordDoneFun();
                break;


        }
    }

    private void setPasswordDoneFun() {
        //  passwordValue = mPass.getText().toString();
        passwordValue = mPass.getText().toString().trim();
        byte[] encodedBytes = Base64.encodeBase64(passwordValue.getBytes());
        if (TextUtils.isEmpty(passwordValue)) {
            Utils.showToast(this, "Please enter your registered mobile number or email");
        } else {
            mISetPasswordPresenter.setPasswordApi(new String(encodedBytes));
        }
    }


    @Override
    public void setPasswordApiSuccess(int pid, SetPasswordModel setPasswordModel) {

        try {
            Utils.stopProgress(SetPasswordActivity.this);
            if (setPasswordModel.getMeta().getCode() == 200) {
                Log.d("success","dialouge");
                showVerifyOtpDialog();
            } else {
                Utils.showToast(SetPasswordActivity.this, setPasswordModel.getMeta().getMessage());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void showVerifyOtpDialog() {
        verifyOtpDialog = new Dialog(SetPasswordActivity.this);
        verifyOtpDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        verifyOtpDialog.setContentView(R.layout.dialog_otp_verify);
        verifyOtpDialog.setCancelable(false);
        mLogin = (Button) verifyOtpDialog.findViewById(R.id.item_otp_submit_btn);
        verifyOtpDialog.getWindow().setGravity(Gravity.CENTER);
        verifyOtpDialog.show();

        mLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SetPasswordActivity.this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

    }

    @Override
    public void setPasswordApiError(int pid, String errorData) {

        try {
            Utils.stopProgress(SetPasswordActivity.this);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
