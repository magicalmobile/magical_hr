package pt.magical.magicalcareers.views.fragments.recruiter;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.flexbox.FlexboxLayout;
import com.jaygoo.widget.RangeSeekBar;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fisk.chipcloud.ChipCloud;
import fisk.chipcloud.ChipCloudConfig;
import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.utils.Utils;
import pt.magical.magicalcareers.views.activities.common.AutoSearchActivity;
import pt.magical.magicalcareers.views.activities.recruiter.RecruiterDashboardActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class CompanyBenefitsFragment extends Fragment {


    public CompanyBenefitsFragment() {
        // Required empty public constructor
    }


    //Fragment Manager
    private FragmentManager mFragmentManager;
    //Context Variable
    private Context mContext;
    //Parent Activity
    private RecruiterDashboardActivity activity;

    @BindView(R.id.benifits_offered_flexbox_deleteable)
    public FlexboxLayout mBenifitsFlexboxLayout;

    @BindView(R.id.benifits_offered_et)
    public EditText mOfferedBenifitET;
    @BindView(R.id.ctc_seekbar)
    public RangeSeekBar mCtcRangeSeekBar;
    @BindView(R.id.selected_ctc_value_tv)
    public TextView mSelectedCtcTV;
    @BindView(R.id.benifits_offered_ctc_tv)
    public TextView mOfferedCtcTV;
    ChipCloudConfig mChipCloudConfig;

    private String dataChoosed, dataType;
    private float minValue, maxValue;

    ChipCloud deleteableCloud;

    @Override
    public void onAttach(Context context) {
        this.mContext = context;
        super.onAttach(context);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_company_benefits, container, false);
        ButterKnife.bind(this, view);
        activity = (RecruiterDashboardActivity) getActivity();
        mFragmentManager = activity.getSupportFragmentManager();
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try {
            settingUpBenifitsSelected();
            mCtcRangeSeekBar.setOnRangeChangedListener(new RangeSeekBar.OnRangeChangedListener() {
                @Override
                public void onRangeChanged(RangeSeekBar view, float min, float max, boolean isFromUser) {
                    minValue = Utils.round(min, 2);
                    maxValue = Utils.round(max, 2);
                    Log.d("DataValues", "are" + minValue + maxValue);

                    mCtcRangeSeekBar.setProgressDescription((int) min + "%");
                    mSelectedCtcTV.setText("(" + minValue + " k" + "  to  " + maxValue + " k" + ")");

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void settingUpBenifitsSelected() {

        mChipCloudConfig = new ChipCloudConfig()
                .selectMode(ChipCloud.SelectMode.multi)
                .checkedChipColor(Color.parseColor("#ddaa00"))
                .checkedTextColor(Color.parseColor("#ffffff"))
                .uncheckedChipColor(Color.parseColor("#e0e0e0"))
                .showClose(Color.parseColor("#a6a6a6"))
                .useInsetPadding(false)
                .uncheckedTextColor(Color.parseColor("#000000"));

        deleteableCloud = new ChipCloud(getActivity(), mBenifitsFlexboxLayout, mChipCloudConfig);
        deleteableCloud.addChip("Health Insurance");
        deleteableCloud.addChip("Cab facility");


    }

    /**
     * Setting Click Listeners to all the views of WhoAreUHiringForFragment.
     */
    @OnClick({R.id.company_benifits_offered_close_iv, R.id.benifits_offered_next_button, R.id.benifits_offered_et})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.company_benifits_offered_close_iv:
                Utils.closingThisFrag(mFragmentManager);
                break;
            case R.id.benifits_offered_next_button:
                goToSkillsLookingFor();
                break;
            case R.id.benifits_offered_et:
                goToChooseBenifitsOfferedFromList();
                break;

            case R.id.what_pos_close_iv:
                Utils.closingThisFrag(mFragmentManager);
                break;
        }
    }


    private void goToSkillsLookingFor() {
        if (minValue == maxValue || maxValue == 0.0) {
            mOfferedCtcTV.setTextColor(getResources().getColor(R.color.colorRed));
            Utils.showToast(mContext, "Please choose your CTC");
        } else {
            mOfferedCtcTV.setTextColor(getResources().getColor(R.color.frag_value_head_color));
            mFragmentManager.beginTransaction().setCustomAnimations(R.anim.right_to_left_enter, R.anim.right_to_left_exit, R.anim.left_to_right_enter, R.anim.left_to_right_exit).
                    replace(R.id.recruiter_dashboard_home_act_frame_layout, new SkillsLookingForFragment())
                    .addToBackStack(null).commit();
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        try {
            if (getView() != null) {
                getView().setFocusableInTouchMode(true);
                getView().requestFocus();
                getView().setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_BACK) {
                            mFragmentManager.popBackStack();
                        }
                        return true;
                    }
                });
            }
        } catch (Exception e) {
            Log.e("error", "" + e);
        }
    }


    private void goToChooseBenifitsOfferedFromList() {

        ArrayList<String> companyBenifitsList = new ArrayList<>();
        companyBenifitsList.clear();
        for (int i = 0; i < 10; i++) {
            companyBenifitsList.add("Company Benifits " + (i + 1));
        }

        Intent intent = new Intent(mContext, AutoSearchActivity.class);
        intent.putExtra("dataType", Utils.dataCompanyBenifits);
        intent.putExtra("pageTitle", "Add Location");
        intent.putExtra("dataTitle", "Location Name");
        intent.putStringArrayListExtra("dataList", companyBenifitsList);
        startActivityForResult(intent, Utils.CHOOSE_DATA_FROM_GIVEN_LIST);// Activity is started with requestCode 2
    }


    // Call Back method  to get the Message form other Activity

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            // check if the request code is same as what is passed  here it is Utils.CHOOSE_DATA_FROM_GIVEN_LIST
            if (requestCode == Utils.CHOOSE_DATA_FROM_GIVEN_LIST) {
                dataChoosed = data.getStringExtra(Utils.dataChoosed);
                dataType = data.getStringExtra(Utils.dataType);

                Log.d("DataChoosed", "in WhatPositionFragment" + dataChoosed + " of  " + dataType);

                /**
                 * Checking the choosed data type and setting at respective place
                 */
                if (dataType.equals(Utils.dataCompanyBenifits)) {
                    // mOfferedBenifitET.setText(dataChoosed);
                    deleteableCloud.addChip(dataChoosed);

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
