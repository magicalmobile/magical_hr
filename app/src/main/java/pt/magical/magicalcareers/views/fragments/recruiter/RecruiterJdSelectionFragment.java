package pt.magical.magicalcareers.views.fragments.recruiter;

/**
 * Created by RAdhu on 22 Aug 2017.
 */

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.OnClick;
import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.views.activities.recruiter.RecruiterDashboardActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class RecruiterJdSelectionFragment extends Fragment {


    public RecruiterJdSelectionFragment() {
        // Required empty public constructor
    }

    //Fragment Manager
    private FragmentManager mFragmentManager;
    //Context Variable
    private Context mContext;
    //Parent Activity
    private RecruiterDashboardActivity activity;

    @Override
    public void onAttach(Context context) {
        this.mContext = context;
        super.onAttach(context);
    }

  /*  @BindView(R.id.recruiter_company_profile_videos_recycler_view)
    public RecyclerView mVideosRecyclerView;*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_recruiter_jd_selection, container, false);
        ButterKnife.bind(this, view);
        activity = (RecruiterDashboardActivity) getActivity();
        mFragmentManager = activity.getSupportFragmentManager();
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }


    /**
     * Setting Click Listeners to all the views of RecruiterCompanyProfileFragment.
     */
    @OnClick({R.id.fragment_candidate_view_profile_back_iv})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_candidate_view_profile_back_iv:
                getFragmentManager().beginTransaction().
                        replace(R.id.recruiter_dashboard_home_act_frame_layout, new ChattingViewFragment())
                        .commit();
                break;
        }
    }
}
