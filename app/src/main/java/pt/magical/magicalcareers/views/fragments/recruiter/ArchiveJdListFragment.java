package pt.magical.magicalcareers.views.fragments.recruiter;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.adapters.recruiter.ArchivedJdListViewAdapter;
import pt.magical.magicalcareers.views.activities.recruiter.RecruiterDashboardActivity;
import pt.magical.magicalcareers.views.activities.recruiter.RecruiterProfileParentActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class ArchiveJdListFragment extends Fragment {


    public ArchiveJdListFragment() {
        // Required empty public constructor
    }


    //Fragment Manager
    private FragmentManager mFragmentManager;
    //Context Variable
    private Context mContext;
    //Parent Activity
    private RecruiterProfileParentActivity activity;


    @Override
    public void onAttach(Context context) {
        this.mContext = context;
        super.onAttach(context);
    }

    /**
     * Views References of fragment_archive_jd_list.xml
     */

    @BindView(R.id.archiveJdListRecyclerView)
    public RecyclerView mArchiveJdListRecyclerView;

    @BindView(R.id.archiveJdListNoDataTV)
    public TextView mArchiveJdListNoDataTV;

    @BindView(R.id.archiveJdListActToolbar)
    public Toolbar mToolbar;

    @BindView(R.id.achiveJdListActBackIV)
    public ImageView mBackImageView;


    private ArrayList<String> mArchiveJobsList = new ArrayList<>();
    private ArchivedJdListViewAdapter archivedJdListViewAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_archive_jd_list, container, false);
        ButterKnife.bind(this, view);
        activity = (RecruiterProfileParentActivity) getActivity();
        mFragmentManager = activity.getSupportFragmentManager();
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeViews();
    }

    private void initializeViews() {
        try {
            setUpArchiveJdList();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setUpArchiveJdList() {
        try {

            mArchiveJobsList.clear();
            for (int i = 0; i < 21; i++) {
                mArchiveJobsList.add("Archive Job No. " + (i + 1));
            }
            if (mArchiveJobsList.size() > 0 || mArchiveJobsList != null) {
                archivedJdListViewAdapter = new ArchivedJdListViewAdapter(mContext, mArchiveJobsList);

                mArchiveJdListRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
                mArchiveJdListRecyclerView.setHasFixedSize(true);
                mArchiveJdListRecyclerView.setAdapter(archivedJdListViewAdapter);
            } else {
                mArchiveJdListRecyclerView.setVisibility(View.GONE);
                mArchiveJdListNoDataTV.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Setting Click Listeners to all the views of WhoAreUHiringForFragment.
     */
    @OnClick({R.id.achiveJdListActBackIV})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.achiveJdListActBackIV:
                mFragmentManager.popBackStack();
                break;
        }
    }

}
