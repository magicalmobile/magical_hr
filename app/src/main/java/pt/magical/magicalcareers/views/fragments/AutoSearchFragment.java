package pt.magical.magicalcareers.views.fragments;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.adapters.TopRatedDataViewAdapter;
import pt.magical.magicalcareers.utils.Utils;

/**
 * A simple {@link Fragment} subclass.
 */
public class AutoSearchFragment extends Fragment {


    public AutoSearchFragment() {
    }

    private FragmentManager mFragmentManager;
    private Context mContext;

    @Override
    public void onAttach(Context context) {
        this.mContext = context;
        super.onAttach(context);
    }

    /**
     * New Instance
     */
    private String pageTitle, dataTitle;
    private ArrayList<String> dataList = new ArrayList<>();
    private Fragment comingFromTAG;
    private int comingFromLayout;

    public static AutoSearchFragment newInstance(String page_title, String data_title, ArrayList<String> dataList, Fragment currentfrag, int layout) {
        AutoSearchFragment fragment = new AutoSearchFragment();
        try {
            fragment.pageTitle = page_title;
            fragment.dataTitle = data_title;
            fragment.dataList = dataList;
            fragment.comingFromTAG = currentfrag;
            fragment.comingFromLayout = layout;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fragment;
    }


    /**
     * Views From fragment_auto_search.xml
     */

    @BindView(R.id.auto_search_page_back_iv)
    public ImageView mBackImageView;

    @BindView(R.id.auto_search_page_title_tv)
    public TextView mPageTitleTextView;

    @BindView(R.id.auto_search_page_data_title_tv)
    public TextView mPageDataTitleTextView;

    @BindView(R.id.auto_search_page_auto_complete_tv)
    public EditText mSearchEditText;

    @BindView(R.id.auto_search_page_recycler_view)
    public RecyclerView mTopRatedDataRecyclerView;

    @BindView(R.id.auto_search_page_done_tv)
    public TextView mDoneTextView;

    private TopRatedDataViewAdapter mTopRatedDataViewAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_auto_search, container, false);
        ButterKnife.bind(this, view);
        mFragmentManager = getActivity().getSupportFragmentManager();
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Utils.showKeyboard(mContext);
        settingData();
    }

    @Override
    public void onResume() {
        super.onResume();
        searchInData();
    }

    private void settingData() {

        mPageTitleTextView.setText(pageTitle);
        mPageDataTitleTextView.setText(dataTitle);

        mTopRatedDataRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
      //  mTopRatedDataViewAdapter = new TopRatedDataViewAdapter(this, dataList);
        mTopRatedDataRecyclerView.setAdapter(mTopRatedDataViewAdapter);
    }

    private void searchInData() {
        mSearchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                filter(editable.toString());
            }
        });
    }

    private void filter(String text) {
        ArrayList<String> filterdNames = new ArrayList<>();
        for (String s : dataList) {
            if (s.toLowerCase().contains(text.toLowerCase())) {
                filterdNames.add(s);
            }
        }
        mTopRatedDataViewAdapter.filterList(filterdNames);
    }

    public void setDataInSearchEditText(String dataValue) {
        mSearchEditText.setText(dataValue);
        mSearchEditText.setSelection(dataValue.length());
    }

    @OnClick({R.id.auto_search_page_back_iv, R.id.auto_search_page_done_tv})
    public void OnClick(View view) {
        switch (view.getId()) {
            case R.id.auto_search_page_back_iv:
                Log.d("Event", "going back");
                mFragmentManager.popBackStack();
                break;
            case R.id.auto_search_page_done_tv:
                Log.d("Event", "going with data " + mSearchEditText.getText().toString());
                Utils.hideKeyboard(mContext);
                //Put the value
                Bundle args = new Bundle();
                args.putString("dataSelected", mSearchEditText.getText().toString());
                comingFromTAG.setArguments(args);
                getFragmentManager().beginTransaction().replace(comingFromLayout, comingFromTAG).commit();
                break;
        }
    }

}
