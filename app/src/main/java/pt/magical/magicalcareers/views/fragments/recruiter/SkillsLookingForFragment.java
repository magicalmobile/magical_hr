package pt.magical.magicalcareers.views.fragments.recruiter;

/**
 * Created by Sachin on 16 Aug 2017.
 */

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.utils.Utils;
import pt.magical.magicalcareers.views.activities.common.AutoSearchActivity;
import pt.magical.magicalcareers.views.activities.recruiter.RecruiterDashboardActivity;

public class SkillsLookingForFragment extends Fragment {


    public SkillsLookingForFragment() {
        // Required empty public constructor
    }

    //Fragment Manager
    private FragmentManager mFragmentManager;

    //Context Variable
    private Context mContext;
    //Parent Activity
    private RecruiterDashboardActivity activity;
    private String dataChoosed, dataType;

    @Override
    public void onAttach(Context context) {
        this.mContext = context;
        super.onAttach(context);
    }

    @BindView(R.id.add_skill1_et)
    public TextView mSkill1ET;
    @BindView(R.id.add_skill2_et)
    public TextView mSkill2ET;
    @BindView(R.id.add_skill3_et)
    public TextView mSkill3ET;
    @BindView(R.id.add_skill4_et)
    public TextView mSkill4ET;
    @BindView(R.id.add_skill5_et)
    public TextView mSkill5ET;

    @BindView(R.id.add_atleast_one_skill_tv)
    public TextView mAtleastOneSkillTV;

    private int selectedPosSkills;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_skills_looking_for, container, false);
        ButterKnife.bind(this, view);
        activity = (RecruiterDashboardActivity) getActivity();
        mFragmentManager = activity.getSupportFragmentManager();
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    /**
     * Setting Click Listeners to all the views of WhoAreUHiringForFragment.
     */
    @OnClick({R.id.skills_looking_for_close_iv, R.id.skills_looking_for_next_button, R.id.add_skill1_et, R.id.add_skill2_et,
            R.id.add_skill3_et, R.id.add_skill4_et, R.id.add_skill5_et})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.skills_looking_for_close_iv:
                Utils.closingThisFrag(mFragmentManager);
                break;
            case R.id.skills_looking_for_next_button:
                goToSkillsLookingForFrag();
                break;
            case R.id.add_skill1_et:
                selectedPosSkills = 1;
                goToChooseSkillsListFromList();
                break;
            case R.id.add_skill2_et:
                selectedPosSkills = 2;
                goToChooseSkillsListFromList();
                break;
            case R.id.add_skill3_et:
                selectedPosSkills = 3;
                goToChooseSkillsListFromList();
                break;
            case R.id.add_skill4_et:
                selectedPosSkills = 4;
                goToChooseSkillsListFromList();
                break;
            case R.id.add_skill5_et:
                selectedPosSkills = 5;
                goToChooseSkillsListFromList();
                break;
        }
    }


    private void goToSkillsLookingForFrag() {
        if (mSkill1ET.getText().toString().length() > 0) {
            mFragmentManager.beginTransaction().setCustomAnimations(R.anim.right_to_left_enter, R.anim.right_to_left_exit, R.anim.left_to_right_enter, R.anim.left_to_right_exit).
                    replace(R.id.recruiter_dashboard_home_act_frame_layout, new KindOfPersonLookingFragment())
                    .addToBackStack(null).commit();
        } else {
            mAtleastOneSkillTV.setTextColor(getResources().getColor(R.color.colorRed));
            Utils.showToast(mContext, "Please select atleast one skill");
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        try {
            initializeViews();
            if (getView() != null) {
                getView().setFocusableInTouchMode(true);
                getView().requestFocus();
                getView().setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_BACK) {
                            mFragmentManager.popBackStack();
                        }
                        return true;
                    }
                });
            }
        } catch (Exception e) {
            Log.e("error", "" + e);
        }
    }

    private void initializeViews() {

        Log.e("initializeViews", "" + "data");

        if (mSkill1ET.getText().toString().length() > 0) {
            Log.e("initializeViews", "1" + "data");
            mAtleastOneSkillTV.setTextColor(getResources().getColor(R.color.frag_title_color));
            mSkill2ET.setEnabled(true);
            mSkill2ET.setBackground(getResources().getDrawable(R.drawable.black_button_transparent_backcground));
        }
        if (mSkill2ET.getText().toString().length() > 0) {
            Log.e("initializeViews", "2" + "data");
            mSkill3ET.setEnabled(true);
            mSkill3ET.setBackground(getResources().getDrawable(R.drawable.black_button_transparent_backcground));
        }
        if (mSkill3ET.getText().toString().length() > 0) {
            Log.e("initializeViews", "3" + "data");
            mSkill4ET.setEnabled(true);
            mSkill4ET.setBackground(getResources().getDrawable(R.drawable.black_button_transparent_backcground));
        }
        if (mSkill4ET.getText().toString().length() > 0) {
            Log.e("initializeViews", "4" + "data");
            mSkill5ET.setEnabled(true);
            mSkill5ET.setBackground(getResources().getDrawable(R.drawable.black_button_transparent_backcground));
        }
    }


    private void goToChooseSkillsListFromList() {

        ArrayList<String> skillsList = new ArrayList<>();
        skillsList.clear();
        for (int i = 0; i < 10; i++) {
            skillsList.add("Skills  " + (i + 1));
        }

        Intent intent = new Intent(mContext, AutoSearchActivity.class);
        intent.putExtra("dataType", Utils.dataSkills);
        intent.putExtra("pageTitle", "Add Skill");
        intent.putExtra("dataTitle", "Skill Name");
        intent.putStringArrayListExtra("dataList", skillsList);
        startActivityForResult(intent, Utils.CHOOSE_DATA_FROM_GIVEN_LIST);// Activity is started with requestCode 2

    }

    // Call Back method  to get the Message form other Activity

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            // check if the request code is same as what is passed  here it is Utils.CHOOSE_DATA_FROM_GIVEN_LIST
            if (requestCode == Utils.CHOOSE_DATA_FROM_GIVEN_LIST) {
                dataChoosed = data.getStringExtra(Utils.dataChoosed);
                dataType = data.getStringExtra(Utils.dataType);

                Log.d("DataChoosed", "in WhatPositionFragment" + dataChoosed + " of  " + dataType);

                /**
                 * Checking the choosed data type and setting at respective place
                 */
                if (dataType.equals(Utils.dataSkills)) {
                    if (selectedPosSkills == 1) {
                        mSkill1ET.setText(dataChoosed);
                    } else if (selectedPosSkills == 2) {
                        mSkill2ET.setText(dataChoosed);
                    } else if (selectedPosSkills == 3) {
                        mSkill3ET.setText(dataChoosed);
                    } else if (selectedPosSkills == 4) {
                        mSkill4ET.setText(dataChoosed);
                    } else if (selectedPosSkills == 5) {
                        mSkill5ET.setText(dataChoosed);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
