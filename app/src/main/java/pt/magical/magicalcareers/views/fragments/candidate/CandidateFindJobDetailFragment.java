package pt.magical.magicalcareers.views.fragments.candidate;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.daprlabs.cardstack.SwipeDeck;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.adapters.candidate.CandidateJobDetailsSwipeSkillStackAdapter;
import pt.magical.magicalcareers.adapters.candidate.CandidateJobDetailsSwipeStackAdapter;
import pt.magical.magicalcareers.views.activities.candidate.CandidateFindJobDetailActivity;
import pt.magical.magicalcareers.views.activities.candidate.CandidateFindJobsActivity;


/**
 * A simple {@link Fragment} subclass.
 */
public class CandidateFindJobDetailFragment extends Fragment implements CandidateJobDetailsSwipeStackAdapter.industryOptionClick ,CandidateJobDetailsSwipeSkillStackAdapter.skillOptionClick{


    @BindView(R.id.candidate_job_details_frag_swipe_deck)
    public SwipeDeck mSwipeDeck;
    @BindView(R.id.candidate_job_details_frag_cancel_iv)
            public ImageView cancelIv;

    CandidateJobDetailsSwipeStackAdapter adapterProfessional;
    CandidateJobDetailsSwipeSkillStackAdapter adapterSkill;



    private Context mContext;
    private int mPos=5;
    private int mSkillPos=0;
    private int mRate=0;

    final ArrayList<String> testData = new ArrayList<>();
    private String mTitle;
    private CandidateFindJobDetailActivity activity;
    private int mCardPos=0;

    {
        testData.add("0");
        testData.add("1");
        testData.add("2");


    }

    public static CandidateFindJobDetailFragment newInstance(String title) {

        CandidateFindJobDetailFragment fragment = new CandidateFindJobDetailFragment();
        fragment.mTitle = title;


        Log.d("title", title);
        return fragment;
    }

    public CandidateFindJobDetailFragment() {

    }

    @Override
    public void onAttach(Context context) {
        this.mContext = context;
        super.onAttach(context);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_candidate_find_job_detail, container, false);
        ButterKnife.bind(this, view);

        initilizeview();
        return view;
    }



    private void initilizeview() {
        activity=(CandidateFindJobDetailActivity)getActivity();
        cancelIv.setImageResource(R.drawable.ic_close);
        if (mTitle.equalsIgnoreCase("profession")) {
            if (CandidateFindJobsActivity.mIndustryPos>0) {
                adapterProfessional = new CandidateJobDetailsSwipeStackAdapter(testData, getActivity(), this, CandidateFindJobsActivity.mIndustryPos);
                mSwipeDeck.setAdapter(adapterProfessional);
            }else{
                adapterProfessional = new CandidateJobDetailsSwipeStackAdapter(testData, getActivity(), this, mPos);
                mSwipeDeck.setAdapter(adapterProfessional);
            }

        } else if (mTitle.equalsIgnoreCase("skill")) {
            adapterSkill = new CandidateJobDetailsSwipeSkillStackAdapter(mContext,this,testData,mSkillPos,mRate);
            mSwipeDeck.setAdapter(adapterSkill);

        }


        mSwipeDeck.setEventCallback(new SwipeDeck.SwipeEventCallback() {
            @Override
            public void cardSwipedLeft(int position) {
                Log.i("MainActivity", "card was swiped left, position in adapter: " + position);
                mCardPos=position+1;
                cancelIv.setImageResource(R.drawable.ic_back);

            }

            @Override
            public void cardSwipedRight(int position) {

                Log.i("MainActivity", "card was swiped right, position in adapter: " + position);
            }

            @Override
            public void cardsDepleted() {
                Log.i("MainActivity", "no more cards");
            }

            @Override
            public void cardActionDown() {

            }

            @Override
            public void cardActionUp() {

            }
        });

    }


    @OnClick({R.id.candidate_job_details_frag_next_button,R.id.candidate_job_details_frag_cancel_iv})
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.candidate_job_details_frag_next_button:
                funNextButton();
                break;
            case R.id.candidate_job_details_frag_cancel_iv:
                if (mCardPos==0){
                    activity.finish();
                }else if (mCardPos==1){
                    mSwipeDeck.setSelection(0);
                    cancelIv.setImageResource(R.drawable.ic_close);
                    mCardPos=0;
                }else if (mCardPos==2){
                    mSwipeDeck.setSelection(1);
                    mCardPos=1;
                }
                break;
        }
    }

    private void funNextButton() {


            if (mCardPos==0){
                if (!CandidateFindJobsActivity.mIndustry.equalsIgnoreCase("") && CandidateFindJobsActivity.mIndustry!=null){
                    mSwipeDeck.swipeTopCardLeft(10);
                }
            }else if (mCardPos==1){
                if (!CandidateFindJobsActivity.mExp.equalsIgnoreCase("") && CandidateFindJobsActivity.mExp!=null){
                    mSwipeDeck.swipeTopCardLeft(10);
                }
            }else if (mCardPos==2){
                if (!CandidateFindJobsActivity.mSalary.equalsIgnoreCase("") && CandidateFindJobsActivity.mSalary!=null){
                    activity.finish();
                }
            }




    }

    @Override
    public void industryOptionClick(int pos, String text) {
        CandidateFindJobsActivity.mIndustry=text;
        CandidateFindJobsActivity.mIndustryPos=pos;
        adapterProfessional = new CandidateJobDetailsSwipeStackAdapter(testData, getActivity(),this,pos);
        mSwipeDeck.setAdapter(adapterProfessional);
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            if (getView() != null) {
                getView().setFocusableInTouchMode(true);
                getView().requestFocus();
                getView().setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_BACK) {

                        }
                        return true;
                    }
                });
            }
        } catch (Exception e) {
            Log.e("error", "" + e);
        }
    }

    @Override
    public void skillOptionClick(int pos, String title, String exp, int rate) {
        Log.d("rate value",""+rate);
        adapterSkill = new CandidateJobDetailsSwipeSkillStackAdapter(mContext,this,testData,pos,rate);
        mSwipeDeck.setAdapter(adapterSkill);
        mSwipeDeck.setSelection(mCardPos);
    }
}
