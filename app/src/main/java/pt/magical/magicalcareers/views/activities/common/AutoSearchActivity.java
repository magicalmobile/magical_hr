package pt.magical.magicalcareers.views.activities.common;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yarolegovich.discretescrollview.DiscreteScrollView;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.adapters.TopRatedDataViewAdapter;
import pt.magical.magicalcareers.adapters.candidate.CandidateExperienceAdapter;
import pt.magical.magicalcareers.holders.candidate.CandidateExperienceViewHolder;
import pt.magical.magicalcareers.interfaces.pass_selected_data.IPassSelectedData;
import pt.magical.magicalcareers.utils.Utils;

public class AutoSearchActivity extends AppCompatActivity implements
        DiscreteScrollView.OnItemChangedListener<CandidateExperienceViewHolder>, DiscreteScrollView.ScrollStateChangeListener {

    /**
     * New Instance
     */
    private String pageTitle, dataTitle, dataType;
    private ArrayList<String> dataList = new ArrayList<>();

    /**
     * Views From fragment_auto_search.xml
     */

    @BindView(R.id.auto_search_page_back_iv)
    public ImageView mBackImageView;

    @BindView(R.id.auto_search_page_title_tv)
    public TextView mPageTitleTextView;

    @BindView(R.id.auto_search_page_data_title_tv)
    public TextView mPageDataTitleTextView;

    @BindView(R.id.auto_search_page_auto_complete_tv)
    public EditText mSearchEditText;

    @BindView(R.id.auto_search_page_recycler_view)
    public RecyclerView mTopRatedDataRecyclerView;

    @BindView(R.id.auto_search_experience_ll)
    public LinearLayout mSearchExperienceLl;

    @BindView(R.id.auto_search_page_done_tv)
    public TextView mDoneTextView;

    @BindView(R.id.auto_search_experience_discrete_scrollview)
    public DiscreteScrollView mExperienceDiscreteScrollView;


    private TopRatedDataViewAdapter mTopRatedDataViewAdapter;

    private IPassSelectedData mIPassSelectedData;

    private CandidateExperienceAdapter mCandidateExperienceAdapter;
    private String[] list = new String[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13"};
    private int mCurrentPosValue;
    private int mPos = 5;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auto_search);
        ButterKnife.bind(this);

        initializeViews();
    }

    private void initializeViews() {
        Utils.showKeyboard(this);

        Intent intent = getIntent();
        if (intent != null) {
            if (intent.getStringExtra("pageTitle") != null) {
                pageTitle = intent.getStringExtra("pageTitle");
            }
            if (intent.getStringExtra("dataTitle") != null) {
                dataTitle = intent.getStringExtra("dataTitle");
            }

            if (intent.getStringArrayListExtra("dataList") != null) {
                dataList = intent.getStringArrayListExtra("dataList");
            }
            if (intent.getStringExtra("dataType") != null) {
                dataType = intent.getStringExtra("dataType");
            }
        }

        settingData();
        setDiscreteView(mPos);
    }

    private void settingData() {


        mPageTitleTextView.setText(pageTitle);
        mPageDataTitleTextView.setText(dataTitle);

        mTopRatedDataRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mTopRatedDataViewAdapter = new TopRatedDataViewAdapter(this, dataList);
        mTopRatedDataRecyclerView.setAdapter(mTopRatedDataViewAdapter);

    }

    private void searchInData() {
        mSearchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mDoneTextView.setTextColor(getResources().getColor(R.color.colorGray));
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mDoneTextView.setTextColor(getResources().getColor(R.color.colorGreen));
            }

            @Override
            public void afterTextChanged(Editable editable) {
                mDoneTextView.setTextColor(getResources().getColor(R.color.colorGreen));
                filter(editable.toString());
            }
        });
    }

    private void filter(String text) {
        ArrayList<String> filterdNames = new ArrayList<>();
        for (String s : dataList) {
            if (s.toLowerCase().contains(text.toLowerCase())) {
                filterdNames.add(s);
            }
        }
        mTopRatedDataViewAdapter.filterList(filterdNames);
        if (dataType.equals(Utils.dataSkills)) {
            mTopRatedDataRecyclerView.setVisibility(View.GONE);
            mSearchExperienceLl.setVisibility(View.VISIBLE);
        }
    }

    public void setDataInSearchEditText(String dataValue) {
        mSearchEditText.setText(dataValue);
        mSearchEditText.setSelection(dataValue.length());
        if (dataType.equals(Utils.dataSkills)) {
            mTopRatedDataRecyclerView.setVisibility(View.GONE);
            mSearchExperienceLl.setVisibility(View.VISIBLE);

            mSearchEditText.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence s, int start, int before,
                                          int count) {
                    if (dataType.equals(Utils.dataSkills)) {
                        mTopRatedDataRecyclerView.setVisibility(View.GONE);
                        mSearchExperienceLl.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count,
                                              int after) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (dataType.equals(Utils.dataSkills)) {
                        mTopRatedDataRecyclerView.setVisibility(View.VISIBLE);
                        mSearchExperienceLl.setVisibility(View.VISIBLE);
                    }
                }

            });
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        searchInData();

      /*  mSearchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    choosedData();
                }
                return false;
            }
        });*/
    }

    @OnClick({R.id.auto_search_page_back_iv, R.id.auto_search_page_done_tv})
    public void OnClick(View view) {
        switch (view.getId()) {
            case R.id.auto_search_page_back_iv:
                onBackPressed();
                break;
            case R.id.auto_search_page_done_tv:
                Log.d("Event", "going with data to fragment " + mSearchEditText.getText().toString());
                choosedData();
                break;
        }
    }

    private void choosedData() {
        try {
            Utils.hideKeyboard(this);

            if (mSearchEditText.getText().toString() != null && mSearchEditText.getText().toString().length() > 0) {
                Log.d("DataChoosed", "in AutoSearchActivity" + mSearchEditText.getText().toString().trim() + " of  " + dataType);
                if (dataType.equals(Utils.dataSkills)) {
                    Intent intent = new Intent();
                    intent.putExtra("dataChoosed", mSearchEditText.getText().toString().trim() + "\n" + "(" + mCurrentPosValue + " yrs)     ");
                    intent.putExtra("dataType", dataType);
                    setResult(Utils.CHOOSE_DATA_FROM_GIVEN_LIST, intent);
                    finish();//finishing activity
                } else {
                    Intent intent = new Intent();
                    intent.putExtra("dataChoosed", mSearchEditText.getText().toString().trim());
                    intent.putExtra("dataType", dataType);
                    setResult(Utils.CHOOSE_DATA_FROM_GIVEN_LIST, intent);
                    finish();//finishing activitygfffggvcnbbnn
                }
            } else {
                mSearchEditText.setError("Please select some data");
            }
        } catch (
                Exception e)

        {
            e.printStackTrace();
        }

    }


    private void setDiscreteView(int pos) {

        Log.d("setDiscreteView", "setting");
        mExperienceDiscreteScrollView.setAdapter(new CandidateExperienceAdapter(AutoSearchActivity.this, list, pos));
        mExperienceDiscreteScrollView.addOnItemChangedListener(AutoSearchActivity.this);
        mExperienceDiscreteScrollView.addScrollStateChangeListener(AutoSearchActivity.this);
        mExperienceDiscreteScrollView.scrollToPosition(pos);
        mExperienceDiscreteScrollView.setItemTransformer(new ScaleTransformer.Builder().setMinScale(0.8f).build());
    }

    @Override
    public void onCurrentItemChanged(@Nullable CandidateExperienceViewHolder viewHolder, int adapterPosition) {
        mCurrentPosValue = adapterPosition;
        if (adapterPosition == mPos) {
            viewHolder.itemView.findViewById(R.id.year_tv).setVisibility(View.VISIBLE);
            TextView yearText = (TextView) viewHolder.itemView.findViewById(R.id.year_tv);
            yearText.setTextColor(getResources().getColor(R.color.white));
            TextView expText = (TextView) viewHolder.itemView.findViewById(R.id.item_experience_tv);
            expText.setTextColor(getResources().getColor(R.color.white));
            viewHolder.itemView.findViewById(R.id.exp_main_ll).setBackgroundColor(getResources().getColor(R.color.blue_dark_color));
        }
    }


    @Override
    public void onScrollStart(@NonNull RecyclerView.ViewHolder currentItemHolder, int adapterPosition) {
        currentItemHolder.itemView.findViewById(R.id.year_tv).setVisibility(View.INVISIBLE);
        TextView expText = (TextView) currentItemHolder.itemView.findViewById(R.id.item_experience_tv);
        expText.setTextColor(getResources().getColor(R.color.black));
        currentItemHolder.itemView.findViewById(R.id.exp_main_ll).setBackgroundColor(getResources().getColor(R.color.colorGray));
    }

    @Override
    public void onScrollEnd(@NonNull RecyclerView.ViewHolder currentItemHolder, int adapterPosition) {
        currentItemHolder.itemView.findViewById(R.id.year_tv).setVisibility(View.VISIBLE);
        TextView yearText = (TextView) currentItemHolder.itemView.findViewById(R.id.year_tv);
        yearText.setTextColor(getResources().getColor(R.color.white));
        TextView expText = (TextView) currentItemHolder.itemView.findViewById(R.id.item_experience_tv);
        expText.setTextColor(getResources().getColor(R.color.white));
        currentItemHolder.itemView.findViewById(R.id.exp_main_ll).setBackgroundColor(getResources().getColor(R.color.blue_dark_color));
    }

    @Override
    public void onScroll(float scrollPosition, @NonNull RecyclerView.ViewHolder currentHolder, @NonNull RecyclerView.ViewHolder newCurrent) {
    }
}
