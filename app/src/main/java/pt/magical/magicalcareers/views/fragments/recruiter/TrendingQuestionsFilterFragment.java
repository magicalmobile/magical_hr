package pt.magical.magicalcareers.views.fragments.recruiter;
/**
 * Created by RAdhu on 17 Aug 2017.
 */

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.adapters.recruiter.ScreeningQuestionListRecyclerAdapter;
import pt.magical.magicalcareers.adapters.recruiter.TrendingQuestionFilterRecyclerAdapter;
import pt.magical.magicalcareers.adapters.recruiter.TrendingQuestionViewPagerAdapter;
import pt.magical.magicalcareers.views.activities.recruiter.RecruiterDashboardActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class TrendingQuestionsFilterFragment extends Fragment {


    private TrendingQuestionFilterRecyclerAdapter mTrendingQuestionFilterRecyclerAdapter;

    public TrendingQuestionsFilterFragment() {
        // Required empty public constructor
    }

    //Fragment Manager
    private FragmentManager mFragmentManager;
    //Context Variable
    private Context mContext;
    //Parent Activity
    private RecruiterDashboardActivity activity;

    @BindView(R.id.fragment_trending_question_filter_by_rv)
    public RecyclerView mFilterByRV;

    @Override
    public void onAttach(Context context) {
        this.mContext = context;
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_trending_question_filter, container, false);
        ButterKnife.bind(this, view);
        activity = (RecruiterDashboardActivity) getActivity();
        mFragmentManager = activity.getSupportFragmentManager();

        initializeView();
        return view;
    }

    private void initializeView() {
        mFilterByRV.setAdapter(new TrendingQuestionFilterRecyclerAdapter(this));

        mFilterByRV.setHasFixedSize(true);
        mTrendingQuestionFilterRecyclerAdapter = new TrendingQuestionFilterRecyclerAdapter(this);
        mFilterByRV.setAdapter(mTrendingQuestionFilterRecyclerAdapter);
        mFilterByRV.setLayoutManager(new LinearLayoutManager(getActivity()));
        mTrendingQuestionFilterRecyclerAdapter.notifyDataSetChanged();
    }


    /**
     * Setting Click Listeners to all the views of CreateJdParentFragment.
     */
    @OnClick({R.id.fragment_trending_question_filter_close_iv,R.id.fragment_trending_question_filter_skill_tv,R.id.fragment_trending_question_filter_experience_tv,R.id.fragment_trending_question_filter_complexcity_tv})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_trending_question_filter_close_iv:
                mFragmentManager.popBackStack();
                break;
            case R.id.fragment_trending_question_filter_skill_tv:
            break;
            case R.id.fragment_trending_question_filter_experience_tv:
            break;
            case R.id.fragment_trending_question_filter_complexcity_tv:
            break;
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


}

