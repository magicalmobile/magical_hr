package pt.magical.magicalcareers.views.activities.candidate;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pt.magical.magicalcareers.R;

public class CandidateProgressActivity extends AppCompatActivity {

    @BindView(R.id.candidate_progress_see_question_act)
    public TextView mSeeQuestion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_candidate_progress);

        ButterKnife.bind(this);

        initilizeView();

    }


    private void initilizeView() {
    }

    @OnClick({R.id.candidate_progress_see_question_act})
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.candidate_progress_see_question_act:

                showQuestionActivity();
                break;
        }
    }

    private void showQuestionActivity() {
        Intent intent=new Intent(this,CandidateSeeQuestionActivity.class);
        startActivity(intent);
    }

}
