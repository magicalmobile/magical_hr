package pt.magical.magicalcareers.views.activities.common;

/**
 * Created by Sachin on 18 Aug 2017.
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.adapters.recruiter.NotificationsTypeAdapter;
import pt.magical.magicalcareers.utils.Utils;
import pt.magical.magicalcareers.views.fragments.common.TermsAndConditionsFragment;
import pt.magical.magicalcareers.views.fragments.recruiter.ChangePasswordFragment;

public class SettingsActivity extends AppCompatActivity {


    private FragmentManager mFragmentManager;
    private String TAG = "SettingsActivity";
    private Context mContext;

    /**
     * All Views references of activity_recruiter_profile_parent.xml
     */
    @BindView(R.id.recruiter_settings_act_toolbar)
    public Toolbar mToolbar;
    @BindView(R.id.recruiter_settings_act_frame_layout)
    public FrameLayout mFrameLayout;
    @BindView(R.id.recruiterProfilePageTitleValueTextView)
    public TextView recruiterProfilePageTitleValueTextView;

    public ProgressDialog progressDialog;
    private RecyclerView mNotificationsTypeRecyclerView;
    private NotificationsTypeAdapter mNotificationsTypeAdapter;

    private String selected_notifications_type_value = "";

    private ArrayList<String> notificationsTypeList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);
        initializeViews();
    }

    /**
     * setting up the components methods
     */
    private void initializeViews() {
        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        setSupportActionBar(mToolbar);
        mContext = SettingsActivity.this;
        mFragmentManager = getSupportFragmentManager();

        notificationsTypeList.clear();
        notificationsTypeList.add("Mute for 1 hour");
        notificationsTypeList.add("Mute for 8 hour");
        notificationsTypeList.add("Mute for 24 hour");
        notificationsTypeList.add("Turn on");

        mNotificationsTypeAdapter = new NotificationsTypeAdapter(SettingsActivity.this, notificationsTypeList);

    }


    /**
     * Setting Click Listeners to all the views of RecruiterSettingsFragment.
     */
    @OnClick({R.id.recruiter_settings_act_back_iv, R.id.recruiter_settings_act_change_password_rl,
            R.id.recruiter_settings_act_terms_and_conditions_rl, R.id.recruiter_settings_act_signout_rl,
            R.id.recruiter_settings_act_chat_notifications_rl})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.recruiter_settings_act_back_iv:
                onBackPressed();
                break;
            case R.id.recruiter_settings_act_change_password_rl:
                changePassFunc();
                break;
            case R.id.recruiter_settings_act_terms_and_conditions_rl:
                termsAndConditionsFunc();
                break;
            case R.id.recruiter_settings_act_signout_rl:
                signoutFunc();
                break;
            case R.id.recruiter_settings_act_chat_notifications_rl:
                chatNotifications();
                break;
        }
    }

    /**
     * Setting Change Password Fragment to recruiter settings page fframe layout
     */
    private void changePassFunc() {
        mFragmentManager.beginTransaction().setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_up)
                .replace(R.id.recruiter_settings_act_frame_layout, new ChangePasswordFragment())
                .addToBackStack(null).commit();
    }


    private void termsAndConditionsFunc() {
        mFragmentManager.beginTransaction().setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_up)
                .replace(R.id.recruiter_settings_act_frame_layout, new TermsAndConditionsFragment())
                .addToBackStack(null).commit();
    }

    private void signoutFunc() {
        Intent intent = new Intent(SettingsActivity.this, LoginActivity.class);
        overridePendingTransition(R.anim.right_to_left_enter, R.anim.right_to_left_exit);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void chatNotifications() {
        try {
            if (null != progressDialog && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            progressDialog = ProgressDialog.show(mContext, "", "", true);
            progressDialog.setCanceledOnTouchOutside(true);
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progressDialog.setContentView(R.layout.dialog_notification_type);

            mNotificationsTypeRecyclerView = (RecyclerView) progressDialog.findViewById(R.id.dialog_notifications_recycler_view);

            mNotificationsTypeRecyclerView.setLayoutManager(new LinearLayoutManager(SettingsActivity.this));
            mNotificationsTypeRecyclerView.setAdapter(mNotificationsTypeAdapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
