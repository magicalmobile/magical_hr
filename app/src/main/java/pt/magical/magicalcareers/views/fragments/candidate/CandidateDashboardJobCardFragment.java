package pt.magical.magicalcareers.views.fragments.candidate;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.daprlabs.cardstack.SwipeDeck;

import butterknife.BindView;
import butterknife.ButterKnife;
import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.adapters.candidate.CandidateDashboardJobCardSwipeAdapter;
import pt.magical.magicalcareers.interfaces.BubbleLayoutInterface;
import pt.magical.magicalcareers.views.activities.candidate.CandidateHomeActivity;

/**
 * Created by danish on 16-08-2017.
 */

public class CandidateDashboardJobCardFragment extends Fragment implements BubbleLayoutInterface{

    private Context context;

    @BindView(R.id.candidate_dashboard_swap_card_job)
    public SwipeDeck mSwipeCardView;

    private CandidateDashboardJobCardSwipeAdapter cardAdapter;
    private String TAG="CandidateDashboardJobFrag";
    private String[] data=new String[]{"Photoshop","Indesign","Illustrator","Coreldraw","After Effects"};
    private CandidateHomeActivity activity;
    private FragmentManager fragmentManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_candidate_dashboard_job_card,container,false);
        ButterKnife.bind(this,v);
        initializeView();
        return v;
    }

    private void initializeView() {
        CandidateDashboardFragment.isLearnClick=false;
        activity=(CandidateHomeActivity)getActivity();
        fragmentManager=activity.getSupportFragmentManager();
        cardAdapter= new CandidateDashboardJobCardSwipeAdapter(context,data,this,fragmentManager);
        mSwipeCardView.setAdapter(cardAdapter);
        mSwipeCardView.setEventCallback(new SwipeDeck.SwipeEventCallback() {
            @Override
            public void cardSwipedLeft(int position) {

            }

            @Override
            public void cardSwipedRight(int position) {

            }

            @Override
            public void cardsDepleted() {

            }

            @Override
            public void cardActionDown() {

            }

            @Override
            public void cardActionUp() {

            }
        });

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context=context;
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            if (getView() != null) {
                getView().setFocusableInTouchMode(true);
                getView().requestFocus();
                getView().setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_BACK) {
                            activity.finish();
                        }
                        return true;
                    }
                });
            }
        } catch (Exception e) {
            Log.e("error", "" + e);
        }
    }


    @Override
    public void onBubbleItemClick(int itemPosition, String itemTitle) {

    }
}
