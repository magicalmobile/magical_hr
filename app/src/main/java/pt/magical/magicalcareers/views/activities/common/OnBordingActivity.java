package pt.magical.magicalcareers.views.activities.common;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;

import com.rd.PageIndicatorView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.adapters.common.OnbordingAdapter;
import pt.magical.magicalcareers.utils.Sharedpreferences;
import pt.magical.magicalcareers.views.activities.candidate.CandidateFindJobsActivity;
import pt.magical.magicalcareers.views.activities.recruiter.RecruiterDashboardActivity;

public class OnBordingActivity extends AppCompatActivity {

    @BindView(R.id.onbording_act_viewpager)
    public ViewPager mViewPager;

    @BindView(R.id.onbording_pageindicatorview)
    public PageIndicatorView mPageIndicatorView;

    @BindView(R.id.onbording_trans_rl)
    public RelativeLayout mOnbordingTransRl;

    @BindView(R.id.bottom_sheet)
    public View mBottomSheet;

    int images[] = {R.mipmap.ic_launcher, R.mipmap.ic_launcher_round, R.mipmap.ic_launcher, R.mipmap.ic_launcher_round};
    String title[] = {"hello", "Hi", "Welcome", "FFFF"};


    private OnbordingAdapter mOnbordingAdapter;
    private BottomSheetBehavior<View> behavior;

    private String mType="";
    private Sharedpreferences mPrefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_on_bording);

        ButterKnife.bind(this);

        initializeViews();
    }

    private void initializeViews() {
        mPrefs = Sharedpreferences.getUserDataObj(this);
        mPageIndicatorView.setViewPager(mViewPager);
        mOnbordingAdapter = new OnbordingAdapter(this, images, title);
        mViewPager.setAdapter(mOnbordingAdapter);

        bottomsheetFun();
    }

    private void bottomsheetFun() {

        behavior = BottomSheetBehavior.from(mBottomSheet);
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_DRAGGING:
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_DRAGGING");
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_SETTLING");
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_EXPANDED");
                        break;
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_COLLAPSED");
                        break;
                    case BottomSheetBehavior.STATE_HIDDEN:
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_HIDDEN");
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                Log.i("BottomSheetCallback", "slideOffset: " + slideOffset);
            }
        });

    }

    @OnClick({R.id.onbording_main_get_started_bt,R.id.onbording_bottomsheet_candidate_btn,R.id.onbording_bottomsheet_recruiter_btn,R.id.onbording_main_login_tv})
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.onbording_main_get_started_bt:
                mType="get started";
                openBottomSheetFunc();
                break;
            case R.id.onbording_bottomsheet_candidate_btn:
                checkWhichTypeToGo(mType,"candidate");
                break;

            case R.id.onbording_bottomsheet_recruiter_btn:
                checkWhichTypeToGo(mType,"recruiter");
                break;
            case R.id.onbording_main_login_tv:
                mType="login";
                openBottomSheetFunc();
                break;

        }
    }

    private void checkWhichTypeToGo(String mLoginType, String type) {
        if ( type.equalsIgnoreCase("candidate")) {
            mPrefs.setRoleType(1);
        }else if (type.equalsIgnoreCase("recruiter")) {
            mPrefs.setRoleType(2);
        }

        if (mLoginType.equalsIgnoreCase("login")){
            Intent intent=new Intent(this,LinkedInLoginActivity.class);
            intent.putExtra("type",type);
            startActivity(intent);
            finish();
        }else if (mLoginType.equalsIgnoreCase("get started") && type.equalsIgnoreCase("candidate")){
            mPrefs.setRoleType(1);
            Intent intent=new Intent(this,CandidateFindJobsActivity.class);
            startActivity(intent);
            finish();
        }else if (mLoginType.equalsIgnoreCase("get started") && type.equalsIgnoreCase("recruiter")){
            mPrefs.setRoleType(2);
            Intent intent=new Intent(this,RecruiterDashboardActivity.class);
            startActivity(intent);
            finish();
        }

    }

    private void openBottomSheetFunc() {
        if (behavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
            behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            mOnbordingTransRl.setVisibility(View.VISIBLE);
        } else {
            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            mOnbordingTransRl.setVisibility(View.GONE);
        }
    }

}
