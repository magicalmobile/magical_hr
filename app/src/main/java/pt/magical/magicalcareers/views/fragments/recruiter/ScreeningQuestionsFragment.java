package pt.magical.magicalcareers.views.fragments.recruiter;


/**
 * Created by Sachin on 16 Aug 2017.
 */

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.getbase.floatingactionbutton.FloatingActionsMenu;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.views.activities.recruiter.RecruiterDashboardActivity;


public class ScreeningQuestionsFragment extends Fragment {


    public ScreeningQuestionsFragment() {
        // Required empty public constructor
    }

    //Fragment Manager
    private FragmentManager mFragmentManager;
    //Context Variable
    private Context mContext;
    //Parent Activity
    private RecruiterDashboardActivity activity;

    @Override
    public void onAttach(Context context) {
        this.mContext = context;
        super.onAttach(context);
    }

    @BindView(R.id.add_screening_qus_floating_action_menu)
    public FloatingActionsMenu mAddScreeninngQusFloatingActionsMenu;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_screening_questions, container, false);
        ButterKnife.bind(this, view);
        activity = (RecruiterDashboardActivity) getActivity();
        mFragmentManager = activity.getSupportFragmentManager();
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    /**
     * Setting Click Listeners to all the views of ScreeningQuestionsFragment.
     */
    @OnClick({R.id.add_screening_qus_create_qus_fab, R.id.add_screening_qus_add_trending_qus_fab})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_screening_qus_create_qus_fab:
                goToCreateQuestionFrag();
                break;
            case R.id.add_screening_qus_add_trending_qus_fab:
                goToAddTrendingQus();
                break;
        }
    }

    private void goToCreateQuestionFrag() {
        mFragmentManager.beginTransaction().
                setCustomAnimations(R.anim.bottom_to_top_enter, R.anim.bottom_to_top_exit, R.anim.top_to_bottom_enter, R.anim.top_to_bottom_exit).
                replace(R.id.recruiter_dashboard_home_act_frame_layout, new CreateQuestionFragment())
                .addToBackStack(null).commit();
    }

    private void goToAddTrendingQus() {
        mFragmentManager.beginTransaction().setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_up).
                replace(R.id.recruiter_dashboard_home_act_frame_layout, new TrendingQuestionsFragment())
                .addToBackStack(null).commit();
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            if (getView() != null) {
                getView().setFocusableInTouchMode(true);
                getView().requestFocus();
                getView().setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_BACK) {
                            mFragmentManager.popBackStack();
                        }
                        return true;
                    }
                });
            }
        } catch (Exception e) {
            Log.e("error", "" + e);
        }
    }

}
