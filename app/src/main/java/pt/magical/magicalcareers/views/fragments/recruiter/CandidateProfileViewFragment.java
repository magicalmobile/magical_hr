package pt.magical.magicalcareers.views.fragments.recruiter;

/**
 * Created by RAdhu on 22 Aug 2017.
 */

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.adapters.recruiter.RecruiterCompanyImagesViewAdapter;
import pt.magical.magicalcareers.adapters.recruiter.RecruiterCompanyVideosViewAdapter;
import pt.magical.magicalcareers.views.activities.recruiter.RecruiterDashboardActivity;
import pt.magical.magicalcareers.views.activities.recruiter.RecruiterProfileParentActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class CandidateProfileViewFragment extends Fragment {


    public CandidateProfileViewFragment() {
        // Required empty public constructor
    }

    //Fragment Manager
    private FragmentManager mFragmentManager;
    //Context Variable
    private Context mContext;
    //Parent Activity
    private RecruiterDashboardActivity activity;

    @Override
    public void onAttach(Context context) {
        this.mContext = context;
        super.onAttach(context);
    }

  /*  @BindView(R.id.recruiter_company_profile_videos_recycler_view)
    public RecyclerView mVideosRecyclerView;*/


    @Override
    public void onResume() {
        super.onResume();
        try {

            if (getView() != null) {
                getView().setFocusableInTouchMode(true);
                getView().requestFocus();
                getView().setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_BACK) {
                            getFragmentManager().beginTransaction().
                                    replace(R.id.recruiter_dashboard_home_act_frame_layout, new ChattingViewFragment())
                                    .commit();
                        }
                        return true;
                    }
                });
            }
        } catch (Exception e) {
            Log.e("error", "" + e);
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_candidate_view_profile, container, false);
        ButterKnife.bind(this, view);
        activity = (RecruiterDashboardActivity) getActivity();
        mFragmentManager = activity.getSupportFragmentManager();
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }


    /**
     * Setting Click Listeners to all the views of RecruiterCompanyProfileFragment.
     */
    @OnClick({R.id.fragment_candidate_view_profile_back_iv})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_candidate_view_profile_back_iv:
                getFragmentManager().beginTransaction().
                        replace(R.id.recruiter_dashboard_home_act_frame_layout, new ChattingViewFragment())
                        .commit();
                break;
        }
    }
}
