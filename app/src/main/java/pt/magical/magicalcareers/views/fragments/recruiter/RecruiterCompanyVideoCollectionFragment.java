package pt.magical.magicalcareers.views.fragments.recruiter;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.adapters.recruiter.RecruiterCompanyVideosViewAdapter;
import pt.magical.magicalcareers.views.activities.recruiter.RecruiterProfileParentActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class RecruiterCompanyVideoCollectionFragment extends Fragment {


    public RecruiterCompanyVideoCollectionFragment() {
        // Required empty public constructor
    }

    private RecruiterProfileParentActivity activity;
    //Fragment Manager
    private FragmentManager mFragmentManager;
    //Context Variable
    private Context mContext;


    @Override
    public void onAttach(Context context) {
        this.mContext = context;
        super.onAttach(context);
    }

    @BindView(R.id.recruiter_company_video_collections_recycler_view)
    public RecyclerView mVideoCollectionsRecyclerView;

    private RecruiterCompanyVideosViewAdapter mRecruiterCompanyVideosViewAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_recruiter_company_video_collections, container, false);
        ButterKnife.bind(this, view);
        activity = (RecruiterProfileParentActivity) getActivity();
        mFragmentManager = activity.getSupportFragmentManager();
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        settingUpVideosLayout();
    }

    private void settingUpVideosLayout() {
        mVideoCollectionsRecyclerView.setHasFixedSize(true);
        mVideoCollectionsRecyclerView.setLayoutManager(new StaggeredGridLayoutManager(3, 1));
        mRecruiterCompanyVideosViewAdapter = new RecruiterCompanyVideosViewAdapter(mContext);
        mVideoCollectionsRecyclerView.setAdapter(mRecruiterCompanyVideosViewAdapter);
        mVideoCollectionsRecyclerView.setNestedScrollingEnabled(false);

    }

    /**
     * Setting Click Listeners to all the views of RecruiterCompanyVideoCollectionsFragment.
     */
    @OnClick({R.id.recruiter_company_video_collections_back_iv})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.recruiter_company_video_collections_back_iv:
                mFragmentManager.popBackStack();
                break;
        }
    }

}
