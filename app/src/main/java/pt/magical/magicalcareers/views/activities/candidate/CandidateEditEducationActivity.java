package pt.magical.magicalcareers.views.activities.candidate;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.adapters.candidate.CandidateEditEducationRecyclerViewAdapter;

public class CandidateEditEducationActivity extends AppCompatActivity {

    @BindView(R.id.candidate_edit_education_recycler_view)
    public RecyclerView mEducationRecyclerView;


    private CandidateEditEducationRecyclerViewAdapter mCandidateEditEducationRecyclerViewAdapter;


    public ArrayList<String> addEducationList = new ArrayList<>();
    private int addEducationListSize = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_candidate_edit_education);

        ButterKnife.bind(this);

        initilizeView();

    }

    @OnClick({R.id.addTextView})
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.addTextView:
                addEducationListSize = addEducationListSize + 1;
                mCandidateEditEducationRecyclerViewAdapter = new CandidateEditEducationRecyclerViewAdapter(this, addEducationListSize);
                mEducationRecyclerView.setAdapter(mCandidateEditEducationRecyclerViewAdapter);
                Log.d("addEducationListSize", "is" + addEducationListSize);
                mCandidateEditEducationRecyclerViewAdapter.notifyDataSetChanged();
                break;

        }
    }


    private void initilizeView() {

        mEducationRecyclerView.setHasFixedSize(true);
        mEducationRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mCandidateEditEducationRecyclerViewAdapter = new CandidateEditEducationRecyclerViewAdapter(this, addEducationListSize);
        mEducationRecyclerView.setAdapter(mCandidateEditEducationRecyclerViewAdapter);

    }

}
