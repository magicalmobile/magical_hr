package pt.magical.magicalcareers.views.fragments.recruiter;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.interfaces.add_question.AddQuestionImpl;
import pt.magical.magicalcareers.interfaces.add_question.IAddQuestionPresenter;
import pt.magical.magicalcareers.interfaces.add_question.IAddQuestionView;
import pt.magical.magicalcareers.interfaces.review_jd.IReviewJDPresenter;
import pt.magical.magicalcareers.interfaces.review_jd.ReviewJDImpl;
import pt.magical.magicalcareers.models.recruiter.add_question.AddQuestionModel;
import pt.magical.magicalcareers.utils.LogToastUtility;
import pt.magical.magicalcareers.utils.Utils;
import pt.magical.magicalcareers.views.activities.recruiter.RecruiterDashboardActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class CreateQuestionFragment extends Fragment implements IAddQuestionView{


    private String question,answer;
    private int key_skill;

    public CreateQuestionFragment() {
        // Required empty public constructor
    }

    private IAddQuestionPresenter mIAddQuestionPresenter;

    //Fragment Manager
    private FragmentManager mFragmentManager;
    //Context Variable
    private Context mContext;
    //Parent Activity
    private RecruiterDashboardActivity activity;


    @BindView(R.id.fragment_create_question_create_bt)
    public Button mCreateBt;
    @BindView(R.id.fragment_create_question_relavent_skill)
    public TextView mSkillTv;
    @BindView(R.id.fragment_create_question_question_tv)
    public TextView mQuestionTv;
    @BindView(R.id.fragment_create_question_option1_tv)
    public TextView mOption1Tv;
    @BindView(R.id.fragment_create_question_option2_tv)
    public TextView mOption2Tv;
    @BindView(R.id.fragment_create_question_option3_tv)
    public TextView mOption3Tv;
    @BindView(R.id.fragment_create_question_option4_tv)
    public TextView mOption4Tv;
    @BindView(R.id.fragment_create_question_important_cb)
    public CheckBox mImportantCb;

    @Override
    public void onAttach(Context context) {
        this.mContext = context;
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_create_question, container, false);
        ButterKnife.bind(this, view);
        activity = (RecruiterDashboardActivity) getActivity();
        mFragmentManager = activity.getSupportFragmentManager();
        return view; // Inflate the layout for this fragment
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    /**
     * Setting Click Listeners to all the views of CreateQuestionFragment.
     */
    @OnClick({R.id.crate_qus_frag_close_iv,R.id.fragment_create_question_create_bt})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.crate_qus_frag_close_iv:
                Utils.closingThisFrag(mFragmentManager);
                break;
            case R.id.fragment_create_question_create_bt:
                if(isValidate())
                {
                    mIAddQuestionPresenter = new AddQuestionImpl(this);
                    key_skill=1;
                    int jd_id=23;
                    question=mQuestionTv.getText().toString();
                    answer="";
                    JSONArray arrayAnswer=new JSONArray();
                    try {
                        if(!mOption1Tv.getText().toString().equals(""))
                            arrayAnswer.put(new JSONObject().put("answer",mOption1Tv.getText().toString()).put("flag","True"));
                        if(!mOption2Tv.getText().toString().equals(""))
                            arrayAnswer.put(new JSONObject().put("answer",mOption2Tv.getText().toString()).put("flag","False"));
                        if(!mOption3Tv.getText().toString().equals(""))
                            arrayAnswer.put(new JSONObject().put("answer",mOption3Tv.getText().toString()).put("flag","False"));
                        if(!mOption4Tv.getText().toString().equals(""))
                            arrayAnswer.put(new JSONObject().put("answer",mOption4Tv.getText().toString()).put("flag","False"));
                    } catch (JSONException e) {
                            e.printStackTrace();
                    }
                    mIAddQuestionPresenter.addQuestion(jd_id,question,arrayAnswer.toString(),key_skill);
                }
                break;
        }
    }

    private boolean isValidate() {
        if(mSkillTv.getText().toString().equals("")) {
            Utils.showToast(mContext,"Please enter relevant skill.");
            return false;
        }else
        if(mQuestionTv.getText().toString().equals("")) {
            Utils.showToast(mContext,"Please enter question.");
            return false;
        }else
        if(mOption1Tv.getText().toString().equals("")||mOption2Tv.getText().toString().equals("")) {
            Utils.showToast(mContext,"Please enter options.");
            return false;
        }else

        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            if (getView() != null) {
                getView().setFocusableInTouchMode(true);
                getView().requestFocus();
                getView().setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_BACK) {
                            mFragmentManager.popBackStack();
                        }
                        return true;
                    }
                });
            }
        } catch (Exception e) {
            Log.e("error", "" + e);
        }
    }

    @Override
    public void getAddQuestionDataSuccess(int pId, AddQuestionModel mAddQuestionModel) {
        try {
            LogToastUtility.TOAST_L(mContext,mAddQuestionModel.getMeta().getMessage());
            Utils.stopProgress(mContext);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getAddQuestionDataError(int pId, String errorData) {
        try {
            LogToastUtility.TOAST_L(mContext,errorData);
            Utils.stopProgress(mContext);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
