package pt.magical.magicalcareers.views.custom_component;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.LayoutRes;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.interfaces.networkstate.OnDialogButtonClickListener;
import pt.magical.magicalcareers.views.activities.common.LoginActivity;

/**
 * Created by radhu on 8/4/2017.
 */

public class CustomListDialog {
    /**
     * Return an alert dialog
     *
     * @param title  message for the alert dialog
     * @param arrayItems  list item of alert dialog
     * @param listener listener to trigger selection methods
     */
    public static void openAlertDialog(Context context, String title, final String [] arrayItems,
                                final OnDialogButtonClickListener listener) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = LayoutInflater.from(context);
        View view=inflater.inflate(R.layout.custom_dialog_title, null);

        TextView tv=(TextView) view.findViewById(R.id.title);tv.setText(title);
        builder.setCustomTitle(view);
       // builder.setAdapter(new ArrayAdapter(context, android.R.layout.select_dialog_item, arrayItems),
        builder.setAdapter(new ArrayAdapter(context, R.layout.custom_dialog_item, arrayItems),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Utils.showToast(getBaseContext(),"which::"+which+arrayItems.get(which));
                        listener.onItemClick(arrayItems [which]);
                    }
                });
        builder.create();
        builder.show();
    }
}