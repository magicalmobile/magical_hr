package pt.magical.magicalcareers.views.fragments.common;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.utils.LogToastUtility;
import pt.magical.magicalcareers.views.activities.common.LinkedInLoginActivity;
import pt.magical.magicalcareers.views.activities.common.LoginActivity;
/**
 *  Created By Radhu Chaudhari 5aug17
 */

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginMainFragment extends Fragment {


    private FragmentManager mFragmentManager;
    @BindView(R.id.fragment_login_main_login_tv)
    public TextView mloginTv;
    @BindView(R.id.fragment_login_main_get_started_bt)
    public Button mGetStarted;
    private Context mContext;


    public LoginMainFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login_main, container, false);
        ButterKnife.bind(this, view);
        initializeView();
        return view;
    }

    private void initializeView() {
        mContext=getContext();
        mFragmentManager = getActivity().getSupportFragmentManager();

        LogToastUtility.LI("init","login frag");
    }


    @OnClick({R.id.fragment_login_main_get_started_bt,R.id.fragment_login_main_login_tv})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_login_main_get_started_bt:
                LogToastUtility.TOAST_L(mContext, "In progress");
                //mFragmentManager.popBackStack();
            case R.id.fragment_login_main_login_tv:
                Intent in=new Intent(getActivity(), LinkedInLoginActivity.class);
                startActivity(in);
                getActivity().overridePendingTransition(R.anim.slide_in_up, 0);
                getActivity().finish();
                //mFragmentManager.beginTransaction().replace(R.id.home_act_frame_layout, new LoginDashboardFragment()).addToBackStack(null).commit();
                break;
        }
    }

}