package pt.magical.magicalcareers.views.fragments.candidate;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.views.activities.candidate.CandidateHomeActivity;
import pt.magical.magicalcareers.views.activities.candidate.CandidateProfileActivity;
import pt.magical.magicalcareers.views.activities.common.UserChatActivity;

/**
 * Created by danish on 15-08-2017.
 */

public class CandidateDashboardFragment extends Fragment{

    @BindView(R.id.candidate_dash_job_rb)
    public RadioButton jobRadioButton;
    @BindView(R.id.candidate_dash_learn_rb)
    public RadioButton learnRadioButton;


    private Context context;
    private CandidateHomeActivity activity;
    private FragmentManager fragmentManager;
    public static boolean isLearnClick=false;
    private View v;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v=inflater.inflate(R.layout.fragment_candidate_dashboard,container,false);
        ButterKnife.bind(this,v);
        initializeView();

        return v;

    }

    private void initializeView() {
        activity=(CandidateHomeActivity)getActivity();
        fragmentManager=activity.getSupportFragmentManager();
        jobRadioButton.setChecked(true);
        learnRadioButton.setChecked(false);
        setSwipeCardAccordingRadioButton("Jobs");
    }

    private void setSwipeCardAccordingRadioButton(String title) {
        if (title.equalsIgnoreCase("Jobs")){
            fragmentManager.beginTransaction().replace(R.id.container_candidate_dashboard,new CandidateDashboardJobCardFragment()).commit();
        }else if (title.equalsIgnoreCase("Learn")){
            fragmentManager.beginTransaction().replace(R.id.container_candidate_dashboard,new CandidateDashboardLearnCardFragment()).commit();
        }
    }

    @OnClick({R.id.candidate_dash_job_rb,R.id.candidate_dash_learn_rb,R.id.candidate_dashboard_profile_iv,R.id.candidate_chat_toolbar})
    public void onClick(View v){
        switch (v.getId()){
            case R.id.candidate_dash_job_rb:
               checkRadioButtonClickFunc();
                break;
            case R.id.candidate_dash_learn_rb:
                checkRadioButtonClickFunc();
                break;
            case R.id.candidate_dashboard_profile_iv:
                Intent intent = new Intent(context, CandidateProfileActivity.class);
                startActivity(intent);
                break;
            case R.id.candidate_chat_toolbar:
                Intent i=new Intent(context, UserChatActivity.class);
                context.startActivity(i);
                break;

        }
    }

    private void checkRadioButtonClickFunc() {
        if (jobRadioButton.isChecked()){
            setSwipeCardAccordingRadioButton("Jobs");
        }else if (learnRadioButton.isChecked()){
            setSwipeCardAccordingRadioButton("Learn");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context=context;
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            if (isLearnClick){
                learnRadioButton.setChecked(true);
                jobRadioButton.setChecked(false);
                setSwipeCardAccordingRadioButton("Learn");
            }
            if (getView() != null) {
                getView().setFocusableInTouchMode(true);
                getView().requestFocus();
                getView().setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_BACK) {
                            activity.finish();
                        }
                        return true;
                    }
                });
            }
        } catch (Exception e) {
            Log.e("error", "" + e);
        }
    }

}
