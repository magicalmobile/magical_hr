package pt.magical.magicalcareers.views.fragments.recruiter;
/**
 * Created by RAdhu on 17 Aug 2017.
 */

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.view.menu.MenuPopupHelper;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.flexbox.FlexboxLayout;

import java.lang.reflect.Field;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fisk.chipcloud.ChipCloud;
import fisk.chipcloud.ChipCloudConfig;
import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.adapters.recruiter.ScreeningQuestionListRecyclerAdapter;
import pt.magical.magicalcareers.interfaces.screening_question_list.IScreeningQuestionListPresenter;
import pt.magical.magicalcareers.interfaces.screening_question_list.IScreeningQuestionListView;
import pt.magical.magicalcareers.interfaces.screening_question_list.ScreeningQuestionListImpl;
import pt.magical.magicalcareers.models.recruiter.screening_question_list.Answer;
import pt.magical.magicalcareers.models.recruiter.screening_question_list.JdQuestionList;
import pt.magical.magicalcareers.models.recruiter.screening_question_list.ScreeningQuestionListModel;
import pt.magical.magicalcareers.models.recruiter.trending_question_list.AnswerList;
import pt.magical.magicalcareers.models.recruiter.trending_question_list.Datum;
import pt.magical.magicalcareers.utils.LogToastUtility;
import pt.magical.magicalcareers.utils.Utils;
import pt.magical.magicalcareers.views.activities.recruiter.RecruiterDashboardActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class ScreeningQuestionListFragment extends Fragment implements IScreeningQuestionListView{

    IScreeningQuestionListPresenter mScreeningQuestionListPresenter;

    private ChipCloudConfig mChipCloudConfig;
    private ScreeningQuestionListRecyclerAdapter mScreeningQusRecyclerAdapter;
    private List<JdQuestionList> jdQuestionList;

    // Required empty public constructor
    public ScreeningQuestionListFragment() {
    }


    //Fragment Manager
    private FragmentManager mFragmentManager;
    //Context Variable
    private Context mContext;
    //Parent Activity
    private RecruiterDashboardActivity activity;

    /**
     * Variable Instances and Views OnBind (all fragment_create_jd_parent.xml views)
     */

    @BindView(R.id.fragment_screening_question_list_subtitle_tv)
    public TextView mScreeningQusSubTitleTV;
    @BindView(R.id.fragment_screening_question_list_screening_qus_rv)
    public RecyclerView mScreeningQusListRV;
    @Override
    public void onAttach(Context context) {
        this.mContext = context;
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_screening_question_list, container, false);
        ButterKnife.bind(this, view);
        activity = (RecruiterDashboardActivity) getActivity();
        mFragmentManager = activity.getSupportFragmentManager();
        initializeViews();
        return view;
    }

    private void initializeViews() {
        mScreeningQuestionListPresenter=new ScreeningQuestionListImpl(this);
        int jd_id=23;
        mScreeningQuestionListPresenter.getScreeningQuestionList(jd_id);
    }

    private void setScreeningQuestionRecyclerAdapter(List<JdQuestionList> jdQuestionList) {
        mScreeningQusListRV.setHasFixedSize(true);
        mScreeningQusRecyclerAdapter = new ScreeningQuestionListRecyclerAdapter(this,jdQuestionList);
        mScreeningQusListRV.setAdapter(mScreeningQusRecyclerAdapter);
        mScreeningQusListRV.setLayoutManager(new LinearLayoutManager(getActivity()));
        mScreeningQusRecyclerAdapter.notifyDataSetChanged();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            if (getView() != null) {
                getView().setFocusableInTouchMode(true);
                getView().requestFocus();
                getView().setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_BACK) {
                            mFragmentManager.popBackStack();
                        }
                        return true;
                    }
                });
            }
        } catch (Exception e) {
            Log.e("error", "" + e);
        }
    }
    /**
     * Setting Click Listeners to all the views of ScreeningQusList.
     */
    @OnClick({R.id.fragment_screening_question_list_back_iv})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_screening_question_list_back_iv:
                mFragmentManager.popBackStack();
                break;
            case R.id.fragment_screening_question_list_screening_qus_rv_next_btn:
                mFragmentManager.popBackStack();
                break;
        }
    }


    @Override
    public void getScreeningQuestionDataSuccess(int pId, ScreeningQuestionListModel mScreeningQuestionListModel) {
        Utils.stopProgress(mContext);
        JdQuestionList.deleteAll(JdQuestionList.class);
        Answer.deleteAll(Answer.class);
        for (JdQuestionList jdquestionList:mScreeningQuestionListModel.getData().getJdQuestionList()) {
            jdquestionList.save();
            for (Answer answer:jdquestionList.getAnswer()) {
                answer.setQuestionId(jdquestionList.getJdQuestionId());
                answer.save();
            }
        }
        jdQuestionList = JdQuestionList.findWithQuery(JdQuestionList.class, "SELECT * FROM jd_question_list");
        setAdapter();
    }

    private void setAdapter() {
        if(jdQuestionList.size()>0) {
            setScreeningQuestionRecyclerAdapter(jdQuestionList);
            mScreeningQusListRV.setVerticalScrollBarEnabled(false);
        }
        mScreeningQusSubTitleTV.setText(jdQuestionList.size()+" Questions");
    }

    @Override
    public void getScreeningQuestionDataError(int pId, String errorData) {
        try {
            Utils.stopProgress(mContext);
            LogToastUtility.TOAST_L(mContext,errorData);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
