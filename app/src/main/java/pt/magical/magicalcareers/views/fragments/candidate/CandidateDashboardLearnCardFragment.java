package pt.magical.magicalcareers.views.fragments.candidate;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.daprlabs.cardstack.SwipeDeck;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.adapters.candidate.CandidateDashboardLearnCardSwipeAdapter;
import pt.magical.magicalcareers.models.candidate.learn_test.LearnTestList;
import pt.magical.magicalcareers.views.activities.candidate.CandidateHomeActivity;

/**
 * Created by danish on 16-08-2017.
 */

public class CandidateDashboardLearnCardFragment extends Fragment implements CandidateDashboardLearnCardSwipeAdapter.learnOptionClick{

    private Context context;

    @BindView(R.id.candidate_dashboard_swap_card_learn)
    public SwipeDeck mSwipeCardView;

    private CandidateDashboardLearnCardSwipeAdapter cardAdapter;
    private String TAG="CandidateDashboardLearnFrag";
    private String[] data=new String[]{"which is best for design","what is java ?","c++ is oop language"};
    private String[] option1=new String[]{"Photoshop","programing language","yes"};
    private String[] option2=new String[]{"Indesign","core language","No"};
    private String[] option3=new String[]{"Illustrator","computer language","Partially"};
    private String[] option4=new String[]{"Coreldraw","all","no of these"};
    private CandidateHomeActivity activity;
    private  List<LearnTestList> list;
    private FragmentManager fragmentManager;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_candidate_dashboard_learn_card,container,false);
        ButterKnife.bind(this,v);
        initializeView();
        return v;
    }

    private void initializeView() {
        CandidateDashboardFragment.isLearnClick=true;
        setTestQuestionData();
        activity=(CandidateHomeActivity)getActivity();
        fragmentManager=activity.getSupportFragmentManager();
        setCardAdapter(-1,0);
        mSwipeCardView.setEventCallback(new SwipeDeck.SwipeEventCallback() {
            @Override
            public void cardSwipedLeft(int position) {
                Log.d("swipe left",""+position);
            }

            @Override
            public void cardSwipedRight(int position) {
                Log.d("swipe Right",""+position);
            }

            @Override
            public void cardsDepleted() {

            }

            @Override
            public void cardActionDown() {

            }

            @Override
            public void cardActionUp() {

            }
        });


    }

    private void setCardAdapter(int cardPos,int option) {
        if (cardPos==-1){
            cardAdapter= new CandidateDashboardLearnCardSwipeAdapter(context,this,list,cardPos,option,fragmentManager);
            mSwipeCardView.setAdapter(cardAdapter);
        }else {
            cardAdapter = new CandidateDashboardLearnCardSwipeAdapter(context, this, list, cardPos, option,fragmentManager);
            mSwipeCardView.setAdapter(cardAdapter);
            mSwipeCardView.setSelection(cardPos);
            cardAdapter.notifyDataSetChanged();
        }
    }

    private void setTestQuestionData() {
        list=new ArrayList<>();
        for (int i=0;i<data.length;i++) {
            LearnTestList learnTestList = new LearnTestList();
            learnTestList.setQuestion(data[i]);
            learnTestList.setOptionOne(option1[i]);
            learnTestList.setOptionTwo(option2[i]);
            learnTestList.setOptionThree(option3[i]);
            learnTestList.setOptionFour(option4[i]);
            list.add(learnTestList);
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context=context;
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            if (getView() != null) {
                getView().setFocusableInTouchMode(true);
                getView().requestFocus();
                getView().setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_BACK) {
                            activity.finish();
                        }
                        return true;
                    }
                });
            }
        } catch (Exception e) {
            Log.e("error", "" + e);
        }
    }


    @Override
    public void learnOptionClick(int Position, int option) {
        Log.d("pos",""+Position+"option clicked"+option);
        setCardAdapter(Position,option);
    }
}
