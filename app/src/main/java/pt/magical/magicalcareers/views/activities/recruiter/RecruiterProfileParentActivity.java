package pt.magical.magicalcareers.views.activities.recruiter;

/**
 * Created by Sachin on 17 Aug 2017.
 */

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.adapters.recruiter.RecruiterParentProfileAdapter;
import pt.magical.magicalcareers.interfaces.recruiter_master_data.RecruiterMasterDataPresenterImpl;
import pt.magical.magicalcareers.interfaces.recruiter_profile_data.IRecruiterProfileParentDataPresenter;
import pt.magical.magicalcareers.interfaces.recruiter_profile_data.IRecruiterProfileParentDataView;
import pt.magical.magicalcareers.interfaces.recruiter_profile_data.RecruiterProfileParentDataPresenterImpl;
import pt.magical.magicalcareers.models.recruiter_profile_data.RecruiterProfileParentDataModel;
import pt.magical.magicalcareers.utils.Utils;
import pt.magical.magicalcareers.views.activities.common.SettingsActivity;

public class RecruiterProfileParentActivity extends AppCompatActivity implements IRecruiterProfileParentDataView {


    private IRecruiterProfileParentDataPresenter mIRecruiterProfileParentDataPresenter;
    /**
     * All Views references of activity_recruiter_profile_parent.xml
     */
    @BindView(R.id.recruiter_main_profile_toolbar)
    public Toolbar mToolbar;
    @BindView(R.id.recruiter_parent_profile_viewpager)
    public ViewPager mViewPager;
    @BindView(R.id.recruiter_profile_back_iv)
    public ImageView mBackIV;
    @BindView(R.id.recruiter_parent_profile_tablayout)
    public TabLayout mTabLayout;
    @BindView(R.id.recruiter_own_profile_frame_layout)
    public FrameLayout mRecruiterOwnProfileFrameLayout;

    // Adapter to set in view pager of RecruiterParentProfile
    private RecruiterParentProfileAdapter mRecruiterParentProfileAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recruiter_profile_parent);
        ButterKnife.bind(this);

        initializeViews();
    }

    /**
     * setting up the components methods
     */
    private void initializeViews() {
        setSupportActionBar(mToolbar);
        mIRecruiterProfileParentDataPresenter = new RecruiterProfileParentDataPresenterImpl(this);

        mIRecruiterProfileParentDataPresenter.getRecruiterProfileParentDataApi();

    }


    /**
     * Setting up View Pager
     */
    private void settingUpViewPager() {
        mRecruiterParentProfileAdapter = new RecruiterParentProfileAdapter(getSupportFragmentManager(), mTabLayout.getTabCount());
        mViewPager.setAdapter(mRecruiterParentProfileAdapter);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));
    }


    /**
     * Setting up Tabs (Own & Company)
     */
    private void settingUpTabLayout() {

        mTabLayout.addTab(mTabLayout.newTab().setText("You"));
        mTabLayout.addTab(mTabLayout.newTab().setText("Company"));
        mTabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        mTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
                //  tab.getIcon().setColorFilter(Color.parseColor("#e15822"), PorterDuff.Mode.SRC_IN);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
                //    tab.getIcon().setColorFilter(Color.parseColor("#000000"), PorterDuff.Mode.SRC_IN);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }


    /**
     * Setting Click Listeners to all the views of WhatPositionFragment.
     */
    @OnClick({R.id.recruiter_profile_back_iv, R.id.recruiter_profile_settings_iv})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.recruiter_profile_back_iv:
                onBackPressed();
                break;
            case R.id.recruiter_profile_settings_iv:
                goToSettingsParentPage();
                break;
        }
    }

    private void goToSettingsParentPage() {
        Intent intent = new Intent(RecruiterProfileParentActivity.this, SettingsActivity.class);
        overridePendingTransition(R.anim.right_to_left_enter, R.anim.right_to_left_exit);
        startActivity(intent);
    }


    /**
     * Supporting callback onActivityResult in this activity's childrens fragments
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onGetRecruiterProfileParentDataApiSuccess(int pid, RecruiterProfileParentDataModel recruiterProfileParentDataModel) {
        Utils.stopProgress(this);
        Log.d("onGetRecruiterProfileParentDataApiSuccess", "data" + new Gson().toJson(recruiterProfileParentDataModel));
        settingUpTabLayout();
        settingUpViewPager();
    }

    @Override
    public void onGetRecruiterProfileParentDataApiError(int pid, String errorData) {
        Utils.stopProgress(this);
        Utils.showToast(this, errorData);
    }
}
