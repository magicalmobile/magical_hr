package pt.magical.magicalcareers.views.fragments.candidate;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daprlabs.cardstack.SwipeDeck;
import com.yarolegovich.discretescrollview.DiscreteScrollView;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.adapters.candidate.CandidateDashboardLearnCardSwipeAdapter;
import pt.magical.magicalcareers.adapters.candidate.CandidateLearnDetailsDiscreteScrollAdapter;
import pt.magical.magicalcareers.adapters.candidate.CandidateLearnSkillCompanyAdapter;
import pt.magical.magicalcareers.adapters.candidate.CandidateSkillsDialogAdapter;
import pt.magical.magicalcareers.adapters.recruiter.JdListDiscreteAdapter;
import pt.magical.magicalcareers.models.candidate.learn_test.LearnTestList;
import pt.magical.magicalcareers.views.activities.candidate.CandidateHomeActivity;

/**
 * Created by danish on 16-08-2017.
 */

public class CandidateLearnSkillCompanyDetailsFragment extends Fragment implements CandidateLearnSkillCompanyAdapter.learnOptionClick,CandidateLearnDetailsDiscreteScrollAdapter.JdClickListener,DiscreteScrollView.OnItemChangedListener{

    private Context context;

    @BindView(R.id.candidate_learn_swap_card)
    public SwipeDeck mSwipeCardView;
    @BindView(R.id.learn_details_title_tv)
    public TextView titleTV;
    @BindView(R.id.learn_details_company_tv)
    public TextView companyTV;
    @BindView(R.id.learn_details_discrete_scroll)
    public DiscreteScrollView mLearnDiscreteScroll;
    @BindView(R.id.learn_discrete_rl)
    public RelativeLayout discreteRL;


    private CandidateLearnSkillCompanyAdapter cardAdapter;
    private String TAG="CandidateDashboardLearnFrag";
    private String[] data=new String[]{"which is best for design","what is java ?","c++ is oop language"};
    private String[] option1=new String[]{"Photoshop","programing language","yes"};
    private String[] option2=new String[]{"Indesign","core language","No"};
    private String[] option3=new String[]{"Illustrator","computer language","Partially"};
    private String[] option4=new String[]{"Coreldraw","all","no of these"};
    private String[] skillList=new String[]{"RabbitMQ","Java","Hibernate","Java Applets","Json"};
    private CandidateHomeActivity activity;
    private  List<LearnTestList> list;
    private String mType;
    private FragmentManager fragmentManager;
    private ProgressDialog progressDialog;
    private List<String> mSkillList;
    private int selectPos=1;

    public static CandidateLearnSkillCompanyDetailsFragment newInstance(String type) {
        CandidateLearnSkillCompanyDetailsFragment fragment = new CandidateLearnSkillCompanyDetailsFragment();
        try {
            fragment.mType=type;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_learn_skill_company_details,container,false);
        ButterKnife.bind(this,v);
        initializeView();
        return v;
    }

    private void initializeView() {
        mSkillList=new ArrayList<String>();
        for (int i=0;i<skillList.length;i++){
            mSkillList.add(skillList[i]);
        }
        setTestQuestionData();
        activity=(CandidateHomeActivity)getActivity();
        fragmentManager=activity.getSupportFragmentManager();
       setCardSwipeAccordingToType(mType);
        mSwipeCardView.setEventCallback(new SwipeDeck.SwipeEventCallback() {
            @Override
            public void cardSwipedLeft(int position) {
                Log.d("swipe left",""+position);
            }

            @Override
            public void cardSwipedRight(int position) {
                Log.d("swipe Right",""+position);
            }

            @Override
            public void cardsDepleted() {

            }

            @Override
            public void cardActionDown() {

            }

            @Override
            public void cardActionUp() {

            }
        });


    }

    private void setCardSwipeAccordingToType(String type) {
        if (type.equalsIgnoreCase("skills")) {
            companyTV.setVisibility(View.GONE);
            titleTV.setText(skillList[selectPos]);
            titleTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            setCardAdapter(-1, 0,type);
        }else if (type.equalsIgnoreCase("company")){
            discreteRL.setVisibility(View.GONE);
            companyTV.setVisibility(View.VISIBLE);
            titleTV.setText("JD Name");
            titleTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_down_arrow, 0);
            setCardAdapter(-1, 0,type);
            setDiscreateAdapter();
        }
    }

    private void setCardAdapter(int cardPos,int option,String type) {
        if (cardPos==-1){
            cardAdapter= new CandidateLearnSkillCompanyAdapter(context,this,list,cardPos,option,type,skillList[selectPos]);
            mSwipeCardView.setAdapter(cardAdapter);
        }else {
            cardAdapter = new CandidateLearnSkillCompanyAdapter(context, this, list, cardPos, option,type,skillList[selectPos]);
            mSwipeCardView.setAdapter(cardAdapter);
            mSwipeCardView.setSelection(cardPos);
            cardAdapter.notifyDataSetChanged();
        }
    }

    private void setDiscreateAdapter() {
        mLearnDiscreteScroll.setAdapter(new CandidateLearnDetailsDiscreteScrollAdapter(context,this));
        mLearnDiscreteScroll.addOnItemChangedListener(this);
        //mLearnDiscreteScroll.addScrollStateChangeListener(this);
        mLearnDiscreteScroll.scrollToPosition(1);
        mLearnDiscreteScroll.setItemTransformer(new ScaleTransformer.Builder().setMinScale(0.8f).build());
    }

    private void setTestQuestionData() {
        list=new ArrayList<>();
        for (int i=0;i<data.length;i++) {
            LearnTestList learnTestList = new LearnTestList();
            learnTestList.setQuestion(data[i]);
            learnTestList.setOptionOne(option1[i]);
            learnTestList.setOptionTwo(option2[i]);
            learnTestList.setOptionThree(option3[i]);
            learnTestList.setOptionFour(option4[i]);
            list.add(learnTestList);
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context=context;
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            if (getView() != null) {
                getView().setFocusableInTouchMode(true);
                getView().requestFocus();
                getView().setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_BACK) {
                           fragmentManager.popBackStack();
                        }
                        return true;
                    }
                });
            }
        } catch (Exception e) {
            Log.e("error", "" + e);
        }
    }

    @OnClick({R.id.learn_details_close_iv,R.id.candidate_title_desc_ll})
    public void onClick(View v){
        switch (v.getId()){
            case R.id.learn_details_close_iv:
                fragmentManager.popBackStack();
                break;
            case R.id.candidate_title_desc_ll:
                if (mType.equalsIgnoreCase("company")){
                    if (discreteRL.getVisibility()==View.VISIBLE)
                    discreteRL.setVisibility(View.GONE);
                    else
                        discreteRL.setVisibility(View.VISIBLE);
                }
                break;
        }
    }


    @Override
    public void learnOptionClick(int Position, int option,String type) {
        Log.d("pos",""+Position+"option clicked"+option);
        if (type.equalsIgnoreCase(""))
        setCardAdapter(Position,option,type);
        else if (type.equalsIgnoreCase("company")){
            mType=type;
            setCardSwipeAccordingToType(type);
        }else if (type.equalsIgnoreCase("skills")){
           // showSkillsListDialog(mSkillList,selectPos);
            mType=type;
            setCardSwipeAccordingToType(type);
        }
    }

    @Override
    public void onJdClick(int position) {
        Log.d("pos",""+position);
    }

    @Override
    public void onCurrentItemChanged(@Nullable RecyclerView.ViewHolder viewHolder, int adapterPosition) {


    }

    /**
     * Set Show Skills dialog box.
     */
    /*public void showSkillsListDialog(List<String> skillList,int pos) {
        try {
            if (null != progressDialog && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            progressDialog = ProgressDialog.show(context, "", "", true);
            progressDialog.setCanceledOnTouchOutside(true);
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progressDialog.setContentView(R.layout.candidate_dialog_skills_list);
            RecyclerView recyclerView=(RecyclerView)progressDialog.findViewById(R.id.skill_dialog_rv);
            LinearLayoutManager horizontalLayoutManagaer
                    = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(horizontalLayoutManagaer);
            CandidateSkillsDialogAdapter skillListAdapter = new CandidateSkillsDialogAdapter(context,this,skillList);
            recyclerView.setAdapter(skillListAdapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

   /* @Override
    public void itemClick(int pos, String name) {
        selectPos=pos;
        setCardSwipeAccordingToType("skills");
        progressDialog.cancel();
    }*/
}
