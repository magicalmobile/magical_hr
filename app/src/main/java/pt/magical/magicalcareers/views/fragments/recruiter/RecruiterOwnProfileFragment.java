package pt.magical.magicalcareers.views.fragments.recruiter;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.views.activities.recruiter.RecruiterProfileParentActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class RecruiterOwnProfileFragment extends Fragment {


    public RecruiterOwnProfileFragment() {
        // Required empty public constructor
    }

    //Fragment Manager
    private FragmentManager mFragmentManager;
    //Context Variable
    private Context mContext;
    //Parent Activity
    private RecruiterProfileParentActivity activity;

    @Override
    public void onAttach(Context context) {
        this.mContext = context;
        super.onAttach(context);
    }


    @BindView(R.id.recruiter_own_profile_archive_jobs_ll)
    public LinearLayout mArchiveJobsLinerarLayout;
    @BindView(R.id.recruiter_own_profile_general_details_edit_iv)
    public ImageView mGeneralDetailsEditIV;
    @BindView(R.id.recruiter_own_profile_general_details_edit_profile_ll)
    public LinearLayout mGeneralDetailsEditProfileLL;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_recruiter_own_profile, container, false);
        ButterKnife.bind(this, view);
        activity = (RecruiterProfileParentActivity) getActivity();
        mFragmentManager = activity.getSupportFragmentManager();
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    /**
     * Setting Click Listeners to all the views of WhoAreUHiringForFragment.
     */
    @OnClick({R.id.recruiter_own_profile_archive_jobs_ll, R.id.recruiter_own_profile_general_details_edit_iv,
            R.id.recruiter_own_profile_contact_edit_iv, R.id.recruiter_own_profile_about_edit_iv, R.id.recruiter_own_profile_general_details_edit_profile_ll
    })
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.recruiter_own_profile_archive_jobs_ll:
                goToArchiveJobsListAct();
                break;
            case R.id.recruiter_own_profile_general_details_edit_iv:
                goToEditGeneralDetails();
                break;
            case R.id.recruiter_own_profile_contact_edit_iv:
                goToEditContactDetails();
                break;
            case R.id.recruiter_own_profile_about_edit_iv:
                goToEditAboutDetails();
                break;
            case R.id.recruiter_own_profile_general_details_edit_profile_ll:
                goToEditRecruiterPicture();
                break;
        }
    }

    private void goToEditGeneralDetails() {
        mFragmentManager.beginTransaction().
                setCustomAnimations(R.anim.bottom_to_top_enter, R.anim.bottom_to_top_exit, R.anim.top_to_bottom_enter, R.anim.top_to_bottom_exit).
                replace(R.id.recruiter_own_profile_frame_layout, new RecruiterOwnProfileBasicInfoFragment())
                .addToBackStack(null).commit();
    }

    private void goToEditContactDetails() {
        mFragmentManager.beginTransaction().
                setCustomAnimations(R.anim.bottom_to_top_enter, R.anim.bottom_to_top_exit, R.anim.top_to_bottom_enter, R.anim.top_to_bottom_exit).
                replace(R.id.recruiter_own_profile_frame_layout, new RecruiterOwnProfileContactInfoFragment())
                .addToBackStack(null).commit();
    }

    private void goToEditAboutDetails() {
        mFragmentManager.beginTransaction().
                setCustomAnimations(R.anim.bottom_to_top_enter, R.anim.bottom_to_top_exit, R.anim.top_to_bottom_enter, R.anim.top_to_bottom_exit).
                replace(R.id.recruiter_own_profile_frame_layout, new RecruiterOwnProfileAboutInfoFragment())
                .addToBackStack(null).commit();
    }

    private void goToArchiveJobsListAct() {
        mFragmentManager.beginTransaction().setCustomAnimations(R.anim.right_to_left_enter, R.anim.right_to_left_exit, R.anim.left_to_right_enter, R.anim.left_to_right_exit).
                replace(R.id.recruiter_own_profile_frame_layout, new ArchiveJdListFragment())
                .addToBackStack(null).commit();
    }


    private void goToEditRecruiterPicture() {


    }

}
