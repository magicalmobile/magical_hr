package pt.magical.magicalcareers.views.fragments.candidate;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.OnClick;
import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.views.activities.candidate.CandidateHomeActivity;

/**
 * Created by danish on 17-08-2017.
 */

public class CandidateViewJDFragment extends Fragment {

    private Context context;
    private CandidateHomeActivity activity;
    private FragmentManager fragmentManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_candidate_view_jd,container,false);
        ButterKnife.bind(this,view);
        initializeView();
        return view;
    }

    private void initializeView() {
        activity=(CandidateHomeActivity)getActivity();
        fragmentManager=activity.getSupportFragmentManager();
    }

    @OnClick({R.id.view_jd_close_iv})
    public void onClick(View v){
        switch (v.getId()){
            case R.id.view_jd_close_iv:
                fragmentManager.popBackStack();
                break;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context=context;
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            if (getView() != null) {
                getView().setFocusableInTouchMode(true);
                getView().requestFocus();
                getView().setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_BACK) {
                            fragmentManager.popBackStack();
                        }
                        return true;
                    }
                });
            }
        } catch (Exception e) {
            Log.e("error", "" + e);
        }
    }

}
