package pt.magical.magicalcareers.views.fragments.recruiter;
/**
 * Created by RAdhu on 19 Aug 2017.
 */

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.view.menu.MenuPopupHelper;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.lang.reflect.Field;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.adapters.recruiter.ChattingCandidateListRecyclerAdapter;
import pt.magical.magicalcareers.adapters.recruiter.TrendingQuestionFilterRecyclerAdapter;
import pt.magical.magicalcareers.utils.LogToastUtility;
import pt.magical.magicalcareers.views.activities.recruiter.RecruiterDashboardActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChattingCandidatesListFragment extends Fragment {


    private ChattingCandidateListRecyclerAdapter mChattingCandidateListRecyclerAdapter;

    public ChattingCandidatesListFragment() {
        // Required empty public constructor
    }

    //Fragment Manager
    private FragmentManager mFragmentManager;
    //Context Variable
    private Context mContext;
    //Parent Activity
    private RecruiterDashboardActivity activity;

    @BindView(R.id.fragment_chatting_candidate_list_rv)
    public RecyclerView mChattingCandidateListRV;

    @Override
    public void onAttach(Context context) {
        this.mContext = context;
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chatting_candidates_list, container, false);
        ButterKnife.bind(this, view);
        activity = (RecruiterDashboardActivity) getActivity();
        mFragmentManager = activity.getSupportFragmentManager();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        initializeView();

    }

    private void initializeView() {
        mChattingCandidateListRV.setHasFixedSize(true);
        mChattingCandidateListRecyclerAdapter = new ChattingCandidateListRecyclerAdapter(this);
        mChattingCandidateListRV.setAdapter(mChattingCandidateListRecyclerAdapter);
        mChattingCandidateListRV.setLayoutManager(new LinearLayoutManager(getActivity()));
        mChattingCandidateListRecyclerAdapter.notifyDataSetChanged();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }



}

