package pt.magical.magicalcareers.views.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.views.fragments.AutoSearchFragment;
import pt.magical.magicalcareers.views.fragments.HomeFragment;

public class HomeActivity extends AppCompatActivity {

    private FragmentManager mFragmentManager;

    @BindView(R.id.home_act_frame_layout)
    public FrameLayout mFrameLayout;
    private String selectedData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        initializeViews();
        if (savedInstanceState != null) {
            selectedData = savedInstanceState.getString("dataSelected");
        }

        Toast.makeText(HomeActivity.this, "" + selectedData, Toast.LENGTH_SHORT).show();
    }

    /**
     * Initialization of all Instances created up , Filter Setting
     */
    private void initializeViews() {
        mFragmentManager = getSupportFragmentManager();

        settingHome();
    }

    private void settingHome() {
        mFragmentManager.beginTransaction().replace(R.id.home_act_frame_layout, new HomeFragment())
                .addToBackStack(null).commit();

    }


}
