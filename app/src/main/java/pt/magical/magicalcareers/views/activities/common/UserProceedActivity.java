package pt.magical.magicalcareers.views.activities.common;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.google.gson.Gson;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.interfaces.custom_proceed.CustomProceedPresenterImpl;
import pt.magical.magicalcareers.interfaces.custom_proceed.ICustomProceedView;
import pt.magical.magicalcareers.models.candidate_profile.Data;
import pt.magical.magicalcareers.models.common.custom_proceed.CustomProceedModel;
import pt.magical.magicalcareers.utils.AppConstants;
import pt.magical.magicalcareers.utils.Sharedpreferences;
import pt.magical.magicalcareers.utils.Utils;

public class UserProceedActivity extends AppCompatActivity implements ICustomProceedView {


    @BindView(R.id.act_proceed_email_et)
    public EditText mEmail;
    private CustomProceedPresenterImpl mCustomProceedPresent;
    private String username;
    private Boolean availability;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_proceed);

        ButterKnife.bind(this);

        initializeView();

    }

    private void initializeView() {
    }


    @OnClick({R.id.act_proceed_button})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.act_proceed_button:
                usernameValidation();

                break;

        }
    }

    private void usernameValidation() {
        username=mEmail.getText().toString().trim();
        if (username!=null && !username.equalsIgnoreCase("")){
            if (android.util.Patterns.EMAIL_ADDRESS.matcher(username).matches())
                checkUserExistingFunc(username);
            else if (android.util.Patterns.PHONE.matcher(username).matches() && username.length()==10) {
                checkUserExistingFunc("91" + username);
            }else{
                mEmail.setError("Please enter valid username");
            }
        }else{
            mEmail.setError("Please enter username");
        }
    }

    private void checkUserExistingFunc(String username) {
        mCustomProceedPresent=new CustomProceedPresenterImpl(this);
        mCustomProceedPresent.checkExistingUserApi(username,true);
    }

    @Override
    public void checkExistingUserApiSuccess(int pid, CustomProceedModel customProceedModel) {
        try {
            if (customProceedModel.getMeta().getCode()== AppConstants.SUCCESS_CODE){
                Utils.stopProgress(this);
                availability=customProceedModel.getData().getAvailability();
                if (availability){
                    Intent i=new Intent(UserProceedActivity.this,LoginActivity.class);
                    i.putExtra("username",username);
                    startActivity(i);
                    finish();
                }else{
                    Intent i=new Intent(UserProceedActivity.this,RegisterActivity.class);
                    i.putExtra("username",username);
                    startActivity(i);
                    finish();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void checkExistingUserApiError(int pid, String error) {
        try {
            Utils.stopProgress(this);
            Utils.showToast(this,"Oops something went wrong! please try again");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
