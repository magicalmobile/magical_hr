package pt.magical.magicalcareers.views.fragments.recruiter;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.OnClick;
import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.views.activities.recruiter.RecruiterProfileParentActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class RecruiterCompanySeniorPersonInfoFragment extends Fragment {


    public RecruiterCompanySeniorPersonInfoFragment() {
        // Required empty public constructor
    }

    private RecruiterProfileParentActivity activity;
    //Fragment Manager
    private FragmentManager mFragmentManager;
    //Context Variable
    private Context mContext;


    @Override
    public void onAttach(Context context) {
        this.mContext = context;
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_recruiter_company_senior_person_info, container, false);
        ButterKnife.bind(this, view);
        activity = (RecruiterProfileParentActivity) getActivity();
        mFragmentManager = activity.getSupportFragmentManager();
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    /**
     * Setting Click Listeners to all the views of RecruiterCompanySeniorPersonInfoFragment.
     */
    @OnClick({R.id.recruiter_company_senior_person_info_back_iv})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.recruiter_company_senior_person_info_back_iv:
                mFragmentManager.popBackStack();
                break;
        }
    }
}
