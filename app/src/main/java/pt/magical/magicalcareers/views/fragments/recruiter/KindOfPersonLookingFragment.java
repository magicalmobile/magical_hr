package pt.magical.magicalcareers.views.fragments.recruiter;
/**
 * Created by Sachin on 16 Aug 2017.
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.interfaces.networkstate.OnDialogButtonClickListener;
import pt.magical.magicalcareers.utils.Utils;
import pt.magical.magicalcareers.views.activities.recruiter.RecruiterDashboardActivity;
import pt.magical.magicalcareers.views.custom_component.CustomListDialog;

public class KindOfPersonLookingFragment extends Fragment implements OnDialogButtonClickListener {


    public KindOfPersonLookingFragment() {
        // Required empty public constructor
    }

    //Fragment Manager
    private FragmentManager mFragmentManager;


    //Context Variable
    private Context mContext;
    //Parent Activity
    private RecruiterDashboardActivity activity;

    @Override
    public void onAttach(Context context) {
        this.mContext = context;
        super.onAttach(context);
    }

    @BindView(R.id.kind_of_person_looking_for_job_type_et)
    public EditText mJobTypeET;
    @BindView(R.id.kind_of_person_looking_for_highest_education_level_et)
    public EditText mHighestEducationLevelET;
    private ProgressDialog progressDialog;
    private ImageView fullTimeIv, partTimeIv, consultingIv, contractIv, internshipIv;
    private TextView fullTimeTv, partTimeTv, consultingTv, contractTv, internshipTv;
    private Button mselect_employment_type_button;

    private String selected_employment_type_value = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_kind_of_person_looking, container, false);
        ButterKnife.bind(this, view);
        activity = (RecruiterDashboardActivity) getActivity();
        mFragmentManager = activity.getSupportFragmentManager();
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    /**
     * Setting Click Listeners to all the views of WhoAreUHiringForFragment.
     */
    @OnClick({R.id.kind_of_person_frag_close_iv, R.id.kind_of_person_frag_next_button, R.id.kind_of_person_looking_for_job_type_et,
            R.id.kind_of_person_looking_for_highest_education_level_et})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.kind_of_person_frag_close_iv:
                Utils.closingThisFrag(mFragmentManager);
                break;
            case R.id.kind_of_person_frag_next_button:
                goToPositionInvolveFrag();
                break;
            case R.id.kind_of_person_looking_for_job_type_et:
                showCandidateEmploymentTypeDialog();
                break;
            case R.id.kind_of_person_looking_for_highest_education_level_et:
                String strArray[] = new String[]{"Atleast graduate", "Atleast postgraduate", "Atleast Doctorate"};
                String title = "Highest education level";
                CustomListDialog.openAlertDialog(mContext, title, strArray, this);
                break;
        }
    }

    private void goToPositionInvolveFrag() {
        mFragmentManager.beginTransaction().setCustomAnimations(R.anim.right_to_left_enter, R.anim.right_to_left_exit, R.anim.left_to_right_enter, R.anim.left_to_right_exit).
                replace(R.id.recruiter_dashboard_home_act_frame_layout, new PositionInvolveFragment())
                .addToBackStack(null).commit();
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            if (getView() != null) {
                getView().setFocusableInTouchMode(true);
                getView().requestFocus();
                getView().setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_BACK) {
                            mFragmentManager.popBackStack();
                        }
                        return true;
                    }
                });
            }
        } catch (Exception e) {
            Log.e("error", "" + e);
        }
    }


    private void showCandidateEmploymentTypeDialog() {

        try {
            if (null != progressDialog && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            progressDialog = ProgressDialog.show(mContext, "", "", true);
            progressDialog.setCanceledOnTouchOutside(true);
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progressDialog.setContentView(R.layout.dialog_act_candidate_employment_type);
            fullTimeTv = (TextView) progressDialog.findViewById(R.id.full_time_tv);
            partTimeTv = (TextView) progressDialog.findViewById(R.id.part_time_tv);
            consultingTv = (TextView) progressDialog.findViewById(R.id.consulting_tv);
            contractTv = (TextView) progressDialog.findViewById(R.id.contract_tv);
            internshipTv = (TextView) progressDialog.findViewById(R.id.internship_tv);


            fullTimeIv = (ImageView) progressDialog.findViewById(R.id.full_time_check_iv);
            partTimeIv = (ImageView) progressDialog.findViewById(R.id.part_time_iv);
            consultingIv = (ImageView) progressDialog.findViewById(R.id.consulting_iv);
            contractIv = (ImageView) progressDialog.findViewById(R.id.contract_iv);
            internshipIv = (ImageView) progressDialog.findViewById(R.id.internship_iv);

            mselect_employment_type_button = (Button) progressDialog.findViewById(R.id.select_employment_type_button);


            Log.d("Data", "value" + selected_employment_type_value);
            if (selected_employment_type_value.contains("Full Time")) {
                fullTimeIv.setVisibility(View.VISIBLE);
            }
            if (selected_employment_type_value.contains("Part Time")) {
                partTimeIv.setVisibility(View.VISIBLE);
            }
            if (selected_employment_type_value.contains("Consulting ")) {
                consultingIv.setVisibility(View.VISIBLE);
            }
            if (selected_employment_type_value.contains("Contract ")) {
                contractIv.setVisibility(View.VISIBLE);
            }
            if (selected_employment_type_value.contains("Internship")) {
                internshipIv.setVisibility(View.VISIBLE);
            }


            fullTimeTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (fullTimeIv.getVisibility() == View.VISIBLE) {
                        fullTimeIv.setVisibility(View.INVISIBLE);
                        if (selected_employment_type_value.contains("Full Time")) {
                            selected_employment_type_value.replace("Full Time", "");
                        }
                    } else {
                        fullTimeIv.setVisibility(View.VISIBLE);
                        if (!selected_employment_type_value.contains("Full Time")) {
                            selected_employment_type_value = " Full Time ";
                        }
                    }
                }
            });
            partTimeTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (partTimeIv.getVisibility() == View.VISIBLE) {
                        partTimeIv.setVisibility(View.INVISIBLE);
                        if (selected_employment_type_value.contains("Part Time")) {
                            selected_employment_type_value.replace("Part Time", "");
                        }
                    } else {
                        partTimeIv.setVisibility(View.VISIBLE);
                        if (!selected_employment_type_value.contains("Part Time")) {
                            selected_employment_type_value += " ,Part Time";
                        }
                    }
                }
            });
            consultingTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (consultingIv.getVisibility() == View.VISIBLE) {
                        consultingIv.setVisibility(View.INVISIBLE);
                        if (selected_employment_type_value.contains("Consulting ")) {
                            selected_employment_type_value.replace("Consulting ", "");
                        }
                    } else {
                        consultingIv.setVisibility(View.VISIBLE);
                        if (!selected_employment_type_value.contains("Consulting ")) {
                            selected_employment_type_value += " ,Consulting";
                        }
                    }
                }
            });
            contractTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (contractIv.getVisibility() == View.VISIBLE) {
                        contractIv.setVisibility(View.INVISIBLE);
                        if (!selected_employment_type_value.contains(" Contract")) {
                            selected_employment_type_value.replace("Contract ", "");
                        }
                    } else {
                        contractIv.setVisibility(View.VISIBLE);
                        if (!selected_employment_type_value.contains("Contract")) {
                            selected_employment_type_value += " ,Contract";
                        }
                    }
                }
            });
            internshipTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (internshipIv.getVisibility() == View.VISIBLE) {
                        internshipIv.setVisibility(View.INVISIBLE);
                        if (!selected_employment_type_value.contains(" Internship")) {
                            selected_employment_type_value.replace("Internship ", "");
                        }

                    } else {
                        internshipIv.setVisibility(View.VISIBLE);
                        if (!selected_employment_type_value.contains("Internship")) {
                            selected_employment_type_value += " ,Internship";
                        }
                    }
                }
            });

            mselect_employment_type_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (selected_employment_type_value.length() == 0) {
                        Utils.showToast(mContext, "Please select atleast a job type");
                    } else {
                        progressDialog.dismiss();
                        mJobTypeET.setText(selected_employment_type_value);
                    }
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onItemClick(String arrayItem) {
        if (arrayItem != null) {
            if (arrayItem.length() > 0) {
                mHighestEducationLevelET.setText(arrayItem);
            }
        }
    }
}
