package pt.magical.magicalcareers.views.activities.candidate;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;
import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.views.fragments.candidate.CandidateDashboardFragment;

/**
 * Created by danish on 15-08-2017.
 */

public class CandidateHomeActivity extends AppCompatActivity {

    private FragmentManager fragmentManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_candidate_home);
        ButterKnife.bind(this);
        initializeView();

    }

    private void initializeView() {
        fragmentManager=getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.container_candidate_home,new CandidateDashboardFragment()).commit();
    }
}
