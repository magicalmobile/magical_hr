package pt.magical.magicalcareers.interfaces.set_password;

import pt.magical.magicalcareers.models.forgot_password.ForgotPasswordModel;
import pt.magical.magicalcareers.models.set_password.SetPasswordModel;

/**
 * Created by Vivek on 26-08-2017.
 */

public interface ISetPasswordView {

    void setPasswordApiSuccess(int pid, SetPasswordModel setPasswordModel);

    void setPasswordApiError(int pid, String errorData);

}
