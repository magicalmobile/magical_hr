package pt.magical.magicalcareers.interfaces.add_question;

/**
 * Created by radhu on 25-08-2017.
 */

public interface IAddQuestionPresenter {
    void addQuestion(int jd_id, String question,String answer,int master_key_skill);
}
