package pt.magical.magicalcareers.interfaces.review_jd;

import java.util.Map;

/**
 * Created by radhu on 24-08-2017.
 */

public interface IReviewJDPresenter {
    void reviewJdData(int jd_id);
}
