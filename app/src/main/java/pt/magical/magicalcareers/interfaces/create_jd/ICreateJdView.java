package pt.magical.magicalcareers.interfaces.create_jd;

import pt.magical.magicalcareers.models.create_jd.CreateJdModel;

/**
 * Created by Sachin on 8/24/2017.
 */

public interface ICreateJdView {

    void onCreateJdApiSuccess(int pid, CreateJdModel createJdModel);

    void onCreateJdApiError(int pid, String errorData);
}
