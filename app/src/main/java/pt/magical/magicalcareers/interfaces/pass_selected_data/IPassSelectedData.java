package pt.magical.magicalcareers.interfaces.pass_selected_data;

/**
 * Created by Sachin on 8/5/2017.
 */

public interface IPassSelectedData {
     void passDataToFragment(String selectedData);
}
