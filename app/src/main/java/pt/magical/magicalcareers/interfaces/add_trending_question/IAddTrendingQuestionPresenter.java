package pt.magical.magicalcareers.interfaces.add_trending_question;

/**
 * Created by radhu on 24-08-2017.
 */

public interface IAddTrendingQuestionPresenter {
    void addTrendingQuestion(int jd_id,int master_screen_ques_id);
}
