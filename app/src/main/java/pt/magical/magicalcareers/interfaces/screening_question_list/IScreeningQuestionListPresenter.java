package pt.magical.magicalcareers.interfaces.screening_question_list;

/**
 * Created by radhu on 26-08-2017.
 */

public interface IScreeningQuestionListPresenter {
    void getScreeningQuestionList(int jd_id);
}
