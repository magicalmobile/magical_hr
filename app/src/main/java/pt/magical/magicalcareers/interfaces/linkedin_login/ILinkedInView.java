package pt.magical.magicalcareers.interfaces.linkedin_login;

/**
 * Created by danish on 26-08-2017.
 */

public interface ILinkedInView {
    void linkedInLoginApiCallSuccess(int pid,String responseString);
    void linkedInLoginApiCallError(int pid,String error);
}
