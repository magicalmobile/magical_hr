package pt.magical.magicalcareers.interfaces.review_jd;

import pt.magical.magicalcareers.models.recruiter.review_jd.ReviewJDModel;

/**
 * Created by radhu on 24-08-2017.
 */

public interface IReviewJDView {
    void getReviewJDDataSuccess(int pId, ReviewJDModel mReviewJDModel);
    void getReviewJDDataError(int pId, String errorData);
 }
