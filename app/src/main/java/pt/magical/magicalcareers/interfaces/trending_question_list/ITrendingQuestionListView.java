package pt.magical.magicalcareers.interfaces.trending_question_list;

import pt.magical.magicalcareers.models.recruiter.add_trending_question.AddTrendingQuestionModel;
import pt.magical.magicalcareers.models.recruiter.trending_question_list.TrendingQuestionListModel;

/**
 * Created by radhu on 26-08-2017.
 */

public interface ITrendingQuestionListView {
    void getTrendingQuestionDataSuccess(int pId, TrendingQuestionListModel mTrendingQuestionListModel);
    void getTrendingQuestionDataError(int pId, String errorData);
 }
