package pt.magical.magicalcareers.interfaces.add_trending_question;

import pt.magical.magicalcareers.models.recruiter.add_trending_question.AddTrendingQuestionModel;
import pt.magical.magicalcareers.models.recruiter.review_jd.ReviewJDModel;

/**
 * Created by radhu on 25-08-2017.
 */

public interface IAddTrendingQuestionView {
    void getAddTrendingQuestionDataSuccess(int pId, AddTrendingQuestionModel mAddTrendingQuestionModel);
    void getAddTrendingQuestionDataError(int pId, String errorData);
 }
