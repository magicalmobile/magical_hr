package pt.magical.magicalcareers.interfaces.add_question;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import pt.magical.magicalcareers.async.AsyncInteractor;
import pt.magical.magicalcareers.async.OnRequestListener;
import pt.magical.magicalcareers.models.recruiter.add_question.AddQuestionModel;
import pt.magical.magicalcareers.models.recruiter.add_question.error.AddQuestionErrorModel;
import pt.magical.magicalcareers.models.recruiter.add_trending_question.AddTrendingQuestionModel;
import pt.magical.magicalcareers.models.recruiter.add_trending_question.error.AddTrendingQuestionErrorModel;
import pt.magical.magicalcareers.utils.AppConstants;
import pt.magical.magicalcareers.utils.NetworkStatus;
import pt.magical.magicalcareers.utils.Utils;
import pt.magical.magicalcareers.views.fragments.recruiter.CreateQuestionFragment;
import pt.magical.magicalcareers.views.fragments.recruiter.TrendingQuestionsFragment;

/**
 * Created by radhu on 25-08-2017.
 */

public class AddQuestionImpl implements IAddQuestionPresenter, OnRequestListener {

    private final AsyncInteractor mAsyncInteractor;
    private final IAddQuestionView mIAddQuestionView;
  //  private final Context mContext;
    private AddQuestionModel mAddQuestionModel;
    private CreateQuestionFragment mCreateQuestionFragment;
    private AddQuestionErrorModel mAddQuestionErrorModel;

    public AddQuestionImpl(IAddQuestionView iAddQuestionView) {
        this.mIAddQuestionView = iAddQuestionView;
        this.mCreateQuestionFragment = (CreateQuestionFragment) iAddQuestionView;
        //this.mContext=mViewJdParentFragment.getActivity();
        mAsyncInteractor = new AsyncInteractor();
    }

    @Override
    public void onRequestCompletion(int pid, JSONObject responseJson, JSONArray responseArray) {

    }

    @Override
    public void onRequestCompletion(final int pid, final String responseJson) throws JSONException {
        if (responseJson != null) {
            if (pid == AppConstants.TAG_ID_RECRUITER_ADD_QUESTION) {
                try {
                    Gson gson = new Gson();
                    mAddQuestionModel = gson.fromJson(responseJson, AddQuestionModel.class);
                    mIAddQuestionView.getAddQuestionDataSuccess(pid, mAddQuestionModel);
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onRequestCompletionError(int pid, String error) {
        if (pid == AppConstants.TAG_ID_RECRUITER_ADD_QUESTION) {
            Gson gson = new Gson();
            mAddQuestionErrorModel = gson.fromJson(error, AddQuestionErrorModel.class);
            mIAddQuestionView.getAddQuestionDataError(pid, mAddQuestionErrorModel.getData().getErrorMessage().get(0).toString());
        }
    }

    @Override
    public void onRequestCompletionHomeError(int pid, String error) {
    }

    @Override
    public void addQuestion(int jd_id, String question, String answer, int master_key_skill) {
        if (NetworkStatus.checkNetworkStatus(mCreateQuestionFragment.getActivity())) {
            Utils.showProgress(mCreateQuestionFragment.getActivity());
            Map<String, String> params = new HashMap<String, String>();
            params.put("jd_id", String.valueOf(jd_id));
            params.put("question", String.valueOf(question));
            params.put("answer", String.valueOf(answer));
            params.put("master_key_skill", String.valueOf(master_key_skill));
            mAsyncInteractor.validateCredentialsAsync(Utils.methodPost,this, AppConstants.TAG_ID_RECRUITER_ADD_QUESTION,
                    AppConstants.URL.RECRUITER_ADD_QUESTION.getUrl(), params);
        } else {
            Utils.showToast(mCreateQuestionFragment.getActivity(), "Please connect to internet");
        }
    }
}
