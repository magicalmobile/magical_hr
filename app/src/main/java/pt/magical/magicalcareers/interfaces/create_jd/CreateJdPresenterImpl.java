package pt.magical.magicalcareers.interfaces.create_jd;

import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import pt.magical.magicalcareers.async.AsyncInteractor;
import pt.magical.magicalcareers.async.OnRequestListener;
import pt.magical.magicalcareers.interfaces.recruiter_profile_data.IRecruiterProfileParentDataPresenter;
import pt.magical.magicalcareers.interfaces.recruiter_profile_data.IRecruiterProfileParentDataView;
import pt.magical.magicalcareers.models.create_jd.CreateJdModel;
import pt.magical.magicalcareers.models.recruiter_profile_data.RecruiterProfileParentDataModel;
import pt.magical.magicalcareers.utils.AppConstants;
import pt.magical.magicalcareers.utils.NetworkStatus;
import pt.magical.magicalcareers.utils.Utils;
import pt.magical.magicalcareers.views.activities.recruiter.RecruiterDashboardActivity;
import pt.magical.magicalcareers.views.activities.recruiter.RecruiterProfileParentActivity;
import pt.magical.magicalcareers.views.fragments.recruiter.PositionInvolveFragment;

/**
 * Created by Sachin on 8/24/2017.
 */

public class CreateJdPresenterImpl implements ICreateJdPresenter, OnRequestListener {

    private ICreateJdView mICreateJdView;
    //    private RecruiterDashboardActivity mRecruiterDashboardActivity;
    private PositionInvolveFragment mPositionInvolveFragment;
    private AsyncInteractor mAsyncInteractor;
    private CreateJdModel mCreateJdModel;

    public CreateJdPresenterImpl(ICreateJdView mICreateJdView) {
        this.mICreateJdView = mICreateJdView;
        this.mPositionInvolveFragment = (PositionInvolveFragment) mICreateJdView;
        mAsyncInteractor = new AsyncInteractor();
    }

    @Override
    public void createJdApi(String master_designation, String experience_from, String experience_to, String location, String latitude,
                            String longitude, String ctc_from, String ctc_to, String benefits,
                            String institute_type, String highest_education_level, String notice_period, String job_type,
                            String job_description, Map<String, String> job_description_file) {

        if (NetworkStatus.checkNetworkStatus(mPositionInvolveFragment.getActivity())) {
            Utils.showProgress(mPositionInvolveFragment.getActivity());


            Map<String, String> params = new HashMap<String, String>();
            params.put("master_designation", master_designation);
            params.put("experience_from", experience_from);
            params.put("experience_to", experience_to);
            params.put("location", location);
            params.put("latitude", latitude);
            params.put("longitude", longitude);
            params.put("ctc_from", ctc_from);
            params.put("ctc_to", ctc_to);
            params.put("benefits", benefits);
            params.put("institute_type", institute_type);
            params.put("highest_education_level", highest_education_level);
            params.put("notice_period", notice_period);
            params.put("job_type", job_type);
            params.put("job_description", job_description);
            Log.d("params", "" + params.toString());

            mAsyncInteractor.validateCredentialsMultipartAsync("multipart", this, AppConstants.TAG_ID_CREATE_JD,
                    AppConstants.URL.CREATE_JD.getUrl(), params, job_description_file);

        } else {
            Utils.showToast(mPositionInvolveFragment.getActivity(), "Please connect to internet");
        }
    }

    @Override
    public void onRequestCompletion(int pid, JSONObject responseJson, JSONArray responseArray) {

    }

    @Override
    public void onRequestCompletion(int pid, String responseJson) throws JSONException {
        try {
            if (pid == AppConstants.TAG_ID_CREATE_JD) {
                if (responseJson != null) {
                    Gson gson = new Gson();
                    mCreateJdModel = gson.fromJson(responseJson, CreateJdModel.class);
                    mICreateJdView.onCreateJdApiSuccess(pid, mCreateJdModel);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestCompletionError(int pid, String error) {
        try {
            if (pid == AppConstants.TAG_ID_CREATE_JD) {
                mICreateJdView.onCreateJdApiError(pid, error);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestCompletionHomeError(int pid, String error) {
    }


}




