package pt.magical.magicalcareers.interfaces.recruiter_master_data;

/**
 * Created by Sachin on 8/24/2017.
 */

public interface IRecruiterMasterDataPresenter {

    void getRecruiterMasterdata();

}
