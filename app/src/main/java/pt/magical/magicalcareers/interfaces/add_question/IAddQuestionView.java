package pt.magical.magicalcareers.interfaces.add_question;

import pt.magical.magicalcareers.models.recruiter.add_question.AddQuestionModel;
import pt.magical.magicalcareers.models.recruiter.add_trending_question.AddTrendingQuestionModel;

/**
 * Created by radhu on 25-08-2017.
 */

public interface IAddQuestionView {
    void getAddQuestionDataSuccess(int pId, AddQuestionModel mAddQuestionModel);
    void getAddQuestionDataError(int pId, String errorData);
 }
