package pt.magical.magicalcareers.interfaces.trending_question_list;

/**
 * Created by radhu on 26-08-2017.
 */

public interface ITrendingQuestionListPresenter {
    void getTrendingQuestionList(int master_key_skill);
}
