package pt.magical.magicalcareers.interfaces.custom_proceed;

import pt.magical.magicalcareers.models.common.custom_proceed.CustomProceedModel;

/**
 * Created by danish on 25-08-2017.
 */

public interface ICustomProceedView {

    void checkExistingUserApiSuccess(int pid, CustomProceedModel customProceedModel);
    void checkExistingUserApiError(int pid,String error);
}
