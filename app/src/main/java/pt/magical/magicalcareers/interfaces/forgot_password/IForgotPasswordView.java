package pt.magical.magicalcareers.interfaces.forgot_password;

import pt.magical.magicalcareers.models.candidate_profile.CandidateProfileModel;
import pt.magical.magicalcareers.models.forgot_password.ForgotPasswordModel;

/**
 * Created by Vivek on 25-08-2017.
 */

public interface IForgotPasswordView {

    void forgotPasswordApiSuccess(int pid, ForgotPasswordModel forgotPasswordModel);
    void forgotPasswordApiError(int pid, String errorData);


}
