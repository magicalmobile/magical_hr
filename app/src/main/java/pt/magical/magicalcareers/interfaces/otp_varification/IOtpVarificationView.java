package pt.magical.magicalcareers.interfaces.otp_varification;

import pt.magical.magicalcareers.models.otp_error.ForgotPasswordConfirmationErrorModel;
import pt.magical.magicalcareers.models.resend_otp.ResendOtpModel;


/**
 * Created by Vivek on 25-08-2017.
 */

public interface IOtpVarificationView {

    void resendOtpSuccess(int pid, ResendOtpModel resendOtpModel);
    void resendOtpError(int pid, String errorData);

    void otpVerificationApiCallSuccess(int pid, String successResponse);
    void otpVerificationApiCallError(int pid, ForgotPasswordConfirmationErrorModel errorModel);

}
