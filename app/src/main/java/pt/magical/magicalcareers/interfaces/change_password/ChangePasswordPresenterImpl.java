package pt.magical.magicalcareers.interfaces.change_password;

import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import pt.magical.magicalcareers.async.AsyncInteractor;
import pt.magical.magicalcareers.async.OnRequestListener;
import pt.magical.magicalcareers.interfaces.add_company.IAddCompanyPresenter;
import pt.magical.magicalcareers.interfaces.add_company.IAddCompanyView;
import pt.magical.magicalcareers.models.add_company.AddCompanyModel;
import pt.magical.magicalcareers.models.add_company.error.AddCompanyErrorModel;
import pt.magical.magicalcareers.models.change_password.ChangePasswordModel;
import pt.magical.magicalcareers.models.change_password.error.ChangePasswordErrorModel;
import pt.magical.magicalcareers.utils.AppConstants;
import pt.magical.magicalcareers.utils.NetworkStatus;
import pt.magical.magicalcareers.utils.Utils;
import pt.magical.magicalcareers.views.fragments.recruiter.ChangePasswordFragment;
import pt.magical.magicalcareers.views.fragments.recruiter.WhoAreUHiringForFragment;

/**
 * Created by Sachin on 8/26/2017.
 */

public class ChangePasswordPresenterImpl implements IChangePasswordPresenter, OnRequestListener {

    private IChangePasswordView mIChangePasswordView;
    private ChangePasswordFragment mChangePasswordFragment;
    private AsyncInteractor mAsyncInteractor;
    private ChangePasswordModel mChangePasswordModel;
    private ChangePasswordErrorModel mChangePasswordErrorModel;


    public ChangePasswordPresenterImpl(IChangePasswordView mIChangePasswordView) {
        this.mIChangePasswordView = mIChangePasswordView;
        this.mChangePasswordFragment = (ChangePasswordFragment) mIChangePasswordView;
        mAsyncInteractor = new AsyncInteractor();
    }

    @Override
    public void changePassword(String current_password, String new_password) {
        if (NetworkStatus.checkNetworkStatus(mChangePasswordFragment.getActivity())) {
            Utils.showProgress(mChangePasswordFragment.getActivity());
            Map<String, String> params = new HashMap<String, String>();
            params.put("current_password", current_password);
            params.put("new_password", new_password);

            Log.d("params", "" + params.toString());

            mAsyncInteractor.validateCredentialsAsync(Utils.methodPost, this, AppConstants.TAG_ID_CHANGE_PASSWORD,
                    AppConstants.URL.CHANGE_PASSWORD.getUrl(), params);
        } else {
            Utils.showToast(mChangePasswordFragment.getActivity(), "Please connect to internet");
        }
    }

    @Override
    public void onRequestCompletion(int pid, JSONObject responseJson, JSONArray responseArray) {

    }

    @Override
    public void onRequestCompletion(int pid, String responseJson) throws JSONException {
        try {
            if (pid == AppConstants.TAG_ID_CHANGE_PASSWORD) {
                if (responseJson != null) {
                    Gson gson = new Gson();
                    mChangePasswordModel = gson.fromJson(responseJson, ChangePasswordModel.class);
                    mIChangePasswordView.changePasswordSuccess(pid, mChangePasswordModel);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestCompletionError(int pid, String error) {
        try {
            if (pid == AppConstants.TAG_ID_CHANGE_PASSWORD) {
                Gson gson = new Gson();
                mChangePasswordErrorModel = gson.fromJson(error, ChangePasswordErrorModel.class);
                mIChangePasswordView.changePasswordError(pid, mChangePasswordErrorModel);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestCompletionHomeError(int pid, String error) {
    }


}






