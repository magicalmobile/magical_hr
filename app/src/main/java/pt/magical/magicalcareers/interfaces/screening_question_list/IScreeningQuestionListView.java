package pt.magical.magicalcareers.interfaces.screening_question_list;

import pt.magical.magicalcareers.models.recruiter.screening_question_list.ScreeningQuestionListModel;

/**
 * Created by radhu on 26-08-2017.
 */

public interface IScreeningQuestionListView {
    void getScreeningQuestionDataSuccess(int pId, ScreeningQuestionListModel mScreeningQuestionListModel);
    void getScreeningQuestionDataError(int pId, String errorData);
 }
