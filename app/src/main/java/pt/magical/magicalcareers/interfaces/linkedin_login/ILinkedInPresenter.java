package pt.magical.magicalcareers.interfaces.linkedin_login;

/**
 * Created by danish on 26-08-2017.
 */

public interface ILinkedInPresenter {
    void linkedInLoginApiCall(String linked_in_id, String headline,String first_name,String last_name,String device_id,String device_type,String device_version,String role);
}
