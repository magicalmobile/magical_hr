package pt.magical.magicalcareers.interfaces.custom_login;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import pt.magical.magicalcareers.async.AsyncInteractor;
import pt.magical.magicalcareers.async.OnRequestListener;
import pt.magical.magicalcareers.utils.AppConstants;
import pt.magical.magicalcareers.utils.NetworkStatus;
import pt.magical.magicalcareers.utils.Utils;
import pt.magical.magicalcareers.views.activities.common.LoginActivity;

/**
 * Created by danish on 26-08-2017.
 */

public class CustomLoginPresenterImpl implements ICustomLoginPresenter ,OnRequestListener{

    private final LoginActivity activity;
    private final ICustomLoginView customLoginView;
    private final AsyncInteractor mAsyncInteractor;

    public CustomLoginPresenterImpl(ICustomLoginView customLoginView) {
        this.activity=(LoginActivity)customLoginView;
        this.customLoginView=customLoginView;
        mAsyncInteractor = new AsyncInteractor();
    }

    @Override
    public void customLoginApiCall(String username, String password, boolean show) {
        if (NetworkStatus.checkNetworkStatus(activity)) {
            Utils.showProgress(activity);
            Map<String, String> params = new HashMap<String, String>();
            params.put("username", username);
            params.put("password", password);
            Log.d("params", params.toString());

            mAsyncInteractor.validateCredentialsAsync(Utils.methodPost, this, AppConstants.TAG_ID_CUSTOM_LOGIN,
                    AppConstants.URL.CUSTOM_LOGIN.getUrl(), params);
        } else {
            Utils.showToast(activity, "Please connect to internet");
        }
    }

    @Override
    public void onRequestCompletion(int pid, JSONObject responseJson, JSONArray responseArray) {

    }

    @Override
    public void onRequestCompletion(int pid, String responseJson) throws JSONException {
        try {
            if (pid == AppConstants.TAG_ID_CUSTOM_LOGIN) {
                if (responseJson != null) {
                    customLoginView.customLoginApiCallSuccess(pid,responseJson);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestCompletionError(int pid, String error) {
        try {
            if (pid == AppConstants.TAG_ID_CUSTOM_LOGIN) {
                if (error != null) {
                    customLoginView.customLoginApiError(pid,error);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestCompletionHomeError(int pid, String error) {

    }


}
