package pt.magical.magicalcareers.interfaces.set_password;

import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import pt.magical.magicalcareers.async.AsyncInteractor;
import pt.magical.magicalcareers.async.OnRequestListener;
import pt.magical.magicalcareers.models.set_password.SetPasswordModel;
import pt.magical.magicalcareers.utils.AppConstants;
import pt.magical.magicalcareers.utils.NetworkStatus;
import pt.magical.magicalcareers.utils.Utils;
import pt.magical.magicalcareers.views.activities.common.SetPasswordActivity;

/**
 * Created by Vivek on 26-08-2017.
 */

public class SetPasswordModelImpl implements ISetPasswordPresenter, OnRequestListener {

    private ISetPasswordView mISetPasswordView;
    private SetPasswordActivity mSetPasswordActivity;
    private AsyncInteractor mAsyncInteractor;

    private SetPasswordModel mSetPasswordModel;

    public SetPasswordModelImpl(ISetPasswordView mISetPasswordView) {
        this.mISetPasswordView = mISetPasswordView;
        this.mSetPasswordActivity = (SetPasswordActivity) mISetPasswordView;
        mAsyncInteractor = new AsyncInteractor();
    }



    @Override
    public void setPasswordApi(String password) {

        if (NetworkStatus.checkNetworkStatus(mSetPasswordActivity)) {
            Utils.showProgress(mSetPasswordActivity);
            Map<String, String> params = new HashMap<String, String>();
            params.put("password", password);
            Log.d("params", params.toString());

            mAsyncInteractor.validateCredentialsAsync(Utils.methodPost, this, AppConstants.TAG_ID_SET_PASSWORD_ACTIVITY,
                    AppConstants.URL.SET_PASSWORD.getUrl(), params);
        } else {
            Utils.showToast(mSetPasswordActivity, "Please connect to internet");
        }

    }

    @Override
    public void onRequestCompletion(int pid, JSONObject responseJson, JSONArray responseArray) {



    }

    @Override
    public void onRequestCompletion(int pid, String responseJson) throws JSONException {

        if (pid == AppConstants.TAG_ID_SET_PASSWORD_ACTIVITY) {
            if (responseJson != null) {
                Gson gson = new Gson();
                mSetPasswordModel = gson.fromJson(responseJson, SetPasswordModel.class);
                mISetPasswordView.setPasswordApiSuccess(pid, mSetPasswordModel);
            }
        }

    }

    @Override
    public void onRequestCompletionError(int pid, String error) {

        if (pid == AppConstants.TAG_ID_SET_PASSWORD_ACTIVITY) {
            mISetPasswordView.setPasswordApiError(pid, error);
        }

    }

    @Override
    public void onRequestCompletionHomeError(int pid, String error) {

    }
}
