package pt.magical.magicalcareers.interfaces.change_password;

import pt.magical.magicalcareers.models.change_password.ChangePasswordModel;
import pt.magical.magicalcareers.models.change_password.error.ChangePasswordErrorModel;

/**
 * Created by Sachin on 8/26/2017.
 */

public interface IChangePasswordView {

    void changePasswordSuccess(int pid, ChangePasswordModel changePasswordModel);

    void changePasswordError(int pid, ChangePasswordErrorModel changePasswordErrorModel);
}
