package pt.magical.magicalcareers.interfaces.networkstate;

/**
 * Created by Sachin Rajput on 4 August 2017.
 */
public interface NetworkStateReceiverListener {

     void networkAvailable();
     void networkUnavailable();


}
