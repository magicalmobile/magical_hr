package pt.magical.magicalcareers.interfaces.add_company;

import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import pt.magical.magicalcareers.async.AsyncInteractor;
import pt.magical.magicalcareers.async.OnRequestListener;
import pt.magical.magicalcareers.interfaces.create_jd.ICreateJdPresenter;
import pt.magical.magicalcareers.interfaces.create_jd.ICreateJdView;
import pt.magical.magicalcareers.models.add_company.AddCompanyModel;
import pt.magical.magicalcareers.models.add_company.error.AddCompanyErrorModel;
import pt.magical.magicalcareers.models.create_jd.CreateJdModel;
import pt.magical.magicalcareers.utils.AppConstants;
import pt.magical.magicalcareers.utils.NetworkStatus;
import pt.magical.magicalcareers.utils.Utils;
import pt.magical.magicalcareers.views.fragments.recruiter.PositionInvolveFragment;
import pt.magical.magicalcareers.views.fragments.recruiter.WhoAreUHiringForFragment;

/**
 * Created by Sachin on 8/24/2017.
 */

public class AddCompanyPresenterImpl implements IAddCompanyPresenter, OnRequestListener {

    private IAddCompanyView mIAddCompanyView;
    private WhoAreUHiringForFragment mWhoAreUHiringForFragment;
    private AsyncInteractor mAsyncInteractor;
    private AddCompanyModel mAddCompanyModel;
    private AddCompanyErrorModel addCompanyErrorModel;


    public AddCompanyPresenterImpl(IAddCompanyView mIAddCompanyView) {
        this.mIAddCompanyView = mIAddCompanyView;
        this.mWhoAreUHiringForFragment = (WhoAreUHiringForFragment) mIAddCompanyView;
        mAsyncInteractor = new AsyncInteractor();
    }

    @Override
    public void addACompanyApi(String company_name, String master_industry_type, String website_url,
                               String linked_in_url, Map<String, String> company_logo) {
        if (NetworkStatus.checkNetworkStatus(mWhoAreUHiringForFragment.getActivity())) {
            Utils.showProgress(mWhoAreUHiringForFragment.getActivity());
            Map<String, String> params = new HashMap<String, String>();
            params.put("company_name", company_name);
            params.put("master_industry_type", master_industry_type);
            params.put("website_url", website_url);
            params.put("linked_in_url", linked_in_url);

            Log.d("params", "" + params.toString());

            mAsyncInteractor.validateCredentialsMultipartAsync(Utils.methodMultipart, this, AppConstants.TAG_ID_ADD_COMPANY,
                    AppConstants.URL.ADD_COMPANY.getUrl(), params, company_logo);
        } else {
            Utils.showToast(mWhoAreUHiringForFragment.getActivity(), "Please connect to internet");
        }
    }

    @Override
    public void onRequestCompletion(int pid, JSONObject responseJson, JSONArray responseArray) {

    }

    @Override
    public void onRequestCompletion(int pid, String responseJson) throws JSONException {
        try {
            if (pid == AppConstants.TAG_ID_ADD_COMPANY) {
                if (responseJson != null) {
                    Gson gson = new Gson();
                    mAddCompanyModel = gson.fromJson(responseJson, AddCompanyModel.class);
                    mIAddCompanyView.onaddACompanyApiSuccess(pid, mAddCompanyModel);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestCompletionError(int pid, String error) {
        try {
            if (pid == AppConstants.TAG_ID_ADD_COMPANY) {
                Gson gson = new Gson();
                addCompanyErrorModel = gson.fromJson(error, AddCompanyErrorModel.class);
                mIAddCompanyView.onaddACompanyApiError(pid, addCompanyErrorModel);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestCompletionHomeError(int pid, String error) {
    }


}





