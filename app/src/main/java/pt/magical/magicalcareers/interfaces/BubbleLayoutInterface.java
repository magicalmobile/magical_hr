package pt.magical.magicalcareers.interfaces;

/**
 * Created by danish on 05-08-2017.
 */

public interface BubbleLayoutInterface {
    void onBubbleItemClick(int itemPosition,String itemTitle);
}
