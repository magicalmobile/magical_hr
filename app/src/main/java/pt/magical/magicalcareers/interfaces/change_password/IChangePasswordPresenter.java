package pt.magical.magicalcareers.interfaces.change_password;

/**
 * Created by Sachin on 8/26/2017.
 */

public interface IChangePasswordPresenter {

    void changePassword(String current_password, String new_password);
}
