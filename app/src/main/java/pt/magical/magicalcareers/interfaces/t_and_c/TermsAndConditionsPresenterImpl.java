package pt.magical.magicalcareers.interfaces.t_and_c;

import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import pt.magical.magicalcareers.async.AsyncInteractor;
import pt.magical.magicalcareers.async.OnRequestListener;
import pt.magical.magicalcareers.interfaces.add_company.IAddCompanyPresenter;
import pt.magical.magicalcareers.interfaces.add_company.IAddCompanyView;
import pt.magical.magicalcareers.models.add_company.AddCompanyModel;
import pt.magical.magicalcareers.models.add_company.error.AddCompanyErrorModel;
import pt.magical.magicalcareers.models.t_and_c.TermsAndConditionsModel;
import pt.magical.magicalcareers.utils.AppConstants;
import pt.magical.magicalcareers.utils.NetworkStatus;
import pt.magical.magicalcareers.utils.Utils;
import pt.magical.magicalcareers.views.fragments.common.TermsAndConditionsFragment;
import pt.magical.magicalcareers.views.fragments.recruiter.WhoAreUHiringForFragment;

/**
 * Created by Sachin on 8/26/2017.
 */

public class TermsAndConditionsPresenterImpl implements ITermsAndConditionsPresenter, OnRequestListener {

    private ITermsAndConditionsView mITermsAndConditionsView;
    private TermsAndConditionsFragment mTermsAndConditionsFragment;
    private AsyncInteractor mAsyncInteractor;
    private TermsAndConditionsModel mTermsAndConditionsModel;


    public TermsAndConditionsPresenterImpl(ITermsAndConditionsView mITermsAndConditionsView) {
        this.mITermsAndConditionsView = mITermsAndConditionsView;
        this.mTermsAndConditionsFragment = (TermsAndConditionsFragment) mITermsAndConditionsView;
        mAsyncInteractor = new AsyncInteractor();
    }


    @Override
    public void getTermsAndconditions() {
        if (NetworkStatus.checkNetworkStatus(mTermsAndConditionsFragment.getActivity())) {
            Utils.showProgress(mTermsAndConditionsFragment.getActivity());

            mAsyncInteractor.validateCredentialsAsync(Utils.methodGet, this, AppConstants.TAG_ID_TERMS_AND_CONDITIONS,
                    AppConstants.URL.TERMS_AND_CONDITIONS.getUrl());
        } else {
            Utils.showToast(mTermsAndConditionsFragment.getActivity(), "Please connect to internet");
        }
    }

    @Override
    public void onRequestCompletion(int pid, JSONObject responseJson, JSONArray responseArray) {

    }

    @Override
    public void onRequestCompletion(int pid, String responseJson) throws JSONException {
        try {
            if (pid == AppConstants.TAG_ID_TERMS_AND_CONDITIONS) {
                if (responseJson != null) {
                    Gson gson = new Gson();
                    mTermsAndConditionsModel = gson.fromJson(responseJson, TermsAndConditionsModel.class);
                    mITermsAndConditionsView.getTermsAndconditionsSuccess(pid, mTermsAndConditionsModel);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestCompletionError(int pid, String error) {
        try {
            if (pid == AppConstants.TAG_ID_TERMS_AND_CONDITIONS) {
                Gson gson = new Gson();
                mTermsAndConditionsModel = gson.fromJson(error, TermsAndConditionsModel.class);
                mITermsAndConditionsView.getTermsAndconditionsError(pid, mTermsAndConditionsModel.getMeta().getMessage().toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestCompletionHomeError(int pid, String error) {
    }

}






