package pt.magical.magicalcareers.interfaces.recruiter_profile_data;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pt.magical.magicalcareers.async.AsyncInteractor;
import pt.magical.magicalcareers.async.OnRequestListener;
import pt.magical.magicalcareers.interfaces.recruiter_master_data.IRecruiterMasterDataPresenter;
import pt.magical.magicalcareers.interfaces.recruiter_master_data.IRecruiterMasterDataView;
import pt.magical.magicalcareers.models.recruiter_master_data.RecruiterMasterDataModel;
import pt.magical.magicalcareers.models.recruiter_profile_data.RecruiterProfileParentDataModel;
import pt.magical.magicalcareers.models.recruiter_profile_data.error.RecruiterProfileParentDataErrorModel;
import pt.magical.magicalcareers.utils.AppConstants;
import pt.magical.magicalcareers.utils.NetworkStatus;
import pt.magical.magicalcareers.utils.Utils;
import pt.magical.magicalcareers.views.activities.recruiter.RecruiterDashboardActivity;
import pt.magical.magicalcareers.views.activities.recruiter.RecruiterProfileParentActivity;

/**
 * Created by Sachin on 8/24/2017.
 */

public class RecruiterProfileParentDataPresenterImpl implements IRecruiterProfileParentDataPresenter, OnRequestListener {

    private IRecruiterProfileParentDataView mIRecruiterProfileParentDataView;
    private RecruiterProfileParentActivity mRecruiterProfileParentActivity;
    private AsyncInteractor mAsyncInteractor;
    private RecruiterProfileParentDataModel mRecruiterProfileParentDataModel;
    private RecruiterProfileParentDataErrorModel mRecruiterProfileParentDataErrorModel;

    public RecruiterProfileParentDataPresenterImpl(IRecruiterProfileParentDataView mIRecruiterProfileParentDataView) {
        this.mIRecruiterProfileParentDataView = mIRecruiterProfileParentDataView;
        this.mRecruiterProfileParentActivity = (RecruiterProfileParentActivity) mIRecruiterProfileParentDataView;
        mAsyncInteractor = new AsyncInteractor();
    }


    @Override
    public void getRecruiterProfileParentDataApi() {
        if (NetworkStatus.checkNetworkStatus(mRecruiterProfileParentActivity)) {
            Utils.showProgress(mRecruiterProfileParentActivity);
            mAsyncInteractor.validateCredentialsAsync(Utils.methodGet, this, AppConstants.TAG_ID_RECRUITER_PROFILE_DATA,
                    AppConstants.URL.RECRUITER_PROFILE_DATA.getUrl());
        } else {
            Utils.showToast(mRecruiterProfileParentActivity, "Please connect to internet");
        }
    }

    @Override
    public void onRequestCompletion(int pid, JSONObject responseJson, JSONArray responseArray) {

    }

    @Override
    public void onRequestCompletion(int pid, String responseJson) throws JSONException {
        try {
            if (pid == AppConstants.TAG_ID_RECRUITER_PROFILE_DATA) {
                if (responseJson != null) {
                    Gson gson = new Gson();
                    mRecruiterProfileParentDataModel = gson.fromJson(responseJson, RecruiterProfileParentDataModel.class);
                    mIRecruiterProfileParentDataView.onGetRecruiterProfileParentDataApiSuccess(pid, mRecruiterProfileParentDataModel);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestCompletionError(int pid, String error) {
        try {
            if (pid == AppConstants.TAG_ID_RECRUITER_PROFILE_DATA) {
                Gson gson = new Gson();
                mRecruiterProfileParentDataErrorModel = gson.fromJson(error, RecruiterProfileParentDataErrorModel.class);
                mIRecruiterProfileParentDataView.onGetRecruiterProfileParentDataApiError(pid, mRecruiterProfileParentDataErrorModel.getMeta().getMessage());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestCompletionHomeError(int pid, String error) {
    }

}




