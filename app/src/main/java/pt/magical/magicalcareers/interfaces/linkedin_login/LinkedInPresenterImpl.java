package pt.magical.magicalcareers.interfaces.linkedin_login;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import pt.magical.magicalcareers.async.AsyncInteractor;
import pt.magical.magicalcareers.async.OnRequestListener;
import pt.magical.magicalcareers.utils.AppConstants;
import pt.magical.magicalcareers.utils.NetworkStatus;
import pt.magical.magicalcareers.utils.Utils;
import pt.magical.magicalcareers.views.activities.common.LinkedInLoginActivity;

/**
 * Created by danish on 26-08-2017.
 */

public class LinkedInPresenterImpl implements ILinkedInPresenter ,OnRequestListener{

    private final ILinkedInView linkedInView;
    private final LinkedInLoginActivity activity;
    private final AsyncInteractor mAsyncInteractor;

    public LinkedInPresenterImpl(ILinkedInView iLinkedInView) {
        this.linkedInView=iLinkedInView;
        this.activity=(LinkedInLoginActivity)iLinkedInView;
        mAsyncInteractor = new AsyncInteractor();
    }

    @Override
    public void linkedInLoginApiCall(String linked_in_id, String headline, String first_name, String last_name, String device_id, String device_type, String device_version, String role) {

        if (NetworkStatus.checkNetworkStatus(activity)) {
            Utils.showProgress(activity);
            Map<String, String> params = new HashMap<String, String>();
            params.put("linked_in_id", linked_in_id);
            params.put("headline", headline);
            params.put("first_name", first_name);
            params.put("last_name", last_name);
            params.put("device_id", device_id);
            params.put("device_type", device_type);
            params.put("device_version", device_version);
            params.put("role", role);
            Log.d("params", params.toString());

            mAsyncInteractor.validateCredentialsAsync(Utils.methodPost, this, AppConstants.TAG_ID_LINKED_LOGIN,
                    AppConstants.URL.LINKED_LOGIN.getUrl(), params);
        } else {
            Utils.showToast(activity, "Please connect to internet");
        }

    }

    @Override
    public void onRequestCompletion(int pid, JSONObject responseJson, JSONArray responseArray) {

    }

    @Override
    public void onRequestCompletion(int pid, String responseJson) throws JSONException {
        try {
            if (pid == AppConstants.TAG_ID_LINKED_LOGIN) {
                if (responseJson != null) {
                    linkedInView.linkedInLoginApiCallSuccess(pid,responseJson);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestCompletionError(int pid, String error) {
        try {
            if (pid == AppConstants.TAG_ID_LINKED_LOGIN) {
                if (error != null) {
                    linkedInView.linkedInLoginApiCallSuccess(pid,error);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestCompletionHomeError(int pid, String error) {

    }
}
