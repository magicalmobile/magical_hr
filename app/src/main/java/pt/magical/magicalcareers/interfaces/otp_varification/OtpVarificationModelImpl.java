package pt.magical.magicalcareers.interfaces.otp_varification;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import pt.magical.magicalcareers.async.AsyncInteractor;
import pt.magical.magicalcareers.async.OnRequestListener;
import pt.magical.magicalcareers.models.candidate_profile.CandidateProfileModel;
import pt.magical.magicalcareers.models.otp_error.ForgotPasswordConfirmationErrorModel;
import pt.magical.magicalcareers.models.resend_otp.ResendOtpModel;
import pt.magical.magicalcareers.utils.AppConstants;
import pt.magical.magicalcareers.utils.NetworkStatus;
import pt.magical.magicalcareers.utils.Utils;
import pt.magical.magicalcareers.views.activities.common.OtpActivity;

/**
 * Created by Vivek on 25-08-2017.
 */

public class OtpVarificationModelImpl implements IOtpVarificationPresenter, OnRequestListener {

    private IOtpVarificationView mIOtpVarificationView;
    private OtpActivity mOtpActivity;
    private AsyncInteractor mAsyncInteractor;
    private ForgotPasswordConfirmationErrorModel mForgotPasswordConfirmationErrorModel;
    private ResendOtpModel mResendOtpModel;

    private CandidateProfileModel mCandidateProfileModel;

    public OtpVarificationModelImpl(IOtpVarificationView mIOtpVarificationView) {
        this.mIOtpVarificationView = mIOtpVarificationView;
        this.mOtpActivity = (OtpActivity) mIOtpVarificationView;
        mAsyncInteractor = new AsyncInteractor();
    }

    @Override
    public void otpVerificationApiCall(String otp_type, String otp, String username) {

        if (NetworkStatus.checkNetworkStatus(mOtpActivity)) {
            Utils.showProgress(mOtpActivity);
            Map<String, String> params = new HashMap<String, String>();
            params.put("otp_type", otp_type);
            params.put("otp", otp);
            params.put("username", username);
            Log.d("params", params.toString());

            mAsyncInteractor.validateCredentialsAsync(Utils.methodPost, this, AppConstants.TAG_ID_FORGOT_PASSWORD_VERIFY_OTP_CONFIRMATION,
                    AppConstants.URL.FORGOT_PASSWORD_OTP_CONFIRM.getUrl(), params);
        } else {
            Utils.showToast(mOtpActivity, "Please connect to internet");
        }


    }

    @Override
    public void resendOtp(String otp_type, String username) {
        if (NetworkStatus.checkNetworkStatus(mOtpActivity)) {
            Utils.showProgress(mOtpActivity);
            Map<String, String> params = new HashMap<String, String>();
            params.put("otp_type", otp_type);
            params.put("username", username);
            Log.d("params", params.toString());

            mAsyncInteractor.validateCredentialsAsync(Utils.methodPost, this, AppConstants.TAG_ID_RESEND_OTP,
                    AppConstants.URL.RESNED_OTP.getUrl(), params);
        } else {
            Utils.showToast(mOtpActivity, "Please connect to internet");
        }


    }

    @Override
    public void onRequestCompletion(int pid, JSONObject responseJson, JSONArray responseArray) {

    }

    @Override
    public void onRequestCompletion(int pid, String responseJson) throws JSONException {

        try {
            if (pid == AppConstants.TAG_ID_FORGOT_PASSWORD_VERIFY_OTP_CONFIRMATION) {
                if (responseJson != null) {
                    mIOtpVarificationView.otpVerificationApiCallSuccess(pid, responseJson);
                }
            }
            else if (pid == AppConstants.TAG_ID_RESEND_OTP) {
            if (responseJson != null) {
                Gson gson = new Gson();
                mResendOtpModel = gson.fromJson(responseJson, ResendOtpModel.class);
                mIOtpVarificationView.resendOtpSuccess(pid, mResendOtpModel);
            }
        }

    } catch (Exception e) {
        e.printStackTrace();
    }

    }

    @Override
    public void onRequestCompletionError(int pid, String error) {

        try {
            Gson gson = new Gson();
            if (pid == AppConstants.TAG_ID_FORGOT_PASSWORD_VERIFY_OTP_CONFIRMATION) {
                mForgotPasswordConfirmationErrorModel= gson.fromJson(error, ForgotPasswordConfirmationErrorModel.class);
                mIOtpVarificationView.otpVerificationApiCallError(pid, mForgotPasswordConfirmationErrorModel);
            }else if (pid == AppConstants.TAG_ID_RESEND_OTP) {
                mResendOtpModel = gson.fromJson(error, ResendOtpModel.class);
                mIOtpVarificationView.resendOtpError(pid, mResendOtpModel.getMeta().getMessage());
            }
        } catch (JsonSyntaxException e) {
            e.printStackTrace();

        }

    }

    @Override
    public void onRequestCompletionHomeError(int pid, String error) {

    }
}
