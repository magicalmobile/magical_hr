package pt.magical.magicalcareers.interfaces.trending_question_list;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import pt.magical.magicalcareers.async.AsyncInteractor;
import pt.magical.magicalcareers.async.OnRequestListener;
import pt.magical.magicalcareers.interfaces.add_trending_question.IAddTrendingQuestionPresenter;
import pt.magical.magicalcareers.models.recruiter.add_trending_question.AddTrendingQuestionModel;
import pt.magical.magicalcareers.models.recruiter.add_trending_question.error.AddTrendingQuestionErrorModel;
import pt.magical.magicalcareers.models.recruiter.trending_question_list.TrendingQuestionListModel;
import pt.magical.magicalcareers.models.recruiter.trending_question_list.error.TrendingQuestionListErrorModel;
import pt.magical.magicalcareers.utils.AppConstants;
import pt.magical.magicalcareers.utils.NetworkStatus;
import pt.magical.magicalcareers.utils.Utils;
import pt.magical.magicalcareers.views.fragments.recruiter.TrendingQuestionsFragment;

/**
 * Created by radhu on 28-08-2017.
 */

public class TrendingQuestionListImpl implements ITrendingQuestionListPresenter, OnRequestListener {

    private final AsyncInteractor mAsyncInteractor;
    private final ITrendingQuestionListView mITrendingQuestionListView;
  //  private final Context mContext;
    private TrendingQuestionListModel mTrendingQuestionListModel;
    private TrendingQuestionsFragment mTrendingQuestionsFragment;
    private TrendingQuestionListErrorModel mTrendingQuestionListErrorModel;

    public TrendingQuestionListImpl(ITrendingQuestionListView iTrendingQuestionListView) {
        this.mITrendingQuestionListView = iTrendingQuestionListView;
        this.mTrendingQuestionsFragment = (TrendingQuestionsFragment) iTrendingQuestionListView;
        //this.mContext=mViewJdParentFragment.getActivity();
        mAsyncInteractor = new AsyncInteractor();
    }

    @Override
    public void onRequestCompletion(int pid, JSONObject responseJson, JSONArray responseArray) {

    }

    @Override
    public void onRequestCompletion(final int pid, final String responseJson) throws JSONException {
        if (responseJson != null) {
            if (pid == AppConstants.TAG_ID_RECRUITER_TRENDING_QUESTION) {
                try {
                    Gson gson = new Gson();
                    mTrendingQuestionListModel = gson.fromJson(responseJson, TrendingQuestionListModel.class);
                    mITrendingQuestionListView.getTrendingQuestionDataSuccess(pid, mTrendingQuestionListModel);
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onRequestCompletionError(int pid, String error) {
        if (pid == AppConstants.TAG_ID_RECRUITER_TRENDING_QUESTION) {
            Gson gson = new Gson();
            mTrendingQuestionListErrorModel = gson.fromJson(error, TrendingQuestionListErrorModel.class);
            mITrendingQuestionListView.getTrendingQuestionDataError(pid, mTrendingQuestionListErrorModel.getData().getMasterKeySkill().get(0).toString());
        }
    }

    @Override
    public void onRequestCompletionHomeError(int pid, String error) {
    }



    @Override
    public void getTrendingQuestionList(int master_key_skill) {
        if (NetworkStatus.checkNetworkStatus(mTrendingQuestionsFragment.getActivity())) {
            Utils.showProgress(mTrendingQuestionsFragment.getActivity());
            Map<String, String> params = new HashMap<String, String>();
            params.put("master_key_skill", String.valueOf(master_key_skill));
            mAsyncInteractor.validateCredentialsAsync(Utils.methodPost,this, AppConstants.TAG_ID_RECRUITER_TRENDING_QUESTION,
                    AppConstants.URL.RECRUITER_QUESTION_LIST.getUrl(), params);
        } else {
            Utils.showToast(mTrendingQuestionsFragment.getActivity(), "Please connect to internet");
        }
    }
}
