package pt.magical.magicalcareers.interfaces.create_jd;

import java.util.Map;

/**
 * Created by Sachin on 8/24/2017.
 */

public interface ICreateJdPresenter {


    void createJdApi(String master_designation, String experience_from, String experience_to, String location, String latitude,
                     String longitude, String ctc_from, String ctc_to/*, String job_description_file*/, String benefits,
                     String institute_type, String highest_education_level, String notice_period, String job_type,
                     String job_description, Map<String, String> job_description_file);
}
