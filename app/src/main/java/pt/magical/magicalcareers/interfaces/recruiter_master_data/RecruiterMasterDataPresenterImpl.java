package pt.magical.magicalcareers.interfaces.recruiter_master_data;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import pt.magical.magicalcareers.async.AsyncInteractor;
import pt.magical.magicalcareers.async.OnRequestListener;
import pt.magical.magicalcareers.models.recruiter_master_data.RecruiterMasterDataModel;
import pt.magical.magicalcareers.utils.AppConstants;
import pt.magical.magicalcareers.utils.NetworkStatus;
import pt.magical.magicalcareers.utils.Utils;
import pt.magical.magicalcareers.views.activities.recruiter.RecruiterDashboardActivity;

/**
 * Created by Sachin on 8/24/2017.
 */

public class RecruiterMasterDataPresenterImpl implements IRecruiterMasterDataPresenter, OnRequestListener {

    private IRecruiterMasterDataView mIRecruiterMasterDataView;
    private RecruiterDashboardActivity mRecruiterDashboardActivity;
    private AsyncInteractor mAsyncInteractor;
    private RecruiterMasterDataModel mRecruiterMasterDataModel;

    public RecruiterMasterDataPresenterImpl(IRecruiterMasterDataView mIRecruiterMasterDataView) {
        this.mIRecruiterMasterDataView = mIRecruiterMasterDataView;
        this.mRecruiterDashboardActivity = (RecruiterDashboardActivity) mIRecruiterMasterDataView;
        mAsyncInteractor = new AsyncInteractor();
    }


    @Override
    public void getRecruiterMasterdata() {

        if (NetworkStatus.checkNetworkStatus(mRecruiterDashboardActivity)) {
            Utils.showProgress(mRecruiterDashboardActivity);
            mAsyncInteractor.validateCredentialsAsync(Utils.methodGet, this, AppConstants.TAG_ID_RECRUITER_MASTER_DATA,
                    AppConstants.URL.RECRUITER_MASTER_DATA.getUrl());
        } else {
            Utils.showToast(mRecruiterDashboardActivity, "Please connect to internet");
        }

    }

    @Override
    public void onRequestCompletion(int pid, JSONObject responseJson, JSONArray responseArray) {

    }

    @Override
    public void onRequestCompletion(int pid, String responseJson) throws JSONException {
        try {
            if (pid == AppConstants.TAG_ID_RECRUITER_MASTER_DATA) {
                if (responseJson != null) {
                    Gson gson = new Gson();
                    mRecruiterMasterDataModel = gson.fromJson(responseJson, RecruiterMasterDataModel.class);
                    mIRecruiterMasterDataView.onRecruiterMasterdataSuccess(pid, mRecruiterMasterDataModel);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestCompletionError(int pid, String error) {
        try {
            if (pid == AppConstants.TAG_ID_RECRUITER_MASTER_DATA) {
                mIRecruiterMasterDataView.onRecruiterMasterdataError(pid, mRecruiterMasterDataModel.getMeta().getMessage().toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestCompletionHomeError(int pid, String error) {
    }

}



