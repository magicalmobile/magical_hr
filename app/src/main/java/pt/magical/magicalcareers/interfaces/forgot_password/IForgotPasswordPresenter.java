package pt.magical.magicalcareers.interfaces.forgot_password;

/**
 * Created by Vivek on 25-08-2017.
 */

public interface IForgotPasswordPresenter {

    void forgotPasswordApi(String username);

}
