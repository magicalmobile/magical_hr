package pt.magical.magicalcareers.interfaces.custom_register;

/**
 * Created by danish on 25-08-2017.
 */

public interface ICustomRegisterPresenter {
    void customRegisterApi(String username,String password,String name,String role,String device_id,String device_type,String device_version,String longitude,String latitude,String location,boolean show);
}
