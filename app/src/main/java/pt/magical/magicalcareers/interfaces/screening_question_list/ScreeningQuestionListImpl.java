package pt.magical.magicalcareers.interfaces.screening_question_list;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import pt.magical.magicalcareers.async.AsyncInteractor;
import pt.magical.magicalcareers.async.OnRequestListener;
import pt.magical.magicalcareers.models.recruiter.screening_question_list.ScreeningQuestionListModel;
import pt.magical.magicalcareers.models.recruiter.screening_question_list.error.ScreeningQuestionListErrorModel;
import pt.magical.magicalcareers.models.recruiter.trending_question_list.TrendingQuestionListModel;
import pt.magical.magicalcareers.models.recruiter.trending_question_list.error.TrendingQuestionListErrorModel;
import pt.magical.magicalcareers.utils.AppConstants;
import pt.magical.magicalcareers.utils.NetworkStatus;
import pt.magical.magicalcareers.utils.Utils;
import pt.magical.magicalcareers.views.fragments.recruiter.ScreeningQuestionListFragment;
import pt.magical.magicalcareers.views.fragments.recruiter.ScreeningQuestionsFragment;
import pt.magical.magicalcareers.views.fragments.recruiter.TrendingQuestionsFragment;

/**
 * Created by radhu on 28-08-2017.
 */

public class ScreeningQuestionListImpl implements IScreeningQuestionListPresenter, OnRequestListener {

    private final AsyncInteractor mAsyncInteractor;
    private final IScreeningQuestionListView mIScreeningQuestionListView;
  //  private final Context mContext;
    private ScreeningQuestionListModel mScreeningQuestionListModel;
    private ScreeningQuestionListFragment mScreeningQuestionListFragment;
    private ScreeningQuestionListErrorModel mScreeningQuestionListErrorModel;

    public ScreeningQuestionListImpl(IScreeningQuestionListView iScreeningQuestionListView) {
        this.mIScreeningQuestionListView = iScreeningQuestionListView;
        this.mScreeningQuestionListFragment = (ScreeningQuestionListFragment) iScreeningQuestionListView;
        //this.mContext=mViewJdParentFragment.getActivity();
        mAsyncInteractor = new AsyncInteractor();
    }

    @Override
    public void onRequestCompletion(int pid, JSONObject responseJson, JSONArray responseArray) {

    }

    @Override
    public void onRequestCompletion(final int pid, final String responseJson) throws JSONException {
        if (responseJson != null) {
            if (pid == AppConstants.TAG_ID_RECRUITER_SCREENING_QUESTION) {
                try {
                    Gson gson = new Gson();
                    mScreeningQuestionListModel = gson.fromJson(responseJson, ScreeningQuestionListModel.class);
                    mIScreeningQuestionListView.getScreeningQuestionDataSuccess(pid, mScreeningQuestionListModel);
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onRequestCompletionError(int pid, String error) {
        if (pid == AppConstants.TAG_ID_RECRUITER_SCREENING_QUESTION) {
            Gson gson = new Gson();
            mScreeningQuestionListErrorModel = gson.fromJson(error, ScreeningQuestionListErrorModel.class);
            mIScreeningQuestionListView.getScreeningQuestionDataError(pid, mScreeningQuestionListErrorModel.getData().getErrorMessage().get(0).toString());
        }
    }

    @Override
    public void onRequestCompletionHomeError(int pid, String error) {
    }

    @Override
    public void getScreeningQuestionList(int jd_id) {
        if (NetworkStatus.checkNetworkStatus(mScreeningQuestionListFragment.getActivity())) {
            Utils.showProgress(mScreeningQuestionListFragment.getActivity());
            Map<String, String> params = new HashMap<String, String>();
            params.put("jd_id", String.valueOf(jd_id));
            mAsyncInteractor.validateCredentialsAsync(Utils.methodPost,this, AppConstants.TAG_ID_RECRUITER_SCREENING_QUESTION,
                    AppConstants.URL.RECRUITER_QUESTION_LIST.getUrl(), params);
        } else {
            Utils.showToast(mScreeningQuestionListFragment.getActivity(), "Please connect to internet");
        }
    }
}
