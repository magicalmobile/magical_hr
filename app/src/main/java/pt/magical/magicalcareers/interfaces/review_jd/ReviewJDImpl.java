package pt.magical.magicalcareers.interfaces.review_jd;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import pt.magical.magicalcareers.async.AsyncInteractor;
import pt.magical.magicalcareers.async.OnRequestListener;
import pt.magical.magicalcareers.models.recruiter.review_jd.ReviewJDModel;
import pt.magical.magicalcareers.models.recruiter.review_jd.error.ErrorReviewJD;
import pt.magical.magicalcareers.utils.AppConstants;
import pt.magical.magicalcareers.utils.NetworkStatus;
import pt.magical.magicalcareers.utils.Utils;
import pt.magical.magicalcareers.views.activities.recruiter.RecruiterDashboardActivity;
import pt.magical.magicalcareers.views.fragments.recruiter.ViewJdParentFragment;

/**
 * Created by radhu on 24-08-2017.
 */

public class ReviewJDImpl implements IReviewJDPresenter, OnRequestListener {

    private final AsyncInteractor mAsyncInteractor;
    private final IReviewJDView mIReviewJDView;
  //  private final Context mContext;
    private ReviewJDModel mReviewJDModel;
    private ViewJdParentFragment mViewJdParentFragment;
    private ErrorReviewJD mErrorReviewJD;

    public ReviewJDImpl(IReviewJDView iIReviewJDView) {
        this.mIReviewJDView = iIReviewJDView;
        this.mViewJdParentFragment = (ViewJdParentFragment) iIReviewJDView;
        //this.mContext=mViewJdParentFragment.getActivity();
        mAsyncInteractor = new AsyncInteractor();
    }

    @Override
    public void onRequestCompletion(int pid, JSONObject responseJson, JSONArray responseArray) {

    }

    @Override
    public void onRequestCompletion(final int pid, final String responseJson) throws JSONException {
        if (responseJson != null) {
            if (pid == AppConstants.TAG_ID_RECRUITER_REVIEW_JD) {
                try {
                    Gson gson = new Gson();
                    mReviewJDModel = gson.fromJson(responseJson, ReviewJDModel.class);
                    mIReviewJDView.getReviewJDDataSuccess(pid, mReviewJDModel);
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onRequestCompletionError(int pid, String error) {
        if (pid == AppConstants.TAG_ID_RECRUITER_REVIEW_JD) {
            Gson gson = new Gson();
            mErrorReviewJD = gson.fromJson(error, ErrorReviewJD.class);
            mIReviewJDView.getReviewJDDataError(pid, mErrorReviewJD.getData().getErrorMessage().get(0).toString());
        }
    }

    @Override
    public void onRequestCompletionHomeError(int pid, String error) {
    }

    @Override
    public void reviewJdData(int jd_id) {
        if (NetworkStatus.checkNetworkStatus(mViewJdParentFragment.getActivity())) {
            Utils.showProgress(mViewJdParentFragment.getActivity());
            Map<String, String> params = new HashMap<String, String>();
            params.put("jd_id", String.valueOf(jd_id));
            mAsyncInteractor.validateCredentialsAsync(Utils.methodPost,this, AppConstants.TAG_ID_RECRUITER_REVIEW_JD,
                    AppConstants.URL.RECRUITER_REVIEW_JD.getUrl(), params);
        } else {
            Utils.showToast(mViewJdParentFragment.getActivity(), "Please connect to internet");
        }
    }
}
