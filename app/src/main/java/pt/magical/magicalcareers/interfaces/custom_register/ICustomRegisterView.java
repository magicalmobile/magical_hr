package pt.magical.magicalcareers.interfaces.custom_register;

import pt.magical.magicalcareers.models.common.custom_register.CustomRegisterModel;
import pt.magical.magicalcareers.models.common.custom_register_error.CustomRegisterErrorModel;

/**
 * Created by danish on 25-08-2017.
 */

public interface ICustomRegisterView {
    void customRegisterApiSuccess(int pid, CustomRegisterModel customRegisterModel);
    void customRegisterApiError(int pid, CustomRegisterErrorModel customRegisterErrorModel);
}
