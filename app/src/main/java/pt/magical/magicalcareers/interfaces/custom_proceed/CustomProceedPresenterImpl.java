package pt.magical.magicalcareers.interfaces.custom_proceed;

import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import pt.magical.magicalcareers.async.AsyncInteractor;
import pt.magical.magicalcareers.async.OnRequestListener;
import pt.magical.magicalcareers.models.common.custom_proceed.CustomProceedModel;
import pt.magical.magicalcareers.utils.AppConstants;
import pt.magical.magicalcareers.utils.NetworkStatus;
import pt.magical.magicalcareers.utils.Utils;
import pt.magical.magicalcareers.views.activities.common.UserProceedActivity;

/**
 * Created by danish on 25-08-2017.
 */

public class CustomProceedPresenterImpl implements ICustomProceedPresenter,OnRequestListener {


    private final AsyncInteractor mAsyncInteractor;
    private final ICustomProceedView mICustomProceedView;
    private final UserProceedActivity mActivity;
    private CustomProceedModel mCustomProceedModel;

    public CustomProceedPresenterImpl(ICustomProceedView iCustomProceedView) {
        this.mICustomProceedView=iCustomProceedView;
        this.mActivity=(UserProceedActivity)iCustomProceedView;
        mAsyncInteractor = new AsyncInteractor();
    }

    @Override
    public void checkExistingUserApi(String username, boolean show) {
        try {
            if (NetworkStatus.checkNetworkStatus(mActivity)) {
                if (show)
                Utils.showProgress(mActivity);
                Map<String, String> params = new HashMap<String, String>();
                params.put("username",username);
                Log.d("params", "" + params.toString());

                mAsyncInteractor.validateCredentialsAsync(Utils.methodPost, this, AppConstants.TAG_ID_CUSTOM_PROCEED,
                        AppConstants.URL.CUSTOM_PROCEED.getUrl(),params);
            } else {
                Utils.showToast(mActivity, "Please connect to internet");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onRequestCompletion(int pid, JSONObject responseJson, JSONArray responseArray) {

    }

    @Override
    public void onRequestCompletion(int pid, String responseJson) throws JSONException {
        try {
            if (pid == AppConstants.TAG_ID_CUSTOM_PROCEED) {
                if (responseJson != null) {
                    Gson gson = new Gson();
                    mCustomProceedModel = gson.fromJson(responseJson, CustomProceedModel.class);
                    mICustomProceedView.checkExistingUserApiSuccess(pid,mCustomProceedModel);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestCompletionError(int pid, String error) {
        try {
            if (pid == AppConstants.TAG_ID_CUSTOM_PROCEED) {
                mICustomProceedView.checkExistingUserApiError(pid,error);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestCompletionHomeError(int pid, String error) {

    }
}
