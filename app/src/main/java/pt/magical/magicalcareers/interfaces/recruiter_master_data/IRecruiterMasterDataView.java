package pt.magical.magicalcareers.interfaces.recruiter_master_data;

import pt.magical.magicalcareers.models.recruiter_master_data.RecruiterMasterDataModel;

/**
 * Created by Sachin on 8/24/2017.
 */

public interface IRecruiterMasterDataView {

    void onRecruiterMasterdataSuccess(int pid, RecruiterMasterDataModel recruiterMasterDataModel);

    void onRecruiterMasterdataError(int pid, String errorData);
}
