package pt.magical.magicalcareers.interfaces.add_company;

import java.util.Map;

/**
 * Created by Sachin on 8/24/2017.
 */

public interface IAddCompanyPresenter {

    void addACompanyApi(String company_name, String master_industry_type,
                        String website_url, String linked_in_url, Map<String, String> company_logo);

}
