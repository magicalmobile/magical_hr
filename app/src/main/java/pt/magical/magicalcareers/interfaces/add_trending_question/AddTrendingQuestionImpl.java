package pt.magical.magicalcareers.interfaces.add_trending_question;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import pt.magical.magicalcareers.async.AsyncInteractor;
import pt.magical.magicalcareers.async.OnRequestListener;
import pt.magical.magicalcareers.models.recruiter.add_trending_question.AddTrendingQuestionModel;
import pt.magical.magicalcareers.models.recruiter.add_trending_question.error.AddTrendingQuestionErrorModel;
import pt.magical.magicalcareers.models.recruiter.review_jd.ReviewJDModel;
import pt.magical.magicalcareers.models.recruiter.review_jd.error.ErrorReviewJD;
import pt.magical.magicalcareers.utils.AppConstants;
import pt.magical.magicalcareers.utils.NetworkStatus;
import pt.magical.magicalcareers.utils.Utils;
import pt.magical.magicalcareers.views.fragments.recruiter.TrendingQuestionsFragment;
import pt.magical.magicalcareers.views.fragments.recruiter.ViewJdParentFragment;

/**
 * Created by radhu on 28-08-2017.
 */

public class AddTrendingQuestionImpl implements IAddTrendingQuestionPresenter, OnRequestListener {

    private final AsyncInteractor mAsyncInteractor;
    private final IAddTrendingQuestionView mIReviewJDView;
  //  private final Context mContext;
    private AddTrendingQuestionModel mAddTrendingQuestionModel;
    private TrendingQuestionsFragment mTrendingQuestionsFragment;
    private AddTrendingQuestionErrorModel mAddTrendingQuestionErrorModel;

    public AddTrendingQuestionImpl(IAddTrendingQuestionView iAddTrendingQuestionView) {
        this.mIReviewJDView = iAddTrendingQuestionView;
        this.mTrendingQuestionsFragment = (TrendingQuestionsFragment) iAddTrendingQuestionView;
        //this.mContext=mViewJdParentFragment.getActivity();
        mAsyncInteractor = new AsyncInteractor();
    }

    @Override
    public void onRequestCompletion(int pid, JSONObject responseJson, JSONArray responseArray) {

    }

    @Override
    public void onRequestCompletion(final int pid, final String responseJson) throws JSONException {
        if (responseJson != null) {
            if (pid == AppConstants.TAG_ID_RECRUITER_ADD_TRENDING_QUESTION) {
                try {
                    Gson gson = new Gson();
                    mAddTrendingQuestionModel = gson.fromJson(responseJson, AddTrendingQuestionModel.class);
                    mIReviewJDView.getAddTrendingQuestionDataSuccess(pid, mAddTrendingQuestionModel);
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onRequestCompletionError(int pid, String error) {
        if (pid == AppConstants.TAG_ID_RECRUITER_ADD_TRENDING_QUESTION) {
            Gson gson = new Gson();
            mAddTrendingQuestionErrorModel = gson.fromJson(error, AddTrendingQuestionErrorModel.class);
            mIReviewJDView.getAddTrendingQuestionDataError(pid, mAddTrendingQuestionErrorModel.getData().getErrorMessage().get(0).toString());
        }
    }

    @Override
    public void onRequestCompletionHomeError(int pid, String error) {
    }

    @Override
    public void addTrendingQuestion(int jd_id, int master_screen_ques_id) {
        if (NetworkStatus.checkNetworkStatus(mTrendingQuestionsFragment.getActivity())) {
            Utils.showProgress(mTrendingQuestionsFragment.getActivity());
            Map<String, String> params = new HashMap<String, String>();
            params.put("jd_id", String.valueOf(jd_id));
            params.put("master_screen_question", String.valueOf(master_screen_ques_id));
            mAsyncInteractor.validateCredentialsAsync(Utils.methodPost,this, AppConstants.TAG_ID_RECRUITER_ADD_TRENDING_QUESTION,
                    AppConstants.URL.RECRUITER_ADD_TRENDING_QUESTION.getUrl(), params);
        } else {
            Utils.showToast(mTrendingQuestionsFragment.getActivity(), "Please connect to internet");
        }
    }
}
