package pt.magical.magicalcareers.interfaces.forgot_password;

import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import pt.magical.magicalcareers.async.AsyncInteractor;
import pt.magical.magicalcareers.async.OnRequestListener;
import pt.magical.magicalcareers.models.candidate_profile.CandidateProfileModel;
import pt.magical.magicalcareers.models.forgot_password.ForgotPasswordModel;
import pt.magical.magicalcareers.models.otp_error.ForgotPasswordConfirmationErrorModel;
import pt.magical.magicalcareers.utils.AppConstants;
import pt.magical.magicalcareers.utils.NetworkStatus;
import pt.magical.magicalcareers.utils.Utils;
import pt.magical.magicalcareers.views.activities.common.ForgotPasswordActivity;

/**
 * Created by  Vivek on 25-08-2017.
 */

public class ForgotPasswordPresenterImpl implements IForgotPasswordPresenter, OnRequestListener {

    private IForgotPasswordView mIForgotPasswordView;
    private ForgotPasswordActivity mForgotPasswordActivity;
    private AsyncInteractor mAsyncInteractor;
    private ForgotPasswordModel mForgotPasswordModel;

    private CandidateProfileModel mCandidateProfileModel;

    public ForgotPasswordPresenterImpl(IForgotPasswordView mIForgotPasswordView) {
        this.mIForgotPasswordView = mIForgotPasswordView;
        this.mForgotPasswordActivity = (ForgotPasswordActivity) mIForgotPasswordView;
        mAsyncInteractor = new AsyncInteractor();
    }

    @Override
    public void forgotPasswordApi(String username) {

        if (NetworkStatus.checkNetworkStatus(mForgotPasswordActivity)) {
            Utils.showProgress(mForgotPasswordActivity);
            Map<String, String> params = new HashMap<String, String>();
            params.put("username", username);
            Log.d("params", params.toString());

            mAsyncInteractor.validateCredentialsAsync(Utils.methodPost, this, AppConstants.TAG_ID_FORGOT_PASSWORD,
                    AppConstants.URL.FORGOT_PASSWORD.getUrl(), params);
        } else {
            Utils.showToast(mForgotPasswordActivity, "Please connect to internet");
        }


    }


    @Override
    public void onRequestCompletion(int pid, JSONObject responseJson, JSONArray responseArray) {

    }

    @Override
    public void onRequestCompletion(int pid, String responseJson) throws JSONException {
        if (pid == AppConstants.TAG_ID_FORGOT_PASSWORD) {
            if (responseJson != null) {
                Gson gson = new Gson();
                mForgotPasswordModel = gson.fromJson(responseJson, ForgotPasswordModel.class);
                mIForgotPasswordView.forgotPasswordApiSuccess(pid, mForgotPasswordModel);
            }
        }

    }

    @Override
    public void onRequestCompletionError(int pid, String error) {
        Gson gson = new Gson();
        if (pid == AppConstants.TAG_ID_FORGOT_PASSWORD) {
            mForgotPasswordModel = gson.fromJson(error, ForgotPasswordModel.class);
            mIForgotPasswordView.forgotPasswordApiError(pid, mForgotPasswordModel.getData().getUsername().toString());
        }
    }

    @Override
    public void onRequestCompletionHomeError(int pid, String error) {

    }

}

