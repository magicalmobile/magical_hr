package pt.magical.magicalcareers.interfaces.custom_register;

import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import pt.magical.magicalcareers.async.AsyncInteractor;
import pt.magical.magicalcareers.async.OnRequestListener;
import pt.magical.magicalcareers.models.common.custom_proceed.CustomProceedModel;
import pt.magical.magicalcareers.models.common.custom_register.CustomRegisterModel;
import pt.magical.magicalcareers.models.common.custom_register_error.CustomRegisterErrorModel;
import pt.magical.magicalcareers.utils.AppConstants;
import pt.magical.magicalcareers.utils.NetworkStatus;
import pt.magical.magicalcareers.utils.Utils;
import pt.magical.magicalcareers.views.activities.common.RegisterActivity;

/**
 * Created by danish on 25-08-2017.
 */

public class CustomRegisterPresenterImpl implements ICustomRegisterPresenter,OnRequestListener {


    private final AsyncInteractor mAsyncInteractor;
    private final RegisterActivity mActivity;
    private final ICustomRegisterView mICustomRegisterView;
    private CustomRegisterModel mCustomRegisterModel;
    private CustomRegisterErrorModel mCustomRegisterErrorModel;


    public CustomRegisterPresenterImpl(ICustomRegisterView iCustomRegisterView) {
        this.mICustomRegisterView=iCustomRegisterView;
        this.mActivity=(RegisterActivity)iCustomRegisterView;
        mAsyncInteractor = new AsyncInteractor();
    }

    @Override
    public void customRegisterApi(String username, String password, String name, String role, String device_id, String device_type, String device_version, String longitude, String latitude, String location, boolean show) {

        try {
            if (NetworkStatus.checkNetworkStatus(mActivity)) {
                if (show)
                    Utils.showProgress(mActivity);
                Map<String, String> params = new HashMap<String, String>();
                params.put("username",username);
                params.put("password",password);
                params.put("name",name);
                params.put("role",role);
                params.put("device_id",device_id);
                params.put("device_type",device_type);
                params.put("device_version",device_version);
                params.put("longitude",longitude);
                params.put("latitude",latitude);
                params.put("location",location);
                Log.d("params", "" + params.toString());

                mAsyncInteractor.validateCredentialsAsync(Utils.methodPost, this, AppConstants.TAG_ID_CUSTOM_REGISTER,
                        AppConstants.URL.CUSTOM_REGISTER.getUrl(),params);
            } else {
                Utils.showToast(mActivity, "Please connect to internet");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onRequestCompletion(int pid, JSONObject responseJson, JSONArray responseArray) {

    }

    @Override
    public void onRequestCompletion(int pid, String responseJson) throws JSONException {
        try {
            if (pid == AppConstants.TAG_ID_CUSTOM_REGISTER) {
                if (responseJson != null) {
                    Gson gson = new Gson();
                    mCustomRegisterModel = gson.fromJson(responseJson, CustomRegisterModel.class);
                    mICustomRegisterView.customRegisterApiSuccess(pid,mCustomRegisterModel);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestCompletionError(int pid, String error) {
        try {
            if (pid == AppConstants.TAG_ID_CUSTOM_REGISTER) {
                if (error != null) {
                    Gson gson = new Gson();
                    mCustomRegisterErrorModel = gson.fromJson(error, CustomRegisterErrorModel.class);
                    mICustomRegisterView.customRegisterApiError(pid,mCustomRegisterErrorModel);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestCompletionHomeError(int pid, String error) {

    }
}
