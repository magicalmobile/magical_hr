package pt.magical.magicalcareers.interfaces.recruiter_profile_data;

import pt.magical.magicalcareers.models.recruiter_profile_data.RecruiterProfileParentDataModel;

/**
 * Created by Sachin on 8/24/2017.
 */

public interface IRecruiterProfileParentDataView {

    void onGetRecruiterProfileParentDataApiSuccess(int pid , RecruiterProfileParentDataModel recruiterProfileParentDataModel);
    void onGetRecruiterProfileParentDataApiError(int pid , String errorData);
}
