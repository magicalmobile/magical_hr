package pt.magical.magicalcareers.interfaces.otp_varification;

/**
 * Created by Vivek on 25-08-2017.
 */

public interface IOtpVarificationPresenter {

    void resendOtp(String otp_type, String username);

    void otpVerificationApiCall(String otp_type , String otp, String username);

}
