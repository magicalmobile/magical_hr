package pt.magical.magicalcareers.interfaces.t_and_c;

import pt.magical.magicalcareers.models.t_and_c.TermsAndConditionsModel;

/**
 * Created by Sachin on 8/26/2017.
 */

public interface ITermsAndConditionsView {

    void getTermsAndconditionsSuccess(int pid, TermsAndConditionsModel termsAndConditionsModel);

    void getTermsAndconditionsError(int pid, String errorData);
}
