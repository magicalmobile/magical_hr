package pt.magical.magicalcareers.interfaces.custom_login;

/**
 * Created by danish on 26-08-2017.
 */

public interface ICustomLoginPresenter {
    void customLoginApiCall(String username, String password,boolean show);
}
