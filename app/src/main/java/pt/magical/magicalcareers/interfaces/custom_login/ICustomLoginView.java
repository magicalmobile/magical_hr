package pt.magical.magicalcareers.interfaces.custom_login;

/**
 * Created by danish on 26-08-2017.
 */

public interface ICustomLoginView {
   void customLoginApiCallSuccess(int pid,String successResponse);
    void customLoginApiError(int pid,String error);
}
