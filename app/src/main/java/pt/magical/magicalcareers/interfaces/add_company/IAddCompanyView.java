package pt.magical.magicalcareers.interfaces.add_company;

import pt.magical.magicalcareers.models.add_company.AddCompanyModel;
import pt.magical.magicalcareers.models.add_company.error.AddCompanyErrorModel;
import pt.magical.magicalcareers.models.recruiter_master_data.RecruiterMasterDataModel;

/**
 * Created by Sachin on 8/24/2017.
 */

public interface IAddCompanyView {

    void onaddACompanyApiSuccess(int pid, AddCompanyModel addCompanyModel);

    void onaddACompanyApiError(int pid, AddCompanyErrorModel addCompanyErrorModel);
}
