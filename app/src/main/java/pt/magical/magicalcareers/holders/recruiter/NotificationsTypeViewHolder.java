package pt.magical.magicalcareers.holders.recruiter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import pt.magical.magicalcareers.R;

/**
 * Created by Sachin on 6/8/2017.
 */

public class NotificationsTypeViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.item_notifications_type_tv)
    public TextView mNotificationsTypeTV;
    @BindView(R.id.item_notifications_type_iv)
    public ImageView mNotificationsTypeIV;
    @BindView(R.id.item_notifications_type_ll)
    public LinearLayout mNotificationsLL;

    public NotificationsTypeViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
