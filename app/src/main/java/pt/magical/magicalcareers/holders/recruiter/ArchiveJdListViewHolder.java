package pt.magical.magicalcareers.holders.recruiter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import pt.magical.magicalcareers.R;

/**
 * Created by Sachin on 6/8/2017.
 */

public class ArchiveJdListViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.itemArchiveJdListHeadTV)
    public TextView itemArchiveJdListHeadTV;

    @BindView(R.id.itemArchiveJdTimeTV)
    public TextView itemArchiveJdTimeTV;

    @BindView(R.id.archiveJdlistActItemMoreIV)
    public ImageView mArchiveJdlistActItemMoreIV;

    public ArchiveJdListViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
