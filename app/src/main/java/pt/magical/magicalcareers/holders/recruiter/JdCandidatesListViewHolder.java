package pt.magical.magicalcareers.holders.recruiter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import pt.magical.magicalcareers.R;

/**
 * Created by RAdhu on 16/08/17.
 */

public class JdCandidatesListViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.item_jd_candidate_list_profile_iv)
    public ImageView mItemJdCandidateListProfileIv;
    @BindView(R.id.item_jd_candidate_list_name_tv)
    public TextView mItemJdCandidateListNameTv;
    @BindView(R.id.item_jd_candidate_list_designation_tv)
    public TextView mItemJdCandidateListDesignationTv;
    @BindView(R.id.item_jd_candidate_list_about_tv)
    public TextView mItemJdCandidateListAboutTv;
    @BindView(R.id.item_jd_candidate_list_time_tv)
    public TextView mItemJdCandidateListTimeTv;

    public JdCandidatesListViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);

    }
}
