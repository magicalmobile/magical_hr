package pt.magical.magicalcareers.holders.recruiter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import pt.magical.magicalcareers.R;

/**
 * Created by RAdhu on 26/08/17.
 */

public class ScreeningQuestionListViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.item_screning_question_edit_bt)
    public TextView mEditBt;
    @BindView(R.id.item_screning_question_remove_bt)
    public TextView mRemoveBt;

    @BindView(R.id.item_screning_question_experience_ll)
    public LinearLayout mExperienceLL;
    @BindView(R.id.item_screning_question_level_ll)
    public LinearLayout mLevelLL;

    @BindView(R.id.item_screning_question_experience_tv)
    public TextView mExperienceTv;
    @BindView(R.id.item_screning_question_level_tv)
    public TextView mLevelTv;
    @BindView(R.id.item_screning_question_skill_tv)
    public TextView mSkillTv;
    @BindView(R.id.item_screning_question_view_name_tv)
    public TextView mQuestionTv;
    @BindView(R.id.item_screning_question_view_option1_tv)
    public TextView mOption1Tv;
    @BindView(R.id.item_screning_question_view_option2_tv)
    public TextView mOption2Tv;
    @BindView(R.id.item_screning_question_view_option3_tv)
    public TextView mOption3Tv;
    @BindView(R.id.item_screning_question_view_option4_tv)
    public TextView mOption4Tv;
    @BindView(R.id.item_screning_question_view_mark_imp_chk)
    public CheckBox mImportanatChk;
    @BindView(R.id.item_screning_question_view_line_bt)
    public View mLineTv;

    public ScreeningQuestionListViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);

    }
}
