package pt.magical.magicalcareers.holders.recruiter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import pt.magical.magicalcareers.R;

/**
 * Created by Sachin on 18 Aug 2017 1:03 AM
 */

public class RecruiterCompanyVideosViewHolder extends RecyclerView.ViewHolder {

    public RecruiterCompanyVideosViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
