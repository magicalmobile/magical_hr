package pt.magical.magicalcareers.holders.recruiter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import pt.magical.magicalcareers.R;

/**
 * Created by RAdhu on 16/08/17.
 */

public class JdListViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.item_jd_list_designation_tv)
    public TextView mItemJdListDesignationTv;

    public JdListViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);

    }
}
