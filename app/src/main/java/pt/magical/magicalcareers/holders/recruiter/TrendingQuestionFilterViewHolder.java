package pt.magical.magicalcareers.holders.recruiter;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import butterknife.ButterKnife;

/**
 * Created by RAdhu on 16/08/17.
 */

public class TrendingQuestionFilterViewHolder extends RecyclerView.ViewHolder {

    /*@BindView(R.id.item_jd_list_designation_tv)
    public TextView mItemJdListDesignationTv;
*/
    public TrendingQuestionFilterViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
