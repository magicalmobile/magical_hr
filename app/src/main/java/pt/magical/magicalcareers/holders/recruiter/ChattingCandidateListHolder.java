package pt.magical.magicalcareers.holders.recruiter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.daimajia.swipe.SwipeLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import pt.magical.magicalcareers.R;

/**
 * Created by RAdhu on 19/08/17.
 */

public class ChattingCandidateListHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.swap_layout)
    public SwipeLayout mSampleLayout;
    @BindView(R.id.item_jd_candidate_list_action_mute)
    public LinearLayout mActionMute;
    @BindView(R.id.item_jd_candidate_list_action_archive)
    public LinearLayout mActionArchive;
    @BindView(R.id.item_jd_candidate_list_action_right)
    public LinearLayout mActionRight;
    @BindView(R.id.item_jd_candidate_list_action_left)
    public LinearLayout mActionleft;
    @BindView(R.id.item_jd_candidate_list_profile_iv)
    public ImageView mProfilePic;


    public ChattingCandidateListHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
