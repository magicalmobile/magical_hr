package pt.magical.magicalcareers.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import pt.magical.magicalcareers.R;

/**
 * Created by Sachin on 7/6/2017.
 */

public class TopRatedDataViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.item_data_tv)
    public TextView mDataTextView;

    @BindView(R.id.item_data_parent_ll)
    public LinearLayout mParentDataLL;

    public TopRatedDataViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
