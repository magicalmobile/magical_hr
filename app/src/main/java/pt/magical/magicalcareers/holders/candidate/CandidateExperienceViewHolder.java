package pt.magical.magicalcareers.holders.candidate;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import pt.magical.magicalcareers.R;

public class CandidateExperienceViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.item_experience_tv)
    public TextView mEexperienceTv;

    @BindView(R.id.year_tv)
    public TextView mYearTv;

    @BindView(R.id.exp_main_ll)
    public LinearLayout mainLL;



    public CandidateExperienceViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);


        // itemRowOfMenuItems.setBackgroundResource(R.drawable.card_background);
    }
}
