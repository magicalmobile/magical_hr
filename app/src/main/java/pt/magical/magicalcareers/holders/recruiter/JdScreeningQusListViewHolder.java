package pt.magical.magicalcareers.holders.recruiter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import pt.magical.magicalcareers.R;

/**
 * Created by RAdhu on 16/08/17.
 */

public class JdScreeningQusListViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.item_viewjd_screening_qus_experience_tv)
    public TextView mExperienceTv;
    @BindView(R.id.item_viewjd_screening_qus_level_tv)
    public TextView mLevelTv;
    @BindView(R.id.item_viewjd_screening_qus_skill_tv)
    public TextView mSkillTv;
    @BindView(R.id.item_viewjd_screening_qus_question_tv)
    public TextView mQuestionTv;
    public JdScreeningQusListViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);

    }
}
