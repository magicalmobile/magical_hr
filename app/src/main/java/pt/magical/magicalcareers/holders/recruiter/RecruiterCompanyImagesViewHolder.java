package pt.magical.magicalcareers.holders.recruiter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import pt.magical.magicalcareers.R;

/**
 * Created by Sachin on 19 Aug 2017 09:25pm
 */

public class RecruiterCompanyImagesViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.item_recruiter_company_image_view)
    public ImageView mItemCompanyIV;

    public RecruiterCompanyImagesViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
