package pt.magical.magicalcareers.holders.candidate;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import pt.magical.magicalcareers.R;

public class CandidateSeeQuestionViewHolder extends RecyclerView.ViewHolder {


    @BindView(R.id.tv)
    public TextView textView;



    public CandidateSeeQuestionViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);


        // itemRowOfMenuItems.setBackgroundResource(R.drawable.card_background);
    }
}
