package pt.magical.magicalcareers.async;

import java.util.Map;

/**
 * Created by jpotts18 on 5/11/15.
 */
public interface IAsyncInteractor {
    //   void validateCredentialsAsync(OnRequestListener listener, String url);
    void validateCredentialsAsync(String method,OnRequestListener listener, int pid, String url);
    void validateCredentialsAsync(String method,OnRequestListener listener, int pid, String url, Map<String, String> data);
    void validateCredentialsMultipartAsync(String method,OnRequestListener listener, int pid, String url, Map<String, String> data,Map<String, String> images);
}
