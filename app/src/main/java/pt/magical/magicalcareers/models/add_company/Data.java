package pt.magical.magicalcareers.models.add_company;

/**
 * Created by Sachin on 8/24/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("company_recruiter")
    @Expose
    private CompanyRecruiter companyRecruiter;

    public CompanyRecruiter getCompanyRecruiter() {
        return companyRecruiter;
    }

    public void setCompanyRecruiter(CompanyRecruiter companyRecruiter) {
        this.companyRecruiter = companyRecruiter;
    }

}
