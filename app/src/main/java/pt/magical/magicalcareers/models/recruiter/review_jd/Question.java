
package pt.magical.magicalcareers.models.recruiter.review_jd;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Question {

    @SerializedName("difficulty_level")
    @Expose
    private Integer difficultyLevel;
    @SerializedName("experience_from ")
    @Expose
    private Integer experienceFrom;
    @SerializedName("skill")
    @Expose
    private String skill;
    @SerializedName("question")
    @Expose
    private String question;
    @SerializedName("experience_to")
    @Expose
    private Integer experienceTo;

    public Integer getDifficultyLevel() {
        return difficultyLevel;
    }

    public void setDifficultyLevel(Integer difficultyLevel) {
        this.difficultyLevel = difficultyLevel;
    }

    public Integer getExperienceFrom() {
        return experienceFrom;
    }

    public void setExperienceFrom(Integer experienceFrom) {
        this.experienceFrom = experienceFrom;
    }

    public String getSkill() {
        return skill;
    }

    public void setSkill(String skill) {
        this.skill = skill;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public Integer getExperienceTo() {
        return experienceTo;
    }

    public void setExperienceTo(Integer experienceTo) {
        this.experienceTo = experienceTo;
    }

}
