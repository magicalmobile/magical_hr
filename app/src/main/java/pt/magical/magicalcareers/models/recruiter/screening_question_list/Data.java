
package pt.magical.magicalcareers.models.recruiter.screening_question_list;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("jd_question_list")
    @Expose
    private List<JdQuestionList> jdQuestionList = null;
    @SerializedName("jd_question_count")
    @Expose
    private Integer jdQuestionCount;

    public List<JdQuestionList> getJdQuestionList() {
        return jdQuestionList;
    }

    public void setJdQuestionList(List<JdQuestionList> jdQuestionList) {
        this.jdQuestionList = jdQuestionList;
    }

    public Integer getJdQuestionCount() {
        return jdQuestionCount;
    }

    public void setJdQuestionCount(Integer jdQuestionCount) {
        this.jdQuestionCount = jdQuestionCount;
    }

}
