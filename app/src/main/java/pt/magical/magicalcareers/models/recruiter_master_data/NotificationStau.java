package pt.magical.magicalcareers.models.recruiter_master_data;

/**
 * Created by Sachin on 8/24/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;
import com.orm.dsl.Table;

@Table(name = "notification_stau_list")
public class NotificationStau extends SugarRecord{

    @SerializedName("notification_id")
    @Expose
    private Integer notificationId;
    @SerializedName("notification_name")
    @Expose
    private String notificationName;

    public Integer getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(Integer notificationId) {
        this.notificationId = notificationId;
    }

    public String getNotificationName() {
        return notificationName;
    }

    public void setNotificationName(String notificationName) {
        this.notificationName = notificationName;
    }

}
