package pt.magical.magicalcareers.models.recruiter_master_data;

/**
 * Created by Sachin on 8/24/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;
import com.orm.dsl.Table;

@Table(name = "institute_list")
public class InstituteList extends SugarRecord {

    @SerializedName("master_institute_type")
    @Expose
    private Integer masterInstituteType;
    @SerializedName("master_location")
    @Expose
    private String masterLocation;
    @SerializedName("master_institute_name")
    @Expose
    private String masterInstituteName;
    @SerializedName("master_institute_type_name")
    @Expose
    private String masterInstituteTypeName;
    @SerializedName("master_university")
    @Expose
    private String masterUniversity;
    @SerializedName("master_institute_id")
    @Expose
    private Integer masterInstituteId;

    public Integer getMasterInstituteType() {
        return masterInstituteType;
    }

    public void setMasterInstituteType(Integer masterInstituteType) {
        this.masterInstituteType = masterInstituteType;
    }

    public String getMasterLocation() {
        return masterLocation;
    }

    public void setMasterLocation(String masterLocation) {
        this.masterLocation = masterLocation;
    }

    public String getMasterInstituteName() {
        return masterInstituteName;
    }

    public void setMasterInstituteName(String masterInstituteName) {
        this.masterInstituteName = masterInstituteName;
    }

    public String getMasterInstituteTypeName() {
        return masterInstituteTypeName;
    }

    public void setMasterInstituteTypeName(String masterInstituteTypeName) {
        this.masterInstituteTypeName = masterInstituteTypeName;
    }

    public String getMasterUniversity() {
        return masterUniversity;
    }

    public void setMasterUniversity(String masterUniversity) {
        this.masterUniversity = masterUniversity;
    }

    public Integer getMasterInstituteId() {
        return masterInstituteId;
    }

    public void setMasterInstituteId(Integer masterInstituteId) {
        this.masterInstituteId = masterInstituteId;
    }

}

