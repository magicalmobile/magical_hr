package pt.magical.magicalcareers.models.forgot_password;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Vivek on 25-08-2017.
 */

public class Data {
    @SerializedName("username")
    @Expose
    private List<String> username = null;

    public List<String> getUsername() {
        return username;
    }

    public void setUsername(List<String> username) {
        this.username = username;
    }



}