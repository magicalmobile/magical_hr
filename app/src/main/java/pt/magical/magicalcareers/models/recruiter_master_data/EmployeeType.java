package pt.magical.magicalcareers.models.recruiter_master_data;

/**
 * Created by Sachin on 8/24/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;
import com.orm.dsl.Table;

@Table(name = "employee_type_list")
public class EmployeeType extends SugarRecord{

    @SerializedName("employee_type_id")
    @Expose
    private Integer employeeTypeId;
    @SerializedName("employee_type")
    @Expose
    private String employeeType;

    public Integer getEmployeeTypeId() {
        return employeeTypeId;
    }

    public void setEmployeeTypeId(Integer employeeTypeId) {
        this.employeeTypeId = employeeTypeId;
    }

    public String getEmployeeType() {
        return employeeType;
    }

    public void setEmployeeType(String employeeType) {
        this.employeeType = employeeType;
    }

}