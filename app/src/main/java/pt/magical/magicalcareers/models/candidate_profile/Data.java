package pt.magical.magicalcareers.models.candidate_profile;

/**
 * Created by Vivek on 25-08-2017.
 */

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;
import com.orm.dsl.Table;

@Table(name = "candidate_profile_data")
public class Data extends SugarRecord {

    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("expected_salary")
    @Expose
    private Integer expectedSalary;
    @SerializedName("is_linked_in_verified")
    @Expose
    private Boolean isLinkedInVerified;
    @SerializedName("profile_image")
    @Expose
    private String profileImage;
    @SerializedName("linked_in_headline")
    @Expose
    private String linkedInHeadline;
    @SerializedName("education_list")
    @Expose
    private List<EducationList> educationList = null;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("resume")
    @Expose
    private String resume;
    @SerializedName("is_anonymous")
    @Expose
    private Boolean isAnonymous;
    @SerializedName("role")
    @Expose
    private Integer role;
    @SerializedName("is_email_verified")
    @Expose
    private Boolean isEmailVerified;
    @SerializedName("profile_completion")
    @Expose
    private String profileCompletion;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("website_url")
    @Expose
    private String websiteUrl;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("bio")
    @Expose
    private String bio;
    @SerializedName("key_skills")
    @Expose
    private List<KeySkill> keySkills = null;
    @SerializedName("notification_status")
    @Expose
    private Integer notificationStatus;
    @SerializedName("industry_type_name")
    @Expose
    private String industryTypeName;
    @SerializedName("is_skill_added")
    @Expose
    private Boolean isSkillAdded;
    @SerializedName("is_phone_verified")
    @Expose
    private Boolean isPhoneVerified;
    @SerializedName("is_education_added")
    @Expose
    private Boolean isEducationAdded;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("role_name")
    @Expose
    private String roleName;
    @SerializedName("linked_in_url")
    @Expose
    private String linkedInUrl;
    @SerializedName("is_notice_period_added")
    @Expose
    private Boolean isNoticePeriodAdded;
    @SerializedName("employee_type")
    @Expose
    private List<EmployeeType> employeeType = null;
    @SerializedName("notice_period")
    @Expose
    private Integer noticePeriod;
    @SerializedName("is_employee_type_added")
    @Expose
    private Boolean isEmployeeTypeAdded;
    @SerializedName("about")
    @Expose
    private String about;
    @SerializedName("notification_status_name")
    @Expose
    private String notificationStatusName;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("designation")
    @Expose
    private String designation;
    @SerializedName("gender")
    @Expose
    private Integer gender;
    @SerializedName("gender_name")
    @Expose
    private String genderName;
    @SerializedName("total_experience")
    @Expose
    private Integer totalExperience;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("industry_type_id")
    @Expose
    private Integer industryTypeId;
    @SerializedName("is_looking_job")
    @Expose
    private Boolean isLookingJob;

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getExpectedSalary() {
        return expectedSalary;
    }

    public void setExpectedSalary(Integer expectedSalary) {
        this.expectedSalary = expectedSalary;
    }

    public Boolean getIsLinkedInVerified() {
        return isLinkedInVerified;
    }

    public void setIsLinkedInVerified(Boolean isLinkedInVerified) {
        this.isLinkedInVerified = isLinkedInVerified;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getLinkedInHeadline() {
        return linkedInHeadline;
    }

    public void setLinkedInHeadline(String linkedInHeadline) {
        this.linkedInHeadline = linkedInHeadline;
    }

    public List<EducationList> getEducationList() {
        return educationList;
    }

    public void setEducationList(List<EducationList> educationList) {
        this.educationList = educationList;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getResume() {
        return resume;
    }

    public void setResume(String resume) {
        this.resume = resume;
    }

    public Boolean getIsAnonymous() {
        return isAnonymous;
    }

    public void setIsAnonymous(Boolean isAnonymous) {
        this.isAnonymous = isAnonymous;
    }

    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }

    public Boolean getIsEmailVerified() {
        return isEmailVerified;
    }

    public void setIsEmailVerified(Boolean isEmailVerified) {
        this.isEmailVerified = isEmailVerified;
    }

    public String getProfileCompletion() {
        return profileCompletion;
    }

    public void setProfileCompletion(String profileCompletion) {
        this.profileCompletion = profileCompletion;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebsiteUrl() {
        return websiteUrl;
    }

    public void setWebsiteUrl(String websiteUrl) {
        this.websiteUrl = websiteUrl;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public List<KeySkill> getKeySkills() {
        return keySkills;
    }

    public void setKeySkills(List<KeySkill> keySkills) {
        this.keySkills = keySkills;
    }

    public Integer getNotificationStatus() {
        return notificationStatus;
    }

    public void setNotificationStatus(Integer notificationStatus) {
        this.notificationStatus = notificationStatus;
    }

    public String getIndustryTypeName() {
        return industryTypeName;
    }

    public void setIndustryTypeName(String industryTypeName) {
        this.industryTypeName = industryTypeName;
    }

    public Boolean getIsSkillAdded() {
        return isSkillAdded;
    }

    public void setIsSkillAdded(Boolean isSkillAdded) {
        this.isSkillAdded = isSkillAdded;
    }

    public Boolean getIsPhoneVerified() {
        return isPhoneVerified;
    }

    public void setIsPhoneVerified(Boolean isPhoneVerified) {
        this.isPhoneVerified = isPhoneVerified;
    }

    public Boolean getIsEducationAdded() {
        return isEducationAdded;
    }

    public void setIsEducationAdded(Boolean isEducationAdded) {
        this.isEducationAdded = isEducationAdded;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getLinkedInUrl() {
        return linkedInUrl;
    }

    public void setLinkedInUrl(String linkedInUrl) {
        this.linkedInUrl = linkedInUrl;
    }

    public Boolean getIsNoticePeriodAdded() {
        return isNoticePeriodAdded;
    }

    public void setIsNoticePeriodAdded(Boolean isNoticePeriodAdded) {
        this.isNoticePeriodAdded = isNoticePeriodAdded;
    }

    public List<EmployeeType> getEmployeeType() {
        return employeeType;
    }

    public void setEmployeeType(List<EmployeeType> employeeType) {
        this.employeeType = employeeType;
    }

    public Integer getNoticePeriod() {
        return noticePeriod;
    }

    public void setNoticePeriod(Integer noticePeriod) {
        this.noticePeriod = noticePeriod;
    }

    public Boolean getIsEmployeeTypeAdded() {
        return isEmployeeTypeAdded;
    }

    public void setIsEmployeeTypeAdded(Boolean isEmployeeTypeAdded) {
        this.isEmployeeTypeAdded = isEmployeeTypeAdded;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getNotificationStatusName() {
        return notificationStatusName;
    }

    public void setNotificationStatusName(String notificationStatusName) {
        this.notificationStatusName = notificationStatusName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getGenderName() {
        return genderName;
    }

    public void setGenderName(String genderName) {
        this.genderName = genderName;
    }

    public Integer getTotalExperience() {
        return totalExperience;
    }

    public void setTotalExperience(Integer totalExperience) {
        this.totalExperience = totalExperience;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Integer getIndustryTypeId() {
        return industryTypeId;
    }

    public void setIndustryTypeId(Integer industryTypeId) {
        this.industryTypeId = industryTypeId;
    }

    public Boolean getIsLookingJob() {
        return isLookingJob;
    }

    public void setIsLookingJob(Boolean isLookingJob) {
        this.isLookingJob = isLookingJob;
    }

}