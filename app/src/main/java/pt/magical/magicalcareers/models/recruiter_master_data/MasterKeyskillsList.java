package pt.magical.magicalcareers.models.recruiter_master_data;

/**
 * Created by Sachin on 8/24/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;
import com.orm.dsl.Table;

@Table(name = "master_skill_list")
public class MasterKeyskillsList extends SugarRecord {

    @SerializedName("master_skill_name")
    @Expose
    private String masterSkillName;
    @SerializedName("master_skill_id")
    @Expose
    private Integer masterSkillId;

    private Integer masterIndustryId;


    public String getMasterSkillName() {
        return masterSkillName;
    }

    public void setMasterSkillName(String masterSkillName) {
        this.masterSkillName = masterSkillName;
    }

    public Integer getMasterSkillId() {
        return masterSkillId;
    }

    public void setMasterSkillId(Integer masterSkillId) {
        this.masterSkillId = masterSkillId;
    }

    public Integer getMasterIndustryId() {
        return masterIndustryId;
    }

    public void setMasterIndustryId(Integer masterIndustryId) {
        this.masterIndustryId = masterIndustryId;
    }
}
