package pt.magical.magicalcareers.models.candidate_profile;

/**
 * Created by Vivek on 25-08-2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;
import com.orm.dsl.Table;

@Table(name = "candidate_education_list")
public class EducationList extends SugarRecord {

    @SerializedName("master_institution_id")
    @Expose
    private Integer masterInstitutionId;
    @SerializedName("master_degree_name")
    @Expose
    private String masterDegreeName;
    @SerializedName("user_education_id")
    @Expose
    private Integer userEducationId;
    @SerializedName("master_degree_id")
    @Expose
    private Integer masterDegreeId;
    @SerializedName("is_latest_degree")
    @Expose
    private Boolean isLatestDegree;
    @SerializedName("master_institution_name")
    @Expose
    private String masterInstitutionName;

    public Integer getMasterInstitutionId() {
        return masterInstitutionId;
    }

    public void setMasterInstitutionId(Integer masterInstitutionId) {
        this.masterInstitutionId = masterInstitutionId;
    }

    public String getMasterDegreeName() {
        return masterDegreeName;
    }

    public void setMasterDegreeName(String masterDegreeName) {
        this.masterDegreeName = masterDegreeName;
    }

    public Integer getUserEducationId() {
        return userEducationId;
    }

    public void setUserEducationId(Integer userEducationId) {
        this.userEducationId = userEducationId;
    }

    public Integer getMasterDegreeId() {
        return masterDegreeId;
    }

    public void setMasterDegreeId(Integer masterDegreeId) {
        this.masterDegreeId = masterDegreeId;
    }

    public Boolean getIsLatestDegree() {
        return isLatestDegree;
    }

    public void setIsLatestDegree(Boolean isLatestDegree) {
        this.isLatestDegree = isLatestDegree;
    }

    public String getMasterInstitutionName() {
        return masterInstitutionName;
    }

    public void setMasterInstitutionName(String masterInstitutionName) {
        this.masterInstitutionName = masterInstitutionName;
    }

}