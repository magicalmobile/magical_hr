package pt.magical.magicalcareers.models.recruiter_master_data;

/**
 * Created by Sachin on 8/24/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;
import com.orm.dsl.Table;

@Table(name = "benfit_list")
public class BenfitsList extends SugarRecord{

    @SerializedName("master_benfit_id")
    @Expose
    private Integer masterBenfitId;
    @SerializedName("master_benfit_name")
    @Expose
    private String masterBenfitName;

    public Integer getMasterBenfitId() {
        return masterBenfitId;
    }

    public void setMasterBenfitId(Integer masterBenfitId) {
        this.masterBenfitId = masterBenfitId;
    }

    public String getMasterBenfitName() {
        return masterBenfitName;
    }

    public void setMasterBenfitName(String masterBenfitName) {
        this.masterBenfitName = masterBenfitName;
    }

}
