package pt.magical.magicalcareers.models.recruiter_master_data;

/**
 * Created by Sachin on 8/24/2017.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;
import com.orm.dsl.Table;

@Table(name = "degree_list")
public class DegreeList extends SugarRecord{

    @SerializedName("master_degree_type")
    @Expose
    private Integer masterDegreeType;
    @SerializedName("master_degree_name")
    @Expose
    private String masterDegreeName;
    @SerializedName("master_degree_type_name")
    @Expose
    private String masterDegreeTypeName;
    @SerializedName("master_degree_id")
    @Expose
    private Integer masterDegreeId;

    public Integer getMasterDegreeType() {
        return masterDegreeType;
    }

    public void setMasterDegreeType(Integer masterDegreeType) {
        this.masterDegreeType = masterDegreeType;
    }

    public String getMasterDegreeName() {
        return masterDegreeName;
    }

    public void setMasterDegreeName(String masterDegreeName) {
        this.masterDegreeName = masterDegreeName;
    }

    public String getMasterDegreeTypeName() {
        return masterDegreeTypeName;
    }

    public void setMasterDegreeTypeName(String masterDegreeTypeName) {
        this.masterDegreeTypeName = masterDegreeTypeName;
    }

    public Integer getMasterDegreeId() {
        return masterDegreeId;
    }

    public void setMasterDegreeId(Integer masterDegreeId) {
        this.masterDegreeId = masterDegreeId;
    }

}