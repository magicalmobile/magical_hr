
package pt.magical.magicalcareers.models.recruiter_profile_data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("is_linked_in_verified")
    @Expose
    private Boolean isLinkedInVerified;
    @SerializedName("profile_image")
    @Expose
    private String profileImage;
    @SerializedName("company_profile")
    @Expose
    private CompanyProfile companyProfile;
    @SerializedName("linked_in_headline")
    @Expose
    private String linkedInHeadline;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("company_id")
    @Expose
    private Integer companyId;
    @SerializedName("role")
    @Expose
    private Integer role;
    @SerializedName("is_email_verified")
    @Expose
    private Boolean isEmailVerified;
    @SerializedName("profile_completion")
    @Expose
    private Integer profileCompletion;
    @SerializedName("is_company_admin")
    @Expose
    private Boolean isCompanyAdmin;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("website_url")
    @Expose
    private String websiteUrl;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("notification_status")
    @Expose
    private Integer notificationStatus;
    @SerializedName("company_name")
    @Expose
    private String companyName;
    @SerializedName("is_phone_verified")
    @Expose
    private Boolean isPhoneVerified;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("role_name")
    @Expose
    private String roleName;
    @SerializedName("linked_in_url")
    @Expose
    private String linkedInUrl;
    @SerializedName("about")
    @Expose
    private String about;
    @SerializedName("notification_status_name")
    @Expose
    private String notificationStatusName;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("gender")
    @Expose
    private Integer gender;
    @SerializedName("gender_name")
    @Expose
    private String genderName;

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Boolean getIsLinkedInVerified() {
        return isLinkedInVerified;
    }

    public void setIsLinkedInVerified(Boolean isLinkedInVerified) {
        this.isLinkedInVerified = isLinkedInVerified;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public CompanyProfile getCompanyProfile() {
        return companyProfile;
    }

    public void setCompanyProfile(CompanyProfile companyProfile) {
        this.companyProfile = companyProfile;
    }

    public String getLinkedInHeadline() {
        return linkedInHeadline;
    }

    public void setLinkedInHeadline(String linkedInHeadline) {
        this.linkedInHeadline = linkedInHeadline;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }

    public Boolean getIsEmailVerified() {
        return isEmailVerified;
    }

    public void setIsEmailVerified(Boolean isEmailVerified) {
        this.isEmailVerified = isEmailVerified;
    }

    public Integer getProfileCompletion() {
        return profileCompletion;
    }

    public void setProfileCompletion(Integer profileCompletion) {
        this.profileCompletion = profileCompletion;
    }

    public Boolean getIsCompanyAdmin() {
        return isCompanyAdmin;
    }

    public void setIsCompanyAdmin(Boolean isCompanyAdmin) {
        this.isCompanyAdmin = isCompanyAdmin;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebsiteUrl() {
        return websiteUrl;
    }

    public void setWebsiteUrl(String websiteUrl) {
        this.websiteUrl = websiteUrl;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getNotificationStatus() {
        return notificationStatus;
    }

    public void setNotificationStatus(Integer notificationStatus) {
        this.notificationStatus = notificationStatus;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Boolean getIsPhoneVerified() {
        return isPhoneVerified;
    }

    public void setIsPhoneVerified(Boolean isPhoneVerified) {
        this.isPhoneVerified = isPhoneVerified;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getLinkedInUrl() {
        return linkedInUrl;
    }

    public void setLinkedInUrl(String linkedInUrl) {
        this.linkedInUrl = linkedInUrl;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getNotificationStatusName() {
        return notificationStatusName;
    }

    public void setNotificationStatusName(String notificationStatusName) {
        this.notificationStatusName = notificationStatusName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getGenderName() {
        return genderName;
    }

    public void setGenderName(String genderName) {
        this.genderName = genderName;
    }

}
