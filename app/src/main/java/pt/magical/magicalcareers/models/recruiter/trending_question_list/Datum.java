
package pt.magical.magicalcareers.models.recruiter.trending_question_list;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;
import com.orm.dsl.Table;

@Table(name = "trending_question_list")
public class Datum extends SugarRecord{

    @SerializedName("experience_to")
    @Expose
    private Integer experienceTo;
    @SerializedName("master_skill")
    @Expose
    private Integer masterSkill;
    @SerializedName("user")
    @Expose
    private String user;
    @SerializedName("answer_list")
    @Expose
    private List<AnswerList> answerList = null;
    @SerializedName("difficulty_level")
    @Expose
    private Integer difficultyLevel;
    @SerializedName("experience_from")
    @Expose
    private Integer experienceFrom;
    @SerializedName("question")
    @Expose
    private String question;
    @SerializedName("master_screening_question.id")
    @Expose
    private Integer masterScreeningQuestionId;

    private boolean isAdded;

    public Integer getExperienceTo() {
        return experienceTo;
    }

    public void setExperienceTo(Integer experienceTo) {
        this.experienceTo = experienceTo;
    }

    public Integer getMasterSkill() {
        return masterSkill;
    }

    public void setMasterSkill(Integer masterSkill) {
        this.masterSkill = masterSkill;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public List<AnswerList> getAnswerList() {
        return answerList;
    }

    public void setAnswerList(List<AnswerList> answerList) {
        this.answerList = answerList;
    }

    public Integer getDifficultyLevel() {
        return difficultyLevel;
    }

    public void setDifficultyLevel(Integer difficultyLevel) {
        this.difficultyLevel = difficultyLevel;
    }

    public Integer getExperienceFrom() {
        return experienceFrom;
    }

    public void setExperienceFrom(Integer experienceFrom) {
        this.experienceFrom = experienceFrom;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public Integer getMasterScreeningQuestionId() {
        return masterScreeningQuestionId;
    }

    public void setMasterScreeningQuestionId(Integer masterScreeningQuestionId) {
        this.masterScreeningQuestionId = masterScreeningQuestionId;
    }

    public boolean getIsAdded() {
        return isAdded;
    }

    public void setIsAdded(boolean isAdded) {
        this.isAdded = isAdded;
    }


}
