
package pt.magical.magicalcareers.models.recruiter.screening_question_list;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;
import com.orm.dsl.Table;

@Table(name = "jd_answer_list")
public class Answer extends SugarRecord{

    @SerializedName("answer")
    @Expose
    private String answer;
    @SerializedName("is_right_answer")
    @Expose
    private Boolean isRightAnswer;

    private int questionId;

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Boolean getIsRightAnswer() {
        return isRightAnswer;
    }

    public void setIsRightAnswer(Boolean isRightAnswer) {
        this.isRightAnswer = isRightAnswer;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

}
