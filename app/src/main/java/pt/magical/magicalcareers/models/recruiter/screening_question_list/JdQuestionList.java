
package pt.magical.magicalcareers.models.recruiter.screening_question_list;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;
import com.orm.dsl.Table;

@Table(name = "jd_question_list")
public class JdQuestionList extends SugarRecord{

    @SerializedName("experience_to")
    @Expose
    private Integer experienceTo;
    @SerializedName("screening_question")
    @Expose
    private String screeningQuestion;
    @SerializedName("answer")
    @Expose
    private List<Answer> answer = null;
    @SerializedName("experience_from")
    @Expose
    private Integer experienceFrom;
    @SerializedName("user")
    @Expose
    private String user;
    @SerializedName("screening_skill")
    @Expose
    private Integer screeningSkill;
    @SerializedName("difficulty_level")
    @Expose
    private Integer difficultyLevel;
    @SerializedName("jd_question_id")
    @Expose
    private Integer jdQuestionId;
    @SerializedName("jd_id")
    @Expose
    private Integer jdId;

    public Integer getExperienceTo() {
        return experienceTo;
    }

    public void setExperienceTo(Integer experienceTo) {
        this.experienceTo = experienceTo;
    }

    public String getScreeningQuestion() {
        return screeningQuestion;
    }

    public void setScreeningQuestion(String screeningQuestion) {
        this.screeningQuestion = screeningQuestion;
    }

    public List<Answer> getAnswer() {
        return answer;
    }

    public void setAnswer(List<Answer> answer) {
        this.answer = answer;
    }

    public Integer getExperienceFrom() {
        return experienceFrom;
    }

    public void setExperienceFrom(Integer experienceFrom) {
        this.experienceFrom = experienceFrom;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Integer getScreeningSkill() {
        return screeningSkill;
    }

    public void setScreeningSkill(Integer screeningSkill) {
        this.screeningSkill = screeningSkill;
    }

    public Integer getDifficultyLevel() {
        return difficultyLevel;
    }

    public void setDifficultyLevel(Integer difficultyLevel) {
        this.difficultyLevel = difficultyLevel;
    }

    public Integer getJdQuestionId() {
        return jdQuestionId;
    }

    public void setJdQuestionId(Integer jdQuestionId) {
        this.jdQuestionId = jdQuestionId;
    }

    public Integer getJdId() {
        return jdId;
    }

    public void setJdId(Integer jdId) {
        this.jdId = jdId;
    }

}
