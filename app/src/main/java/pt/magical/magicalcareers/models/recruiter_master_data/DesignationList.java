package pt.magical.magicalcareers.models.recruiter_master_data;

/**
 * Created by Sachin on 8/24/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;
import com.orm.dsl.Table;

@Table(name = "designation_list")
public class DesignationList extends SugarRecord {

    @SerializedName("master_designation_id")
    @Expose
    private Integer masterDesignationId;
    @SerializedName("master_designation_name")
    @Expose
    private String masterDesignationName;

    public Integer getMasterDesignationId() {
        return masterDesignationId;
    }

    public void setMasterDesignationId(Integer masterDesignationId) {
        this.masterDesignationId = masterDesignationId;
    }

    public String getMasterDesignationName() {
        return masterDesignationName;
    }

    public void setMasterDesignationName(String masterDesignationName) {
        this.masterDesignationName = masterDesignationName;
    }

}
