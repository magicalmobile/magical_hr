package pt.magical.magicalcareers.models.create_jd_flow_data_in_app;

import com.orm.SugarRecord;
import com.orm.dsl.Table;

/**
 * Created by Sachin on 8/25/2017.
 */

@Table(name = "create_jd_flow_data_in_app_model")
public class CreateJdFlowDataInAppModel extends SugarRecord {


    private String companyLogoPath;
    private String companyName;
    private String industryName;
    private String websiteLink;
    private String linkedinLink;

    private String designationValue;
    private String minExpValue;
    private String maxExpValue;
    private String locationValue;

    private String minCtcValue;
    private String maxCtcValue;
    private String benifitsValue;

    private String choosedSkills1Value1;
    private String choosedSkills1Value2;
    private String choosedSkills1Value3;
    private String choosedSkills1Value4;
    private String choosedSkills1Value5;

    private String jobTypeValue;
    private String noticePeriodValue;
    private String highestEducaionValue;
    private String choosedInstituteValue;
    private String choosedGenderValue;
    private String jobDescriptionValue;
    private String jobDescriptionPathValue;


    public String getCompanyLogoPath() {
        return companyLogoPath;
    }

    public void setCompanyLogoPath(String companyLogoPath) {
        this.companyLogoPath = companyLogoPath;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getIndustryName() {
        return industryName;
    }

    public void setIndustryName(String industryName) {
        this.industryName = industryName;
    }

    public String getWebsiteLink() {
        return websiteLink;
    }

    public void setWebsiteLink(String websiteLink) {
        this.websiteLink = websiteLink;
    }

    public String getLinkedinLink() {
        return linkedinLink;
    }

    public void setLinkedinLink(String linkedinLink) {
        this.linkedinLink = linkedinLink;
    }

    public String getDesignationValue() {
        return designationValue;
    }

    public void setDesignationValue(String designationValue) {
        this.designationValue = designationValue;
    }

    public String getMinExpValue() {
        return minExpValue;
    }

    public void setMinExpValue(String minExpValue) {
        this.minExpValue = minExpValue;
    }

    public String getMaxExpValue() {
        return maxExpValue;
    }

    public void setMaxExpValue(String maxExpValue) {
        this.maxExpValue = maxExpValue;
    }

    public String getLocationValue() {
        return locationValue;
    }

    public void setLocationValue(String locationValue) {
        this.locationValue = locationValue;
    }

    public String getMinCtcValue() {
        return minCtcValue;
    }

    public void setMinCtcValue(String minCtcValue) {
        this.minCtcValue = minCtcValue;
    }

    public String getMaxCtcValue() {
        return maxCtcValue;
    }

    public void setMaxCtcValue(String maxCtcValue) {
        this.maxCtcValue = maxCtcValue;
    }

    public String getBenifitsValue() {
        return benifitsValue;
    }

    public void setBenifitsValue(String benifitsValue) {
        this.benifitsValue = benifitsValue;
    }

    public String getChoosedSkills1Value1() {
        return choosedSkills1Value1;
    }

    public void setChoosedSkills1Value1(String choosedSkills1Value1) {
        this.choosedSkills1Value1 = choosedSkills1Value1;
    }

    public String getChoosedSkills1Value2() {
        return choosedSkills1Value2;
    }

    public void setChoosedSkills1Value2(String choosedSkills1Value2) {
        this.choosedSkills1Value2 = choosedSkills1Value2;
    }

    public String getChoosedSkills1Value3() {
        return choosedSkills1Value3;
    }

    public void setChoosedSkills1Value3(String choosedSkills1Value3) {
        this.choosedSkills1Value3 = choosedSkills1Value3;
    }

    public String getChoosedSkills1Value4() {
        return choosedSkills1Value4;
    }

    public void setChoosedSkills1Value4(String choosedSkills1Value4) {
        this.choosedSkills1Value4 = choosedSkills1Value4;
    }

    public String getChoosedSkills1Value5() {
        return choosedSkills1Value5;
    }

    public void setChoosedSkills1Value5(String choosedSkills1Value5) {
        this.choosedSkills1Value5 = choosedSkills1Value5;
    }

    public String getJobTypeValue() {
        return jobTypeValue;
    }

    public void setJobTypeValue(String jobTypeValue) {
        this.jobTypeValue = jobTypeValue;
    }

    public String getNoticePeriodValue() {
        return noticePeriodValue;
    }

    public void setNoticePeriodValue(String noticePeriodValue) {
        this.noticePeriodValue = noticePeriodValue;
    }

    public String getHighestEducaionValue() {
        return highestEducaionValue;
    }

    public void setHighestEducaionValue(String highestEducaionValue) {
        this.highestEducaionValue = highestEducaionValue;
    }

    public String getChoosedInstituteValue() {
        return choosedInstituteValue;
    }

    public void setChoosedInstituteValue(String choosedInstituteValue) {
        this.choosedInstituteValue = choosedInstituteValue;
    }

    public String getChoosedGenderValue() {
        return choosedGenderValue;
    }

    public void setChoosedGenderValue(String choosedGenderValue) {
        this.choosedGenderValue = choosedGenderValue;
    }

    public String getJobDescriptionValue() {
        return jobDescriptionValue;
    }

    public void setJobDescriptionValue(String jobDescriptionValue) {
        this.jobDescriptionValue = jobDescriptionValue;
    }

    public String getJobDescriptionPathValue() {
        return jobDescriptionPathValue;
    }

    public void setJobDescriptionPathValue(String jobDescriptionPathValue) {
        this.jobDescriptionPathValue = jobDescriptionPathValue;
    }
}
