
package pt.magical.magicalcareers.models.recruiter.trending_question_list.error;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("master_key_skill")
    @Expose
    private List<String> masterKeySkill = null;

    public List<String> getMasterKeySkill() {
        return masterKeySkill;
    }

    public void setMasterKeySkill(List<String> masterKeySkill) {
        this.masterKeySkill = masterKeySkill;
    }

}
