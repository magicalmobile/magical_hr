package pt.magical.magicalcareers.models.candidate_profile;

/**
 * Created by Vivek on 25-08-2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;
import com.orm.dsl.Table;

@Table(name = "candidate_employee_type")
public class EmployeeType extends SugarRecord {

    @SerializedName("employee_type_id")
    @Expose
    private Integer employeeTypeId;
    @SerializedName("employee_type_name")
    @Expose
    private String employeeTypeName;

    public Integer getEmployeeTypeId() {
        return employeeTypeId;
    }

    public void setEmployeeTypeId(Integer employeeTypeId) {
        this.employeeTypeId = employeeTypeId;
    }

    public String getEmployeeTypeName() {
        return employeeTypeName;
    }

    public void setEmployeeTypeName(String employeeTypeName) {
        this.employeeTypeName = employeeTypeName;
    }

}