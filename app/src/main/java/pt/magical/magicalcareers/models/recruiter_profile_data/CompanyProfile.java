
package pt.magical.magicalcareers.models.recruiter_profile_data;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CompanyProfile {

    @SerializedName("company_logo")
    @Expose
    private String companyLogo;
    @SerializedName("company_rockstars")
    @Expose
    private List<Object> companyRockstars = null;
    @SerializedName("linked_in_url")
    @Expose
    private String linkedInUrl;
    @SerializedName("media_images")
    @Expose
    private List<Object> mediaImages = null;
    @SerializedName("company_banner")
    @Expose
    private String companyBanner;
    @SerializedName("company_email")
    @Expose
    private String companyEmail;
    @SerializedName("company_phone")
    @Expose
    private String companyPhone;
    @SerializedName("about")
    @Expose
    private String about;
    @SerializedName("is_company_added")
    @Expose
    private Boolean isCompanyAdded;
    @SerializedName("media_videos")
    @Expose
    private List<Object> mediaVideos = null;
    @SerializedName("company_name")
    @Expose
    private String companyName;
    @SerializedName("employee_count")
    @Expose
    private Integer employeeCount;
    @SerializedName("industry_type")
    @Expose
    private String industryType;
    @SerializedName("website_url")
    @Expose
    private String websiteUrl;
    @SerializedName("location")
    @Expose
    private String location;

    public String getCompanyLogo() {
        return companyLogo;
    }

    public void setCompanyLogo(String companyLogo) {
        this.companyLogo = companyLogo;
    }

    public List<Object> getCompanyRockstars() {
        return companyRockstars;
    }

    public void setCompanyRockstars(List<Object> companyRockstars) {
        this.companyRockstars = companyRockstars;
    }

    public String getLinkedInUrl() {
        return linkedInUrl;
    }

    public void setLinkedInUrl(String linkedInUrl) {
        this.linkedInUrl = linkedInUrl;
    }

    public List<Object> getMediaImages() {
        return mediaImages;
    }

    public void setMediaImages(List<Object> mediaImages) {
        this.mediaImages = mediaImages;
    }

    public String getCompanyBanner() {
        return companyBanner;
    }

    public void setCompanyBanner(String companyBanner) {
        this.companyBanner = companyBanner;
    }

    public String getCompanyEmail() {
        return companyEmail;
    }

    public void setCompanyEmail(String companyEmail) {
        this.companyEmail = companyEmail;
    }

    public String getCompanyPhone() {
        return companyPhone;
    }

    public void setCompanyPhone(String companyPhone) {
        this.companyPhone = companyPhone;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public Boolean getIsCompanyAdded() {
        return isCompanyAdded;
    }

    public void setIsCompanyAdded(Boolean isCompanyAdded) {
        this.isCompanyAdded = isCompanyAdded;
    }

    public List<Object> getMediaVideos() {
        return mediaVideos;
    }

    public void setMediaVideos(List<Object> mediaVideos) {
        this.mediaVideos = mediaVideos;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Integer getEmployeeCount() {
        return employeeCount;
    }

    public void setEmployeeCount(Integer employeeCount) {
        this.employeeCount = employeeCount;
    }

    public String getIndustryType() {
        return industryType;
    }

    public void setIndustryType(String industryType) {
        this.industryType = industryType;
    }

    public String getWebsiteUrl() {
        return websiteUrl;
    }

    public void setWebsiteUrl(String websiteUrl) {
        this.websiteUrl = websiteUrl;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

}
