package pt.magical.magicalcareers.models.recruiter_master_data;

/**
 * Created by Sachin on 8/24/2017.
 */
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;
import com.orm.dsl.Table;

@Table(name = "industry_list")
public class IndustryList extends SugarRecord{

    @SerializedName("master_industry_name")
    @Expose
    private String masterIndustryName;
    @SerializedName("master_keyskills_list")
    @Expose
    private List<MasterKeyskillsList> masterKeyskillsList = null;
    @SerializedName("master_industry_id")
    @Expose
    private Integer masterIndustryId;

    public String getMasterIndustryName() {
        return masterIndustryName;
    }

    public void setMasterIndustryName(String masterIndustryName) {
        this.masterIndustryName = masterIndustryName;
    }

    public List<MasterKeyskillsList> getMasterKeyskillsList() {
        return masterKeyskillsList;
    }

    public void setMasterKeyskillsList(List<MasterKeyskillsList> masterKeyskillsList) {
        this.masterKeyskillsList = masterKeyskillsList;
    }

    public Integer getMasterIndustryId() {
        return masterIndustryId;
    }

    public void setMasterIndustryId(Integer masterIndustryId) {
        this.masterIndustryId = masterIndustryId;
    }

}