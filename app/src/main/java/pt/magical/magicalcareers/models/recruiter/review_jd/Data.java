
package pt.magical.magicalcareers.models.recruiter.review_jd;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("experience_from")
    @Expose
    private Integer experienceFrom;
    @SerializedName("experience_to")
    @Expose
    private Integer experienceTo;
    @SerializedName("designation")
    @Expose
    private String designation;
    @SerializedName("jd_file")
    @Expose
    private String jdFile;
    @SerializedName("skills")
    @Expose
    private List<String> skills = null;
    @SerializedName("company_logo")
    @Expose
    private String companyLogo;
    @SerializedName("job_type")
    @Expose
    private List<String> jobType = null;
    @SerializedName("ctc_to")
    @Expose
    private Integer ctcTo;
    @SerializedName("linked_in_url")
    @Expose
    private String linkedInUrl;
    @SerializedName("ctc_from")
    @Expose
    private Integer ctcFrom;
    @SerializedName("job_description")
    @Expose
    private String jobDescription;
    @SerializedName("company_name")
    @Expose
    private String companyName;
    @SerializedName("questions")
    @Expose
    private List<Question> questions = null;
    @SerializedName("highest_education_level")
    @Expose
    private String highestEducationLevel;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("benefits")
    @Expose
    private List<String> benefits = null;
    @SerializedName("company_banner")
    @Expose
    private String companyBanner;
    @SerializedName("notice_period")
    @Expose
    private Integer noticePeriod;
    @SerializedName("website_url")
    @Expose
    private String websiteUrl;
    @SerializedName("location")
    @Expose
    private String location;

    public Integer getExperienceFrom() {
        return experienceFrom;
    }

    public void setExperienceFrom(Integer experienceFrom) {
        this.experienceFrom = experienceFrom;
    }

    public Integer getExperienceTo() {
        return experienceTo;
    }

    public void setExperienceTo(Integer experienceTo) {
        this.experienceTo = experienceTo;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getJdFile() {
        return jdFile;
    }

    public void setJdFile(String jdFile) {
        this.jdFile = jdFile;
    }

    public List<String> getSkills() {
        return skills;
    }

    public void setSkills(List<String> skills) {
        this.skills = skills;
    }

    public String getCompanyLogo() {
        return companyLogo;
    }

    public void setCompanyLogo(String companyLogo) {
        this.companyLogo = companyLogo;
    }

    public List<String> getJobType() {
        return jobType;
    }

    public void setJobType(List<String> jobType) {
        this.jobType = jobType;
    }

    public Integer getCtcTo() {
        return ctcTo;
    }

    public void setCtcTo(Integer ctcTo) {
        this.ctcTo = ctcTo;
    }

    public String getLinkedInUrl() {
        return linkedInUrl;
    }

    public void setLinkedInUrl(String linkedInUrl) {
        this.linkedInUrl = linkedInUrl;
    }

    public Integer getCtcFrom() {
        return ctcFrom;
    }

    public void setCtcFrom(Integer ctcFrom) {
        this.ctcFrom = ctcFrom;
    }

    public String getJobDescription() {
        return jobDescription;
    }

    public void setJobDescription(String jobDescription) {
        this.jobDescription = jobDescription;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    public String getHighestEducationLevel() {
        return highestEducationLevel;
    }

    public void setHighestEducationLevel(String highestEducationLevel) {
        this.highestEducationLevel = highestEducationLevel;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<String> getBenefits() {
        return benefits;
    }

    public void setBenefits(List<String> benefits) {
        this.benefits = benefits;
    }

    public String getCompanyBanner() {
        return companyBanner;
    }

    public void setCompanyBanner(String companyBanner) {
        this.companyBanner = companyBanner;
    }

    public Integer getNoticePeriod() {
        return noticePeriod;
    }

    public void setNoticePeriod(Integer noticePeriod) {
        this.noticePeriod = noticePeriod;
    }

    public String getWebsiteUrl() {
        return websiteUrl;
    }

    public void setWebsiteUrl(String websiteUrl) {
        this.websiteUrl = websiteUrl;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

}
