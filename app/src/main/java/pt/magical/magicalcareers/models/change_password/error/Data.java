package pt.magical.magicalcareers.models.change_password.error;

/**
 * Created by Sachin on 8/26/2017.
 */


import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("new_password")
    @Expose
    private List<String> newPassword = null;
    @SerializedName("current_password")
    @Expose
    private List<String> currentPassword = null;

    public List<String> getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(List<String> newPassword) {
        this.newPassword = newPassword;
    }

    public List<String> getCurrentPassword() {
        return currentPassword;
    }

    public void setCurrentPassword(List<String> currentPassword) {
        this.currentPassword = currentPassword;
    }

}
