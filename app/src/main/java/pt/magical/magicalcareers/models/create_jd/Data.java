
package pt.magical.magicalcareers.models.create_jd;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("jd_id")
    @Expose
    private Integer jdId;
    @SerializedName("ques_count")
    @Expose
    private Integer quesCount;

    public Integer getJdId() {
        return jdId;
    }

    public void setJdId(Integer jdId) {
        this.jdId = jdId;
    }

    public Integer getQuesCount() {
        return quesCount;
    }

    public void setQuesCount(Integer quesCount) {
        this.quesCount = quesCount;
    }

}
