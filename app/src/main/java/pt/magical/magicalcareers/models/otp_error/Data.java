package pt.magical.magicalcareers.models.otp_error;

/**
 * Created by Vivek on 25-08-2017.
 */
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("otp")
    @Expose
    private List<String> otp = null;

    public List<String> getOtp() {
        return otp;
    }

    public void setOtp(List<String> otp) {
        this.otp = otp;
    }

}