package pt.magical.magicalcareers.models.add_company.error;

/**
 * Created by Sachin on 8/25/2017.
 */

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("error_message")
    @Expose
    private List<String> errorMessage = null;

    @SerializedName("linked_in_url")
    @Expose
    private List<String> linkedInUrl = null;
    @SerializedName("website_url")
    @Expose
    private List<String> websiteUrl = null;

    public List<String> getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(List<String> errorMessage) {
        this.errorMessage = errorMessage;
    }

    public List<String> getLinkedInUrl() {
        return linkedInUrl;
    }

    public void setLinkedInUrl(List<String> linkedInUrl) {
        this.linkedInUrl = linkedInUrl;
    }

    public List<String> getWebsiteUrl() {
        return websiteUrl;
    }

    public void setWebsiteUrl(List<String> websiteUrl) {
        this.websiteUrl = websiteUrl;
    }

}
