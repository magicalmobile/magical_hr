package pt.magical.magicalcareers.models.candidate_profile;

/**
 * Created by Vivek on 25-08-2017.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;
import com.orm.dsl.Table;

@Table(name = "candidate_key_skill")
public class KeySkill  extends SugarRecord{

    @SerializedName("user_skill_id")
    @Expose
    private Integer userSkillId;
    @SerializedName("rating")
    @Expose
    private Integer rating;
    @SerializedName("master_skill_name")
    @Expose
    private String masterSkillName;
    @SerializedName("experience")
    @Expose
    private Integer experience;
    @SerializedName("master_skill_id")
    @Expose
    private Integer masterSkillId;

    public Integer getUserSkillId() {
        return userSkillId;
    }

    public void setUserSkillId(Integer userSkillId) {
        this.userSkillId = userSkillId;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public String getMasterSkillName() {
        return masterSkillName;
    }

    public void setMasterSkillName(String masterSkillName) {
        this.masterSkillName = masterSkillName;
    }

    public Integer getExperience() {
        return experience;
    }

    public void setExperience(Integer experience) {
        this.experience = experience;
    }

    public Integer getMasterSkillId() {
        return masterSkillId;
    }

    public void setMasterSkillId(Integer masterSkillId) {
        this.masterSkillId = masterSkillId;
    }

}