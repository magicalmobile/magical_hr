package pt.magical.magicalcareers.models.recruiter_profile_data.error;

/**
 * Created by Sachin on 8/26/2017.
 */

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("error_message")
    @Expose
    private List<String> errorMessage = null;

    public List<String> getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(List<String> errorMessage) {
        this.errorMessage = errorMessage;
    }

}
