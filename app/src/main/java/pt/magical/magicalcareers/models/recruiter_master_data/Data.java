package pt.magical.magicalcareers.models.recruiter_master_data;

/**
 * Created by Sachin on 8/24/2017.
 */

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;
import com.orm.dsl.Table;

@Table(name = "recruiter_side_master_data")
public class Data extends SugarRecord {

    @SerializedName("designation_list")
    @Expose
    private List<DesignationList> designationList = null;
    @SerializedName("employee_types")
    @Expose
    private List<EmployeeType> employeeTypes = null;
    @SerializedName("notification_staus")
    @Expose
    private List<NotificationStau> notificationStaus = null;
    @SerializedName("ctc_range_end")
    @Expose
    private Integer ctcRangeEnd;
    @SerializedName("ctc_unit")
    @Expose
    private String ctcUnit;
    @SerializedName("degree_list")
    @Expose
    private List<DegreeList> degreeList = null;
    @SerializedName("ctc_range_start")
    @Expose
    private Integer ctcRangeStart;
    @SerializedName("institute_list")
    @Expose
    private List<InstituteList> instituteList = null;
    @SerializedName("industry_list")
    @Expose
    private List<IndustryList> industryList = null;
    @SerializedName("benfits_list")
    @Expose
    private List<BenfitsList> benfitsList = null;

    public List<DesignationList> getDesignationList() {
        return designationList;
    }

    public void setDesignationList(List<DesignationList> designationList) {
        this.designationList = designationList;
    }

    public List<EmployeeType> getEmployeeTypes() {
        return employeeTypes;
    }

    public void setEmployeeTypes(List<EmployeeType> employeeTypes) {
        this.employeeTypes = employeeTypes;
    }

    public List<NotificationStau> getNotificationStaus() {
        return notificationStaus;
    }

    public void setNotificationStaus(List<NotificationStau> notificationStaus) {
        this.notificationStaus = notificationStaus;
    }

    public Integer getCtcRangeEnd() {
        return ctcRangeEnd;
    }

    public void setCtcRangeEnd(Integer ctcRangeEnd) {
        this.ctcRangeEnd = ctcRangeEnd;
    }

    public String getCtcUnit() {
        return ctcUnit;
    }

    public void setCtcUnit(String ctcUnit) {
        this.ctcUnit = ctcUnit;
    }

    public List<DegreeList> getDegreeList() {
        return degreeList;
    }

    public void setDegreeList(List<DegreeList> degreeList) {
        this.degreeList = degreeList;
    }

    public Integer getCtcRangeStart() {
        return ctcRangeStart;
    }

    public void setCtcRangeStart(Integer ctcRangeStart) {
        this.ctcRangeStart = ctcRangeStart;
    }

    public List<InstituteList> getInstituteList() {
        return instituteList;
    }

    public void setInstituteList(List<InstituteList> instituteList) {
        this.instituteList = instituteList;
    }

    public List<IndustryList> getIndustryList() {
        return industryList;
    }

    public void setIndustryList(List<IndustryList> industryList) {
        this.industryList = industryList;
    }

    public List<BenfitsList> getBenfitsList() {
        return benfitsList;
    }

    public void setBenfitsList(List<BenfitsList> benfitsList) {
        this.benfitsList = benfitsList;
    }

}
