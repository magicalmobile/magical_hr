package pt.magical.magicalcareers.models.add_company;

/**
 * Created by Sachin on 8/24/2017.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CompanyRecruiter {

    @SerializedName("master_industry_type")
    @Expose
    private Integer masterIndustryType;
    @SerializedName("company_logo")
    @Expose
    private String companyLogo;
    @SerializedName("user")
    @Expose
    private Integer user;
    @SerializedName("linked_in_url")
    @Expose
    private String linkedInUrl;
    @SerializedName("website_url")
    @Expose
    private String websiteUrl;
    @SerializedName("company_name")
    @Expose
    private String companyName;

    public Integer getMasterIndustryType() {
        return masterIndustryType;
    }

    public void setMasterIndustryType(Integer masterIndustryType) {
        this.masterIndustryType = masterIndustryType;
    }

    public String getCompanyLogo() {
        return companyLogo;
    }

    public void setCompanyLogo(String companyLogo) {
        this.companyLogo = companyLogo;
    }

    public Integer getUser() {
        return user;
    }

    public void setUser(Integer user) {
        this.user = user;
    }

    public String getLinkedInUrl() {
        return linkedInUrl;
    }

    public void setLinkedInUrl(String linkedInUrl) {
        this.linkedInUrl = linkedInUrl;
    }

    public String getWebsiteUrl() {
        return websiteUrl;
    }

    public void setWebsiteUrl(String websiteUrl) {
        this.websiteUrl = websiteUrl;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

}

