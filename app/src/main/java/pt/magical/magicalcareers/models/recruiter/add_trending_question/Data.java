
package pt.magical.magicalcareers.models.recruiter.add_trending_question;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("master_screening_question.id")
    @Expose
    private Integer masterScreeningQuestionId;
    @SerializedName("jd_id")
    @Expose
    private Integer jdId;

    public Integer getMasterScreeningQuestionId() {
        return masterScreeningQuestionId;
    }

    public void setMasterScreeningQuestionId(Integer masterScreeningQuestionId) {
        this.masterScreeningQuestionId = masterScreeningQuestionId;
    }

    public Integer getJdId() {
        return jdId;
    }

    public void setJdId(Integer jdId) {
        this.jdId = jdId;
    }

}
