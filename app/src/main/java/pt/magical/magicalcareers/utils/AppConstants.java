package pt.magical.magicalcareers.utils;

import org.apache.http.NameValuePair;

import java.util.List;

/**
 * Created By Sachin Rajput 3August2017
 */

public class AppConstants {


    public static int SUCCESS_CODE = 200;


    public static final int LINKEDIN_REQUEST_CODE = 3672;
    public static int TAG_ID_RECRUITER_MASTER_DATA = 101;
    public static int TAG_ID_CREATE_JD = 102;
    public static int TAG_ID_ADD_COMPANY = 103;
    public static int TAG_ID_RECRUITER_REVIEW_JD = 104;
    public static int TAG_ID_RECRUITER_PROFILE_DATA = 105;
    public static int TAG_ID_TERMS_AND_CONDITIONS = 106;
    public static int TAG_ID_CHANGE_PASSWORD = 107;
    public static int TAG_ID_RECRUITER_TRENDING_QUESTION = 108;
    public static int TAG_ID_RECRUITER_SCREENING_QUESTION = 109;
    public static int TAG_ID_SET_PASSWORD_ACTIVITY=110;
    public static int TAG_ID_RESEND_OTP=111;

    public static int TAG_ID_RECRUITER_ADD_TRENDING_QUESTION = 112;
    public static int TAG_ID_RECRUITER_ADD_QUESTION = 113;
    public static int TAG_ID_CUSTOM_PROCEED = 114;
    public static int TAG_ID_CUSTOM_REGISTER = 115;
    public static int TAG_ID_FORGOT_PASSWORD= 116;
    public static int TAG_ID_FORGOT_PASSWORD_VERIFY_OTP_CONFIRMATION=117;
    public static int TAG_ID_CUSTOM_LOGIN=118;
    public static int TAG_ID_LINKED_LOGIN=119;


    public static boolean IS_LIVE_BUILD = false;
    private static String BASE_URL;
    public static String WEBSERVICE_HOST;


    static {
        if (IS_LIVE_BUILD) {
            WEBSERVICE_HOST = "http://192.168.1.30:8000/app";
        } else {
            WEBSERVICE_HOST = "http://192.168.1.30:8000/app";

        }
        BASE_URL = WEBSERVICE_HOST;
    }

    /*
     * SERVER CALLS

    /*
     *  Enum to get URL and TAG names
     */
    public static enum URL {

        BASE_URL(""),
        RECRUITER_MASTER_DATA("recruiter/recruiter_master_data/"),
        RECRUITER_REVIEW_JD("recruiter/review_and_publish_jd/"),
        RECRUITER_ADD_TRENDING_QUESTION("recruiter/add_trending_question/"),
        RECRUITER_ADD_QUESTION("recruiter/add_custom_jd_questions/"),
        RECRUITER_QUESTION_LIST("recruiter/master_skill_question/"),
        RECRUITER_SCREENING_QUESTION_LIST("recruiter/jd_question_list/"),
        CREATE_JD("recruiter/createjd/"),
        ADD_COMPANY("recruiter/add_company/"),

        RECRUITER_PROFILE_DATA("userinfo/profile/"),
        TERMS_AND_CONDITIONS("userinfo/terms_and_conditions/"),
        CHANGE_PASSWORD("userinfo/change_password/"),

        SET_PASSWORD("userinfo/set_password/"),
        RESNED_OTP("userinfo/resend_otp/"),
        FORGOT_PASSWORD("userinfo/forgot_password/"),
        FORGOT_PASSWORD_OTP_CONFIRM("userinfo/confirm_otp/"),
        CUSTOM_PROCEED("userinfo/process_username/"),
        CUSTOM_REGISTER("userinfo/user_register/"),
        CUSTOM_LOGIN("userinfo/login/"),
        LINKED_LOGIN("userinfo/login/");




        private String urlBase;
        private String url;

        public String baseUrl = "http://192.168.1.35:8000/";

        private URL(String url) {
            this.url = baseUrl + url; this.urlBase = baseUrl;
        }


        public String getUrl() {
            return url;
        }
        public String getBaseUrl() {
            return urlBase;
        }
    }


    public static enum TAGNAME {

        EMAIL("email"), PASSWORD("password"), UID("uid"), TAGS("tags"), DESCRIPTION("description"), SRC
                ("src"), MOBILE("mobile"), KEY("key"), DATA
                ("data"), STATUS("status"), USERID("userId"), DEVICEID("deviceId"), MESSAGE("message"), CMD
                ("cmd"), DEVICE_ID_GCM("deviceid"), FILES("files"),
        LOCATION("location"), LATITUDE("latitude"), LONGITUDE("longitude"), GCMTOKEN("gcmtoken"), TOKEN
                ("token"), FOLLOWERPHONENO("follwerPhoneno"),;

        private String value;

        private TAGNAME(String tag) {
            this.value = tag;
        }

        public String getValue() {
            return value;
        }
    }
   /* ---------------------*/
}
