package pt.magical.magicalcareers.adapters.common;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import com.twilio.chat.Message;

import java.util.ArrayList;

import pt.magical.magicalcareers.R;

/**
 * Created by danish on 19-08-2017.
 */

public class UserChatAdapter extends RecyclerView.Adapter<UserChatAdapter.ViewHolder> {

    private final Context context;
    private final ArrayList<Message> mMessages;

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView mMessageTextView;

        public ViewHolder(TextView textView) {
            super(textView);
            mMessageTextView = textView;
        }
    }

    public UserChatAdapter(Context context, ArrayList<Message> mMessages) {
        this.context=context;
        this.mMessages=mMessages;

    }

    @Override
    public UserChatAdapter
            .ViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        TextView messageTextView = (TextView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_chat_textview, parent, false);
        return new ViewHolder(messageTextView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Message message = mMessages.get(position);
        String messageText = String.format("%s: %s", message.getAuthor(), message.getMessageBody());
        holder.mMessageTextView.setText(messageText);

    }

    @Override
    public int getItemCount() {
        return mMessages.size();
    }
}