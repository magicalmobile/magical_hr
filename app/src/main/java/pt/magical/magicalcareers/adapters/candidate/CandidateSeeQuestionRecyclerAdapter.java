package pt.magical.magicalcareers.adapters.candidate;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;

import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.holders.candidate.CandidateSeeQuestionViewHolder;
import pt.magical.magicalcareers.views.activities.candidate.CandidateSeeQuestionActivity;

/**
 * Updated By Vivek 29 July 2017.
 */

public class CandidateSeeQuestionRecyclerAdapter extends RecyclerView.Adapter<CandidateSeeQuestionViewHolder> {

    private Context context;

    public CandidateSeeQuestionRecyclerAdapter(Context context) {
        this.context = context;

    }


    @Override
    public CandidateSeeQuestionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_row_candidate_see_question, parent, false);
        return new CandidateSeeQuestionViewHolder(view);

    }

    @Override
    public void onBindViewHolder(CandidateSeeQuestionViewHolder holder, int position) {

    }


    @Override
    public int getItemCount() {
        return 10;
    }
}
