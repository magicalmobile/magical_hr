package pt.magical.magicalcareers.adapters.candidate;

/**
 * Created by danish on 18-08-2017.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import pt.magical.magicalcareers.R;


public class CandidateSkillsDialogAdapter extends RecyclerView.Adapter<CandidateSkillsDialogAdapter.DataObjectHolder>{

    private final Context context;
    private final List<String> list;
    private transient onItemClick mClick;

    public interface onItemClick{
        void itemClick(int pos,String name);
    }

    public CandidateSkillsDialogAdapter(Context context,onItemClick activity, List<String> list) {
        this.context = context;
        this.list=list;
        this.mClick=activity;
    }


    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_candidate_skills_dialog, parent, false);
        return new DataObjectHolder(view);
    }


    @Override
    public void onBindViewHolder(final DataObjectHolder holder, final int position) {
        try {
            holder.titleSkillTv.setText(list.get(position));

            holder.titleSkillTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mClick.itemClick(position,list.get(position));
                }
            });

        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public class DataObjectHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_skills_title_tv)
        protected TextView titleSkillTv;
       /* @BindView(R.id.item_skills_check_iv)
        protected ImageView checkIV;
*/

        public DataObjectHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
