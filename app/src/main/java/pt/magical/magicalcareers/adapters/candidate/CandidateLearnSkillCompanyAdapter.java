package pt.magical.magicalcareers.adapters.candidate;

/**
 * Created by danish on 15-08-2017.
 */

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.models.candidate.learn_test.LearnTestList;

public class CandidateLearnSkillCompanyAdapter extends BaseAdapter {

    private final int optionSelect;
    private final int cardPos;
    private final String type;
    private final String skillName;
    private Context mContext;
    private List<LearnTestList> data;
    private TextView companyNameTV;

    private RelativeLayout selectedOption1RL;
    private RelativeLayout selectedOption2RL;
    private RelativeLayout selectedOption3RL;
    private RelativeLayout selectedOption4RL;
    private RelativeLayout unselectOption4RL;
    private RelativeLayout unselectOption1RL;
    private RelativeLayout unselectOption2RL;
    private RelativeLayout unselectOption3RL;
    private TextView unselectOption1TV;
    private TextView unselectOption2TV;
    private TextView unselectOption3TV;
    private TextView unselectOption4TV;
    private TextView questionTV;
    private RelativeLayout mOption1RL;
    private RelativeLayout mOption2RL;
    private RelativeLayout mOption3RL;
    private RelativeLayout mOption4RL;

    private transient learnOptionClick mOptionClick;
    private RelativeLayout skillRL;
    private RelativeLayout companyRL;
    private TextView skillNameTV;


    public interface learnOptionClick{
        void learnOptionClick(int Position,int option,String type);
    }

    public CandidateLearnSkillCompanyAdapter(Context mContext, learnOptionClick activity, List<LearnTestList> data, int cardPos, int option, String type, String skillName) {
        this.data = data;
        this.mContext = mContext;
        this.mOptionClick=activity;
        this.cardPos=cardPos;
        this.optionSelect=option;
        this.type=type;
        this.skillName=skillName;
    }


    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;

    }

    @Override
    public View getView(final int position, final View convertView, final ViewGroup parent) {
        Log.d("getView pos",""+position);
        View v = convertView;

        if (v == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            v = inflater.inflate(R.layout.item_card_learn_candidate_dashboard, parent, false);

            companyNameTV = (TextView) v.findViewById(R.id.item_learn_company_name_tv);
            mOption1RL = (RelativeLayout) v.findViewById(R.id.learn_option1_main_rl);
            mOption2RL = (RelativeLayout) v.findViewById(R.id.learn_option2_main_rl);
            mOption3RL = (RelativeLayout) v.findViewById(R.id.learn_option3_main_rl);
            mOption4RL = (RelativeLayout) v.findViewById(R.id.learn_option4_main_rl);
            skillRL = (RelativeLayout) v.findViewById(R.id.item_learn_skill_rl);
            companyRL = (RelativeLayout) v.findViewById(R.id.item_learn_company_rl);


            selectedOption1RL = (RelativeLayout) v.findViewById(R.id.learn_option1_select_rl);
            selectedOption2RL = (RelativeLayout) v.findViewById(R.id.learn_option2_select_rl);
            selectedOption3RL = (RelativeLayout) v.findViewById(R.id.learn_option3_select_rl);
            selectedOption4RL = (RelativeLayout) v.findViewById(R.id.learn_option4_select_rl);
            unselectOption1RL = (RelativeLayout) v.findViewById(R.id.learn_option1_unselect_rl);
            unselectOption2RL = (RelativeLayout) v.findViewById(R.id.learn_option2_unselect_rl);
            unselectOption3RL = (RelativeLayout) v.findViewById(R.id.learn_option3_unselect_rl);
            unselectOption4RL = (RelativeLayout) v.findViewById(R.id.learn_option4_unselect_rl);

            unselectOption1TV = (TextView) v.findViewById(R.id.learn_unselect_option1_tv);
            unselectOption2TV = (TextView) v.findViewById(R.id.learn_unselect_option2_tv);
            unselectOption3TV = (TextView) v.findViewById(R.id.learn_unselect_option3_tv);
            unselectOption4TV = (TextView) v.findViewById(R.id.learn_unselect_option4_tv);
            questionTV = (TextView) v.findViewById(R.id.learn_question_tv);
            skillNameTV=(TextView)v.findViewById(R.id.item_learn_skill_name_tv);
        }

        if (type.equalsIgnoreCase("skills")){
            skillRL.setVisibility(View.GONE);
            companyRL.setVisibility(View.VISIBLE);
        }else if (type.equalsIgnoreCase("company")){
            skillRL.setVisibility(View.VISIBLE);
            companyRL.setVisibility(View.GONE);
            skillNameTV.setText(skillName);
        }

        questionTV.setText(data.get(position).getQuestion());
        unselectOption1TV.setText(data.get(position).getOptionOne());
        unselectOption2TV.setText(data.get(position).getOptionTwo());
        unselectOption3TV.setText(data.get(position).getOptionThree());
        unselectOption4TV.setText(data.get(position).getOptionFour());


        if (cardPos>=0){
            if (cardPos==position){
                if (optionSelect==1){
                    selectedOption1RL.setVisibility(View.VISIBLE);
                    unselectOption1RL.setVisibility(View.GONE);
                }else if (optionSelect==2){
                    selectedOption2RL.setVisibility(View.VISIBLE);
                    unselectOption2RL.setVisibility(View.GONE);
                }else if (optionSelect==3){
                    selectedOption3RL.setVisibility(View.VISIBLE);
                    unselectOption3RL.setVisibility(View.GONE);
                }else if (optionSelect==4){
                    selectedOption4RL.setVisibility(View.VISIBLE);
                    unselectOption4RL.setVisibility(View.GONE);
                }else{
                    resetOtherOptionFunc();
                }
            }

        }else{
            resetOtherOptionFunc();
        }

        mOption1RL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("click1 pos",""+data.get(position).getOptionOne());
                mOptionClick.learnOptionClick(position,1,"");

            }
        });

        mOption2RL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("click1 pos",""+data.get(position).getOptionTwo());
                mOptionClick.learnOptionClick(position,2,"");
            }
        });

        mOption3RL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("click1 pos",""+data.get(position).getOptionThree());
                mOptionClick.learnOptionClick(position,3,"");
            }
        });

        mOption4RL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("click1 pos",""+data.get(position).getOptionFour());
                mOptionClick.learnOptionClick(position,4,"");
            }
        });

        skillRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOptionClick.learnOptionClick(position,0,"skills");
            }
        });

        companyRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOptionClick.learnOptionClick(position,0,"company");
            }
        });

        return v;

    }

    private void resetOtherOptionFunc() {
        selectedOption1RL.setVisibility(View.GONE);
        selectedOption2RL.setVisibility(View.GONE);
        selectedOption3RL.setVisibility(View.GONE);
        selectedOption4RL.setVisibility(View.GONE);
        unselectOption1RL.setVisibility(View.VISIBLE);
        unselectOption2RL.setVisibility(View.VISIBLE);
        unselectOption3RL.setVisibility(View.VISIBLE);
        unselectOption4RL.setVisibility(View.VISIBLE);
    }



}