package pt.magical.magicalcareers.adapters.candidate;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.holders.recruiter.JdListViewHolder;

/**
 * Created by danish on 18/08/17.
 */

public class CandidateLearnDetailsDiscreteScrollAdapter extends RecyclerView.Adapter<JdListViewHolder> {


    private Context mContext;
    private LayoutInflater layoutInflater;
    private transient JdClickListener mJdClick;



    public CandidateLearnDetailsDiscreteScrollAdapter(Context context,JdClickListener activity) {
        this.mContext = context;
        this.mJdClick=activity;
        this.layoutInflater = LayoutInflater.from(mContext);
    }
    public interface JdClickListener {
        void onJdClick(int position);
    }
    @Override
    public JdListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_jd_list_discrete_view, parent, false);
        return new JdListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(JdListViewHolder holder, final int position) {


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mJdClick.onJdClick(position);
            }
        });
    }
    @Override
    public int getItemCount() {
        return 5;
    }
}
