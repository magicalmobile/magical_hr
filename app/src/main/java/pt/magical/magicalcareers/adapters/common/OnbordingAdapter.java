package pt.magical.magicalcareers.adapters.common;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import pt.magical.magicalcareers.R;

/**
 * Created by Vivek on 17/08/17.
 */

public class OnbordingAdapter extends PagerAdapter {

    Context context;
    int images[];
    String title[];
    LayoutInflater layoutInflater;


    public OnbordingAdapter(Context context, int images[], String title[]) {
        this.context = context;
        this.images = images;
        this.title = title;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = layoutInflater.inflate(R.layout.items_onbording_layout, container, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.onbording_imageView);
        TextView textView = (TextView) itemView.findViewById(R.id.onbording_textview);
        imageView.setImageResource(images[position]);
        textView.setText(title[position]);

        container.addView(itemView);

        //listening to image click
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "you clicked image " + (position + 1), Toast.LENGTH_LONG).show();
            }
        });

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }
}
