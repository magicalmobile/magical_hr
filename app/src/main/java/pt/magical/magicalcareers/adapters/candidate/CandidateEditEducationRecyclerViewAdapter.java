package pt.magical.magicalcareers.adapters.candidate;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.views.activities.candidate.CandidateEditEducationActivity;


/**
 * Created by Vivek on 7/19/2017.
 */

public class CandidateEditEducationRecyclerViewAdapter extends RecyclerView.Adapter<CandidateEditEducationRecyclerViewAdapter.ViewHolder> {


    private Context mContext;
    LayoutInflater layoutInflater;
    private CandidateEditEducationActivity candidateEditEducationActivity;


    public List<String> addEducationList;
    private int addEducationListSize;

    public CandidateEditEducationRecyclerViewAdapter(Context mContext, int addEducationListSize) {
        this.mContext = mContext;
        this.addEducationListSize = addEducationListSize;


    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.item_candidate_profile_education_row, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final CandidateEditEducationRecyclerViewAdapter.ViewHolder holder, int position) {


        holder.educationFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Calendar myCalendar = Calendar.getInstance();
                DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, monthOfYear);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        String myFormat = "yyyy-MM-dd"; // our format
                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
                        holder.educationFrom.setText(sdf.format(myCalendar.getTime()));
                    }
                };
                DatePickerDialog datePickerDialog = new DatePickerDialog((/*(Activity) */mContext), date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
                datePickerDialog.show();
            }
        });


        holder.educationTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar myCalendar = Calendar.getInstance();
                DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, monthOfYear);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        String myFormat = "yyyy-MM-dd"; // our format
                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
                        holder.educationFrom.setText(sdf.format(myCalendar.getTime()));
                    }
                };
                DatePickerDialog datePickerDialog = new DatePickerDialog((/*(Activity) */mContext), date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
                datePickerDialog.show();

            }
        });


        holder.cardRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.degreeEt.setText("");
                holder.instituteEt.setText("");
                holder.educationFrom.setText("");
                holder.educationTo.setText("");

            }
        });

    }

    @Override
    public int getItemCount() {
        return addEducationListSize;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView cardRemove, educationFrom, educationTo;
        public EditText degreeEt, instituteEt;

        public ViewHolder(View itemView) {
            super(itemView);
            cardRemove = (TextView) itemView.findViewById(R.id.remove_card_tv);
            degreeEt = (EditText) itemView.findViewById(R.id.degree_et);
            instituteEt = (EditText) itemView.findViewById(R.id.institute_et);
            educationFrom = (TextView) itemView.findViewById(R.id.education_from_et);
            educationTo = (TextView) itemView.findViewById(R.id.education_to_et);


        }


    }

}

