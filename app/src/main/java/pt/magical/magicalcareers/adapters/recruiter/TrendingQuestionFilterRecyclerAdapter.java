package pt.magical.magicalcareers.adapters.recruiter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.holders.recruiter.ScreeningQuestionListViewHolder;
import pt.magical.magicalcareers.holders.recruiter.TrendingQuestionFilterViewHolder;
import pt.magical.magicalcareers.views.fragments.recruiter.ScreeningQuestionListFragment;
import pt.magical.magicalcareers.views.fragments.recruiter.TrendingQuestionsFilterFragment;

/**
 * Created by RAdhu on 16/08/17.
 */

public class TrendingQuestionFilterRecyclerAdapter extends RecyclerView.Adapter<TrendingQuestionFilterViewHolder> {


    TrendingQuestionsFilterFragment mTrendingQuestionsFilterFragment;
    LayoutInflater layoutInflater;

    public TrendingQuestionFilterRecyclerAdapter(TrendingQuestionsFilterFragment mTrendingQuestionsFilterFragment) {
        this.mTrendingQuestionsFilterFragment = mTrendingQuestionsFilterFragment;
        this.layoutInflater = LayoutInflater.from(mTrendingQuestionsFilterFragment.getContext());
    }

    @Override
    public TrendingQuestionFilterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_trending_question_filter_recycler_view, parent, false);
        return new TrendingQuestionFilterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TrendingQuestionFilterViewHolder holder, final int position) {

    }
    @Override
    public int getItemCount() {
        return 7;
    }
}
