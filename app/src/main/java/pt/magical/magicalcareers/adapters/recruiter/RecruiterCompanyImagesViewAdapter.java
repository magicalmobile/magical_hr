package pt.magical.magicalcareers.adapters.recruiter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.holders.recruiter.RecruiterCompanyImagesViewHolder;
import pt.magical.magicalcareers.holders.recruiter.RecruiterCompanyVideosViewHolder;
import pt.magical.magicalcareers.utils.Utils;
import pt.magical.magicalcareers.views.fragments.AutoSearchFragment;

/**
 * Created by Sachin on 8/18/2017.
 */

public class RecruiterCompanyImagesViewAdapter extends RecyclerView.Adapter<RecruiterCompanyImagesViewHolder> {


    private LayoutInflater layoutInflater;
    /*
        private ArrayList<String> dataList;
    */
    private Context mContext;
    private List<Uri> mSelected = new ArrayList<>();
    private int size;

    public RecruiterCompanyImagesViewAdapter(Context mContext,/*, ArrayList<String> dataList*/List<Uri> mSelected) {
        this.mContext = mContext;
        this.layoutInflater = LayoutInflater.from(mContext);
        /*this.dataList = dataList;*/
        this.mSelected = mSelected;
    }

    public RecruiterCompanyImagesViewAdapter(Context mContext/*, ArrayList<String> dataList*/) {
        this.mContext = mContext;
        this.layoutInflater = LayoutInflater.from(mContext);
        /*this.dataList = dataList;*/
    }

    @Override
    public RecruiterCompanyImagesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.item_recruiter_company_images_layout, parent, false);
        return new RecruiterCompanyImagesViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecruiterCompanyImagesViewHolder holder, final int position) {
//        holder.mItemCompanyIV.setImageBitmap(Utils.getBitmapFromUri(mContext, mSelected.get(position)));
        if (mSelected.size() > 0) {
            Glide.with(mContext).load(mSelected.get(position)).asBitmap().centerCrop().into(holder.mItemCompanyIV);
        }
    }

    @Override
    public int getItemCount() {
        if (mSelected.size() == 0) {
            size = 6;
        } else {
            size = mSelected.size();
        }
        return size;
    }
}