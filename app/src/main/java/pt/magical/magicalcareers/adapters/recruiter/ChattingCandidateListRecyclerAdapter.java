package pt.magical.magicalcareers.adapters.recruiter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.daimajia.swipe.SwipeLayout;

import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.holders.recruiter.ChattingCandidateListHolder;
import pt.magical.magicalcareers.holders.recruiter.TrendingQuestionFilterViewHolder;
import pt.magical.magicalcareers.views.activities.recruiter.RecruiterProfileParentActivity;
import pt.magical.magicalcareers.views.fragments.recruiter.CandidateProfileViewFragment;
import pt.magical.magicalcareers.views.fragments.recruiter.ChattingCandidatesListFragment;
import pt.magical.magicalcareers.views.fragments.recruiter.RecruiterOwnProfileBasicInfoFragment;
import pt.magical.magicalcareers.views.fragments.recruiter.TrendingQuestionsFilterFragment;
import pt.magical.magicalcareers.views.fragments.recruiter.TrendingQuestionsFragment;

/**
 * Created by RAdhu on 19/08/17.
 */

public class ChattingCandidateListRecyclerAdapter extends RecyclerView.Adapter<ChattingCandidateListHolder> {


    ChattingCandidatesListFragment mChattingCandidatesListFragment;
    LayoutInflater layoutInflater;

    public ChattingCandidateListRecyclerAdapter(ChattingCandidatesListFragment mChattingCandidatesListFragment) {
        this.mChattingCandidatesListFragment = mChattingCandidatesListFragment;
        this.layoutInflater = LayoutInflater.from(mChattingCandidatesListFragment.getContext());
    }

    @Override
    public ChattingCandidateListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_jd_candidates_recycler_view, parent, false);
        return new ChattingCandidateListHolder(view);
    }

    @Override
    public void onBindViewHolder(ChattingCandidateListHolder holder, final int position) {
        swipeAction(holder);
    }

    private void swipeAction(final ChattingCandidateListHolder holder) {

        holder.mSampleLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
        //View starBottView = sample1.findViewById(R.id.starbott);
        holder.mSampleLayout.addDrag(SwipeLayout.DragEdge.Left, holder.mActionleft);
        holder.mSampleLayout.addDrag(SwipeLayout.DragEdge.Right, holder.mActionRight);
        //   holder.mSampleLayout.addDrag(SwipeLayout.DragEdge.Top, starBottView);
        // sample1.addDrag(SwipeLayout.DragEdge.Bottom, starBottView);
        /*holder.mSampleLayout.addRevealListener(R.id.delete, new SwipeLayout.OnRevealListener() {
            @Override
            public void onReveal(View child, SwipeLayout.DragEdge edge, float fraction, int distance) {

            }
        });*/

        holder.mProfilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mChattingCandidatesListFragment.getFragmentManager().beginTransaction().
                        setCustomAnimations(R.anim.bottom_to_top_enter, R.anim.bottom_to_top_exit, R.anim.top_to_bottom_enter, R.anim.top_to_bottom_exit).
                        replace(R.id.recruiter_dashboard_home_act_frame_layout, new CandidateProfileViewFragment())
                        .commit();
            }
        });
        holder.mSampleLayout.getSurfaceView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getA, "Click on surface", Toast.LENGTH_SHORT).show();
                Log.d("adapter", "click on surface");
            }
        });
        holder.mSampleLayout.getSurfaceView().setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Toast.makeText(mChattingCandidatesListFragment.getActivity(), "longClick on surface", Toast.LENGTH_SHORT).show();
                Log.d("adapter", "longClick on surface");
                return true;
            }
        });
        /*shortlist menu*/
        holder.mActionleft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mChattingCandidatesListFragment.getActivity(), "Shortlisted", Toast.LENGTH_SHORT).show();
                holder.mSampleLayout.close();
/*
                holder.mActionleft.setVisibility(View.GONE);
                holder.mActionRight.setVisibility(View.GONE);
*/
            }
        });

        holder.mActionMute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mChattingCandidatesListFragment.getActivity(), "Muted", Toast.LENGTH_SHORT).show();
                holder.mSampleLayout.close();
/*
                holder.mActionleft.setVisibility(View.GONE);
                holder.mActionRight.setVisibility(View.GONE);
*/
            }
        });

        holder.mActionArchive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mChattingCandidatesListFragment.getActivity(), "Archive", Toast.LENGTH_SHORT).show();
                holder.mSampleLayout.close();

                /*holder.mActionleft.setVisibility(View.GONE);
                holder.mActionRight.setVisibility(View.GONE);*/
            }
        });

       /* holder.mSampleLayout.addRevealListener(R.id.starbott, new SwipeLayout.OnRevealListener() {
            @Override
            public void onReveal(View child, SwipeLayout.DragEdge edge, float fraction, int distance) {
                View star = child.findViewById(R.id.star);
                float d = child.getHeight() / 2 - star.getHeight() / 2;
                ViewHelper.setTranslationY(star, d * fraction);
                ViewHelper.setScaleX(star, fraction + 0.6f);
                ViewHelper.setScaleY(star, fraction + 0.6f);
            }
        });*/
    }

    @Override
    public int getItemCount() {
        return 17;
    }
}
