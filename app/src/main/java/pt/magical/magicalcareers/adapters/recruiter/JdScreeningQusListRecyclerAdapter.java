package pt.magical.magicalcareers.adapters.recruiter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.holders.recruiter.JdCandidatesListViewHolder;
import pt.magical.magicalcareers.holders.recruiter.JdScreeningQusListViewHolder;
import pt.magical.magicalcareers.models.recruiter.review_jd.Question;
import pt.magical.magicalcareers.views.fragments.recruiter.ViewJdParentFragment;

/**
 * Created by RAdhu on 16/08/17.
 */

public class JdScreeningQusListRecyclerAdapter extends RecyclerView.Adapter<JdScreeningQusListViewHolder> {
    ViewJdParentFragment mViewJdParentFragment;
    LayoutInflater layoutInflater;
    List<Question> questions;
    public JdScreeningQusListRecyclerAdapter(ViewJdParentFragment mViewJdParentFragment, List<Question> questions) {
        this.mViewJdParentFragment = mViewJdParentFragment;
        this.layoutInflater = LayoutInflater.from(mViewJdParentFragment.getContext());
        this.questions=questions;
    }

    @Override
    public JdScreeningQusListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_viewjd_screening_qus, parent, false);
        return new JdScreeningQusListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(JdScreeningQusListViewHolder holder, final int position) {
           holder.mExperienceTv.setText(questions.get(position).getExperienceFrom()+"-"+questions.get(position).getExperienceTo()+" years");
           if(questions.get(position).getDifficultyLevel()==0)
            holder.mLevelTv.setText("Easy");
           if(questions.get(position).getDifficultyLevel()==1)
            holder.mLevelTv.setText("Medium");
           if(questions.get(position).getDifficultyLevel()==2)
            holder.mLevelTv.setText("Hard");
        holder.mSkillTv.setText(questions.get(position).getSkill());
        holder.mQuestionTv.setText(questions.get(position).getQuestion());
    }
    @Override
    public int getItemCount() {
        return questions.size();
    }
}
