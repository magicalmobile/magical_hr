package pt.magical.magicalcareers.adapters.recruiter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.holders.recruiter.ArchiveJdListViewHolder;

import static android.R.attr.data;


/**
 * Created by Sachin on 08/18/2017.
 */

public class ArchivedJdListViewAdapter extends RecyclerView.Adapter<ArchiveJdListViewHolder> /*implements Filterable */ {

    private Context mContext;
    ArrayList<String> archivedJobs;
    LayoutInflater layoutInflater;


    public ArchivedJdListViewAdapter(Context mContext, ArrayList<String> archivedJobs) {
        this.mContext = mContext;
        this.layoutInflater = LayoutInflater.from(mContext);
        this.archivedJobs = archivedJobs;
    }


    @Override
    public ArchiveJdListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_archive_jd_list, parent, false);
        return new ArchiveJdListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ArchiveJdListViewHolder holder, final int position) {


        holder.itemArchiveJdListHeadTV.setText(archivedJobs.get(position));

        holder.mArchiveJdlistActItemMoreIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PopupMenu popup = new PopupMenu(mContext, v);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.hr_home_archive_jd_list_menu, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()) {

                        }

                        return true;
                    }
                });
                popup.show();

            }
        });
    }

    @Override
    public int getItemCount() {
        return archivedJobs.size();
    }
}

