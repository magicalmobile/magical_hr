package pt.magical.magicalcareers.adapters.recruiter;

import android.content.Context;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.holders.recruiter.ArchiveJdListViewHolder;
import pt.magical.magicalcareers.holders.recruiter.NotificationsTypeViewHolder;
import pt.magical.magicalcareers.views.activities.common.SettingsActivity;


/**
 * Created by Sachin on 08/18/2017.
 */

public class NotificationsTypeAdapter extends RecyclerView.Adapter<NotificationsTypeViewHolder> /*implements Filterable */ {

    private SettingsActivity mContext;
    private ArrayList<String> notificationsTypeList;
    private LayoutInflater layoutInflater;


    public NotificationsTypeAdapter(SettingsActivity mContext, ArrayList<String> notificationsTypeList) {
        this.mContext = mContext;
        this.layoutInflater = LayoutInflater.from(mContext);
        this.notificationsTypeList = notificationsTypeList;
    }


    @Override
    public NotificationsTypeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_notifications_type_layout, parent, false);
        return new NotificationsTypeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final NotificationsTypeViewHolder holder, final int position) {

        holder.mNotificationsTypeTV.setText(notificationsTypeList.get(position));
        holder.mNotificationsLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.mNotificationsTypeIV.setVisibility(View.VISIBLE);

                mContext.recruiterProfilePageTitleValueTextView.setText(notificationsTypeList.get(position));
                mContext.progressDialog.dismiss();
            }
        });

    }

    @Override
    public int getItemCount() {
        return notificationsTypeList.size();
    }
}

