package pt.magical.magicalcareers.adapters.recruiter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import pt.magical.magicalcareers.views.fragments.recruiter.ChattingCandidatesListFragment;
import pt.magical.magicalcareers.views.fragments.recruiter.RecruiterCompanyProfileFragment;
import pt.magical.magicalcareers.views.fragments.recruiter.RecruiterOwnProfileFragment;

/**
 * Created by Radhu on 19 Aug 2017
 */

public class ChattingViewTabAdapter extends FragmentStatePagerAdapter {

    //integer to count number of tabs
    int tabCount;
    private ChattingCandidatesListFragment mChattingCandidatesListFragment;

    //Constructor to the class
    public ChattingViewTabAdapter(FragmentManager fm, int tabCount) {
        super(fm);
        //Initializing tab count
        this.tabCount = tabCount;
    }

    //Overriding method getItem
    @Override
    public Fragment getItem(int position) {
        //Returning the current tabs
        switch (position) {
            case 0:
                mChattingCandidatesListFragment = new ChattingCandidatesListFragment();
                return mChattingCandidatesListFragment;
            case 1:
                mChattingCandidatesListFragment = new ChattingCandidatesListFragment();
                return mChattingCandidatesListFragment;
            case 2:
                mChattingCandidatesListFragment = new ChattingCandidatesListFragment();
                return mChattingCandidatesListFragment;
            default:
                return null;
        }
    }

    //Overriden method getCount to get the number of tabs
    @Override
    public int getCount() {
        return tabCount;
    }
}

