package pt.magical.magicalcareers.adapters.candidate;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.yarolegovich.discretescrollview.DiscreteScrollView;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;

import java.util.ArrayList;
import java.util.List;

import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.holders.candidate.CandidateExperienceViewHolder;
import pt.magical.magicalcareers.interfaces.LayoutBasicInfoInterface;
import pt.magical.magicalcareers.utils.Utils;
import pt.magical.magicalcareers.views.activities.candidate.CandidateFindJobsActivity;
import pt.magical.magicalcareers.views.fragments.candidate.CandidateFindJobDetailFragment;

import static pt.magical.magicalcareers.utils.Utils.context;

/**
 * Created by Vivek on 14-08-2017.
 */

public class CandidateJobDetailsSwipeStackAdapter extends BaseAdapter implements
        DiscreteScrollView.OnItemChangedListener<CandidateExperienceViewHolder>,DiscreteScrollView.ScrollStateChangeListener,CandidateSkillsDialogAdapter.onItemClick {

    private Context mContext;
    LayoutInflater inflater;
    private List<String> data;


    private CandidateExperienceAdapter mCandidateExperienceAdapter;

    LayoutBasicInfoInterface activity;

    String[] list=new String[]{"0","1","2","3","4","5","6","7","8","9","10","11","12","13"};
    String[] salaryList=new String[]{"1 Lakh","2 Lakh","3 Lakh","4 Lakh","5 Lakh","6 Lakh","7 Lakh","8 Lakh","9 Lakh","10 Lakh","11 Lakh","12 Lakh","13 Lakh","14 Lakh","15 Lakh"};
    private DiscreteScrollView mDiscreteScrollView;
    private CandidateExperienceViewHolder mViewHolder;
    private int mPos;
    private int mExpValue=5;
    private TextView salaryTv;
    private ProgressDialog progressDialog;
    private ArrayList<String> mSalaryList;
    private TextView industryEdit1;
    private TextView industryEdit2;
    private TextView industryEdit3;
    private TextView industryEdit4;
    private TextView industrySelect1;
    private TextView industrySelect2;
    private TextView industrySelect3;
    private TextView industrySelect4;

    private transient industryOptionClick mIndustryClick;

    public interface industryOptionClick{
        void industryOptionClick(int pos,String text);
    }
    public CandidateJobDetailsSwipeStackAdapter(List<String> data, Context mContext,industryOptionClick activity,int pos) {
        this.data = data;
        this.mContext = mContext;
        this.mPos=pos;
        this.mIndustryClick=activity;
    }


    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.items_candidate_job_details_frag, parent, false);

        mDiscreteScrollView = (DiscreteScrollView) v.findViewById(R.id.candidate_find_job_frag_discretevcrollview);
        LinearLayout ll1 = (LinearLayout) v.findViewById(R.id.card_first_ll);
        LinearLayout ll2 = (LinearLayout) v.findViewById(R.id.card_second_ll);
        LinearLayout ll3 = (LinearLayout) v.findViewById(R.id.card_third_ll);
        salaryTv = (TextView) v.findViewById(R.id.card_third_salary_tv);
        industryEdit1=(TextView) v.findViewById(R.id.item_industry_1_tv);
        industryEdit2=(TextView) v.findViewById(R.id.item_industry_2_tv);
        industryEdit3=(TextView) v.findViewById(R.id.item_industry_3_tv);
        industryEdit4=(TextView) v.findViewById(R.id.item_industry_4_tv);
        industrySelect1=(TextView) v.findViewById(R.id.item_industry_1_select_tv);
        industrySelect2=(TextView) v.findViewById(R.id.item_industry_2_select_tv);
        industrySelect3=(TextView) v.findViewById(R.id.item_industry_3_select_tv);
        industrySelect4=(TextView) v.findViewById(R.id.item_industry_4_select_tv);


        mSalaryList=new ArrayList<String>();
        for (int i=0;i<salaryList.length;i++){
            mSalaryList.add(salaryList[i]);
        }

        if (mPos==1) {
            industryEdit1.setVisibility(View.GONE);
            industrySelect1.setVisibility(View.VISIBLE);
        }else if (mPos==2){
            industryEdit2.setVisibility(View.GONE);
            industrySelect2.setVisibility(View.VISIBLE);
        }else if (mPos==3){
            industryEdit3.setVisibility(View.GONE);
            industrySelect3.setVisibility(View.VISIBLE);
        }else if (mPos==4){
            industryEdit4.setVisibility(View.GONE);
            industrySelect4.setVisibility(View.VISIBLE);
        }

        if (CandidateFindJobsActivity.mExp!=null && !CandidateFindJobsActivity.mExp.equalsIgnoreCase("")){
            setDiscreteView(Integer.parseInt(CandidateFindJobsActivity.mExp));
        }else {
            setDiscreteView(mExpValue);
        }

        if (CandidateFindJobsActivity.mSalary!=null && !CandidateFindJobsActivity.mSalary.equalsIgnoreCase("")){
            salaryTv.setText(CandidateFindJobsActivity.mSalary+" per annum");
        }

        if (position == 0) {
            ll2.setVisibility(View.GONE);
            ll1.setVisibility(View.VISIBLE);
            ll3.setVisibility(View.GONE);
        } else if (position == 1) {
            ll1.setVisibility(View.GONE);
            ll2.setVisibility(View.VISIBLE);
            ll3.setVisibility(View.GONE);

        } else if (position == 2) {
            ll1.setVisibility(View.GONE);
            ll2.setVisibility(View.GONE);
            ll3.setVisibility(View.VISIBLE);
        }

        salaryTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("click","salary tv");
                showSkillsListDialog(mSalaryList);
            }
        });

        industryEdit1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("edit text",industryEdit1.getText().toString());
               mIndustryClick.industryOptionClick(1,industryEdit1.getText().toString());
            }

        });
        industryEdit2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("edit text",industryEdit2.getText().toString());
                mIndustryClick.industryOptionClick(2,industryEdit2.getText().toString());
            }
        });
        industryEdit3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("edit text",industryEdit3.getText().toString());
                mIndustryClick.industryOptionClick(3,industryEdit3.getText().toString());
            }
        });
        industryEdit4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("edit text",industryEdit4.getText().toString());
                mIndustryClick.industryOptionClick(4,industryEdit4.getText().toString());
            }
        });


        return v;
    }

    private void setDiscreteView(int pos) {
        mDiscreteScrollView.setAdapter(new CandidateExperienceAdapter(mContext,list,pos));
        mDiscreteScrollView.addOnItemChangedListener(this);
        mDiscreteScrollView.addScrollStateChangeListener(this);
        mDiscreteScrollView.scrollToPosition(pos);
        mDiscreteScrollView.setItemTransformer(new ScaleTransformer.Builder().setMinScale(0.8f).build());
    }

    @Override
    public void onCurrentItemChanged(@Nullable CandidateExperienceViewHolder viewHolder, int adapterPosition) {
        mExpValue=adapterPosition;
        CandidateFindJobsActivity.mExp= String.valueOf(mExpValue);
        if (adapterPosition==mExpValue){
            viewHolder.itemView.findViewById(R.id.year_tv).setVisibility(View.VISIBLE);
            TextView yearText = (TextView) viewHolder.itemView.findViewById(R.id.year_tv);
            yearText.setTextColor(mContext.getResources().getColor(R.color.white));
            TextView expText = (TextView) viewHolder.itemView.findViewById(R.id.item_experience_tv);
            expText.setTextColor(mContext.getResources().getColor(R.color.white));
            viewHolder.itemView.findViewById(R.id.exp_main_ll).setBackgroundColor(mContext.getResources().getColor(R.color.blue_dark_color));
        }
    }


    @Override
    public void onScrollStart(@NonNull RecyclerView.ViewHolder currentItemHolder, int adapterPosition) {
        currentItemHolder.itemView.findViewById(R.id.year_tv).setVisibility(View.INVISIBLE);
        TextView expText = (TextView) currentItemHolder.itemView.findViewById(R.id.item_experience_tv);
        expText.setTextColor(mContext.getResources().getColor(R.color.black));
        currentItemHolder.itemView.findViewById(R.id.exp_main_ll).setBackgroundColor(mContext.getResources().getColor(R.color.colorGray));
    }

    @Override
    public void onScrollEnd(@NonNull RecyclerView.ViewHolder currentItemHolder, int adapterPosition) {
        currentItemHolder.itemView.findViewById(R.id.year_tv).setVisibility(View.VISIBLE);
        TextView yearText = (TextView) currentItemHolder.itemView.findViewById(R.id.year_tv);
        yearText.setTextColor(mContext.getResources().getColor(R.color.white));
        TextView expText = (TextView) currentItemHolder.itemView.findViewById(R.id.item_experience_tv);
        expText.setTextColor(mContext.getResources().getColor(R.color.white));
        currentItemHolder.itemView.findViewById(R.id.exp_main_ll).setBackgroundColor(mContext.getResources().getColor(R.color.blue_dark_color));
    }

    @Override
    public void onScroll(float scrollPosition, @NonNull RecyclerView.ViewHolder currentHolder, @NonNull RecyclerView.ViewHolder newCurrent) {
    }

    /**
     * Set Show salary dialog box.
     */
    public void showSkillsListDialog(List<String> skillList) {
        try {
            if (null != progressDialog && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            progressDialog = ProgressDialog.show(mContext, "", "", true);
            progressDialog.setCanceledOnTouchOutside(true);
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progressDialog.setContentView(R.layout.candidate_dialog_skills_list);
            RecyclerView recyclerView=(RecyclerView)progressDialog.findViewById(R.id.skill_dialog_rv);
            LinearLayoutManager horizontalLayoutManagaer
                    = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(horizontalLayoutManagaer);
            CandidateSkillsDialogAdapter skillListAdapter = new CandidateSkillsDialogAdapter(mContext,this,skillList);
            recyclerView.setAdapter(skillListAdapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void itemClick(int pos, String name) {
        progressDialog.cancel();
        salaryTv.setText(name+" per annum");
        CandidateFindJobsActivity.mSalary= String.valueOf(pos+1);
    }
}