package pt.magical.magicalcareers.adapters.recruiter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.holders.recruiter.JdCandidatesListViewHolder;
import pt.magical.magicalcareers.holders.recruiter.JdListViewHolder;

/**
 * Created by RAdhu on 16/08/17.
 */

public class JdCandidatesListRecyclerAdapter extends RecyclerView.Adapter<JdCandidatesListViewHolder> {


    Context mContext;
    LayoutInflater layoutInflater;

    public JdCandidatesListRecyclerAdapter(Context mDashboardActivity) {
        this.mContext = mDashboardActivity;
        this.layoutInflater = LayoutInflater.from(mContext);
    }

    @Override
    public JdCandidatesListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_jd_candidates_recycler_view, parent, false);
        return new JdCandidatesListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(JdCandidatesListViewHolder holder, final int position) {

    }
    @Override
    public int getItemCount() {
        return 8;
    }
}
