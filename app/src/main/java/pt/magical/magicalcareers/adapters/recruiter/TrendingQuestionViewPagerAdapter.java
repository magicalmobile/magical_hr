package pt.magical.magicalcareers.adapters.recruiter;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.models.recruiter.trending_question_list.AnswerList;
import pt.magical.magicalcareers.models.recruiter.trending_question_list.Datum;
import pt.magical.magicalcareers.views.fragments.recruiter.TrendingQuestionsFragment;

/**
 * Created by radhu on 8/26/2017.
 */
public class TrendingQuestionViewPagerAdapter extends PagerAdapter {


    private final Context mContext;
    private LayoutInflater inflater;
    private TextView experienceTv,levelTv,skillTv,questionTv,option1Tv,option2Tv,option3Tv,option4Tv;
    private TextView addBt;
    TrendingQuestionsFragment trendingQuestionsFragment;
    List<Datum> data;
    List<AnswerList> answerList;

    public TrendingQuestionViewPagerAdapter(Context context, TrendingQuestionsFragment trendingQuestionsFragment, List<Datum> data) {
        this.mContext = context;
        this.data = data;
        this.trendingQuestionsFragment=trendingQuestionsFragment;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, final int position) {
        View feedLayout = inflater.inflate(R.layout.item_trending_question_viewpager, view, false);
        try {
            experienceTv=(TextView) feedLayout.findViewById(R.id.item_trending_question_experience_tv);
            levelTv=(TextView) feedLayout.findViewById(R.id.item_trending_question_level_tv);
            skillTv=(TextView) feedLayout.findViewById(R.id.item_trending_question_skill_tv);
            questionTv=(TextView) feedLayout.findViewById(R.id.item_trending_question_question_tv);
            option1Tv=(TextView) feedLayout.findViewById(R.id.item_trending_question_option1_tv);
            option2Tv=(TextView) feedLayout.findViewById(R.id.item_trending_question_option2_tv);
            option3Tv=(TextView) feedLayout.findViewById(R.id.item_trending_question_option3_tv);
            option4Tv=(TextView) feedLayout.findViewById(R.id.item_trending_question_option4_tv);
            addBt=(TextView) feedLayout.findViewById(R.id.item_trending_question_add_bt);

            assert feedLayout != null;
            view.addView(feedLayout, 0);


            experienceTv.setText(data.get(position).getExperienceFrom()+"-"+data.get(position).getExperienceTo()+" years");

            if(data.get(position).getDifficultyLevel()==0)
                levelTv.setText("Easy");
            if(data.get(position).getDifficultyLevel()==1)
                levelTv.setText("Medium");
            if(data.get(position).getDifficultyLevel()==2)
                levelTv.setText("Hard");

            skillTv.setText(data.get(position).getMasterSkill().toString());
            questionTv.setText(data.get(position).getQuestion().toString());

            if(data.get(position).getIsAdded()) {
                addBt.setCompoundDrawablesRelative(mContext.getResources().getDrawable(R.drawable.ic_add), null, null, null);
                addBt.setText("Added");
            }else
            {
                addBt.setCompoundDrawablesRelative(null, null, mContext.getResources().getDrawable(R.drawable.ic_add), null);
                addBt.setText("Add");
            }

            answerList = AnswerList.findWithQuery(AnswerList.class, "SELECT * FROM trending_answer_list where question_id="+data.get(position).getMasterScreeningQuestionId());
            for (int i=0;i<answerList.size();i++)
            {
                switch (i)
                {
                    case 0:
                        option1Tv.setText(answerList.get(i).getAnswer());
                        if(answerList.get(i).getIsRightAnswer())
                            option1Tv.setTextColor(mContext.getResources().getColor(R.color.colorBadge));
                        break;
                    case 1:
                        option2Tv.setText(answerList.get(i).getAnswer());
                        if(answerList.get(i).getIsRightAnswer())
                            option2Tv.setTextColor(mContext.getResources().getColor(R.color.colorBadge));
                        break;
                    case 2:
                        option3Tv.setText(answerList.get(i).getAnswer());
                        if(answerList.get(i).getIsRightAnswer())
                            option3Tv.setTextColor(mContext.getResources().getColor(R.color.colorBadge));
                        break;
                    case 3:
                        option4Tv.setText(answerList.get(i).getAnswer());
                        if(answerList.get(i).getIsRightAnswer())
                            option4Tv.setTextColor(mContext.getResources().getColor(R.color.colorBadge));
                        break;
                }
            }


            addBt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    trendingQuestionsFragment.onAddClick(position,data.get(position).getMasterScreeningQuestionId());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return feedLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }
}