package pt.magical.magicalcareers.adapters.recruiter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import pt.magical.magicalcareers.views.fragments.recruiter.RecruiterCompanyProfileFragment;
import pt.magical.magicalcareers.views.fragments.recruiter.RecruiterOwnProfileFragment;

/**
 * Created by Sachin on 17 Aug 2017
 */

public class RecruiterParentProfileAdapter extends FragmentStatePagerAdapter {

    //integer to count number of tabs
    int tabCount;

    //Constructor to the class
    public RecruiterParentProfileAdapter(FragmentManager fm, int tabCount) {
        super(fm);
        //Initializing tab count
        this.tabCount = tabCount;
    }

    //Overriding method getItem
    @Override
    public Fragment getItem(int position) {
        //Returning the current tabs
        switch (position) {
            case 0:
                RecruiterOwnProfileFragment mRecruiterOwnProfileFragment = new RecruiterOwnProfileFragment();
                return mRecruiterOwnProfileFragment;
            case 1:
                RecruiterCompanyProfileFragment mRecruiterCompanyProfileFragment = new RecruiterCompanyProfileFragment();
                return mRecruiterCompanyProfileFragment;
            default:
                return null;
        }
    }

    //Overriden method getCount to get the number of tabs
    @Override
    public int getCount() {
        return tabCount;
    }
}

