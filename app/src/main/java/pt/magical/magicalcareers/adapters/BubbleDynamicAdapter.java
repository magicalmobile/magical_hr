package pt.magical.magicalcareers.adapters;

/**
 * Created by danish on 04-08-2017.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.interfaces.BubbleLayoutInterface;


public class BubbleDynamicAdapter extends RecyclerView.Adapter<BubbleDynamicAdapter.DataObjectHolder>{

    private final Context context;
    private final List<String> list;
    private List<String> skillList;
    private final boolean isCancel;
    private static BubbleLayoutInterface bubbleLayoutInterface;


    public BubbleDynamicAdapter(Context context, BubbleLayoutInterface activity,List<String> list,List<String> skillList,boolean isCancel) {
        this.context = context;
        this.list=list;
        this.isCancel=isCancel;
        this.bubbleLayoutInterface=activity;
        this.skillList=skillList;
    }


    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bubble_hint, parent, false);
        return new DataObjectHolder(view);
    }


    @Override
    public void onBindViewHolder(final DataObjectHolder holder, final int position) {
        try {
           if (isCancel){// if true then show cancel icon
              /* holder.titleWithCancelTv.setVisibility(View.VISIBLE);
               holder.titleNoCancelTv.setVisibility(View.GONE);
               holder.titleWithCancelTv.setText(list.get(position));*/
               holder.skillRl.setVisibility(View.VISIBLE);
               holder.titleNoCancelTv.setVisibility(View.GONE);
               holder.skillNameTV.setText(list.get(position));
               holder.skillExpTV.setText(skillList.get(position)+"/5");

           }else{ // not show cancel icon
               holder.titleNoCancelTv.setVisibility(View.VISIBLE);
               holder.skillRl.setVisibility(View.GONE);
               //holder.titleWithCancelTv.setVisibility(View.GONE);
               holder.titleNoCancelTv.setText(list.get(position));
           }

           holder.titleNoCancelTv.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View view) {
                   bubbleLayoutInterface.onBubbleItemClick(position,list.get(position));
               }
           });

           /* holder.titleWithCancelTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    bubbleLayoutInterface.onBubbleItemClick(position,list.get(position));
                }
            });
*/
            holder.skillCloseIV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    bubbleLayoutInterface.onBubbleItemClick(position,list.get(position));
                }
            });

        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public class DataObjectHolder extends RecyclerView.ViewHolder {

        /*@BindView(R.id.item_title_with_cancel_tv)
        protected TextView titleWithCancelTv;*/
        @BindView(R.id.item_title_tv)
        protected TextView titleNoCancelTv;
        @BindView(R.id.item_skill_rl)
        protected RelativeLayout skillRl;
        @BindView(R.id.item_skill_name_tv)
        protected TextView skillNameTV;
        @BindView(R.id.item_skill_exp_tv)
        protected TextView skillExpTV;
        @BindView(R.id.item_skill_close_iv)
        protected ImageView skillCloseIV;


        public DataObjectHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
