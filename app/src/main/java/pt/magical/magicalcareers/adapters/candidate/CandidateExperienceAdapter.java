package pt.magical.magicalcareers.adapters.candidate;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.holders.candidate.CandidateExperienceViewHolder;

/**
 * Updated By Vivek 29 July 2017.
 */

public class CandidateExperienceAdapter extends RecyclerView.Adapter<CandidateExperienceViewHolder> {

    private Context context;
    private String[] mList;
    private int mPos;

    public CandidateExperienceAdapter(Context context,String[] mList,int mPos) {
        this.context = context;
        this.mList=mList;
        this.mPos=mPos;
    }



    @Override
    public CandidateExperienceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_row_candiate_experience_items, parent, false);
        return new CandidateExperienceViewHolder(view);


    }

    @Override
    public void onBindViewHolder(CandidateExperienceViewHolder holder, final int position) {

        holder.mEexperienceTv.setText(mList[position]);
       /* if (mPos==position){
            holder.mYearTv.setVisibility(View.VISIBLE);
        }else{
            holder.mYearTv.setVisibility(View.INVISIBLE);
        }*/

    }


    @Override
    public int getItemCount() {
        return mList.length;
    }
}
