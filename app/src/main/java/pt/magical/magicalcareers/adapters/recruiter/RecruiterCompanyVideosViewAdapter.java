package pt.magical.magicalcareers.adapters.recruiter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.holders.recruiter.RecruiterCompanyVideosViewHolder;

/**
 * Created by Sachin on 8/18/2017.
 */

public class RecruiterCompanyVideosViewAdapter extends RecyclerView.Adapter<RecruiterCompanyVideosViewHolder> {

    private LayoutInflater layoutInflater;
/*
    private ArrayList<String> dataList;
*/
    private Context mContext;

    public RecruiterCompanyVideosViewAdapter(Context mContext/*, ArrayList<String> dataList*/) {
        this.mContext = mContext;
        this.layoutInflater = LayoutInflater.from(mContext);
        /*this.dataList = dataList;*/
    }

    @Override
    public RecruiterCompanyVideosViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.item_recruiter_company_video_layout, parent, false);
        return new RecruiterCompanyVideosViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecruiterCompanyVideosViewHolder holder, final int position) {
    }

    @Override
    public int getItemCount() {
        return 6;
    }

}
