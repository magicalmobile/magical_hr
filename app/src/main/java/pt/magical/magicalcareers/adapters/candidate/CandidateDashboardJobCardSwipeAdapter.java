package pt.magical.magicalcareers.adapters.candidate;

/**
 * Created by danish on 15-08-2017.
 */

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.interfaces.BubbleLayoutInterface;
import pt.magical.magicalcareers.utils.Utils;
import pt.magical.magicalcareers.views.fragments.candidate.CandidateViewJDFragment;

/**
 * Created by danish on 14-08-2017.
 */

public class CandidateDashboardJobCardSwipeAdapter extends BaseAdapter {

    private final FragmentManager fragmentManager;
    private Context mContext;
    LayoutInflater inflater;
    private String[] data;
    private BubbleLayoutInterface activity;
    private TextView viewJdTV;


    public CandidateDashboardJobCardSwipeAdapter(Context mContext, String[] data, BubbleLayoutInterface activity, FragmentManager fragmentManager) {
        this.data = data;
        this.mContext = mContext;
        this.activity=activity;
        this.fragmentManager=fragmentManager;
    }


    @Override
    public int getCount() {
        return data.length;
    }

    @Override
    public Object getItem(int position) {
        return data[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.item_card_job_candidate_dashboard, parent, false);
        RecyclerView skillRecyclerView=(RecyclerView)v.findViewById(R.id.job_card_skill_rv);
        viewJdTV=(TextView)v.findViewById(R.id.job_card_view_jd_tv);

        List<String> skillList=new ArrayList<>();
        for (int i=0;i<data.length;i++){
            skillList.add(data[i]);
        }
        Utils.setBubbleLayout(mContext,activity,skillRecyclerView,skillList,skillList,false);

        viewJdTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentManager.beginTransaction().replace(R.id.container_candidate_home,new CandidateViewJDFragment()).addToBackStack(null).commit();
            }
        });

        return v;
    }


}