package pt.magical.magicalcareers.adapters.candidate;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.adapters.candidate.CandidateJobDetailsSwipeStackAdapter;
import pt.magical.magicalcareers.interfaces.LayoutBasicInfoInterface;
import pt.magical.magicalcareers.utils.Utils;
import pt.magical.magicalcareers.views.activities.common.LoginActivity;

/**
 * Created by Vivek on 14-08-2017.
 */

public class CandidateJobDetailsSwipeSkillStackAdapter extends BaseAdapter {

    private final int mPos;
    private final int mRate;
    private Context mContext;
    LayoutInflater inflater;
    private List<String> data;

    private TextView skillTitle1Tv;
    private TextView skillTitle2Tv;
    private TextView skillTitle3Tv;
    private TextView skillTitle4Tv;
    private RelativeLayout skill1RL;

    private transient skillOptionClick mSkillClick;
    private RelativeLayout skill2RL;
    private RelativeLayout skill3RL;
    private RelativeLayout skill4RL;
    private TextView skillTitle1SelectTv;
    private TextView skillTitle2SelectTv;
    private TextView skillTitle3SelectTv;
    private TextView skillTitle4SelectTv;
    private RatingBar skill1Rating;
    private RatingBar skill2Rating;
    private RatingBar skill3Rating;
    private RatingBar skill4Rating;

    public interface skillOptionClick {
        void skillOptionClick(int pos, String title, String exp, int rate);
    }

    public CandidateJobDetailsSwipeSkillStackAdapter(Context mContext, skillOptionClick activity, List<String> data, int pos, int rate) {
        this.data = data;
        this.mContext = mContext;
        inflater = LayoutInflater.from(mContext);
        this.mSkillClick = activity;
        this.mPos = pos;
        this.mRate = rate;

    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View v = convertView;
        if (v == null) {
            // normally use a viewholder
            v = inflater.inflate(R.layout.items_candidate_job_details_skill_frag, parent, false);
        }

        skillTitle1Tv = (TextView) v.findViewById(R.id.skill_title1_tv);
        skillTitle2Tv = (TextView) v.findViewById(R.id.skill_title2_tv);
        skillTitle3Tv = (TextView) v.findViewById(R.id.skill_title3_tv);
        skillTitle4Tv = (TextView) v.findViewById(R.id.skill_title4_tv);
        skillTitle1SelectTv = (TextView) v.findViewById(R.id.skill_title1_select_tv);
        skillTitle2SelectTv = (TextView) v.findViewById(R.id.skill_title2_select_tv);
        skillTitle3SelectTv = (TextView) v.findViewById(R.id.skill_title3_select_tv);
        skillTitle4SelectTv = (TextView) v.findViewById(R.id.skill_title4_select_tv);

        skill1RL = (RelativeLayout) v.findViewById(R.id.skill_title1_rl);
        skill2RL = (RelativeLayout) v.findViewById(R.id.skill_title2_rl);
        skill3RL = (RelativeLayout) v.findViewById(R.id.skill_title3_rl);
        skill4RL = (RelativeLayout) v.findViewById(R.id.skill_title4_rl);

        skill1Rating = (RatingBar) v.findViewById(R.id.skill_title1_rating_bar);
        skill2Rating = (RatingBar) v.findViewById(R.id.skill_title2_rating_bar);
        skill3Rating = (RatingBar) v.findViewById(R.id.skill_title3_rating_bar);
        skill4Rating = (RatingBar) v.findViewById(R.id.skill_title4_rating_bar);

        if (mPos == 1) {
            skill1RL.setVisibility(View.VISIBLE);
            skillTitle1SelectTv.setVisibility(View.VISIBLE);
            skillTitle1Tv.setVisibility(View.GONE);
            skill1Rating.setRating(mRate);


        } else if (mPos == 2) {
            skill2RL.setVisibility(View.VISIBLE);
            skillTitle2SelectTv.setVisibility(View.VISIBLE);
            skillTitle2Tv.setVisibility(View.GONE);
            skill2Rating.setRating(mRate);
        } else if (mPos == 3) {
            skill3RL.setVisibility(View.VISIBLE);
            skillTitle3SelectTv.setVisibility(View.VISIBLE);
            skillTitle3Tv.setVisibility(View.GONE);
            skill3Rating.setRating(mRate);
        } else if (mPos == 4) {
            skill4RL.setVisibility(View.VISIBLE);
            skillTitle4SelectTv.setVisibility(View.VISIBLE);
            skillTitle4Tv.setVisibility(View.GONE);
            skill4Rating.setRating(mRate);
        }

        skillTitle1Tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSkillClick.skillOptionClick(1, skillTitle1Tv.getText().toString(), "", 0);
            }
        });
        skillTitle2Tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSkillClick.skillOptionClick(2, skillTitle2Tv.getText().toString(), "", 0);
            }
        });
        skillTitle3Tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSkillClick.skillOptionClick(3, skillTitle3Tv.getText().toString(), "", 0);
            }
        });
        skillTitle4Tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSkillClick.skillOptionClick(4, skillTitle4Tv.getText().toString(), "", 0);
            }
        });

        skill1Rating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                Log.d("bar click","click"+ratingBar.getRating());
                mSkillClick.skillOptionClick(1, skillTitle1Tv.getText().toString(), "",(int)ratingBar.getRating());
            }
        });
        skill2Rating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                Log.d("bar click","click"+ratingBar.getRating());
                mSkillClick.skillOptionClick(2, skillTitle1Tv.getText().toString(), "",(int)ratingBar.getRating());
            }
        });
        skill3Rating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                Log.d("bar click","click"+ratingBar.getRating());
                mSkillClick.skillOptionClick(3, skillTitle1Tv.getText().toString(), "",(int)ratingBar.getRating());
            }
        });
        skill4Rating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                Log.d("bar click","click"+ratingBar.getRating());
                mSkillClick.skillOptionClick(4, skillTitle1Tv.getText().toString(), "",(int)ratingBar.getRating());
            }
        });



        return v;
    }


}