package pt.magical.magicalcareers.adapters.recruiter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.holders.recruiter.JdListViewHolder;
import pt.magical.magicalcareers.views.fragments.recruiter.ChattingViewFragment;

/**
 * Created by RAdhu on 23/08/17.
 */

public class JdListChattngDiscreteAdapter extends RecyclerView.Adapter<JdListViewHolder> {


    Context mContext;
    LayoutInflater layoutInflater;
    JdClickListener jdClickListener;

    public JdListChattngDiscreteAdapter(Context context,JdClickListener activity) {
        this.mContext = context;
        this.jdClickListener=activity;
        this.layoutInflater = LayoutInflater.from(mContext);
    }


    public interface JdClickListener {
        void onJdClick(int position);
    }
    @Override
    public JdListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_jd_list_discrete_view, parent, false);
        return new JdListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(JdListViewHolder holder, final int position) {


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                jdClickListener.onJdClick(position);
            }
        });
    }
    @Override
    public int getItemCount() {
        return 5;
    }
}
