package pt.magical.magicalcareers.adapters.recruiter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.holders.recruiter.ScreeningQuestionListViewHolder;
import pt.magical.magicalcareers.models.recruiter.screening_question_list.JdQuestionList;
import pt.magical.magicalcareers.views.fragments.recruiter.ScreeningQuestionListFragment;
import pt.magical.magicalcareers.views.fragments.recruiter.ViewJdParentFragment;

/**
 * Created by RAdhu on 16/08/17.
 */

public class ScreeningQuestionListRecyclerAdapter extends RecyclerView.Adapter<ScreeningQuestionListViewHolder> {


    ScreeningQuestionListFragment mScreeningQuestionListFragment;
    LayoutInflater layoutInflater;
    List<JdQuestionList> jdQuestionList;
    public ScreeningQuestionListRecyclerAdapter(ScreeningQuestionListFragment mScreeningQuestionListFragment, List<JdQuestionList> jdQuestionList) {
        this.mScreeningQuestionListFragment = mScreeningQuestionListFragment;
        this.layoutInflater = LayoutInflater.from(mScreeningQuestionListFragment.getContext());
        this.jdQuestionList=jdQuestionList;
    }

    @Override
    public ScreeningQuestionListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_screening_question_recyclerview, parent, false);
        return new ScreeningQuestionListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ScreeningQuestionListViewHolder holder, final int position) {

    }
    @Override
    public int getItemCount() {
        return jdQuestionList.size();
    }
}
