package pt.magical.magicalcareers.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import pt.magical.magicalcareers.R;
import pt.magical.magicalcareers.holders.TopRatedDataViewHolder;
import pt.magical.magicalcareers.views.activities.common.AutoSearchActivity;
import pt.magical.magicalcareers.views.fragments.AutoSearchFragment;

/**
 * Created by Sachin on 7/6/2017.
 */

public class TopRatedDataViewAdapter extends RecyclerView.Adapter<TopRatedDataViewHolder> {


    private AutoSearchFragment mAutoSearchFragment;
    private LayoutInflater layoutInflater;
    private ArrayList<String> dataList;
    private Context mContext;

    public TopRatedDataViewAdapter(Context mContext, ArrayList<String> dataList) {
        this.mContext = mContext;
        this.layoutInflater = LayoutInflater.from(mContext);
        this.dataList = dataList;
    }

    @Override
    public TopRatedDataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.item_auto_complete_tv_layout, parent, false);
        return new TopRatedDataViewHolder(v);
    }

    @Override
    public void onBindViewHolder(TopRatedDataViewHolder holder, final int position) {
        holder.mDataTextView.setText(dataList.get(position).toString());
        holder.mParentDataLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((AutoSearchActivity) mContext).setDataInSearchEditText(dataList.get(position).toString());
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public void filterList(ArrayList<String> filterdNames) {
        this.dataList = filterdNames;
        notifyDataSetChanged();
    }

}
